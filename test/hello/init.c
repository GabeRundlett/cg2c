#include "hello.h"

VkInstance instance;

DeviceProps deviceProps;

VkPhysicalDevice physicalDevice;

const void *deviceFeatures;

VkDevice device;

uint32_t queueFamily;
VkQueue queue;

Fns fns;

static void initInstance(void);
static void initDeviceProps(void);
static void initDevice(void);
static bool haveInstanceLayer(const char*);

static void
linkPNext(void **chain, void *p)
{
	VkBaseOutStructure *link = p;
	link->pNext = *chain;
	*chain = p;
}

void
init(void)
{
	VkResult r;

	fns.vkGetInstanceProcAddr = openVulkan();
	initInstance();
	initDevice();
}

/*
static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT      messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT             messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void*                                       pUserData)
{
	fprintf(stderr, "%s\n", pCallbackData->pMessage);

	return VK_FALSE;
}
*/

void
initInstance(void)
{
	PFN_vkCreateInstance vkCreateInstance = (PFN_vkCreateInstance)fns.vkGetInstanceProcAddr(NULL, "vkCreateInstance");

	VkResult r;

	void *instanceCreateInfoPNext = NULL;

	const char *layers[10] = {};
	size_t layerCount = 0;

	const char *exts[10] = {};
	size_t extCount = 0;

	exts[extCount++] = "VK_KHR_surface";

#ifndef __ANDROID_API__
	exts[extCount++] = "VK_KHR_xlib_surface";
	exts[extCount++] = "VK_KHR_wayland_surface";
#endif

#ifdef __ANDROID_API__
	exts[extCount++] = "VK_KHR_android_surface";
#endif

	// Really we only ever need to enable VK_LAYER_KHRONOS_validation  when
	// we're on Android, and in that case we know we have the validation
	// layer.

	// VK_LAYER_KHRONOS_validation, if enabled, must be the first layer. See
	// ppEnabledLayerNames comment in the Vulkan specification about layer
	// order.
	/*
	VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
		.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
		.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
		.pfnUserCallback = debugCallback,
	};
	*/
	if (haveInstanceLayer("VK_LAYER_KHRONOS_validation")) {
		// linkPNext(&instanceCreateInfoPNext, &debugUtilsMessengerCreateInfo);

		layers[layerCount++] = "VK_LAYER_KHRONOS_validation";
		exts[extCount++] = "VK_EXT_debug_utils";
	}

	// BUG: sync2 layer is incomplete
	if (false && haveInstanceLayer("VK_LAYER_KHRONOS_synchronization2"))
		layers[layerCount++] = "VK_LAYER_KHRONOS_synchronization2";

	if ((r = vkCreateInstance(&(VkInstanceCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = instanceCreateInfoPNext,
		.pApplicationInfo = &(VkApplicationInfo) {
			.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
			// .pApplicationName = "Hello Cg2c",
			.apiVersion = VK_API_VERSION_1_1,
		},
		.enabledLayerCount = (uint32_t)layerCount,
		.ppEnabledLayerNames = layers,
		.enabledExtensionCount = (uint32_t)extCount,
		.ppEnabledExtensionNames = exts,
	}, NULL, &instance)) != VK_SUCCESS)
		fatalvk("vkCreateInstance: ", r);

#define F(fn) fns.fn = (PFN_##fn)fns.vkGetInstanceProcAddr(instance, #fn);
	INSTANCE_FUNCS(F)
#undef F
}

void
initDeviceProps(void)
{
	struct {
		VkStructureType sType;
		void*           p;
	} props[] = {
		{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2, &deviceProps._1_0}, // must be first
	};

	void *pNext = NULL;
	for (size_t i = nelem(props); i-- > 0;) {
		VkBaseOutStructure *p = props[i].p;
		p->sType = props[i].sType;
		p->pNext = pNext;
		pNext = p;
	}

	fns.vkGetPhysicalDeviceProperties2(physicalDevice, &deviceProps._1_0);
}

static void*
ememdup(const void *p, size_t n)
{
	void *q = emalloc(n);
	memmove(q, p, n);
	return q;
}

void
initDevice(void)
{
	VkResult r;

	uint32_t physicalDeviceCount = 1;
	r = fns.vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, &physicalDevice);
	if (physicalDeviceCount == 0)
		fatalf("did not find any Vulkan devices");
	else if (r != VK_SUCCESS && r != VK_INCOMPLETE)
		fatalvk("vkEnumeratePhysicalDevices: ", r);

	initDeviceProps();

	fprintf(stderr, "GPU: %s\n", deviceProps._1_0.properties.deviceName);

	uint32_t nfamily;
	fns.vkGetPhysicalDeviceQueueFamilyProperties2(physicalDevice, &nfamily, NULL);

	VkQueueFamilyProperties2 *families = emalloc(nfamily * sizeof families[0]);
	for (uint32_t i = 0; i < nfamily; i++)
		families[i] = (VkQueueFamilyProperties2) {.sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2};
	fns.vkGetPhysicalDeviceQueueFamilyProperties2(physicalDevice, &nfamily, families);

	queueFamily = 0xffffffff;
	for (uint32_t i = 0; i < nfamily; i++) {
		VkQueueFlags caps = families[i].queueFamilyProperties.queueFlags;
		VkQueueFlags wantCaps = VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT;

		if ((caps & wantCaps) == wantCaps) {
			queueFamily = i;
			break;
		}
	}
	if (queueFamily == 0xffffffff)
		fatalf("no general queue family");

	free(families);

	VkPhysicalDeviceFeatures2 agrh = {.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2};
	fns.vkGetPhysicalDeviceFeatures2(physicalDevice, &agrh);

	// Extension features that appear in the core feature structs should be
	// in the order they appear in those structs. Other extension features
	// should be placed at the end, ordered by vendor.

	void *features = NULL;

	const char *exts[100] = {};
	size_t extCount = 0;

	// Core 1.0

	VkPhysicalDeviceFeatures2 _1_0 = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
		.features.samplerAnisotropy = VK_TRUE,
		.features.shaderStorageImageReadWithoutFormat = agrh.features.shaderStorageImageReadWithoutFormat,
		.features.shaderStorageImageWriteWithoutFormat = VK_TRUE,
		.features.shaderInt64 = agrh.features.shaderInt64,
		.features.shaderInt16 = VK_TRUE,
	};
	linkPNext(&features, ememdup(&_1_0, sizeof _1_0));

	// Core 1.1

	VkPhysicalDevice16BitStorageFeatures _16BitStorage = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES,
		.storageBuffer16BitAccess = VK_TRUE,
	};
	linkPNext(&features, ememdup(&_16BitStorage, sizeof _16BitStorage));

	exts[extCount++] = "VK_KHR_shader_float_controls";
	exts[extCount++] = "VK_KHR_spirv_1_4";

	// Core 1.2

	VkPhysicalDevice8BitStorageFeatures _8BitStorage = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES,
		.storageBuffer8BitAccess = VK_TRUE,
	};
	linkPNext(&features, ememdup(&_8BitStorage, sizeof _8BitStorage));
	exts[extCount++] = "VK_KHR_8bit_storage";

	VkPhysicalDeviceShaderFloat16Int8Features shaderFloat16Int8 = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES,
		.shaderFloat16 = VK_TRUE, // TODO: make this optional
		.shaderInt8 = VK_TRUE,
	};
	linkPNext(&features, ememdup(&shaderFloat16Int8, sizeof shaderFloat16Int8));
	exts[extCount++] = "VK_KHR_shader_float16_int8";

	VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexing = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES,
		.shaderSampledImageArrayNonUniformIndexing = VK_TRUE,
		.shaderStorageImageArrayNonUniformIndexing = VK_TRUE,
		.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE,
		.descriptorBindingStorageImageUpdateAfterBind = VK_TRUE,
		.descriptorBindingUpdateUnusedWhilePending = VK_TRUE,
		.descriptorBindingPartiallyBound = VK_TRUE,
		.runtimeDescriptorArray = VK_TRUE,
	};
	linkPNext(&features, ememdup(&descriptorIndexing, sizeof descriptorIndexing));
	exts[extCount++] = "VK_EXT_descriptor_indexing";

	VkPhysicalDeviceImagelessFramebufferFeatures imagelessFramebuffer = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES,
		.imagelessFramebuffer = VK_TRUE,
	};
	linkPNext(&features, ememdup(&imagelessFramebuffer, sizeof imagelessFramebuffer));
	exts[extCount++] = "VK_KHR_imageless_framebuffer";

	VkPhysicalDeviceBufferDeviceAddressFeatures bufferDeviceAddress = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES,
		.bufferDeviceAddress = VK_TRUE,
	};
	linkPNext(&features, ememdup(&bufferDeviceAddress, sizeof bufferDeviceAddress));
	exts[extCount++] = "VK_KHR_buffer_device_address";

	VkPhysicalDeviceVulkanMemoryModelFeatures vulkanMemoryModel = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES,
		.vulkanMemoryModel = VK_TRUE,
		// Not sure we actually need these
		.vulkanMemoryModelDeviceScope = VK_TRUE,
		.vulkanMemoryModelAvailabilityVisibilityChains = VK_TRUE,
	};
	linkPNext(&features, ememdup(&vulkanMemoryModel, sizeof vulkanMemoryModel));
	exts[extCount++] = "VK_KHR_vulkan_memory_model";

	exts[extCount++] = "VK_KHR_image_format_list";
	exts[extCount++] = "VK_KHR_create_renderpass2";

	// Core in 1.3

	VkPhysicalDeviceSynchronization2Features synchronization2 = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES,
		.synchronization2 = VK_TRUE,
	};
	linkPNext(&features, ememdup(&synchronization2, sizeof synchronization2));
	exts[extCount++] = "VK_KHR_synchronization2";

	if (!agrh.features.shaderStorageImageReadWithoutFormat)
		exts[extCount++] = "VK_KHR_format_feature_flags2";

	// KHR extensions

	exts[extCount++] = "VK_KHR_swapchain";
	exts[extCount++] = "VK_KHR_swapchain_mutable_format"; // TODO: don't depend on this ext, the phone doesn't have it

	// exts[extCount++] = "VK_KHR_push_descriptor"; // TODO: provide a fallback for this ext, the phone doesn't have it

	if ((r = fns.vkCreateDevice(physicalDevice, &(VkDeviceCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = features,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = (VkDeviceQueueCreateInfo[]) {
			{
				.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				.queueFamilyIndex = queueFamily,
				.queueCount = 1,
				.pQueuePriorities = (float[]) {1.0f},
			},
		},
		.enabledExtensionCount = (uint32_t)extCount,
		.ppEnabledExtensionNames = exts,
	}, NULL, &device)) != VK_SUCCESS)
		fatalvk("vkCreateDevice: ", r);

	deviceFeatures = features;

#define F(fn) fns.fn = (PFN_##fn)fns.vkGetDeviceProcAddr(device, #fn);
	DEVICE_FUNCS(F)
#undef F

	/*
	 * BUG: this was broken on Mali blob. We should re-test and re-enable if
	 * it was fixed on the new driver.

		fns.vkGetDeviceQueue2(device, &(VkDeviceQueueInfo2) {
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2,
			.queueFamilyIndex = queueFamily,
			.queueIndex = 0,
		}, &queue);
	*/

	fns.vkGetDeviceQueue(device, queueFamily, 0, &queue);
}

bool
haveInstanceLayer(const char *name)
{
	PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties = (PFN_vkEnumerateInstanceLayerProperties)fns.vkGetInstanceProcAddr(NULL, "vkEnumerateInstanceLayerProperties");

	VkResult r;

	uint32_t layerCount = 0;
	if ((r = vkEnumerateInstanceLayerProperties(&layerCount, NULL)) != VK_SUCCESS)
		fatalvk("vkEnumerateInstanceLayerProperties: ", r);

	VkLayerProperties *layers = emalloc(layerCount * sizeof layers[0]);
	if ((r = vkEnumerateInstanceLayerProperties(&layerCount, layers)) != VK_SUCCESS)
		fatalvk("vkEnumerateInstanceLayerProperties: ", r);

	bool have = false;
	for (uint32_t i = 0; i < layerCount; i++) {
		if (strcmp(layers[i].layerName, name) == 0) {
			have = true;
			break;
		}
	}

	free(layers);

	return have;
}
