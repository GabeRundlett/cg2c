#include <complex.h>
#include <math.h>
#include <stdint.h>
#include <stddef.h>

#ifdef __cg2c__
#include <cg2_subgroup.h>
#endif

// TODO: move into math.h and gate it behind some macro?
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

enum {
	FFTBitReverseIndex = 1,
};

typedef struct FFTParams {
	const float complex *x;
	float complex *X;
	int lgN;
	int s;
	int flags;
	uint32_t i;
} FFTParams;

void butterflies(FFTParams *arg);
