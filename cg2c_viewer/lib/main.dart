import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';

import 'package:flutter/material.dart';

import 'ir_graph_view.dart';
import 'ir_text_view.dart';
import 'ir_view.dart';
import 'material_regexp_field.dart';
import 'ops.dart';

void main(List<String> args) {
  runApp(MyApp(start: _get(args, 0)));
}

T? _get<T>(List<T> l, int i) => i < l.length ? l[i] : null;

class MyApp extends StatelessWidget {
  final String? start;

  const MyApp({super.key, this.start});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cg2c Dump Viewer',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.deepOrange,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.deepOrange,
      ),
      // showPerformanceOverlay: true,
      // checkerboardRasterCacheImages: true,
      home: MyHomePage(start: start),
    );
  }
}

class Pass extends StatefulWidget {
  final String name;
  final List<Value> values;
  final List<Block> blocks;
  final bool changed;

  const Pass({
    super.key,
    required this.name,
    required this.values,
    required this.blocks,
    required this.changed,
  });

  @override
  State<Pass> createState() => _PassState();
}

enum PassViewType { graph, text }

class _PassState extends State<Pass> {
  bool _expanded = false;
  late PassViewType _viewType; // TODO: make this tab-controlled

  final _bucket = PageStorageBucket();

  @override
  void initState() {
    super.initState();

    // TODO: come up with a better criteria?
    if (widget.name == 'schedule') {
      _viewType = PassViewType.text;
    } else {
      _viewType = PassViewType.graph;
    }
  }

  void onHeaderPressed() {
    setState(() => _expanded = !_expanded);
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(color: _color()),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: _buildImpl(context),
    );
  }

  Widget _buildImpl(BuildContext context) {
    var titleStyle = TextStyle(color: _color2());

    if (!_expanded) {
      return RotatedBox(
        quarterTurns: -1,
        child: TextButton(
          onPressed: onHeaderPressed,
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(widget.name, style: titleStyle),
          ),
        ),
      );
    }

    return IntrinsicWidth(
      child: Column(children: [
        Row(children: [
          Expanded(
            child: TextButton(
              onPressed: onHeaderPressed,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(widget.name, style: titleStyle),
              ),
            ),
          ),
          // TODO: replace this button with tab bar underneath the header
          TextButton(
            onPressed: () => setState(() => _viewType =
                _viewType == PassViewType.text
                    ? PassViewType.graph
                    : PassViewType.text),
            child: _viewType == PassViewType.text
                ? const Text('G')
                : const Text('T'),
          ),
        ]),
        /* DefaultTabController(
          length: 2,
          child: Container(
            height: 30.0,
            child: TabBar(
              tabs: [
                Tab(
                  text: 'G',
                ),
                Tab(
                  text: 'T',
                ),
              ],
            ),
          ),
        ), */
        // TODO: should be resizable
        SizedBox(
          width: 512,
          height: 512,
          child: PageStorage(
            bucket: _bucket,
            child: _buildBody(context),
          ),
        ),
      ]),
    );
  }

  Widget _buildBody(BuildContext context) {
    var pageStorageKey = PageStorageKey(_viewType);

    switch (_viewType) {
      case PassViewType.graph:
        return IRView2(
          key: pageStorageKey,
          values: widget.values,
          blocks: widget.blocks,
        );

      case PassViewType.text:
        return Padding(
          key: pageStorageKey,
          padding: const EdgeInsets.fromLTRB(1, 0, 0, 0),
          child: IRView(
            values: widget.values,
            blocks: widget.blocks,
          ),
        );
    }
  }

  // TODO: make a theme object
  Color _color() {
    var theme = Theme.of(context);
    var colorScheme = theme.colorScheme;
    return colorScheme.primaryContainer.withOpacity(widget.changed ? 1.0 : 0.4);
  }

  Color _color2() {
    var theme = Theme.of(context);
    var colorScheme = theme.colorScheme;
    return colorScheme.primary.withOpacity(widget.changed ? 1.0 : 0.62);
  }
}

class Symbol extends StatefulWidget {
  final String name;
  final List<Pass> passes;
  final Map<String, Value> valueMap;

  const Symbol({
    super.key,
    required this.name,
    required this.passes,
    required this.valueMap,
  });

  @override
  State<Symbol> createState() => _SymbolState();
}

class _SymbolState extends State<Symbol> {
  final Map<String, int> _valueHues = {};
  int _hueCtr = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).colorScheme.primary),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          // TODO: can we somehow make text always visible regardless of
          // horizontal scroll?
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              widget.name,
              style: const TextStyle(fontFamily: 'monospace'),
            ),
          ),
        ),
        IntrinsicHeight(
          child: NotificationListener<ValueDefClickNotification>(
            onNotification: (notification) {
              setState(() {
                // Increment the counter regardless of what is being done, as
                // ValueHues looks at the counter value to decide whether to
                // notify dependent widgets.
                var hue = _hueCtr++;

                var id0 = notification.value.id;
                var id0Hue = _valueHues[id0];

                // TODO: paint descendant Values as well
                for (String? id = id0;
                    id != null;
                    id = widget.valueMap[id]?.trace) {
                  if (id0Hue == null) {
                    _valueHues[id] = hue;
                  } else {
                    // TODO: allow a Value to have several hues at once?
                    _valueHues.remove(id);
                  }
                }
              });
              return true; // no use in propagating further
            },
            child: ValueHues(
              hues: _valueHues,
              ctr: _hueCtr,
              child: Row(
                // TODO: coalesce a run of changed passes together like Go's GOSSAFUNC
                // does, and draw those strings with darker color
                children: widget.passes,
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

class _DumpView extends StatefulWidget {
  final RegExp filter;
  final List<Symbol> symbols;

  const _DumpView({super.key, required this.filter, required this.symbols});

  @override
  State<_DumpView> createState() => _DumpViewState();
}

class _DumpViewState extends State<_DumpView> {
  @override
  Widget build(BuildContext context) {
    // TODO: we'll need a new widget that fuses together Visibility,
    // separator, etc. into one, and can preserve scroll location on
    // Visibility changes.

    return Expanded(
      child: InteractiveViewer(
        constrained: false,
        scaleEnabled: false,
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: widget.symbols
                .map((s) => Visibility(
                      visible: widget.filter.hasMatch(s.name),
                      maintainState: true,
                      child: s,
                    ))
                .toList(),
          ),
        ),
      ),
    );
  }
}

// TODO: show recent documents on the home page by default, like evince

// TODO: just merge this widget right into MyApp
class MyHomePage extends StatefulWidget {
  final String? start;

  const MyHomePage({super.key, this.start});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _filter = RegExp('');
  var _diColumns = false;
  late Key _dumpViewKey;
  List<Symbol>? _symbols;

  @override
  void initState() {
    super.initState();

    if (widget.start != null) {
      _openDump(widget.start!);
    }
  }

  @override
  Widget build(BuildContext context) {
    var openDumpButton = ElevatedButton(
      onPressed: () async {
        if (!mounted) return;

        var picked = await FilePicker.platform.pickFiles(
          dialogTitle: 'Open Dump',
          // initialDirectory: '.', // TODO: set initial directory to the last one
          lockParentWindow: true,
        );
        if (picked != null) {
          await _openDump(picked.files.first.path!);
        }
      },
      child: const Icon(Icons.file_open),
    );

    var filterField = Container(
      constraints: const BoxConstraints(maxWidth: 320),
      child: RegExpField(
        decoration: const InputDecoration(isDense: true),
        onChanged: (re) => setState(() => _filter = re),
      ),
    );

    var header = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ButtonBar(
          alignment: MainAxisAlignment.start,
          children: [openDumpButton, filterField],
        ),
        PopupMenuButton<void>(
          itemBuilder: (context) => [
            PopupMenuItem(
              // Note: things do not update in PopupMenus, so we'll need to do
              // something more
              child: Row(children: [
                Checkbox(
                  value: _diColumns,
                  onChanged: (checked) => setState(() => _diColumns = checked!),
                ),
                const Text('DI columns'),
              ]),
            ),
          ],
        ),
      ],
    );

    return Scaffold(
      body: Column(
        children: [
          header,
          Config(
            diSource: false,
            diColumns: _diColumns,
            child: _symbols != null
                ? _DumpView(
                    key: _dumpViewKey,
                    filter: _filter,
                    symbols: _symbols!,
                  )
                : const Text('recently opened files here'),
          ),
        ],
      ),
    );
  }

  Future<void> _openDump(String filename) async {
    // TODO: also set up watcher filesystem watcher to refresh when dump changes on-disk

    Type typeFromJson(dynamic hehe) {
      var kind = Kind.values.byName(hehe['kind']);

      switch (kind) {
        case Kind.tuple:
        case Kind.func:
          return Type(
            kind: kind,
            elems: (hehe['elems'] as List<dynamic>).map(typeFromJson).toList(),
          );

        default:
          return Type(kind: kind);
      }
    }

    try {
      // TODO: might be a good idea to place this code into its own "isolate"

      var data = await File(filename).readAsString();

      try {
        var decoded = jsonDecode(data);

        var valueMap = <String, Value>{};

        // TODO: use some more automated approach that allows us to specify
        // schema declaratively (e.g. using types)
        var m = <String, List<Pass>>{};
        for (var entry in decoded['log']) {
          var passes = m.putIfAbsent(entry['symbol'], () => []);

          var values = <Value>[];

          for (var v in entry['values']) {
            // TODO: be more robust

            var auxInt = v['auxInt'] != null ? BigInt.parse(v['auxInt']) : null;
            if (auxInt != null) assert(auxInt.sign >= 0);

            var v2 = Value(
              id: v['id'],
              // This is going to be a linear search, we really want to make a
              // map for this purpose
              op: Op.values.byName(v['op']),
              type: typeFromJson(v['type']),
              auxInt: auxInt,
              args: (v['args'] as List<dynamic>)
                  .map((s) => valueMap[s as String]!)
                  .toList(),
              pos: Pos(
                filename: v['pos']['filename'],
                line: v['pos']['line'],
                col: v['pos']['col'],
              ),
              trace: v['trace'],
            );

            values.add(v2);

            valueMap[v['id']] = v2;
          }

          passes.add(Pass(
            name: entry['pass'],
            values: values,
            blocks: (entry['blocks'] as List<dynamic>)
                .map((v) => Block(
                      id: v['id'],
                      values: (v['values'] as List<dynamic>)
                          .map((s) => valueMap[s as String]!)
                          .toList(),
                    ))
                .toList(),
            changed: true,
          ));
        }

        var newSymbols = m.entries
            .map((entry) => Symbol(
                name: entry.key, passes: entry.value, valueMap: valueMap))
            .toList();

        if (mounted) {
          setState(() {
            _dumpViewKey = UniqueKey();
            _symbols = newSymbols;
          });
        }
      } on FormatException catch (e) {
        // TODO: repair, retry and report an error only if we fail again
        await _showOpenDumpError(filename, Text('$e'));
      }
    } on FileSystemException catch (e) {
      await _showOpenDumpError(filename, Text('$e'));
    }
  }

  Future<void> _showOpenDumpError(String filename, Widget content) async {
    // In GNOME we'd show a bar at the top like evince does

    if (!mounted) return;

    await showDialog<void>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Unable to open "$filename".'),
        content: Row(children: [const Icon(Icons.error, size: 64), content]),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('OK'),
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }
}

/*
Future<T> _wat<T>(BuildContext context, Future<T> Function() future) async {
  var modalBarrier = Navigator.of(context, rootNavigator: false).push(
    RawDialogRoute(
      pageBuilder: (context, animation, secondaryAnimation) =>
          const SizedBox.shrink(),
      barrierDismissible: false,
    ),
  );

  try {
    return await future();
  } finally {
    var token = Random().nextInt(1 << 32);

    Navigator.pop(context, token);

    assert(await modalBarrier == token);
  }
}
*/
