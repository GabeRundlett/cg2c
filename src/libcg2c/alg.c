#include "all.h"

#include "map_type.h"

// TODO: something better than fnv for cg2c_memhash

size_t
cg2c_memhash(const void *p, size_t n, size_t seed)
{
	// See http://www.isthe.com/chongo/tech/comp/fnv/

	const unsigned char *x = p;

	_Static_assert(~(size_t)0 == 0xffffffffffffffff);

	uint64_t hash = 0xcbf29ce484222325;
	for (size_t i = 0; i < n; i++)
		hash = 0x100000001b3*hash ^ (uint64_t)*x++;
	return (size_t)hash ^ seed;
}

static bool memequal32(const void *p, const void *q) { return memcmp(p, q, 4) == 0; }

static size_t memhash32(const void *p, size_t h) { return cg2c_memhash(p, 4, h); }

static bool memequal64(const void *p, const void *q) { return memcmp(p, q, 8) == 0; }

static size_t memhash64(const void *p, size_t h) { return cg2c_memhash(p, 8, h); }

static bool strequal(const void *p, const void *q) { return cg2c_strcmp(*(const String*)p, *(const String*)q) == 0; }

static size_t
strhash(const void *p, size_t h)
{
	String s = *(const String*)p;
	return cg2c_memhash(cg2c_strdata(s), cg2c_strlen(s), h);
}

static bool ptrequal(const void *p, const void *q) { return *(const void* const*)p == *(const void* const*)q; }

// TODO: change to hashing/comparing uintptrs?
static size_t
ptrhash(const void *p, size_t h)
{
	const void *x = *(const void* const*)p;
	return cg2c_memhash(&x, sizeof x, h);
}

static bool
ptrSliceEqual(const void *p, const void *q)
{
	Slice s = *(const Slice*)p;
	assert(s.cap == 0 || s._elemsize == sizeof(void*));
	Slice t = *(const Slice*)q;
	assert(t.cap == 0 || t._elemsize == sizeof(void*));
	if (s.len != t.len)
		return false;
	return s.len == 0 || memcmp(s.data, t.data, s.len*sizeof(void*)) == 0;
}

static size_t
ptrSliceHash(const void *p, size_t h)
{
	Slice s = *(const Slice*)p;
	assert(s.cap == 0 || s._elemsize == sizeof(void*));
	return cg2c_memhash(s.data, s.len*sizeof(void*), h);
}

const MapType* cg2c_32_32_map = &(MapType) {
	.zero     = (int32_t[1]) {},
	.equal    = memequal32,
	.hasher   = memhash32,
	.keysize  = 4,
	.elemsize = 4,
};

const MapType* cg2c_64_32_map = &(MapType) {
	.zero     = (int32_t[1]) {},
	.equal    = memequal64,
	.hasher   = memhash64,
	.keysize  = 8,
	.elemsize = 4,
};

const MapType* cg2c_str_32_map = &(MapType) {
	.zero     = (int32_t[1]) {},
	.equal    = strequal,
	.hasher   = strhash,
	.keysize  = sizeof(String),
	.elemsize = 4,
};

const MapType* cg2c_str_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = strequal,
	.hasher   = strhash,
	.keysize  = sizeof(String),
	.elemsize = sizeof(void*),
};

const MapType* cg2c_ptr_32_map = &(MapType) {
	.zero     = (int32_t[1]) {},
	.equal    = ptrequal,
	.hasher   = ptrhash,
	.keysize  = sizeof(void*),
	.elemsize = 4,
};

const MapType* cg2c_ptr_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = ptrequal,
	.hasher   = ptrhash,
	.keysize  = sizeof(void*),
	.elemsize = sizeof(void*),
};

const MapType* cg2c_ptr_slice_map = &(MapType) {
	.zero     = &(Slice) {},
	.equal    = ptrequal,
	.hasher   = ptrhash,
	.keysize  = sizeof(void*),
	.elemsize = sizeof(Slice),
};

const MapType* cg2c_ptr_set = &(MapType) {
	.zero     = &(char) {0}, // anything non-NULL
	.equal    = ptrequal,
	.hasher   = ptrhash,
	.keysize  = sizeof(void*),
	.elemsize = 0,
};

const MapType* cg2c_ptrSlice_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = ptrSliceEqual,
	.hasher   = ptrSliceHash,
	.keysize  = sizeof(Slice),
	.elemsize = sizeof(void*),
};
