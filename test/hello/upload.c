#include "hello.h"

void
doImageUpload(Submission *ba, const DoUpload *info)
{
	Node nodes[] = {
		{
			.token = 69,
			.stageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT, // TODO: make this finer
			.accessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
			.image = info->dst,
			// TODO: we don't do per-subresource state tracking yet,
			// so don't discard anything. It might be possible to
			// discard in cases when it's known that we're
			// overwriting the entire subresource, but not sure how
			// worth that would be. That would also require
			//
			// .discard = true,
			.layout = VK_IMAGE_LAYOUT_GENERAL,
			.subresourceRange = {
				.aspectMask = info->imageSubresource.aspectMask,
				.baseMipLevel = 0, // info->imageSubresource.mipLevel
				.levelCount = VK_REMAINING_MIP_LEVELS, // 1
				.baseArrayLayer = 0, // info->imageSubresource.baseArrayLayer
				.layerCount = VK_REMAINING_ARRAY_LAYERS, // info->imageSubresource.layerCount
			},
			.label = "texture upload",
		},
	};

	VkCommandBuffer cb = getcb(ba);

	BufferAndOffset asd = deviceptrinfo((VkDeviceAddress)info->src);

	VkBufferImageCopy region = {
		.bufferOffset = asd.offset,
		.bufferRowLength = info->bufferRowLength,
		.bufferImageHeight = info->bufferImageHeight,
		.imageSubresource = info->imageSubresource,
		.imageOffset = info->imageOffset,
		.imageExtent = info->imageExtent,
	};
	fns.vkCmdCopyBufferToImage(cb, asd.buffer, info->dst->vk, VK_IMAGE_LAYOUT_GENERAL, 1, &region);

	execPass(ba, &(Pass) {
		.nodes = nodes,
		.nodeCount = nelem(nodes),
		.cb = cb,
		.label = "texture upload",
	});
}
