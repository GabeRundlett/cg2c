## Parsing

The parser constructs an AST. Name binding and type checking are performed
during parsing. This is necessary for ambiguity resolution. Ambiguities are
resolved by looking at the name only in cases they can not be resolved
otherwise. For example, `int x` declares `x`, even if `x` binds a type in that
context. This behavior is in line with that of GCC and Clang.

When it is necessary to evaluate a constant expression, an SSA representation is
constructed for the expression and constant folding is performed.

Earlier, parser would construct the SSA representation directly, skipping
construction of the AST, but departure from this approach allowed for a number
of improvements. Notably, deferring SSA construction to happen until after type
checking the AST lets the SSA representation use a simpler type system than the
one used by the AST.

## SSA

Cg2c uses a somewhat unusual way to represent programs.

Block parameters are used to transfer values along with control, replacing
ϕ-instructions.

Scheduling of instructions is deferred until the last few steps of the
compilation process. This makes a number of common transformations such as
common subexpression elimination easier, as there's no schedule to maintain.
Moreover, until scheduling, blocks maintain references only to their
terminators, which enables trivial dead code elimination.

<!-- TODO -->

### Types

<!-- TODO -->

### Adding new ops

Ops should be single-return. E.g. add carry and sub borrow only return carry
and borrow respectively.

Ops such as bit field extracts and insert, bit reverse, etc. should only be used
as lowering ops, as otherwise more rewrite rules will unnecessarily need to be
written.

Ops that use mem usually have Load or Store in their names. Ops that don't,
but that are similar to Load, use Get.

## Vulkan

### Debugging

Use [SPIRV-Cross](https://github.com/KhronosGroup/SPIRV-Cross) to get readable
GLSL view of compiler's output.

#### Getting object code

Some drivers allow the user to view shaders in representations that are internal
to the driver, including object code that the device runs. Depending on the
driver and application in question, there are several ways this can be done.

The most universal method is to use the VK_KHR_pipeline_executable_properties
extension, usually through [RenderDoc](https://renderdoc.org/). Note that not
all drivers support this extension, and not all drivers that do, provide code.
Moreover, RenderDoc only works with windowed programs, which makes it unsuitable
for use with certain programs.

Some [Mesa](https://gitlab.freedesktop.org/mesa/mesa/) drivers have a debug knob
that can be used to spill the compiler's internals to stderr. For RADV and ANV
it is setting `RADV_DEBUG=nocache,shaders` and `INTEL_DEBUG=shaders` environment
variables respectively. Knobs for other drivers may be found by looking at their
source.

AMD's tools [RGP](https://gpuopen.com/rgp/) and [RGA](https://gpuopen.com/rga/)
can be used to view the object code as compiled by AMD's compiler. RGA can be
used on systems without an AMD GPU present.

<!-- TODO: write about using fossilize -->
