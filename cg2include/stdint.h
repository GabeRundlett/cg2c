#ifndef __STDINT_H__
#define __STDINT_H__

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;

// TODO: intptr_t (same as ptrdiff_t) and uintptr_t (same as size_t)

#endif
