import 'package:flutter/material.dart';

class RegExpField extends StatefulWidget {
  final InputDecoration? decoration;
  final ValueChanged<RegExp>? onChanged;

  const RegExpField({
    super.key,
    this.decoration,
    this.onChanged,
  });

  @override
  State<RegExpField> createState() => _RegExpFieldState();
}

class _RegExpFieldState extends State<RegExpField> {
  bool _invalid = false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: widget.decoration,
      onChanged: (s) {
        RegExp? re;
        try {
          re = RegExp(s);
        } on FormatException {
          // s is invalid regexp
        }

        if (re != null) {
          widget.onChanged?.call(re);
        }

        setState(() => _invalid = re == null);
      },
    );
  }
}
