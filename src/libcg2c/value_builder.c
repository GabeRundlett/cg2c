#include "all.h"

#include "map_type.h"

struct Rewriter {
	Slice rules;

	// TODO: acceleration structure
};

typedef struct CSEKey CSEKey;
struct CSEKey {
	Op    op;
	Type  *type;
	void  *aux;
	Slice args; // of Value*
};

static const MapType* cseKey_ptr_map;

static bool patternSatisfied(Pattern *pat, Value *v);

Builder
cg2c_builderWithPos(Builder b, Pos pos, const void *trace)
{
	b.pos = pos;
	b.trace = trace;
	return b;
}

Rewriter*
cg2c_newRewriter(Slice rules, GC *gc)
{
	Rewriter *rewriter = cg2c_malloc(sizeof *rewriter, gc);
	rewriter->rules = rules;
	return rewriter;
}

CSE* cg2c_newCSE(GC *gc) { return (CSE*)cg2c_mkmap(cseKey_ptr_map, gc); }

Value*
cg2c_buildValue(Builder b, Op op, Type *type, void *aux, Slice args)
{
	Rewriter *rewriter = b.rewriter;
	Map *cse = (Map*)b.cse;

	// TODO: optional typechecker which will be part of our validation thing

	Value *vtmp = &(Value) {
		.op   = op,
		.type = type,
		.aux  = aux,
		.args = args,
		// .pos  = b.pos,
	};

	// Apply rewrite rules

	for (size_t i = 0; i < cg2c_sllen(rewriter->rules); i++) {
		RewriteRule rule = *(RewriteRule*)cg2c_slindex(sizeof(RewriteRule), rewriter->rules, i);
		if (patternSatisfied(rule.pattern, vtmp)) {
			Value *v = rule.replace(b, vtmp);
			// assert(v != NULL);
			// assert(v->type == type);
			if (v != vtmp)
				return v;
		}
	}

	// TODO: raise this restriction later
	if (cg2c_opInfo[op].commutative)
		assert(cg2c_sllen(args) == 2);

	Value *v = *(Value* const*)cg2c_mapaccess1(cseKey_ptr_map, cse, &(CSEKey) {
		.op   = op,
		.type = type,
		.aux  = aux,
		.args = args,
	});
	if (v == NULL && cg2c_opInfo[op].commutative) {
		v = *(Value* const*)cg2c_mapaccess1(cseKey_ptr_map, cse, &(CSEKey) {
			.op   = op,
			.type = type,
			.aux  = aux,
			.args = cg2c_mkslicenocopy(sizeof(Value*), (Value*[2]) {
				*(Value**)cg2c_slindex(sizeof(Value*), args, 1),
				*(Value**)cg2c_slindex(sizeof(Value*), args, 0),
			}, 2),
		});

		// Another solution is to teach value_ptr_map->hasher to hash
		// commutative ops using ^ (or any other commutative operator)
		// and value_ptr_map->equal to compare that appropriately.
		//
		// Apparently, CSE is the second hottest thing after rewriter,
		// so that might be a good idea.
	}
	if (v == NULL) {
		v = cg2c_malloc(sizeof *v, b.ctxt->gc);
		v->op = op;
		v->type = type;
		if (!cg2c_opInfo[op].smallaux) {
			v->aux = aux;
		} else {
			v->aux = &v->auxstorage;
			*(uint64_t*)v->aux = *(uint64_t*)aux;
		}
		// Don't needlessly allocate empty slices.
		if (cg2c_sllen(args) > 0)
			v->args = cg2c_slclone(sizeof(Value*), args, b.ctxt->gc);
		v->pos = b.pos;
		if (true)
			v->trace = b.trace;

		*(Value**)cg2c_mapassign(cseKey_ptr_map, cse, &(CSEKey) {
			.op   = op,
			.type = type,
			.aux  = v->aux,
			.args = v->args, // use cloned args because original could've been stack-allocated
		}, b.ctxt->gc) = v;
	}
	return v;
}

bool
patternSatisfied(Pattern *pat, Value *v)
{
	bool ok = true;

	// Any value trivially satisfies an empty pattern.
	if (pat != NULL) {
		ok = ok && v->op == pat->op;

		// If this pattern's parameters list does not end in ellipsis,
		// the number of arguments has to match.
		if (!pat->ddd)
			ok = ok && cg2c_valueNArg(v) == pat->narg;
		else
			ok = ok && cg2c_valueNArg(v) >= pat->narg;

		for (size_t i = 0; i < pat->narg; i++)
			ok = ok && patternSatisfied(pat->args[i], cg2c_valueArg(v, i));
	}
	return ok;
}

static bool
valuePtrSliceEqual(Slice s, Slice t)
{
	return s.len == t.len && (s.len == 0 || memcmp(s.data, t.data, s.len*sizeof(Value*)) == 0);
}

static bool
cseKeyEqual(const void *p, const void *q)
{
	const CSEKey *v = p;
	const CSEKey *u = q;

	bool auxEqual = v->aux == u->aux;
	if (v->op == u->op && cg2c_opInfo[v->op].smallaux) {
		uint64_t x = *(uint64_t*)v->aux;
		uint64_t y = *(uint64_t*)u->aux;
		auxEqual = x == y;
	}

	return v->op == u->op && v->type == u->type && auxEqual && valuePtrSliceEqual(v->args, u->args);
}

static size_t
cseKeyHash(const void *p, size_t h)
{
	CSEKey v = *(const CSEKey*)p;
	h = cg2c_memhash(&v.op, sizeof v.op, h);
	h = cg2c_memhash(&v.type, sizeof v.type, h);
	if (cg2c_opInfo[v.op].smallaux) {
		uint64_t x = *(uint64_t*)v.aux;
		h = cg2c_memhash(&x, sizeof x, h);
	} else {
		h = cg2c_memhash(&v.aux, sizeof v.aux, h);
	}
	h = cg2c_memhash(v.args.data, v.args.len*sizeof(Type*), h);
	return h;
}

static const MapType* cseKey_ptr_map = &(MapType) {
	.zero     = &(void*) {NULL},
	.equal    = cseKeyEqual,
	.hasher   = cseKeyHash,
	.keysize  = sizeof(CSEKey),
	.elemsize = sizeof(void*),
};
