#include "hello.h"

#include <dlfcn.h>

PFN_vkGetInstanceProcAddr
openVulkan(void)
{
	void *libvulkan = dlopen("libvulkan.so", RTLD_LAZY);
	if (libvulkan == NULL)
		fatalf("dlopen libvulkan.so: %s", dlerror());
	return (PFN_vkGetInstanceProcAddr)dlsym(libvulkan, "vkGetInstanceProcAddr");
}
