prologue="""
#include "all.h"

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
	Value *args[] = {a0};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static bool
allLeavesCanOnlyBeGlobalAddresses(Value *v)
{
    switch (v->op) {
    case OpOr:
    case OpLsh:
    case OpZeroExt:
    case OpSignExt:
    case OpIntToPtr: {
        for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
            if (!allLeavesCanOnlyBeGlobalAddresses(cg2c_valueArg(v, i)))
                return false;
        }
        return true;
    }

    case OpConst:
        return true;

    case OpGetIndirectConstant:
    case OpGetPushConstant:
        return true;

    default:
        return false;
    }
}
"""

label = "cg2c_vulkanRules"

rules = [
    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(op="Var")]),
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], false);",
    ),

    # Ugh
    #
    # TODO: think of a better way to solve this
    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(var="p")]),
        guard="allLeavesCanOnlyBeGlobalAddresses(p)",
        replace="return cg2c_buildConst(b, cg2c_types[KindBool], true);",
    ),
]
