#include "hello.h"

// TODO: rename this file to image.c or image_view.c, or image_and_sampler.c

enum {
	ReasonablyBigNumber = 100000,
};

struct ImageView {
	VkImageView vk;
	uint32_t loadStoreHandle;
	uint32_t samplingHandle;
};

struct Sampler {
	VkSampler vk;
	uint32_t handle;
};

static uint32_t usedHandles[(ReasonablyBigNumber + 31) / 32];

static uint32_t handlectr = 100;

VkDescriptorSetLayout bindlessLayout;
VkDescriptorSet bindless;

void
initbindless(void)
{
	VkResult r;

	if ((r = fns.vkCreateDescriptorSetLayout(device, &(VkDescriptorSetLayoutCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.pNext = &(VkDescriptorSetLayoutBindingFlagsCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO,
			.bindingCount = 3,
			.pBindingFlags = (VkDescriptorBindingFlags[]) {
				VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT |
					VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT |
					VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
				VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT |
					VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT |
					VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
				VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT |
					VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT |
					VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
			},
		},
		.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT,
		.bindingCount = 3,
		.pBindings = (VkDescriptorSetLayoutBinding[]) {
			{
				.binding = 0,
				.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER,
				.descriptorCount = ReasonablyBigNumber,
				.stageFlags = VK_SHADER_STAGE_ALL,
			},
			{
				.binding = 1,
				.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
				.descriptorCount = ReasonablyBigNumber,
				.stageFlags = VK_SHADER_STAGE_ALL,
			},
			{
				.binding = 2,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
				.descriptorCount = ReasonablyBigNumber,
				.stageFlags = VK_SHADER_STAGE_ALL,
			}
		},
	}, NULL, &bindlessLayout)) != VK_SUCCESS)
		fatalvk("vkCreateDescriptorSetLayout: ", r);

	VkDescriptorPool pool;
	if ((r = fns.vkCreateDescriptorPool(device, &(VkDescriptorPoolCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT,
		.maxSets = 1,
		.poolSizeCount = 3,
		.pPoolSizes = (VkDescriptorPoolSize[]) {
			{VK_DESCRIPTOR_TYPE_SAMPLER, ReasonablyBigNumber},
			{VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, ReasonablyBigNumber},
			{VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, ReasonablyBigNumber},
		},
	}, NULL, &pool)) != VK_SUCCESS)
		fatalvk("vkCreateDescriptorPool: ", r);

	if ((r = fns.vkAllocateDescriptorSets(device, &(VkDescriptorSetAllocateInfo) {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.descriptorPool = pool,
		.descriptorSetCount = 1,
		.pSetLayouts = (VkDescriptorSetLayout[]) {
			bindlessLayout,
		},
	}, &bindless)) != VK_SUCCESS)
		fatalvk("vkAllocateDescriptorSets: ", r);
}

void
createImageView(const VkImageViewCreateInfo *createInfo, ImageView **out)
{
	VkResult r;

	ImageView *view = emalloc(sizeof *view);

	if ((r = fns.vkCreateImageView(device, createInfo, NULL, &view->vk)) != VK_SUCCESS)
		fatalvk("vkCreateImageView: ", r);

	*out = view;
}

void
destroyImageView(ImageView *view)
{
	// TODO: also free handles
	fns.vkDestroyImageView(device, view->vk, NULL);
	free(view);
}

VkImageView
getImageViewVk(ImageView *view)
{
	return view->vk;
}

// TODO: reduce copypaste and think about sharing image views with GPU

uint32_t
getImageViewSamplingHandle(ImageView *view, VkImageLayout layout)
{
	assert(layout == VK_IMAGE_LAYOUT_GENERAL);

	uint32_t *handlePtr = &view->samplingHandle;

	uint32_t handle = *handlePtr;

	if (handle == 0) {
		handle = handlectr++;

		assert(handle != 0);

		fns.vkUpdateDescriptorSets(device, 1, &(VkWriteDescriptorSet) {
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = bindless,
			.dstBinding = 1, // sampled images
			.dstArrayElement = handle,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
			.pImageInfo = &(VkDescriptorImageInfo) {
				.imageView = view->vk,
				.imageLayout = layout,
			},
		}, 0, NULL);

		*handlePtr = handle;
	}

	return handle;
}

uint32_t
getImageViewHandle(ImageView *view, VkImageLayout layout)
{
	assert(layout == VK_IMAGE_LAYOUT_GENERAL);

	uint32_t *handlePtr = &view->loadStoreHandle;

	uint32_t handle = *handlePtr;

	if (handle == 0) {
		handle = handlectr++;

		assert(handle != 0);

		fns.vkUpdateDescriptorSets(device, 1, &(VkWriteDescriptorSet) {
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = bindless,
			.dstBinding = 2, // storage images
			.dstArrayElement = handle,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
			.pImageInfo = &(VkDescriptorImageInfo) {
				.imageView = view->vk,
				.imageLayout = layout,
			},
		}, 0, NULL);

		*handlePtr = handle;
	}

	return handle;
}

void
createSampler(const VkSamplerCreateInfo *createInfo, Sampler **out)
{
	VkResult r;

	Sampler *sampler = emalloc(sizeof *sampler);

	if ((r = fns.vkCreateSampler(device, createInfo, NULL, &sampler->vk)) != VK_SUCCESS)
		fatalvk("vkCreateSampler: ", r);

	*out = sampler;
}

void
destroySampler(Sampler *sampler)
{
	fns.vkDestroySampler(device, sampler->vk, NULL);
	free(sampler);
}

uint32_t
getSamplerHandle(Sampler *sampler)
{
	uint32_t handle = sampler->handle;

	if (handle == 0) {
		handle = handlectr++;

		assert(handle != 0);

		fns.vkUpdateDescriptorSets(device, 1, &(VkWriteDescriptorSet) {
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = bindless,
			.dstBinding = 0, // samplers
			.dstArrayElement = handle,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER,
			.pImageInfo = &(VkDescriptorImageInfo) {
				.sampler = sampler->vk,
			},
		}, 0, NULL);

		sampler->handle = handle;
	}

	return handle;
}
