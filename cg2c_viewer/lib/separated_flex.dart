import 'package:flutter/material.dart';

class SeparatedRow extends Row {
  SeparatedRow({
    super.key,
    super.mainAxisAlignment,
    super.mainAxisSize,
    super.crossAxisAlignment,
    super.textDirection,
    super.verticalDirection,
    super.textBaseline,
    EdgeInsetsGeometry? padding,
    required Widget separator,
    List<Widget> children = const [],
  }) : super(
          children: _intersperse(
                  (padding != null
                      ? children.map((e) => Padding(padding: padding, child: e))
                      : children) as Iterable<Widget>,
                  separator)
              .toList(),
        );
}

class SeparatedColumn extends Column {
  SeparatedColumn({
    super.key,
    super.mainAxisAlignment,
    super.mainAxisSize,
    super.crossAxisAlignment,
    super.textDirection,
    super.verticalDirection,
    super.textBaseline,
    EdgeInsetsGeometry? padding,
    required Widget separator,
    List<Widget> children = const [],
  }) : super(
          children: _intersperse(
                  (padding != null
                      ? children.map((e) => Padding(padding: padding, child: e))
                      : children) as Iterable<Widget>,
                  separator)
              .toList(),
        );
}

Iterable<T> _intersperse<T>(Iterable<T> iterable, T e) sync* {
  var it = iterable.iterator;
  if (it.moveNext()) {
    yield it.current;
    while (it.moveNext()) {
      yield e;
      yield it.current;
    }
  }
}
