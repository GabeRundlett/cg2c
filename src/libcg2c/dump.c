#include "all.h"

// TODO: add aux printers

typedef struct JSONNesting JSONNesting;
struct JSONNesting {
	enum {
		JsonList = 1,
		JsonMap = 2,
	} type;
	bool comma;
};

typedef struct JSONPrinter JSONPrinter;
struct JSONPrinter {
	FILE *w;
	const char *tab;
	JSONNesting nesting[100];
	int curnest;
};

static void
jsonnewline(JSONPrinter *out)
{
	if (out->nesting[out->curnest].comma)
		fprintf(out->w, ",\n");
}

static void
jsontabs(JSONPrinter *out)
{
	for (int i = 0; i < out->curnest && out->tab != NULL; i++)
		fprintf(out->w, "%s", out->tab);
}

static void
validate(JSONPrinter *out, const char *key)
{
	if (out->curnest > 0) {
		assert((out->nesting[out->curnest-1].type == JsonList) == (key == NULL));
	} else {
		assert(!out->nesting[0].comma);
	}
}

static void
jsonprintf(JSONPrinter *out, const char *key, const char *format, ...)
{
	validate(out, key);

	char b[1024] = {};
	va_list arg;
	va_start(arg, format);
	vsnprintf(b, (sizeof b)-1, format, arg);
	va_end(arg);

	jsonnewline(out);

	// TODO: escape contents of b and key properly
	if (key != NULL)
		fprintf(out->w, "\"%s\": ", key);
	fprintf(out->w, "\"%s\"", b);
	out->nesting[out->curnest].comma = true;
}

static void
jsonputint(JSONPrinter *out, const char *key, int64_t n)
{
	validate(out, key);

	jsonnewline(out);

	// TODO: escape key properly
	if (key != NULL)
		fprintf(out->w, "\"%s\": ", key);
	fprintf(out->w, "%lld", (long long)n);
	out->nesting[out->curnest].comma = true;
}

static void
jsonbegin(JSONPrinter *out, const char *key, int mapOrList)
{
	validate(out, key);

	jsonnewline(out);

	// whether key should be null or not depends on current nesting type!!!
	if (key != NULL)
		fprintf(out->w, "\"%s\": ", key);
	fprintf(out->w, "%c\n", mapOrList == JsonMap ? '{' : '[');

	out->nesting[out->curnest++] = (JSONNesting) {.type = mapOrList};
}

static void
jsonend(JSONPrinter *out)
{
	bool comma = out->nesting[out->curnest].comma;
	out->nesting[out->curnest] = (JSONNesting) {};
	int mapOrList = out->nesting[--out->curnest].type;

	if (comma)
		fprintf(out->w, "\n");

	for (int i = 0; i < out->curnest && out->tab != NULL; i++)
		fprintf(out->w, "%s", out->tab);
	fprintf(out->w, "%c", mapOrList == JsonMap ? '}' : ']');

	out->nesting[out->curnest].comma = true;
}

void
cg2c_dumpinit(Compile *ctxt, FILE *w)
{
	JSONPrinter *dumper = cg2c_malloc(sizeof *dumper, ctxt->gc);
	dumper->w = w;
	dumper->nesting[dumper->curnest].type = JsonList;

	ctxt->dumper = dumper;

	jsonbegin(dumper, NULL, JsonMap);

	jsonbegin(dumper, "log", JsonList);
}

void
cg2c_dumpshutdown(Compile *ctxt)
{
	JSONPrinter *dumper = ctxt->dumper;

	jsonend(dumper);

	jsonend(dumper);

	assert(dumper->curnest == 0);
	fprintf(dumper->w, "\n");
}

static void
dumptype(JSONPrinter *dumper, const char *key, Type *type)
{
	jsonbegin(dumper, key, JsonMap);
	jsonprintf(dumper, "kind", "%s", cg2c_kindName[cg2c_typeKind(type)]);
	switch (cg2c_typeKind(type)) {
	case KindFunc: {
		jsonbegin(dumper, "elems", JsonList);
		dumptype(dumper, NULL, cg2c_funcParam(type));
		dumptype(dumper, NULL, cg2c_funcResult(type));
		jsonend(dumper);

		break;
	}

	case KindTuple: {
		jsonbegin(dumper, "elems", JsonList);
		for (size_t i = 0; i < cg2c_tupleNElem(type); i++)
			dumptype(dumper, NULL, cg2c_tupleElem(type, i));
		jsonend(dumper);

		break;
	}

	default:
		break;
	}
	jsonend(dumper);
}

static void
dumpvalue(Compile *ctxt, Value *v, void *etc)
{
	// TODO: isolate this into its own function so that we don't
	// accidentally use ctxt

	JSONPrinter *dumper = etc;

	jsonbegin(dumper, NULL, JsonMap);

	jsonprintf(dumper, "id", "%p", v);
	jsonprintf(dumper, "op", "%s", cg2c_opInfo[v->op].name);

	dumptype(dumper, "type", v->type);

	switch (v->op) {
	case OpConst: {
		jsonprintf(dumper, "auxInt", "%llu", (long long)*(int64_t*)v->aux);
		break;
	}

	case OpFunc: {
		jsonprintf(dumper, "aux", "%p", v->aux);
		break;
	}

	case OpExtract: {
		jsonprintf(dumper, "auxInt", "%llu", (unsigned long long)*(size_t*)v->aux);
		break;
	}

	default:
		break;
	}

	jsonbegin(dumper, "args", JsonList);
	for (size_t k = 0; k < cg2c_valueNArg(v); k++)
		jsonprintf(dumper, NULL, "%p", cg2c_valueArg(v, k));
	jsonend(dumper);

	PosBase hmm = {};
	Pos pos = v->pos;
	if (pos.base == NULL)
		pos.base = &hmm;
	jsonbegin(dumper, "pos", JsonMap);
	jsonprintf(dumper, "filename", "%.*s", cg2c_strlen2(pos.base->filename), cg2c_strdata(pos.base->filename));
	jsonputint(dumper, "line", pos.line);
	jsonputint(dumper, "col", pos.col);
	jsonend(dumper);

	if (v->trace != NULL)
		jsonprintf(dumper, "trace", "%p", v->trace);

	jsonend(dumper);
}

void
cg2c_dump3(Compile *ctxt, const char *pass, Sym *s)
{
	JSONPrinter *dumper = ctxt->dumper;

	jsonbegin(dumper, NULL, JsonMap);

	jsonprintf(dumper, "symbol", "%.*s", cg2c_strlen2(s->name), cg2c_strdata(s->name));
	jsonprintf(dumper, "pass", "%s", pass);

	jsonbegin(dumper, "values", JsonList);
	// TODO: use our own computeScope that tolerates invalid terminators,
	// etc
	Scope scope = cg2c_computeScope(s->func, ctxt->gc);
	cg2c_scopeForeach(ctxt, dumpvalue, dumper, &scope);
	jsonend(dumper);

	jsonbegin(dumper, "blocks", JsonList);
	for (size_t i = 0; i < cg2c_sllen(scope.funcs); i++) {
		Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope.funcs, i);

		jsonbegin(dumper, NULL, JsonMap);

		jsonprintf(dumper, "id", "%p", f);

		dumptype(dumper, "type", f->type);

		jsonbegin(dumper, "values", JsonList);
		for (size_t j = 0; j < cg2c_sllen(f->values); j++) {
			Value *v = *(Value**)cg2c_slindex(sizeof(Value*), f->values, j);
			jsonprintf(dumper, NULL, "%p", v);
		}
		jsonend(dumper);

		jsonend(dumper);
	}
	jsonend(dumper);

	jsonend(dumper);

	// Flush at the end
	fflush(dumper->w);
}
