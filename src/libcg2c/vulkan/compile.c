#include "all.h"

#include "compile.h"

#include <spirv/unified1/spirv.h>
#include <spirv/unified1/GLSL.std.450.h>

#include <vulkan/vulkan_core.h>

#include <cg2c/cg2c_vulkan.h>

// TODO: transition to int8 scratch

// TODO: naming
//
// emit* bits renamed to gen* or visit* or just remove prefix at all
//
// things that write into Asm be renamed to asm, put, asmput, asmPut or just
// write

// TODO: we have these copy-pasted all over the place, move them into all.h?

static Value*
newValue0I(Builder b, Op op, Type *type, uint64_t i)
{
	return cg2c_buildValue(b, op, type, &i, NULL_SLICE);
}

static Value*
newValue1(Builder b, Op op, Type *type, Value *x)
{
	Value *args[] = {x};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *x, Value *y, Value *z)
{
	Value *args[] = {x, y, z};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

enum {
	SectionXxx,
	SectionEntryPoints,
	SectionExecModes,
	SectionDebug,
	SectionAnnot,
	SectionTypes,
	SectionUserConsts,
	SectionCode,
	SectionLast,
};

// TODO: rename this to something like InsnBuf, InstrBuf, WordsBuf
typedef struct Asm Asm;
struct Asm {
	Slice buf;
	Pos   pos;
	GC    *gc;
};

// TODO: variants for result type + result id, just result id (these arise when
// e.g. defining types) and others

static void asInsn(Asm *as, SpvOp op, Slice args);
static void asInsn0(Asm *as, SpvOp op);
static void asInsn1(Asm *as, SpvOp op, uint32_t a0);
static void asInsn2(Asm *as, SpvOp op, uint32_t a0, uint32_t a1);
static void asInsn3(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2);
static void asInsn4(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3);
static void asInsn5(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4);
static void asInsn6(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5);

// TODO: we just need it for image types, make a dedicated function for that purpose
static void asInsn8(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5, uint32_t a6, uint32_t a7);

// TODO: make these take the id rather than allocate?

static SpvId
asInsnR(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc, Slice args)
{
	SpvId id = (SpvId)++*idAlloc;
	Slice firsttwo = cg2c_mkslicenocopy(sizeof(uint32_t), (uint32_t[]) {resultType, id}, 2);
	Slice realArgs = cg2c_slcat(sizeof(uint32_t), firsttwo, args, as->gc);
	asInsn(as, op, realArgs);
	return id;
}

static SpvId
asInsn0R(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc)
{
	SpvId id = (SpvId)++*idAlloc;
	asInsn2(as, op, (uint32_t)resultType, id);
	return id;
}

static SpvId
asInsn1R(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc, uint32_t a0)
{
	SpvId id = (SpvId)++*idAlloc;
	asInsn3(as, op, (uint32_t)resultType, id, a0);
	return id;
}

static SpvId
asInsn2R(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc, uint32_t a0, uint32_t a1)
{
	SpvId id = (SpvId)++*idAlloc;
	asInsn4(as, op, (uint32_t)resultType, id, a0, a1);
	return id;
}

static SpvId
asInsn3R(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc, uint32_t a0, uint32_t a1, uint32_t a2)
{
	SpvId id = (SpvId)++*idAlloc;
	asInsn5(as, op, (uint32_t)resultType, id, a0, a1, a2);
	return id;
}

static SpvId
asInsn4R(Asm *as, SpvOp op, SpvId resultType, uint32_t *idAlloc, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3)
{
	SpvId id = (SpvId)++*idAlloc;
	asInsn6(as, op, (uint32_t)resultType, id, a0, a1, a2, a3);
	return id;
}

typedef struct CtxtVk CtxtVk;
struct CtxtVk {
	// TODO: Vulkan features we can use

	VkShaderStageFlagBits stage;

	// TODO: have a variable for each section, we don't use the fact that
	// these are ordered in many cases
	Asm sections[SectionLast];

	SpvId pushConstant;

	SpvId samplers;
	SpvId sampledImages[DimLast][2 /* arrayed */][2 /* multisampled */][3 /* float, int32, int64 */];
	SpvId storageImages[DimLast][2 /* arrayed */][2 /* multisampled */][3 /* float, int32, int64 */];

	SpvId sysvals[SVLast];

	SpvId scratch;

	SpvId outPosition;

	SpvId in[10];
	SpvId out[10];

	// Constants

	Map *consts; // uint32_t → SpvId, internal scalar constants

	// Types
	//
	// TODO: create types lazily

	// Basic types translated as-is
	SpvId types[KindLast];
	// Integer types translated to SPIR-V floating point types
	SpvId floatTypes[KindLast];

	// Maps storage type × class to a pointer to type in class
	//
	// BUG: this actually doesn't work that well, we need to take special
	// care of physical pointers
	Map *ptrs; // SpvId × SpvStorageClass → SpvId

	SpvId typeInt32x2;
	SpvId typeInt32x3;
	SpvId typeInt32x4;
	SpvId typeFloat32x2;
	SpvId typeFloat32x4;
	SpvId voidType;
	SpvId typeVoidFunction; // garbage

	SpvId ptrToSampler;
	SpvId samplerType;

	struct {
		SpvId base, ptrTo;
		SpvId ffs;
	} imageTypes2[DimLast][2 /* arrayed */][2 /* multisampled */][3 /* float, int32, int64 component */][2 /* sampling or load/store */];

	// SpvId accelerationStructure;

	// Misc

	SpvId glsl450;

	// Pointer serialization

	// TODO: maybe add a second void* → String map to record reasons why
	// some Value*s were left without an ID?
	Map     *tupleIds; // void* → Slice of SpvId
	Map     *ids; // void* → SpvId
	uint32_t lastId;

	GC *gc;
};

static SpvId nextId(CtxtVk *c) { return (SpvId)++c->lastId; }

static SpvId
getId(CtxtVk *c, const void *x)
{
	SpvId id = *(const SpvId*)cg2c_mapaccess1(cg2c_ptr_32_map, c->ids, &x);
	assert(id != 0);
	return id;
}

static void
setId(CtxtVk *c, const void *x, SpvId id)
{
	assert(!cg2c_mapcontains(cg2c_ptr_32_map, c->ids, &x));

	*(SpvId*)cg2c_mapassign(cg2c_ptr_32_map, c->ids, &x, c->gc) = id;
}

static SpvId ptrTo(CtxtVk *c, SpvId elem, SpvStorageClass class);
static SpvId arrayOf(CtxtVk *c, SpvId elem, uint32_t len);

static SpvId const32(CtxtVk *c, uint32_t val);

static bool
hassuffix(const char *s, const char *suffix)
{
	size_t m = strlen(s);
	size_t n = strlen(suffix);
	return m >= n && memcmp(s+m-n, suffix, n) == 0;
}

static Slice
strWords(Slice args, String s, GC *gc)
{
	for (size_t i = 0; i < cg2c_strlen(s);) {
		uint32_t a = 0;
		for (size_t j = 0; j < 4 && i < cg2c_strlen(s); j++, i++)
			a |= (uint32_t)(unsigned char)cg2c_strdata(s)[i] << (j * 8);
		args = cg2c_slappend(sizeof(uint32_t), args, &a, gc);
	}
	if (cg2c_strlen(s) % 4 == 0)
		args = cg2c_slappend(sizeof(uint32_t), args, &(uint32_t) {0}, gc);
	return args;
}

static SpvId
posBase(CtxtVk *c, PosBase *base)
{
	assert(base != NULL);

	SpvId id = *(const SpvId*)cg2c_mapaccess1(cg2c_ptr_32_map, c->ids, &base);
	if (id == 0) {
		id = nextId(c);

		Slice args = NULL_SLICE;
		args = cg2c_slappend(sizeof(uint32_t), args, &(uint32_t) {id}, c->gc);
		args = strWords(args, base->filename, c->gc);
		asInsn(&c->sections[SectionDebug], SpvOpString, args);

		// TODO: enable this once we register our source language ID
		// asInsn3(&c->sections[SectionDebug], SpvOpSource, 0x00326743, 1, id);

		*(SpvId*)cg2c_mapassign(cg2c_ptr_32_map, c->ids, &base, c->gc) = id;
	}
	return id;
}

SpvId
const32(CtxtVk *c, uint32_t val)
{
	bool ok;
	SpvId id = *(const SpvId*)cg2c_mapaccess2(cg2c_32_32_map, c->consts, &val, &ok);
	if (!ok) {
		id = asInsn1R(&c->sections[SectionTypes], SpvOpConstant, c->types[KindInt32], &c->lastId, val);
		*(SpvId*)cg2c_mapassign(cg2c_32_32_map, c->consts, &val, c->gc) = id;
	}
	return id;
}

static SpvId
imageType(CtxtVk *c, int comp, Dim dim)
{
	return 0;
}

// TODO: see if we can move ScratchLayout out of here

typedef struct ScratchLayout ScratchLayout;
struct ScratchLayout {
	Map      *offsets; // Value* → uint32_t scratch offsets for each var
	uint32_t size;     // total no. of scratch bytes
};

static uint32_t
roundUp32(uint32_t x, uint32_t y)
{
	assert(y > 0 && (y & (y - 1)) == 0);
	return (x + y - 1) & ~(y - 1);
}

static void
emulateScratch2(Compile *ctxt, Value *v, void *etc)
{
	ScratchLayout *es = etc;

	// TODO: maybe just collect list of vars, then sort by alignment or size
	// or some combination of those
	if (v->op == OpVar) {
		uint64_t size = cg2c_valueAuxUint(cg2c_valueArg(v, 0));
		uint64_t align = cg2c_valueAuxUint(cg2c_valueArg(v, 1));

		if (!cg2c_mapcontains(cg2c_ptr_32_map, es->offsets, &v)) {
			uint32_t off = roundUp32(es->size, (uint32_t)align);
			*(uint32_t*)cg2c_mapassign(cg2c_ptr_32_map, es->offsets, &v, ctxt->gc) = off;
			es->size = off + (uint32_t)size;
		}
	}
}

static ScratchLayout
emulateScratch(Compile *ctxt, Scope *scope)
{
	ScratchLayout es = {
		.offsets = cg2c_mkmap(cg2c_ptr_32_map, ctxt->gc),
		.size    = 0,
	};

	cg2c_scopeForeach(ctxt, emulateScratch2, &es, scope);

	return es;
}

static SpvId
emitOp1(CtxtVk *c, SpvOp op, SpvId type, SpvId x)
{
	return asInsn1R(&c->sections[SectionCode], op, type, &c->lastId, x);
}

static SpvId
emitOp2(CtxtVk *c, SpvOp op, SpvId type, SpvId x, SpvId y)
{
	return asInsn2R(&c->sections[SectionCode], op, type, &c->lastId, x, y);
}

static SpvId
translateOp2(CtxtVk *c, SpvOp op, Value *v)
{
	SpvId type = c->types[cg2c_typeKind(v->type)];
	SpvId x = getId(c, cg2c_valueArg(v, 0));
	SpvId y = getId(c, cg2c_valueArg(v, 1));
	return emitOp2(c, op, type, x, y);
}

static SpvOp intAndFloatOps[OpLast] = {
	[OpAdd]  = SpvOpIAdd,
	[OpFAdd] = SpvOpFAdd,
	[OpSub]  = SpvOpISub,
	[OpFSub] = SpvOpFSub,
	[OpMul]  = SpvOpIMul,
	[OpFMul] = SpvOpFMul,
	[OpFDiv] = SpvOpFDiv,
	[OpDiv]  = SpvOpSDiv,
	[OpUDiv] = SpvOpUDiv,
	[OpRem]  = SpvOpSRem, // sign(a % b) == sign(a)
	[OpURem] = SpvOpUMod,
	[OpAnd]  = SpvOpBitwiseAnd,
	[OpOr]   = SpvOpBitwiseOr,
	[OpXor]  = SpvOpBitwiseXor,
	[OpLsh]  = SpvOpShiftLeftLogical,
	[OpRsh]  = SpvOpShiftRightArithmetic,
	[OpURsh] = SpvOpShiftRightLogical,
	[OpEq]   = SpvOpIEqual,
	[OpEqF]  = SpvOpFOrdEqual,
	[OpNeq]  = SpvOpINotEqual,
	[OpNeqF] = SpvOpFUnordNotEqual,
	[OpLt]   = SpvOpSLessThan,
	[OpLtU]  = SpvOpULessThan,
	[OpLtF]  = SpvOpFOrdLessThan,
	[OpLte]  = SpvOpSLessThanEqual,
	[OpLteU] = SpvOpULessThanEqual,
	[OpLteF] = SpvOpFOrdLessThanEqual,
	[OpNeg]  = SpvOpSNegate,
	[OpFNeg] = SpvOpFNegate,
	[OpSignExt] = SpvOpSConvert,
	[OpZeroExt] = SpvOpUConvert,
	[OpIToF] = SpvOpConvertSToF,
	[OpUToF] = SpvOpConvertUToF,
	[OpFToI] = SpvOpConvertFToS,
	[OpFToU] = SpvOpConvertFToU,
	[OpFToF] = SpvOpFConvert,
	// [OpBitrev] = SpvOpBitReverse,
};

static SpvOp boolOps[OpLast] = {
	[OpAnd] = SpvOpLogicalAnd,
	[OpOr]  = SpvOpLogicalOr,
	[OpEq]  = SpvOpLogicalEqual,
	[OpNeq] = SpvOpLogicalNotEqual,
};

static uint32_t glsl450Ops[OpLast] = {
	[OpFMA]       = GLSLstd450Fma,
	[OpSqrt]      = GLSLstd450Sqrt,
	[OpFloor]     = GLSLstd450Floor,
	[OpCeil]      = GLSLstd450Ceil,
	[OpRoundEven] = GLSLstd450RoundEven,
	[OpFMin]      = GLSLstd450NMin,
	[OpFMax]      = GLSLstd450NMax,
	[OpFAbs]      = GLSLstd450FAbs,
	[OpPowr]      = GLSLstd450Pow,
	[OpSin]       = GLSLstd450Sin,
	[OpCos]       = GLSLstd450Cos,
	[OpTanh]      = GLSLstd450Tanh,
	[OpExp2]      = GLSLstd450Exp2,
	[OpLog2]      = GLSLstd450Log2,
};

/*
static SpvOp groupOps[OpLast] = {
	[OpAdd]  = SpvOpGroupNonUniformIAdd,
	[OpFAdd] = SpvOpGroupNonUniformFAdd,
	[OpMul]  = SpvOpGroupNonUniformIMul,
	[OpFMul] = SpvOpGroupNonUniformFMul,
};
*/

static void
genNonFpOp2(CtxtVk *ctxt, Value *v)
{
	// SpvId v_id = nextId(ctxt);
	SpvId v_0_id = getId(ctxt, cg2c_valueArg(v, 0));
	SpvId v_1_id = getId(ctxt, cg2c_valueArg(v, 1));

	assert(intAndFloatOps[v->op] != 0);

	SpvId v_id = asInsn2R(&ctxt->sections[SectionCode], intAndFloatOps[v->op], ctxt->types[cg2c_typeKind(v->type)], &ctxt->lastId, v_0_id, v_1_id);

	setId(ctxt, v, v_id);
}

static void
genBoolOp2(CtxtVk *ctxt, Value *v)
{
	// SpvId v_id = nextId(ctxt);
	SpvId v_0_id = getId(ctxt, cg2c_valueArg(v, 0));
	SpvId v_1_id = getId(ctxt, cg2c_valueArg(v, 1));

	// fprintf(stderr, "%s %d %d\n", cg2c_opInfo[v->op].name, cg2c_typeKind(cg2c_valueArg(v, 0)->type), cg2c_typeKind(cg2c_valueArg(v, 1)->type));

	assert(boolOps[v->op] != 0);

	SpvId v_id = asInsn2R(&ctxt->sections[SectionCode], boolOps[v->op], ctxt->types[cg2c_typeKind(v->type)], &ctxt->lastId, v_0_id, v_1_id);

	setId(ctxt, v, v_id);
}

static void
genFloatOp2(CtxtVk *ctxt, Value *v)
{
	Value *v_0 = cg2c_valueArg(v, 0);
	Value *v_1 = cg2c_valueArg(v, 1);

	assert(intAndFloatOps[v->op] != 0);

	SpvId v_0_id = asInsn1R(&ctxt->sections[SectionCode], SpvOpBitcast, ctxt->floatTypes[cg2c_typeKind(v_0->type)], &ctxt->lastId, getId(ctxt, v_0));
	SpvId v_1_id = asInsn1R(&ctxt->sections[SectionCode], SpvOpBitcast, ctxt->floatTypes[cg2c_typeKind(v_1->type)], &ctxt->lastId, getId(ctxt, v_1));
	SpvId tmp_id = asInsn2R(&ctxt->sections[SectionCode], intAndFloatOps[v->op], ctxt->floatTypes[cg2c_typeKind(v->type)], &ctxt->lastId, v_0_id, v_1_id);
	SpvId v_id = asInsn1R(&ctxt->sections[SectionCode], SpvOpBitcast, ctxt->types[cg2c_typeKind(v->type)], &ctxt->lastId, tmp_id);

	setId(ctxt, v, v_id);
}

static void
genFloatCmpOp2(CtxtVk *ctxt, Value *v)
{
	Value *v_0 = cg2c_valueArg(v, 0);
	Value *v_1 = cg2c_valueArg(v, 1);

	assert(intAndFloatOps[v->op] != 0);

	SpvId v_0_id = asInsn1R(&ctxt->sections[SectionCode], SpvOpBitcast, ctxt->floatTypes[cg2c_typeKind(v_0->type)], &ctxt->lastId, getId(ctxt, v_0));
	SpvId v_1_id = asInsn1R(&ctxt->sections[SectionCode], SpvOpBitcast, ctxt->floatTypes[cg2c_typeKind(v_1->type)], &ctxt->lastId, getId(ctxt, v_1));
	SpvId v_id = asInsn2R(&ctxt->sections[SectionCode], intAndFloatOps[v->op], ctxt->types[KindBool], &ctxt->lastId, v_0_id, v_1_id);

	setId(ctxt, v, v_id);
}

// TODO: all these functions should define their own ids

static SpvId genLoad(CtxtVk *c, Value *v, ScratchLayout *es);

static SpvId
genLoadGlobal(CtxtVk *c, Type *t, SpvId p, uint32_t align)
{
	switch (cg2c_typeKind(t)) {
	case KindInt8:
	case KindInt16:
	case KindInt32:
	case KindInt64:
	case KindPtr: {
		SpvId p_type = ptrTo(c, c->types[cg2c_typeKind(t)], SpvStorageClassPhysicalStorageBuffer);
		SpvId p_typed = emitOp1(c, SpvOpBitcast, p_type, p);
		return asInsn3R(&c->sections[SectionCode], SpvOpLoad, c->types[cg2c_typeKind(t)], &c->lastId, p_typed, SpvMemoryAccessAlignedMask|SpvMemoryAccessNonPrivatePointerMask, align);
	}

	default:
		assert(!"can't load a value of this type");
		return 0;
	}
}

static SpvId
genLoadScratch(CtxtVk *c, Type *t, SpvId p, ScratchLayout *es)
{
	SpvId ptrToI32Scratch = ptrTo(c, c->types[KindInt32], SpvStorageClassPrivate);

	switch (cg2c_typeKind(t)) {
	case KindInt32: {
		SpvId pDivBy4 = emitOp2(c, SpvOpUDiv, c->types[KindInt32], p, const32(c, 4));
		SpvId ptr = asInsn2R(&c->sections[SectionCode], SpvOpAccessChain, ptrToI32Scratch, &c->lastId, c->scratch, pDivBy4);
		SpvId id = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->types[KindInt32], &c->lastId, ptr);
		return id;
	}

	case KindInt64:
	case KindPtr: {
		SpvId parts[2] = {};

		for (uint32_t i = 0; i < 2; i++) {
			SpvId scratchAddr = emitOp2(c, SpvOpIAdd, c->types[KindInt32], p, const32(c, i*4));
			SpvId divBy4 = emitOp2(c, SpvOpUDiv, c->types[KindInt32], scratchAddr, const32(c, 4));
			SpvId ptr = asInsn2R(&c->sections[SectionCode], SpvOpAccessChain, ptrToI32Scratch, &c->lastId, c->scratch, divBy4);
			parts[i] = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->types[KindInt32], &c->lastId, ptr);
		}

		SpvId tmp = emitOp2(c, SpvOpCompositeConstruct, c->typeInt32x2, parts[0], parts[1]);
		if (cg2c_typeKind(t) == KindInt64)
			tmp = emitOp1(c, SpvOpBitcast, c->types[KindInt64], tmp);
		if (cg2c_typeKind(t) == KindPtr)
			tmp = emitOp1(c, SpvOpBitcast, c->types[KindPtr], tmp);
		return tmp;
	}

	default:
		assert(0);
		return 0;
	}
}

static SpvId
genLoad(CtxtVk *c, Value *v, ScratchLayout *es)
{
	SpvId zero = const32(c, 0);

	uint32_t align = (uint32_t)*(int64_t*)v->aux;

	Value *p = cg2c_valueArg(v, 1);

	SpvId p_id = getId(c, p);

	Type *valueType = v->type;

	SpvId p_uvec2 = emitOp1(c, SpvOpBitcast, c->typeInt32x2, p_id);

	SpvId p_base_uvec2 = emitOp1(c, SpvOpBitcast, c->typeInt32x2, getId(c, cg2c_ptrchase(p)));
	SpvId p_base_hi = emitOp2(c, SpvOpCompositeExtract, c->types[KindInt32], p_base_uvec2, 1);

	// TODO: change this to compare tag, in accordance with
	// the way we implement generic addressing

	SpvId isScratch = emitOp2(c, SpvOpSLessThan, c->types[KindBool], p_base_hi, zero);

	SpvId bbGlobal = nextId(c);
	SpvId bbScratch = nextId(c);
	SpvId bbMerge = nextId(c);

	asInsn2(&c->sections[SectionCode], SpvOpSelectionMerge, bbMerge, 0 /* control */);
	asInsn3(&c->sections[SectionCode], SpvOpBranchConditional, isScratch, bbScratch, bbGlobal);

	asInsn1(&c->sections[SectionCode], SpvOpLabel, bbGlobal);

	SpvId globalResult = genLoadGlobal(c, valueType, p_id, align);

	asInsn1(&c->sections[SectionCode], SpvOpBranch, bbMerge);

	asInsn1(&c->sections[SectionCode], SpvOpLabel, bbScratch);

	SpvId p_scratch = emitOp2(c, SpvOpCompositeExtract, c->types[KindInt32], p_uvec2, 0);

	SpvId scratchResult = genLoadScratch(c, valueType, p_scratch, es);

	asInsn1(&c->sections[SectionCode], SpvOpBranch, bbMerge);

	asInsn1(&c->sections[SectionCode], SpvOpLabel, bbMerge);

	uint32_t phi[] = {
		globalResult, bbGlobal,
		scratchResult, bbScratch,
	};
	return asInsnR(&c->sections[SectionCode], SpvOpPhi, c->types[cg2c_typeKind(valueType)], &c->lastId, cg2c_mkslicenocopy(sizeof phi[0], phi, nelem(phi)));
}

static void
genStoreGlobal(CtxtVk *c, SpvId p, Type *t, SpvId v, uint32_t align)
{
	switch (cg2c_typeKind(t)) {
	case KindInt8: case KindInt16: case KindInt32: case KindInt64: case KindPtr: {
		SpvId p_type = ptrTo(c, c->types[cg2c_typeKind(t)], SpvStorageClassPhysicalStorageBuffer);
		SpvId p_typed = emitOp1(c, SpvOpBitcast, p_type, p);

		asInsn4(&c->sections[SectionCode], SpvOpStore, p_typed, v, SpvMemoryAccessAlignedMask|SpvMemoryAccessNonPrivatePointerMask, align);

		break;
	}

	default:
		assert(!"global store not implemented for this type");
	}
}

static void
genStoreScratch(CtxtVk *c, SpvId p, Type *t, SpvId v, ScratchLayout *es)
{
	SpvId ptrToI32Scratch = ptrTo(c, c->types[KindInt32], SpvStorageClassPrivate);

	switch (cg2c_typeKind(t)) {
	case KindInt32: {
		SpvId pDivBy4 = emitOp2(c, SpvOpUDiv, c->types[KindInt32], p, const32(c, 4));
		SpvId scratchSlot = emitOp2(c, SpvOpAccessChain, ptrToI32Scratch, c->scratch, pDivBy4);
		asInsn2(&c->sections[SectionCode], SpvOpStore, scratchSlot, v);
		break;
	}

	default:
		assert(!"scratch store not implemented for this type");
	}
}

static void
genAtomicStore(CtxtVk *c, Value *store)
{
	Value *ptr = cg2c_valueArg(store, 1);
	Value *val = cg2c_valueArg(store, 2);

	SpvId ptrType = ptrTo(c, c->types[cg2c_typeKind(val->type)], SpvStorageClassPhysicalStorageBuffer);
	SpvId ptrTyped = emitOp1(c, SpvOpBitcast, ptrType, getId(c, ptr));

	SpvId scope = const32(c, SpvScopeDevice);

	// BUG: is SubgroupMemory necessary?
	//
	// BUG: is OutputMemory necessary?
	//
	// BUG: is Volatile necessary for atomic accesses?
	SpvId semantics = const32(c,
		SpvMemorySemanticsReleaseMask |
		SpvMemorySemanticsUniformMemoryMask |
		SpvMemorySemanticsSubgroupMemoryMask |
		SpvMemorySemanticsWorkgroupMemoryMask |
		SpvMemorySemanticsImageMemoryMask |
		SpvMemorySemanticsOutputMemoryMask |
		SpvMemorySemanticsMakeAvailableMask |
		SpvMemorySemanticsVolatileMask);

	asInsn4(&c->sections[SectionCode], SpvOpAtomicStore, ptrTyped, scope, semantics, getId(c, val));
}

static void
genImageStore(CtxtVk *c, Value *store)
{
	AuxImage *aux = store->aux;
	Value *image = cg2c_valueArg(store, 1);
	Value *xyz = cg2c_valueArg(store, 2);
	assert(cg2c_typeKind(xyz->type) == KindTuple);
	Value *rgba = cg2c_valueArg(store, 3);
	assert(cg2c_typeKind(rgba->type) == KindTuple);

	SpvId gep = asInsn2R(&c->sections[SectionCode], SpvOpAccessChain,
		c->imageTypes2[aux->dim][aux->array][aux->ms][0][1].ptrTo, &c->lastId,
		c->storageImages[aux->dim][aux->array][aux->ms][0], getId(c, image));
	SpvId descriptor = asInsn1R(&c->sections[SectionCode], SpvOpLoad,
		c->imageTypes2[aux->dim][aux->array][aux->ms][0][1].base, &c->lastId, gep);
	asInsn2(&c->sections[SectionAnnot], SpvOpDecorate, descriptor, SpvDecorationNonUniform);

	SpvId coord = asInsn2R(&c->sections[SectionCode], SpvOpCompositeConstruct, c->typeInt32x2, &c->lastId, getId(c, xyz)+0, getId(c, xyz)+1);
	SpvId tmp = asInsn4R(&c->sections[SectionCode], SpvOpCompositeConstruct, c->typeInt32x4, &c->lastId, getId(c, rgba)+0, getId(c, rgba)+1, getId(c, rgba)+2, getId(c, rgba)+3);
	SpvId texel = emitOp1(c, SpvOpBitcast, c->typeFloat32x4, tmp);

	asInsn3(&c->sections[SectionCode], SpvOpImageWrite, descriptor, coord, texel);
}

static void
genTex(CtxtVk *c, Value *tex)
{
	Value *image = cg2c_valueArg(tex, 1);
	Value *sampler = cg2c_valueArg(tex, 2);

	Value *xyz = cg2c_valueArg(tex, 3);
	assert(cg2c_typeKind(xyz->type) == KindTuple);

	// ???
	AuxImage *aux = &(AuxImage) {
		.dim = Dim2D,
		.array = 0,
		.ms = 0,
	};

	SpvId imagePtr = asInsn2R(&c->sections[SectionCode], SpvOpAccessChain,
		c->imageTypes2[aux->dim][aux->array][aux->ms][0][0].ptrTo, &c->lastId,
		c->sampledImages[aux->dim][aux->array][aux->ms][0], getId(c, image));
	SpvId imageDesc = asInsn1R(&c->sections[SectionCode], SpvOpLoad,
		c->imageTypes2[aux->dim][aux->array][aux->ms][0][0].base, &c->lastId, imagePtr);

	SpvId samplerPtr = asInsn2R(&c->sections[SectionCode], SpvOpAccessChain,
		c->ptrToSampler, &c->lastId,
		c->samplers, getId(c, sampler));
	SpvId samplerDesc = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->samplerType, &c->lastId, samplerPtr);

	SpvId combined = asInsn2R(&c->sections[SectionCode], SpvOpSampledImage,
		c->imageTypes2[aux->dim][aux->array][aux->ms][0][0].ffs, &c->lastId, imageDesc, samplerDesc);
	asInsn2(&c->sections[SectionAnnot], SpvOpDecorate, combined, SpvDecorationNonUniform);

	SpvId coordTmp = asInsn2R(&c->sections[SectionCode], SpvOpCompositeConstruct, c->typeInt32x2, &c->lastId, getId(c, xyz)+0, getId(c, xyz)+1);
	SpvId coord = asInsn1R(&c->sections[SectionCode], SpvOpBitcast, c->typeFloat32x2, &c->lastId, coordTmp);

	SpvId result = asInsn2R(&c->sections[SectionCode], SpvOpImageSampleImplicitLod, c->typeFloat32x4, &c->lastId, combined, coord);

	SpvId id = nextId(c);

	for (size_t i = 1; i < cg2c_tupleNElem(tex->type); i++)
		nextId(c);

	for (size_t i = 0; i < cg2c_tupleNElem(tex->type); i++) {
		SpvId tmp = asInsn2R(&c->sections[SectionCode], SpvOpCompositeExtract, c->floatTypes[KindInt32], &c->lastId, result, (uint32_t)i);

		asInsn3(&c->sections[SectionCode], SpvOpBitcast, c->types[KindInt32], id+(SpvId)i, tmp);
	}

	setId(c, tex, id);
}

static void
emitLineInfo(CtxtVk *c, int section, Pos pos)
{
	if (pos.base != NULL)
		asInsn3(&c->sections[section], SpvOpLine, posBase(c, pos.base), pos.line, pos.col);
	else
		asInsn0(&c->sections[section], SpvOpNoLine);
}

static SpvId
translateOp(CtxtVk *c, Value *v, Map *preds, ScratchLayout *es)
{
	if (v->op == OpConst) {
		uint64_t val = cg2c_valueAuxUint(v);

		emitLineInfo(c, SectionUserConsts, v->pos);

		switch (cg2c_typeKind(v->type)) {
		case KindInt8:
		case KindInt16:
		case KindInt32:
			return asInsn1R(&c->sections[SectionUserConsts], SpvOpConstant, c->types[cg2c_typeKind(v->type)], &c->lastId, (uint32_t)val);

		case KindInt64:
			return asInsn2R(&c->sections[SectionUserConsts], SpvOpConstant, c->types[cg2c_typeKind(v->type)], &c->lastId, (uint32_t)val, (uint32_t)(val >> 32));

		default:
			assert(0);
		}

		return 0;
	}

	// TODO: don't emit it for instructions that don't need it
	emitLineInfo(c, SectionCode, v->pos);

	switch (v->op) {
	case OpAdd:
	case OpSub:
		genNonFpOp2(c, v);
		break;

	case OpMul:
		genNonFpOp2(c, v);
		break;

	case OpAddc:
	case OpSubb: {
		SpvId v_0_id = getId(c, cg2c_valueArg(v, 0));
		SpvId v_1_id = getId(c, cg2c_valueArg(v, 1));

		// SpvOpIAddCarry

		assert(0);
		break;
	}

	case OpDiv:
	case OpUDiv:
	case OpRem:
	case OpURem:
		genNonFpOp2(c, v);
		break;

	case OpFAdd:
	case OpFSub:
	case OpFMul:
		genFloatOp2(c, v);
		break;
	case OpFMA:
		assert(0);
		break;
	case OpFDiv:
		genFloatOp2(c, v);
		break;

	case OpAnd:
	case OpOr:
		if (cg2c_typeKind(v->type) != KindBool)
			genNonFpOp2(c, v);
		else
			genBoolOp2(c, v);
		break;
	case OpXor:
		genNonFpOp2(c, v);
		break;

	/*
	case OpBitrev:
		return emitOp1(c, intAndFloatOps[v->op], c->types[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
	*/

	case OpLsh:
	case OpRsh:
	case OpURsh:
		genNonFpOp2(c, v);
		break;

	case OpEq:
	case OpNeq:
		if (cg2c_typeKind(cg2c_valueArg(v, 0)->type) != KindBool)
			genNonFpOp2(c, v);
		else
			genBoolOp2(c, v);
		break;

	case OpLt:
	case OpLtU:
	case OpLte:
	case OpLteU:
		genNonFpOp2(c, v);
		break;

	case OpEqF:
	case OpNeqF:
	case OpLtF:
	case OpLteF:
		genFloatCmpOp2(c, v);
		break;

	case OpIf: {
		SpvId condid = getId(c, cg2c_valueArg(v, 0));
		SpvId xid = getId(c, cg2c_valueArg(v, 1));
		SpvId yid = getId(c, cg2c_valueArg(v, 2));

		return asInsn3R(&c->sections[SectionCode], SpvOpSelect, c->types[cg2c_typeKind(v->type)], &c->lastId, condid, xid, yid);
	}

	case OpNeg:
		return emitOp1(c, intAndFloatOps[v->op], c->types[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));

	case OpFNeg: {
		SpvId xF = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
		SpvId negated = emitOp1(c, intAndFloatOps[v->op], c->floatTypes[cg2c_typeKind(v->type)], xF);
		return emitOp1(c, SpvOpBitcast, c->types[cg2c_typeKind(v->type)], negated);
	}

	case OpNot: {
		SpvId xid = getId(c, cg2c_valueArg(v, 0));

		return emitOp1(c, cg2c_typeKind(v->type) == KindBool ? SpvOpLogicalNot : SpvOpNot, c->types[cg2c_typeKind(v->type)], xid);
	}

	case OpSqrt:
	case OpSin:
	case OpCos:
	case OpTanh:
	case OpExp2:
	case OpLog2: {
		SpvId xF = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
		SpvId sine = asInsn3R(&c->sections[SectionCode], SpvOpExtInst, c->floatTypes[cg2c_typeKind(v->type)], &c->lastId, c->glsl450, glsl450Ops[v->op], xF);
		return emitOp1(c, SpvOpBitcast, c->types[cg2c_typeKind(v->type)], sine);
	}

	case OpPowr: {
		SpvId xF = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
		SpvId yF = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 1)));
		SpvId sine = asInsn4R(&c->sections[SectionCode], SpvOpExtInst, c->floatTypes[cg2c_typeKind(v->type)], &c->lastId, c->glsl450, glsl450Ops[v->op], xF, yF);
		return emitOp1(c, SpvOpBitcast, c->types[cg2c_typeKind(v->type)], sine);
	}

	case OpParam: {
		// Tuple parameters should've been flattened

		// OpPhi or OpFunctionParam

		SpvId id = nextId(c);

		Func *f = cg2c_valueArgFunc(v, 0);

		Slice incs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, preds, &f);

		// This is a function entry. TODO: check this in another way,
		// and handle function entries appropriately
		if (cg2c_sllen(incs) == 0)
			break;

		// TODO: make OpParam lowering more general
		if (cg2c_typeKind(v->type) != KindTuple) {
			assert(cg2c_typeKind(v->type) == KindMem);
			break;
		}

		for (size_t i = 1; i < cg2c_tupleNElem(v->type); i++)
			nextId(c);

		for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
			Type *et = cg2c_tupleElem(v->type, i);

			switch (cg2c_typeKind(et)) {
			case KindBool: case KindInt8: case KindInt16: case KindInt32: case KindInt64: case KindPtr: {
				Slice phi = NULL_SLICE;
				phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {c->types[cg2c_typeKind(et)]}, c->gc);
				phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {id+(SpvId)i}, c->gc);
				for (size_t j = 0; j < cg2c_sllen(incs); j++) {
					Incoming inc = *(const Incoming*)cg2c_slindex(sizeof(Incoming), incs, j);
					assert(inc.v->op == OpMakeTuple);
					phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {getId(c, inc.v)+(SpvId)i}, c->gc);
					phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {getId(c, inc.f)}, c->gc);
				}
				asInsn(&c->sections[SectionCode], SpvOpPhi, phi);
				break;
			}

			case KindMem:
				// do nothing
				break;

			case KindTuple:
				// no nested tuples

			default:
				assert(0);
			}
		}

		return id;

		break;
	}

	case OpPhi: {
		assert(0);

		SpvId id = nextId(c);

		Slice phi = {};
		phi = cg2c_slappend(sizeof(uint32_t), phi, &c->types[cg2c_typeKind(v->type)], c->gc);
		phi = cg2c_slappend(sizeof(uint32_t), phi, &id, c->gc);

		assert(cg2c_sllen(v->args) > 0);
		for (size_t i = 1; i < cg2c_sllen(v->args); i += 2) {
			Value *a = cg2c_valueArg(v, i+0);
			Func *pred = cg2c_valueArgFunc(v, i+1);
			// assert(cg2c_typeKind(a->type) != KindFunc);

			phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {getId(c, a)}, c->gc);
			phi = cg2c_slappend(sizeof(uint32_t), phi, &(uint32_t) {getId(c, pred)}, c->gc);
		}

		asInsn(&c->sections[SectionCode], SpvOpPhi, phi);

		return id;
	}

	case OpConst: {
		assert(0);

		break;
	}

	case OpFunc: {
		// Handled in emitFuncSym
		break;
	}

	case OpSignExt:
	case OpZeroExt: {
		// NOTE: SPIR-V disallows useless SConvert and UConvert. We rely
		// on a rewrite rule to get rid of those.
		return emitOp1(c, intAndFloatOps[v->op], c->types[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
	}

	case OpIToF:
	case OpUToF: {
		SpvId tmp = emitOp1(c, intAndFloatOps[v->op], c->floatTypes[cg2c_typeKind(v->type)], getId(c, cg2c_valueArg(v, 0)));
		return emitOp1(c, SpvOpBitcast, c->types[cg2c_typeKind(v->type)], tmp);
	}

	case OpFToI:
	case OpFToU: {
		Value *v_0 = cg2c_valueArg(v, 0);
		SpvId tmp = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v_0->type)], getId(c, v_0));
		return emitOp1(c, intAndFloatOps[v->op], c->types[cg2c_typeKind(v->type)], tmp);
	}

	case OpFToF: {
		Value *v_0 = cg2c_valueArg(v, 0);
		SpvId tmp = emitOp1(c, SpvOpBitcast, c->floatTypes[cg2c_typeKind(v_0->type)], getId(c, v_0));
		SpvId tmp2 = emitOp1(c, intAndFloatOps[v->op], c->floatTypes[cg2c_typeKind(v->type)], tmp);
		return emitOp1(c, SpvOpBitcast, c->types[cg2c_typeKind(v->type)], tmp);
	}

	case OpPtrToInt: {
		Value *x = cg2c_valueArg(v, 0);
		return emitOp1(c, SpvOpBitcast, c->types[KindInt64], getId(c, x));
	}

	case OpIntToPtr: {
		Value *x = cg2c_valueArg(v, 0);
		return emitOp1(c, SpvOpBitcast, c->types[KindPtr], getId(c, x));
	}

	case OpMakeTuple: {
		SpvId id = nextId(c);

		for (size_t i = 1; i < cg2c_tupleNElem(v->type); i++)
			nextId(c);

		for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
			Value *v_i = cg2c_valueArg(v, i);
			if (cg2c_typeKind(v_i->type) == KindMem)
				continue;
			asInsn3(&c->sections[SectionCode], SpvOpCopyObject, c->types[cg2c_typeKind(v_i->type)], id+(uint32_t)i, getId(c, v_i));
		}

		return id;
	}

	case OpExtract: {
		if (cg2c_typeKind(v->type) == KindMem)
			break;

		return getId(c, cg2c_valueArg(v, 0)) + (uint32_t)*(uint64_t*)v->aux;
	}

	case OpVar: {
		// TODO: maybe arrange bits of emulate scratch to be done here

		bool ok;
		uint32_t off = *(const uint32_t*)cg2c_mapaccess2(cg2c_ptr_32_map, es->offsets, &v, &ok);
		assert(ok);

		SpvId lo = const32(c, off);
		SpvId hi = const32(c, (uint32_t)1 << 31);
		SpvId addr = emitOp2(c, SpvOpCompositeConstruct, c->typeInt32x2, lo, hi);
		return emitOp1(c, SpvOpBitcast, c->types[KindPtr], addr);
	}

	case OpLoad:
		return genLoad(c, v, es);

	case OpAtomicStore:
		genAtomicStore(c, v);
		break;

	case OpTex:
		genTex(c, v);
		break;

	case OpImageStore:
		genImageStore(c, v);
		break;

	case OpCall: {
		assert(cg2c_typeKind(v->type) == KindNoret);
		Value *v_0 = cg2c_valueArg(v, 0);
		if (v_0->op == OpRet) {
			asInsn0(&c->sections[SectionCode], SpvOpReturn);
			break;
		}
		if (v_0->op == OpFunc) {
			asInsn1(&c->sections[SectionCode], SpvOpBranch, getId(c, v_0->aux));
			break;
		}
		assert(0);
		break;
	}

	case OpCallStructuredIf: {
		Value *cond = cg2c_valueArg(v, 0);
		assert(cg2c_typeKind(cond->type) == KindBool);
		Func *then = cg2c_valueArgFunc(v, 1);
		Func *else_ = cg2c_valueArgFunc(v, 2);
		Func *merge = cg2c_valueArgFunc(v, 3);

		asInsn2(&c->sections[SectionCode], SpvOpSelectionMerge, getId(c, merge), 0); // control
		asInsn3(&c->sections[SectionCode], SpvOpBranchConditional, getId(c, cond), getId(c, then), getId(c, else_));

		break;
	}

	case OpCallc2: {
		assert(0);

		break;
	}

	case OpRet:
		break;

	case OpGetPushConstant: {
		uint32_t off = (uint32_t)cg2c_valueAuxUint(cg2c_valueArg(v, 0));

		assert(off % 4 == 0); // TODO: raise this restriction

		switch (cg2c_typeKind(v->type)) {
		case KindInt32: {
			SpvId zero = const32(c, 0);
			SpvId pushConstantWord = const32(c, off/4);

			SpvId ptrToI32PushConstant = ptrTo(c, c->types[KindInt32], SpvStorageClassPushConstant);

			SpvId ptr = asInsn3R(&c->sections[SectionCode], SpvOpAccessChain, ptrToI32PushConstant, &c->lastId, c->pushConstant, zero, pushConstantWord);

			return asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->types[KindInt32], &c->lastId, ptr);
		}

		default:
			assert(0);
		}

		break;
	}

	case OpLoadSysval:
		// fallthrough for now
		//
		// BUG: proper implementation would emit Volatile-qualified OpLoad

	case OpGetSysval: {
		switch (cg2c_valueAuxUint(cg2c_valueArg(v, 0))) {
		case SVVertexID: {
			return asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->types[KindInt32], &c->lastId, c->sysvals[SVVertexID]);
		}

		case SVFragCoord: {
			assert(0);
		}

		case SVGlobalInvocationID: {
			SpvId giid = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->typeInt32x3, &c->lastId, c->sysvals[SVGlobalInvocationID]);
			return asInsn2R(&c->sections[SectionCode], SpvOpCompositeExtract, c->types[KindInt32], &c->lastId, giid, 0);
		}

		case SVGlobalInvocationIDY: {
			SpvId giid = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->typeInt32x3, &c->lastId, c->sysvals[SVGlobalInvocationID]);
			return asInsn2R(&c->sections[SectionCode], SpvOpCompositeExtract, c->types[KindInt32], &c->lastId, giid, 1);
		}

		default:
			assert(0);
		}

		break;
	}

	case OpGetInput: {
		int64_t location = (int64_t)cg2c_valueAuxUint(cg2c_valueArg(v, 0));

		// This whole bit of code just stinks

		SpvId id = nextId(c);

		for (size_t i = 1; i < cg2c_tupleNElem(v->type); i++)
			nextId(c);

		// TODO: the instruction associates slot to load from, just use that
		SpvId val = asInsn1R(&c->sections[SectionCode], SpvOpLoad, c->typeFloat32x4, &c->lastId, c->in[location]);

		for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
			SpvId tmp = asInsn2R(&c->sections[SectionCode], SpvOpCompositeExtract, c->floatTypes[KindInt32], &c->lastId, val, (uint32_t)i);

			asInsn3(&c->sections[SectionCode], SpvOpBitcast, c->types[KindInt32], id+(SpvId)i, tmp);
		}

		return id;
	}

	case OpStoreOutput: {
		int64_t location = cg2c_valueAuxUint(cg2c_valueArg(v, 1));

		// TODO: separate instruction for fixed function outputs
		// (position, etc?)

		switch (location) {
		case -1: {
			assert(c->stage == VK_SHADER_STAGE_VERTEX_BIT);

			SpvId x = getId(c, cg2c_valueArg(v, 2));
			SpvId tmp = asInsn4R(&c->sections[SectionCode], SpvOpCompositeConstruct, c->typeInt32x4, &c->lastId, x+0, x+1, x+2, x+3);
			SpvId val = emitOp1(c, SpvOpBitcast, c->typeFloat32x4, tmp);

			asInsn2(&c->sections[SectionCode], SpvOpStore, c->outPosition, val);

			break;
		}

		// TODO: separate this pls?
		default: {
			assert(location >= 0);
			// TODO: maybe let's accept components as individual
			// arguments? tuple arguments are slightly annoying

			SpvId r = getId(c, cg2c_valueArg(v, 2));
			SpvId tmp = asInsn4R(&c->sections[SectionCode], SpvOpCompositeConstruct, c->typeInt32x4, &c->lastId, r+0, r+1, r+2, r+3);
			SpvId color = emitOp1(c, SpvOpBitcast, c->typeFloat32x4, tmp);

			asInsn2(&c->sections[SectionCode], SpvOpStore, c->out[0], color);

			break;
		}
		}

		break;
	}

	case OpStoreGlobal: {
		uint64_t align = (uint32_t)*(int64_t*)v->aux;

		Value *p = cg2c_valueArg(v, 1);
		Value *w = cg2c_valueArg(v, 2);

		SpvId pId = getId(c, p);
		SpvId wId = getId(c, w);

		genStoreGlobal(c, pId, w->type, wId, align);

		break;
	}

	case OpStoreScratch: {
		Value *p = cg2c_valueArg(v, 1);
		SpvId pId = getId(c, p);
		Value *w = cg2c_valueArg(v, 2);
		SpvId wId = getId(c, w);

		SpvId pUVec2 = emitOp1(c, SpvOpBitcast, c->typeInt32x2, pId);
		SpvId scratchOff = emitOp2(c, SpvOpCompositeExtract, c->types[KindInt32], pUVec2, 0);

		genStoreScratch(c, scratchOff, w->type, wId, es);

		break;
	}

	default: {
		fprintf(stderr, "unknown op: %s\n", cg2c_opInfo[v->op].name);
		assert(0);
	}
	}

	return 0; // TODO: provide reason for leaving an SpvId undefined for ease of debugging?
}

static void
genBB(CtxtVk *c, Func *f, Map *preds, ScratchLayout *es)
{
	asInsn1(&c->sections[SectionCode], SpvOpLabel, getId(c, f));

	for (size_t i = 0; i < cg2c_sllen(f->values); i++) {
		Value *v = *(Value**)cg2c_slindex(sizeof(Value*), f->values, i);
		SpvId id = translateOp(c, v, preds, es);
		if (id != 0)
			setId(c, v, id);
	}
}

static void
emitFuncSym(CtxtVk *c, Sym *s)
{
	SpvId symId = nextId(c);
	setId(c, s, symId);

	Scope scope = cg2c_computeScope(s->func, c->gc);

	ScratchLayout es = emulateScratch(&(Compile) {.gc = c->gc}, &scope);

	{
		if (es.size < 1)
			es.size = 1;

		SpvId arrayOfI32 = arrayOf(c, c->types[KindInt32], (es.size + 3) / 4);

		SpvId ptrToArrayOfI32Private = ptrTo(c, arrayOfI32, SpvStorageClassPrivate);

		c->scratch = nextId(c);
		asInsn3(&c->sections[SectionCode], SpvOpVariable, ptrToArrayOfI32Private, c->scratch, SpvStorageClassPrivate);
	}

	asInsn4(&c->sections[SectionCode], SpvOpFunction, c->voidType, symId, 0 /* control */, c->typeVoidFunction);

	// Basic blocks may have forward references
	for (size_t i = 0; i < cg2c_sllen(scope.funcs); i++) {
		setId(c, *(Func**)cg2c_slindex(sizeof(Func*), scope.funcs, i), nextId(c));
	}

	Map *preds = cg2c_predecessors(&scope, c->gc);

	for (size_t i = 0; i < cg2c_sllen(scope.funcs); i++) {
		genBB(c, *(Func**)cg2c_slindex(sizeof(Func*), scope.funcs, i), preds, &es);
	}

	asInsn0(&c->sections[SectionCode], SpvOpFunctionEnd);
}

static Sym*
findsym(Slice a, String name)
{
	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);
		if (cg2c_strcmp(s->name, name) == 0)
			return s;
	}
	return NULL;
}

static uint32_t
load32(const unsigned char *p)
{
	return (uint32_t)p[0] | (uint32_t)p[1]<<8 | (uint32_t)p[2]<<16 | (uint32_t)p[3]<<24;
}

static Value*
buildGetSysval(Builder b, Type *type, int sv)
{
	return newValue1(b, OpGetSysval, type, cg2c_buildConst(b, cg2c_types[KindInt32], sv));
}

static Value*
loadShaderArg(Builder b, const Cg2cInputVk *input)
{
	Compile *ctxt = b.ctxt;

	switch (input->type) {
	case CG2C_IN_CONSTANT: {
		assert(input->size % 4 == 0);

		Slice vals = NULL_SLICE;
		for (uint32_t i = 0; i < input->size; i += 4) {
			assert(!b.ctxt->bigEndian);
			Value *val = cg2c_buildConst(b, cg2c_types[KindInt32], load32(input->data+i));
			vals = cg2c_slappend(sizeof(Value*), vals, &val, ctxt->gc);
		}
		return cg2c_buildMakeTuple(b, vals);
	}

	case CG2C_IN_INDIRECT_CONSTANT:
		assert(0); // not implemented
		return NULL;

	case CG2C_IN_PUSH_CONSTANT: {
		assert(input->size % 4 == 0);

		Slice vals = NULL_SLICE;
		for (uint32_t i = 0; i < input->size; i += 4) {
			Value *off = cg2c_buildConst(b, cg2c_types[KindInt32], input->srcOffset+i);
			Value *val = newValue1(b, OpGetPushConstant, cg2c_types[KindInt32], off);
			vals = cg2c_slappend(sizeof(Value*), vals, &val, ctxt->gc);
		}
		return cg2c_buildMakeTuple(b, vals);
	}

	case CG2C_IN_VERTEX_ID: {
		assert(input->size == 4);
		return buildGetSysval(b, cg2c_types[KindInt32], SVVertexID);
	}

	case CG2C_IN_INSTANCE_ID:
		assert(0); // not implemented
		return NULL;

	case CG2C_IN_DRAW_ID:
		assert(0); // not implemented
		return NULL;

	case CG2C_IN_FS_INPUT: {
		assert(input->size == 16);
		Value *location = cg2c_buildConst(b, cg2c_types[KindInt32], input->location);
		Type *vec4 = cg2c_tuple4(b.ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32]);
		return newValue1(b, OpGetInput, vec4, location);
	}

	case CG2C_IN_FRAG_COORD: {
		assert(input->size == 16);
		Type *vec4 = cg2c_tuple4(b.ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32]);
		return buildGetSysval(b, vec4, SVFragCoord);
	}

	case CG2C_IN_GLOBAL_INVOCATION_ID_X: {
		assert(input->size == 4);
		return buildGetSysval(b, cg2c_types[KindInt32], SVGlobalInvocationID);
	}

	case CG2C_IN_GLOBAL_INVOCATION_ID_XY: {
		assert(input->size == 8);
		Value *x = buildGetSysval(b, cg2c_types[KindInt32], SVGlobalInvocationID);
		Value *y = buildGetSysval(b, cg2c_types[KindInt32], SVGlobalInvocationIDY);
		return cg2c_buildMakeTuple2(b, x, y);
	}

	case CG2C_IN_GLOBAL_INVOCATION_ID:
		assert(0); // not implemented
		return NULL;
	}

	assert(0);
	return NULL;
}

static Value*
prologue(Builder b, const Cg2cFunctionVk *f, Value *mem)
{
	uint32_t interfaceSize = 0;
	for (uint32_t i = 0; i < f->ninput; i++) {
		const Cg2cInputVk *input = &f->inputs[i];

		uint32_t off = input->dstOffset + input->size;
		if (interfaceSize < off)
			interfaceSize = off;
	}

	Value *base = cg2c_buildVar(b, interfaceSize, 16);

	for (uint32_t i = 0; i < f->ninput; i++) {
		const Cg2cInputVk *input = &f->inputs[i];

		Value *ptr = cg2c_buildAddPtrConst(b, base, input->dstOffset);
		Value *val = loadShaderArg(b, input);
		mem = cg2c_buildStore(b, 1, mem, ptr, val);
	}

	return cg2c_buildMakeTuple2(b, mem, base);
}

Value*
epilogue(Builder b, const Cg2cFunctionVk *f, Value *mem, Value *val)
{
	Type *vec4 = cg2c_tuple4(b.ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32]);

	Value *var = cg2c_buildVar(b, cg2c_typeSize(b.ctxt, val->type), 1);

	mem = cg2c_buildStore(b, 1, mem, var, val);

	for (uint32_t i = 0; i < f->noutput; i++) {
		const Cg2cOutputVk *output = &f->outputs[i];

		switch (output->type) {
		case CG2C_OUT_POSITION: {
			assert(output->size == 16);

			Value *location = cg2c_buildConst(b, cg2c_types[KindInt32], -1);
			Value *xyzw = cg2c_buildLoad(b, vec4, 1, mem, cg2c_buildAddPtrConst(b, var, output->offset));
			mem = newValue3(b, OpStoreOutput, cg2c_types[KindMem], mem, location, xyzw);

			break;
		}

		case CG2C_OUT_ATTRIBUTE:
		case CG2C_OUT_COLOR: {
			assert(output->size == 16);

			// TODO: think about fp16 attributes and color outputs
			for (uint32_t off = 0; off < output->size; off += 16) {
				Value *location = cg2c_buildConst(b, cg2c_types[KindInt32], output->location+off/16);
				Value *xyzw = cg2c_buildLoad(b, vec4, 1, mem, cg2c_buildAddPtrConst(b, var, output->offset+off));
				mem = newValue3(b, OpStoreOutput, cg2c_types[KindMem], mem, location, xyzw);
			}

			break;
		}

		default: {
			assert(0); // not implemented
		}
		}
	}

	return mem;
}

/*
static void
perfDiag(Compile *ctxt, Value *v, void *_unused)
{
	if (v->op == OpDiv || v->op == OpDivU || v->op == OpRem || v->op == OpRemU) {
		Value *x = cg2c_valueArg(v, 0);
		Value *y = cg2c_valueArg(v, 1);

		if (y->op != OpConst)
			cg2c_warnfAt(ctxt, v->pos, "division by a non-constant");
	}
}
*/

static Value*
newRet(Builder b, Func *f)
{
	Type *t = cg2c_funct(b.ctxt, cg2c_funcResult(f->type), cg2c_types[KindNoret]);
	return newValue1(b, OpRet, t, cg2c_buildFuncValue(b, f));
}

static Value*
actuallyLink(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	Map *m = etc;

	switch (op) {
	case OpSym: {
		String name = *(String*)aux;

		Sym *s = *(Sym* const*)cg2c_mapaccess1(cg2c_str_ptr_map, m, &name);
		if (s == NULL) {
			cg2c_errorfAt(b.ctxt, b.pos, "unresolved symbol: %.*s", cg2c_strlen2(name), cg2c_strdata(name));
			break;
		}
		return cg2c_buildFuncValue(b, s->func);
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

// TODO: add lazy symbol stuff for image ops and other things which are
// implemented directly in the IR
//
// TODO: linking should be moved to common code
static void
link(Compile *ctxt, Slice a)
{
	GC *gc = ctxt->gc;

	Map *m = cg2c_mkmap(cg2c_str_ptr_map, gc);
	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);
		Sym *s1 = *(Sym* const*)cg2c_mapaccess1(cg2c_str_ptr_map, m, &s->name);

		if (s1 == NULL) {
			*(Sym**)cg2c_mapassign(cg2c_str_ptr_map, m, &s->name, gc) = s;
		} else {
			cg2c_errorfAt(ctxt, (Pos) {}, "duplicate symbol %.*s\n"
				"...", cg2c_strlen2(s->name), cg2c_strdata(s->name));
		}
	}

	if (ctxt->errored)
		return;

	Rewriter *rewriter = cg2c_newRewriter(cg2c_commonRules, gc);

	// TODO: we should be efficient and only cover symbols we care about
	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

		Scope scope = cg2c_computeScope(s->func, gc);

		Builder b = {
			.ctxt = ctxt,
			.rewriter = rewriter,
			.cse = s->cse,
		};

		cg2c_scopeRewriteFunc2(b, actuallyLink, m, &scope, NULL);
	}
}

void cg2c_dumpinit(Compile *ctxt, FILE *w);
void cg2c_dumpshutdown(Compile *ctxt);

int
compile(const Cg2cCompileInfoVk *compileInfo, const Cg2cAllocFuncs *_unused, Cg2cBlob **aOut, GC *gc)
{
	assert(_unused == NULL);

	static int ctr = 0;

	++ctr;

	char path[256];

	sprintf(path, "%d.json", ctr);

	FILE *dump = fopen(path, "wb");

	Compile *compile = &(Compile) {
		.unsignedChar = CHAR_MIN == 0,
		.longSize     = sizeof(long),
		.ptrSize      = sizeof(void*),
		.funcSize     = 8,
		.bigEndian    = false,

		.funcs  = cg2c_mkmap(cg2c_ptrSlice_ptr_map, gc),
		.tuples = cg2c_mkmap(cg2c_ptrSlice_ptr_map, gc),

		.diag  = compileInfo->diag,
		.udata = compileInfo->userData,

		.gc = gc,
	};

	cg2c_dumpinit(compile, dump);

	DeviceFeatures features = {};
	cg2c_collectEnabledFeaturesVk(compile, compileInfo->features, &features);

	// Validate shader IO and parse

	/*
	{
		const Cg2cFunctionVk *func = compileInfo->func;

		uint32_t interfaceSize = 0;
		for (uint32_t i = 0; i < func->ninput; i++) {
			const Cg2cInputVk *input = &func->inputs[i];

			// TODO: emit a normal error
			assert(input->dstOffset == interfaceSize);
			assert(input->dstOffset+input->size >= interfaceSize);

			interfaceSize = input->dstOffset + input->size;
		}
	}
	*/

	Slice a = NULL_SLICE; // of Sym*
	for (size_t i = 0; i < compileInfo->nfile; i++) {
		if (hassuffix(compileInfo->files[i], ".c")) {
			const char *ccode = compileInfo->readFile(compileInfo->files[i], compileInfo->userData);
			String code = cg2c_mkstringnocopy(ccode);

			Slice o = cg2c_parse(compile, cg2c_strdata(code), cg2c_strlen(code)); // of Sym*
			a = cg2c_slcat(sizeof(Sym*), a, o, compile->gc);
		} else {
			cg2c_errorf(compile, "%s does not end in .c", compileInfo->files[i]);
		}
	}

	if (compile->errored)
		return -1;

	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

		cg2c_dump3(compile, "gen", s);
	}

	Sym *s = findsym(a, cg2c_mkstringnocopy(compileInfo->func->name));
	if (s == NULL) {
		cg2c_errorf(compile, "undefined: %s", compileInfo->func->name);
		return -1;
	}

	// Because of the way we compute scope at the moment (i.e. just all
	// reachable blocks), link will introduce multiple Func values for the
	// same Func, which will make dumpview complain. That's fine and will be
	// fixed by computing scopes properly.
	link(compile, a);

	// Cull unused symbols. This could probably be merged into link.
	{
		size_t j = 0;
		for (size_t i = 0; i < cg2c_sllen(a); i++) {
			Sym *t = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);
			if (t == s) {
				*(Sym**)cg2c_slindex(sizeof(Sym*), a, j) = t;
				j++;
			}
		}
		a = cg2c_slice(sizeof(Sym*), a, 0, j);
	}

	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

		cg2c_dump3(compile, "link", s);
	}

	// TODO: link and drop all other symbols

	// TODO: move this into its own function
	{
		Builder b = {
			.ctxt = compile,
			.rewriter = cg2c_newRewriter(cg2c_commonRules, compile->gc),
			.cse = s->cse,
		};

		PosBase *posBase = cg2c_malloc(sizeof *posBase, compile->gc);
		posBase->filename = cg2c_mkstringnocopy("<prologue>");

		PosBase *posBase2 = cg2c_malloc(sizeof *posBase2, compile->gc);
		posBase2->filename = cg2c_mkstringnocopy("<epilogue>");

		b = cg2c_builderWithPos(b, (Pos) {.base = posBase}, NULL);

		Func *shell = cg2c_malloc(sizeof *shell, compile->gc);
		shell->type = cg2c_funct(compile, cg2c_types[KindMem], cg2c_types[KindMem]);

		Value *arg = cg2c_buildParam(b, shell);
		Value *argRewritten = prologue(b, compileInfo->func, arg);

		b = cg2c_builderWithPos(b, (Pos) {.base = posBase2}, NULL);

		Value *ret = newRet(b, shell);

		Func *hehe = cg2c_malloc(sizeof *hehe, b.ctxt->gc);
		hehe->type = cg2c_funct(b.ctxt, cg2c_funcResult(s->func->type), cg2c_types[KindNoret]);
		Value *retVal = cg2c_buildParam(b, hehe);
		Value *mem2 = cg2c_buildExtract(b, 0, retVal);
		Value *agrh = cg2c_buildExtract(b, 1, retVal);
		mem2 = epilogue(b, compileInfo->func, mem2, agrh);
		cg2c_funcTerminate(hehe, cg2c_buildCall(b, ret, mem2), b.ctxt->gc);

		Func *inlined = cg2c_inline(b, s->func, cg2c_buildFuncValue(b, hehe));

		cg2c_funcTerminate(shell, cg2c_buildCall(b, cg2c_buildFuncValue(b, inlined), argRewritten), compile->gc);

		// BUG: we really should create new Sym rather than patch the existing one

		s->func = shell;

		cg2c_dump3(compile, "shaderIO", s);
	}

	// TODO: also dump initial representation of these symbols

	// Optimize

	{
		Slice rules = NULL_SLICE;

		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_commonRules, gc);

		Rewriter *rewriter = cg2c_newRewriter(rules, compile->gc);

		for (size_t i = 0; i < cg2c_sllen(a); i++) {
			Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

			Scope scope = cg2c_computeScope(s->func, compile->gc);

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			for (;;) {
				bool changed = false;

				// We really only need to apply this once, for
				// now, but I'm hoping to be able to use
				// function pointers for composition in the
				// future.
				if (cg2c_inlineCallc(b, &scope)) {
					// Temporary hack
					scope = cg2c_computeScope(s->func, compile->gc);
					changed = true;
				}
				// changed = cg2c_inlineCallc(b, &scope) || changed;

				cg2c_dump3(compile, "inlineCallc", s);

				changed = cg2c_uselessCalls(b, &scope) || changed;

				cg2c_dump3(compile, "uselessCalls", s);

				// __asm__("int3");

				{
					bool changed2 = cg2c_loadsToSSA(b, &scope);

					cg2c_dump3(compile, "loadsToSSA", s);

					if (changed2) {
						changed2 = cg2c_phisToParams(b, &scope) || changed2;

						cg2c_dump3(compile, "phisToParams", s);
					}

					changed = changed2 || changed;
				}

				// elimDeadStores produces new dead stores on
				// the first iteration of the optimization loop,
				// so running it repeatedly helps bring down the
				// total number of passes.
				//
				// TODO: consider only running it if any prior
				// passes made progress or something.
				for (;;) {
					bool changed2 = cg2c_elimDeadStores(b, &scope);

					cg2c_dump3(compile, "elimDeadStores", s);

					if (!changed2)
						break;

					changed = true;
				}

				if (!changed)
					break;
			}
		}
	}

	// Print some performance warnings here
	/*
		for (size_t i = 0; i < cg2c_sllen(a); i++) {
			Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

			Scope scope = cg2c_computeScope(s->func, compile->gc);

			cg2c_scopeForeach(compile, perfDiag, NULL, &scope);
		}
	*/

	bool genericAddressing = false;

	// Lower generic addressing

	if (!genericAddressing) {
		Slice rules = NULL_SLICE;

		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_commonRules, gc);
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_vulkanRules, gc);

		Rewriter *rewriter = cg2c_newRewriter(rules, compile->gc);

		for (size_t i = 0; i < cg2c_sllen(a); i++) {
			Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			Scope scope = cg2c_computeScope(s->func, compile->gc);

			// Generic addressing is lowered in one pass
			cg2c_genericAddressingToTags(b, &scope);

			cg2c_dump3(compile, "genericAddressingToTags", s);
		}
	}

	// Lower further

	{
		Slice rules = NULL_SLICE;

		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_commonRules, gc);
		rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_vulkanRules, gc);

		if (!features.shaderInt64)
			rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_int64ToInt32Rules, gc);

		if (!genericAddressing)
			rules = cg2c_slcat(sizeof(RewriteRule), rules, cg2c_genericAddressingToTagsVkRules, gc);

		Rewriter *rewriter = cg2c_newRewriter(rules, compile->gc);

		for (size_t i = 0; i < cg2c_sllen(a); i++) {
			Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

			Builder b = {.ctxt = compile, .rewriter = rewriter, .cse = s->cse};

			Scope scope = cg2c_computeScope(s->func, gc);

			cg2c_scopeRewrite(b, &scope, NULL /* re */);

			// TODO: is this useful to call here?
			cg2c_uselessCalls(b, &scope);

			cg2c_dump3(compile, "uselessCalls", s);
		}
	}

	if (compile->errored) {
		// Lowering error
		return -1;
	}

	// Schedule

	for (size_t i = 0; i < cg2c_sllen(a); i++) {
		Sym *s = *(Sym**)cg2c_slindex(sizeof(Sym*), a, i);

		Scope scope = cg2c_computeScope(s->func, compile->gc);

		cg2c_schedule(compile, &scope);

		cg2c_dump3(compile, "schedule", s);
	}

	CtxtVk *c = &(CtxtVk) {.gc = gc};

	c->stage = compileInfo->func->stage;

	for (int i = SectionXxx+1; i < SectionLast; i++)
		c->sections[i].gc = gc;

	c->ids = cg2c_mkmap(cg2c_ptr_32_map, c->gc);

	// https://www.khronos.org/registry/SPIR-V/specs/unified1/SPIRV.html
	// 2.3. Physical Layout of a SPIR-V Module and Instruction
	// 2.4. Logical Layout of a Module

	uint32_t spirvVersion = compileInfo->spirvVersion;

	// TODO: turn it into an informative message
	assert(spirvVersion == 0x10400 || spirvVersion == 0x10600);

	uint32_t header[] = {
		0x07230203, // SPIR-V magic
		spirvVersion,
		0x63326743, // our magic, "Cg2c"
		0,          // bound (will be written at the end)
		0,          // reserved
	};

	Asm out = {.buf = cg2c_mkslice(sizeof header[0], header, nelem(header), gc), .gc = gc};

	// TODO: associate features with caps and caps with extensions

	// These caps are required
	SpvCapability caps[] = {
		SpvCapabilityShader,
		SpvCapabilitySampled1D,
		SpvCapabilityImage1D,
		SpvCapabilityStorageImageReadWithoutFormat,
		SpvCapabilityStorageImageWriteWithoutFormat,
		SpvCapabilityInt64,
		SpvCapabilityInt16,
		SpvCapabilityStorageBuffer16BitAccess,
		SpvCapabilityStorageBuffer8BitAccess,
		SpvCapabilityInt8,
		SpvCapabilityStorageImageArrayNonUniformIndexing,
		SpvCapabilitySampledImageArrayNonUniformIndexing,
		SpvCapabilityRuntimeDescriptorArray,
		SpvCapabilityPhysicalStorageBufferAddresses,
		SpvCapabilityVulkanMemoryModel,
		SpvCapabilityVulkanMemoryModelDeviceScope,
		SpvCapabilityShaderNonUniform,
	};
	for (size_t i = 0; i < nelem(caps); i++)
		asInsn1(&out, SpvOpCapability, caps[i]);

	const char *exts[] = {
		"SPV_KHR_8bit_storage",
		"SPV_EXT_descriptor_indexing",
		"SPV_KHR_physical_storage_buffer",
		"SPV_KHR_vulkan_memory_model",
	};
	for (size_t i = 0; i < nelem(exts); i++)
		asInsn(&out, SpvOpExtension, strWords(NULL_SLICE, cg2c_mkstringnocopy(exts[i]), gc));

	{
		c->glsl450 = nextId(c);
		Slice args = NULL_SLICE;
		args = cg2c_slappend(sizeof(uint32_t), args, &(uint32_t) {c->glsl450}, gc);
		args = strWords(args, cg2c_mkstringnocopy("GLSL.std.450"), gc);
		asInsn(&out, SpvOpExtInstImport, args);
	}

	asInsn2(&out, SpvOpMemoryModel, SpvAddressingModelPhysicalStorageBuffer64, SpvMemoryModelVulkan);

	c->consts = cg2c_mkmap(cg2c_32_32_map, gc);

	c->ptrs = cg2c_mkmap(cg2c_64_32_map, gc);

	c->types[KindBool] = nextId(c);
	asInsn1(&c->sections[SectionTypes], SpvOpTypeBool, c->types[KindBool]);

	c->types[KindInt8] = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeInt, c->types[KindInt8], 8, 0);

	c->types[KindInt16] = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeInt, c->types[KindInt16], 16, 0);

	c->types[KindInt32] = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeInt, c->types[KindInt32], 32, 0);

	if (features.shaderInt64) {
		c->types[KindInt64] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpTypeInt, c->types[KindInt64], 64, 0);
	}

	c->types[KindPtr] = ptrTo(c, c->types[KindInt8], SpvStorageClassPhysicalStorageBuffer);

	c->floatTypes[KindInt32] = nextId(c);
	asInsn2(&c->sections[SectionTypes], SpvOpTypeFloat, c->floatTypes[KindInt32], 32);

	c->voidType = nextId(c);
	asInsn1(&c->sections[SectionTypes], SpvOpTypeVoid, c->voidType);

	c->typeVoidFunction = nextId(c);
	asInsn2(&c->sections[SectionTypes], SpvOpTypeFunction, c->typeVoidFunction, c->voidType);

	c->typeInt32x2 = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeVector, c->typeInt32x2, c->types[KindInt32], 2);

	c->typeInt32x3 = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeVector, c->typeInt32x3, c->types[KindInt32], 3);

	c->typeInt32x4 = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeVector, c->typeInt32x4, c->types[KindInt32], 4);

	c->typeFloat32x2 = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeVector, c->typeFloat32x2, c->floatTypes[KindInt32], 2);

	c->typeFloat32x4 = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeVector, c->typeFloat32x4, c->floatTypes[KindInt32], 4);

	c->samplerType = nextId(c);
	asInsn1(&c->sections[SectionTypes], SpvOpTypeSampler, c->samplerType);

	{
		c->ptrToSampler = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpTypePointer, c->ptrToSampler, SpvStorageClassUniformConstant, c->samplerType);
	}

	SpvDim dims[DimLast] = {
		[Dim1D]   = SpvDim1D,
		[Dim2D]   = SpvDim2D,
		[Dim3D]   = SpvDim3D,
		[DimCube] = SpvDimCube,
	};

	SpvId comps[] = {
		c->floatTypes[KindInt32],
		c->types[KindInt32],
		// Needs its own set of caps that are
		// also gated behind various features
		// c->types[KindInt64],
	};

	for (int dim = DimXxx+1; dim < DimLast; dim++) {
		for (int arr = 0; arr < 1; arr++) {
			// Need storage image multisample cap which is gated
			// behind shaderStorageImageMultisample
			for (int ms = 0; ms < 1; ms++) {
				for (int k = 0; k < nelem(comps); k++) {
					for (int sampledOrStorage = 0; sampledOrStorage < 2; sampledOrStorage++) {
						SpvId id = nextId(c);
						asInsn8(&c->sections[SectionTypes], SpvOpTypeImage, id,
							comps[k],
							dims[dim],
							2, // depth unknown
							arr,
							ms,
							1+sampledOrStorage,
							SpvImageFormatUnknown);
						c->imageTypes2[dim][arr][ms][k][sampledOrStorage].base = id;

						SpvId id2 = nextId(c);
						asInsn3(&c->sections[SectionTypes], SpvOpTypePointer, id2, SpvStorageClassUniformConstant, id);
						c->imageTypes2[dim][arr][ms][k][sampledOrStorage].ptrTo = id2;

						if (sampledOrStorage == 0) {
							SpvId id3 = nextId(c);
							asInsn2(&c->sections[SectionTypes], SpvOpTypeSampledImage, id3, id);
							c->imageTypes2[dim][arr][ms][k][sampledOrStorage].ffs = id3;
						}
					}
				}
			}
		}
	}

	{
		const Cg2cFunctionVk *func = compileInfo->func;

		uint32_t pushConstantSize = 0;
		for (uint32_t i = 0; i < func->ninput; i++) {
			const Cg2cInputVk *input = &func->inputs[i];

			if (input->type == CG2C_IN_PUSH_CONSTANT) {
				uint32_t off = input->srcOffset + input->size;
				if (pushConstantSize < off)
					pushConstantSize = off;
			}
		}

		if (pushConstantSize > 0) {
			SpvId lenID = const32(c, (pushConstantSize + 3) / 4);
			SpvId arrayOfI32 = nextId(c);
			asInsn3(&c->sections[SectionTypes], SpvOpTypeArray, arrayOfI32, c->types[KindInt32], lenID);
			asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, arrayOfI32, SpvDecorationArrayStride, 4);

			SpvId arrayOfI32Block = nextId(c);
			asInsn2(&c->sections[SectionAnnot], SpvOpDecorate, arrayOfI32Block, SpvDecorationBlock);
			asInsn4(&c->sections[SectionAnnot], SpvOpMemberDecorate, arrayOfI32Block, 0, SpvDecorationOffset, 0);
			asInsn2(&c->sections[SectionTypes], SpvOpTypeStruct, arrayOfI32Block, arrayOfI32);

			SpvId ptrToArrayOfI32Block = ptrTo(c, arrayOfI32Block, SpvStorageClassPushConstant);

			c->pushConstant = asInsn1R(&c->sections[SectionTypes], SpvOpVariable, ptrToArrayOfI32Block, &c->lastId, SpvStorageClassPushConstant);
		}
	}

	if (compileInfo->bindlessImages != NULL) {
		SpvId arrayOfSamplers = nextId(c);
		asInsn2(&c->sections[SectionTypes],
			SpvOpTypeRuntimeArray,
			arrayOfSamplers,
			c->samplerType);

		SpvId ptrToArrayOfSamplers = nextId(c);
		asInsn3(&c->sections[SectionTypes],
			SpvOpTypePointer,
			ptrToArrayOfSamplers,
			SpvStorageClassUniformConstant,
			arrayOfSamplers);

		SpvId descriptors = nextId(c);
		asInsn3(&c->sections[SectionTypes],
			SpvOpVariable,
			ptrToArrayOfSamplers,
			descriptors,
			SpvStorageClassUniformConstant);

		asInsn3(&c->sections[SectionAnnot],
			SpvOpDecorate,
			descriptors,
			SpvDecorationDescriptorSet,
			compileInfo->bindlessImages->set);

		asInsn3(&c->sections[SectionAnnot],
			SpvOpDecorate,
			descriptors,
			SpvDecorationBinding,
			compileInfo->bindlessImages->bindings.samplers);

		c->samplers = descriptors;
	}

	if (compileInfo->bindlessImages != NULL) {
		for (int dim = DimXxx+1; dim < DimLast; dim++) {
			for (int arr = 0; arr < 2; arr++) {
				for (int ms = 0; ms < 2; ms++) {
					for (int k = 0; k < 3; k++) {
						// Don't create descriptor array views
						// for unsupported image types
						if (c->imageTypes2[dim][arr][ms][k][0].base == 0)
							continue;

						SpvId arrayOfImages = nextId(c);
						asInsn2(&c->sections[SectionTypes],
							SpvOpTypeRuntimeArray,
							arrayOfImages,
							c->imageTypes2[dim][arr][ms][k][0].base);

						SpvId ptrToArrayOfImages = nextId(c);
						asInsn3(&c->sections[SectionTypes],
							SpvOpTypePointer,
							ptrToArrayOfImages,
							SpvStorageClassUniformConstant,
							arrayOfImages);

						SpvId descriptors = nextId(c);
						asInsn3(&c->sections[SectionTypes],
							SpvOpVariable,
							ptrToArrayOfImages,
							descriptors,
							SpvStorageClassUniformConstant);

						asInsn3(&c->sections[SectionAnnot],
							SpvOpDecorate,
							descriptors,
							SpvDecorationDescriptorSet,
							compileInfo->bindlessImages->set);

						asInsn3(&c->sections[SectionAnnot],
							SpvOpDecorate,
							descriptors,
							SpvDecorationBinding,
							compileInfo->bindlessImages->bindings.sampledImages);

						c->sampledImages[dim][arr][ms][k] = descriptors;
					}
				}
			}
		}
	}

	if (compileInfo->bindlessImages != NULL) {
		for (int dim = DimXxx+1; dim < DimLast; dim++) {
			for (int arr = 0; arr < 2; arr++) {
				for (int ms = 0; ms < 2; ms++) {
					for (int k = 0; k < 3; k++) {
						// Don't create descriptor array views
						// for unsupported image types
						if (c->imageTypes2[dim][arr][ms][k][1].base == 0)
							continue;

						SpvId arrayOfImages = nextId(c);
						asInsn2(&c->sections[SectionTypes],
							SpvOpTypeRuntimeArray,
							arrayOfImages,
							c->imageTypes2[dim][arr][ms][k][1].base);

						SpvId ptrToArrayOfImages = nextId(c);
						asInsn3(&c->sections[SectionTypes],
							SpvOpTypePointer,
							ptrToArrayOfImages,
							SpvStorageClassUniformConstant,
							arrayOfImages);

						SpvId descriptors = nextId(c);
						asInsn3(&c->sections[SectionTypes],
							SpvOpVariable,
							ptrToArrayOfImages,
							descriptors,
							SpvStorageClassUniformConstant);

						asInsn3(&c->sections[SectionAnnot],
							SpvOpDecorate,
							descriptors,
							SpvDecorationDescriptorSet,
							compileInfo->bindlessImages->set);

						asInsn3(&c->sections[SectionAnnot],
							SpvOpDecorate,
							descriptors,
							SpvDecorationBinding,
							compileInfo->bindlessImages->bindings.storageImages);

						c->storageImages[dim][arr][ms][k] = descriptors;
					}
				}
			}
		}
	}

	{
		// TODO: add a function for constructing vector types

		SpvId inputPtrToInt32x3 = ptrTo(c, c->typeInt32x3, SpvStorageClassInput);

		c->sysvals[SVGlobalInvocationID] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, inputPtrToInt32x3, c->sysvals[SVGlobalInvocationID], SpvStorageClassInput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->sysvals[SVGlobalInvocationID], SpvDecorationBuiltIn, SpvBuiltInGlobalInvocationId);
	}

	{
		SpvId inputPtrToFloat32x4 = ptrTo(c, c->typeFloat32x4, SpvStorageClassInput);

		c->sysvals[SVFragCoord] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, inputPtrToFloat32x4, c->sysvals[SVFragCoord], SpvStorageClassInput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->sysvals[SVFragCoord], SpvDecorationBuiltIn, SpvBuiltInFragCoord);
	}

	{
		SpvId inputPtrToInt32 = ptrTo(c, c->types[KindInt32], SpvStorageClassInput);

		c->sysvals[SVVertexID] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, inputPtrToInt32, c->sysvals[SVVertexID], SpvStorageClassInput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->sysvals[SVVertexID], SpvDecorationBuiltIn, SpvBuiltInVertexIndex);
	}

	{
		SpvId inputPtrToFloat32x4 = ptrTo(c, c->typeFloat32x4, SpvStorageClassInput);

		c->in[0] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, inputPtrToFloat32x4, c->in[0], SpvStorageClassInput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->in[0], SpvDecorationLocation, 0);
	}

	// Always decorate all outputs invariant

	{
		SpvId outputPtrToFloat32x4 = ptrTo(c, c->typeFloat32x4, SpvStorageClassOutput);

		c->outPosition = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, outputPtrToFloat32x4, c->outPosition, SpvStorageClassOutput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->outPosition, SpvDecorationBuiltIn, SpvBuiltInPosition);
		asInsn2(&c->sections[SectionAnnot], SpvOpDecorate, c->outPosition, SpvDecorationInvariant);
	}

	{
		SpvId outputPtrToFloat32x4 = ptrTo(c, c->typeFloat32x4, SpvStorageClassOutput);

		c->out[0] = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpVariable, outputPtrToFloat32x4, c->out[0], SpvStorageClassOutput);
		asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, c->out[0], SpvDecorationLocation, 0);
		asInsn2(&c->sections[SectionAnnot], SpvOpDecorate, c->out[0], SpvDecorationInvariant);
	}

	emitFuncSym(c, s);

	// Don't leak Lines in UserConsts into the section that follows.
	asInsn0(&c->sections[SectionUserConsts], SpvOpNoLine);

	{
		const Cg2cFunctionVk *e = compileInfo->func;

		SpvId id = getId(c, s);

		Slice ep = NULL_SLICE;
		switch (e->stage) {
		case VK_SHADER_STAGE_VERTEX_BIT: {
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {SpvExecutionModelVertex}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {id}, gc);
			ep = strWords(ep, s->name, gc);

			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->sysvals[SVVertexID]}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->outPosition}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->out[0]}, gc);

			break;
		}

		case VK_SHADER_STAGE_FRAGMENT_BIT: {
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {SpvExecutionModelFragment}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {id}, gc);
			ep = strWords(ep, s->name, gc);

			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->sysvals[SVFragCoord]}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->in[0]}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->out[0]}, gc);

			asInsn2(&c->sections[SectionExecModes], SpvOpExecutionMode, id, SpvExecutionModeOriginUpperLeft);
			asInsn2(&c->sections[SectionExecModes], SpvOpExecutionMode, id, SpvExecutionModeEarlyFragmentTests);

			break;
		}

		case VK_SHADER_STAGE_COMPUTE_BIT: {
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {SpvExecutionModelGLCompute}, gc);
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {id}, gc);
			ep = strWords(ep, s->name, gc);

			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->sysvals[SVGlobalInvocationID]}, gc);

			const Cg2cComputeVk *extra = e->extra;

			asInsn5(&c->sections[SectionExecModes], SpvOpExecutionMode, id, SpvExecutionModeLocalSize, extra->workgroupSize[0], extra->workgroupSize[1], extra->workgroupSize[2]);

			break;
		}

		default:
			assert(0);
			break;
		}

		if (c->pushConstant != 0)
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->pushConstant}, gc);
		if (c->scratch != 0)
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->scratch}, gc);
		if (c->samplers != 0)
			ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->samplers}, gc);
		for (int dim = DimXxx+1; dim < DimLast; dim++) {
			for (int arr = 0; arr < 2; arr++) {
				for (int ms = 0; ms < 2; ms++) {
					for (int k = 0; k < 3; k++) {
						if (c->sampledImages[dim][arr][ms][k] != 0)
							ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->sampledImages[dim][arr][ms][k]}, gc);
						if (c->storageImages[dim][arr][ms][k] != 0)
							ep = cg2c_slappend(sizeof(uint32_t), ep, &(uint32_t) {c->storageImages[dim][arr][ms][k]}, gc);
					}
				}
			}
		}

		asInsn(&c->sections[SectionEntryPoints], SpvOpEntryPoint, ep);
	}

	*(uint32_t*)cg2c_slindex(sizeof(uint32_t), out.buf, 3) = c->lastId + 1;

	for (int i = SectionXxx+1; i < SectionLast; i++)
		out.buf = cg2c_slcat(sizeof(uint32_t), out.buf, c->sections[i].buf, compile->gc);

	size_t len = cg2c_sllen(out.buf);

	uint8_t *out2 = cg2c_malloc(len*4 * sizeof *out2, gc); // TODO: recover
	for (size_t i = 0; i < len; i++) {
		uint32_t x = *(uint32_t*)cg2c_slindex(sizeof(uint32_t), out.buf, i);
		out2[4*i+0] = (uint8_t)x;
		out2[4*i+1] = (uint8_t)(x >> 8);
		out2[4*i+2] = (uint8_t)(x >> 16);
		out2[4*i+3] = (uint8_t)(x >> 24);
	}

	cg2c_dumpshutdown(compile);
	fclose(dump);

	Cg2cBlob *blob = cg2c_export(out2, len*4, _unused);
	// TODO: return error if we failed to allocate
	*aOut = blob;

	{
		sprintf(path, "%d.spv", ctr);

		FILE *f = fopen(path, "wb");
		fwrite(out2, len*4, 1, f);
		fclose(f);
	}

	return 0;
}

SpvId
ptrTo(CtxtVk *c, SpvId elem, SpvStorageClass class)
{
	uint32_t key[] = {elem, class};

	bool ok;
	SpvId id = *(const SpvId*)cg2c_mapaccess2(cg2c_64_32_map, c->ptrs, key, &ok);
	if (!ok) {
		id = nextId(c);
		asInsn3(&c->sections[SectionTypes], SpvOpTypePointer, id, class, elem);
		if (class == SpvStorageClassPhysicalStorageBuffer) {
			uint32_t elemSize = 0;
			if (elem == c->types[KindInt8])
				elemSize = 1;
			else if (elem == c->types[KindInt16])
				elemSize = 2;
			else if (elem == c->types[KindInt32])
				elemSize = 4;
			else if (elem == c->types[KindInt64] || elem == c->types[KindPtr])
				elemSize = 8;
			assert(elemSize > 0);
			asInsn3(&c->sections[SectionAnnot], SpvOpDecorate, id, SpvDecorationArrayStride, elemSize);
		}
		*(SpvId*)cg2c_mapassign(cg2c_64_32_map, c->ptrs, key, c->gc) = id;
	}
	return id;
}

SpvId
arrayOf(CtxtVk *c, SpvId elem, uint32_t len)
{
	// TODO: interning
	//uint32_t key[] = {elem, len};

	SpvId lenID = const32(c, len);
	SpvId id = nextId(c);
	asInsn3(&c->sections[SectionTypes], SpvOpTypeArray, id, elem, lenID);
	return id;
}

// TODO: suffix these with Raw and ops that take result + id should not be
// suffixed R as they're the most commonly arising

void
asInsn(Asm *as, SpvOp op, Slice args)
{
	uint32_t first = (uint32_t)op | ((uint32_t)(1+cg2c_sllen(args)) << 16);
	as->buf = cg2c_slappend(sizeof first, as->buf, &first, as->gc);
	as->buf = cg2c_slcat(sizeof(uint32_t), as->buf, args, as->gc);
}

void
asInsn0(Asm *as, SpvOp op)
{
	asInsn(as, op, NULL_SLICE);
}

void
asInsn1(Asm *as, SpvOp op, uint32_t a0)
{
	uint32_t args[] = {a0};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn2(Asm *as, SpvOp op, uint32_t a0, uint32_t a1)
{
	uint32_t args[] = {a0, a1};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn3(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2)
{
	uint32_t args[] = {a0, a1, a2};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn4(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3)
{
	uint32_t args[] = {a0, a1, a2, a3};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn5(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4)
{
	uint32_t args[] = {a0, a1, a2, a3, a4};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn6(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
	uint32_t args[] = {a0, a1, a2, a3, a4, a5};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

void
asInsn8(Asm *as, SpvOp op, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5, uint32_t a6, uint32_t a7)
{
	uint32_t args[] = {a0, a1, a2, a3, a4, a5, a6, a7};
	asInsn(as, op, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}
