#define VK_NO_PROTOTYPES

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <vulkan/vulkan_core.h>

#include <cg2c/cg2c.h>
#include <cg2c/cg2c_vulkan.h>

#define nelem(x) (sizeof(x)/sizeof((x)[0]))

void* emalloc(size_t);

void fatalf(const char*, ...);

typedef struct DeviceProps DeviceProps;
typedef struct Fns         Fns;
typedef struct Image       Image;
typedef struct ImageView   ImageView;
typedef struct Pass        Pass;
typedef struct Sampler     Sampler;
typedef struct Submission  Submission;

// Device-level functions
#define DEVICE_FUNCS(F)                                         \
	F(vkAcquireNextImageKHR)                                \
	F(vkAllocateCommandBuffers)                             \
	F(vkAllocateDescriptorSets)                             \
	F(vkAllocateMemory)                                     \
	F(vkBeginCommandBuffer)                                 \
	F(vkBindBufferMemory2)                                  \
	F(vkBindImageMemory2)                                   \
	F(vkCmdBeginRenderPass2KHR)                             \
	F(vkCmdBindDescriptorSets)                              \
	F(vkCmdBindPipeline)                                    \
	F(vkCmdBlitImage)                                       \
	F(vkCmdClearColorImage)                                 \
	F(vkCmdCopyBufferToImage)                               \
	F(vkCmdCopyImageToBuffer)                               \
	F(vkCmdDispatch)                                        \
	F(vkCmdDraw)                                            \
	F(vkCmdEndRenderPass2KHR)                               \
	F(vkCmdFillBuffer)                                      \
	F(vkCmdPipelineBarrier2KHR)                             \
	F(vkCmdPushConstants)                                   \
	F(vkCmdSetScissor)                                      \
	F(vkCmdSetViewport)                                     \
	F(vkCreateBuffer)                                       \
	F(vkCreateCommandPool)                                  \
	F(vkCreateComputePipelines)                             \
	F(vkCreateDescriptorPool)                               \
	F(vkCreateDescriptorSetLayout)                          \
	F(vkCreateFence)                                        \
	F(vkCreateFramebuffer)                                  \
	F(vkCreateGraphicsPipelines)                            \
	F(vkCreateImage)                                        \
	F(vkCreateImageView)                                    \
	F(vkCreatePipelineLayout)                               \
	F(vkCreateRenderPass2KHR)                               \
	F(vkCreateSampler)                                      \
	F(vkCreateSemaphore)                                    \
	F(vkCreateShaderModule)                                 \
	F(vkCreateSwapchainKHR)                                 \
	F(vkDestroyFramebuffer)                                 \
	F(vkDestroyImageView)                                   \
	F(vkDestroySampler)                                     \
	F(vkDestroyShaderModule)                                \
	F(vkDestroySwapchainKHR)                                \
	F(vkDeviceWaitIdle)                                     \
	F(vkEndCommandBuffer)                                   \
	F(vkFlushMappedMemoryRanges)                            \
	F(vkFreeMemory)                                         \
	F(vkGetBufferDeviceAddressKHR)                          \
	F(vkGetBufferMemoryRequirements2)                       \
	F(vkGetDeviceQueue)                                     \
	F(vkGetDeviceQueue2)                                    \
	F(vkGetImageMemoryRequirements2)                        \
	F(vkGetSwapchainImagesKHR)                              \
	F(vkInvalidateMappedMemoryRanges)                       \
	F(vkMapMemory)                                          \
	F(vkQueuePresentKHR)                                    \
	F(vkQueueSubmit)                                        \
	F(vkQueueSubmit2KHR)                                    \
	F(vkResetCommandPool)                                   \
	F(vkUpdateDescriptorSets)

// Instance-level and VK_EXT_debug_utils functions
#define INSTANCE_FUNCS(F)                                       \
	F(vkCmdBeginDebugUtilsLabelEXT)                         \
	F(vkCmdEndDebugUtilsLabelEXT)                           \
	F(vkCmdInsertDebugUtilsLabelEXT)                        \
	F(vkCreateDevice)                                       \
	F(vkEnumeratePhysicalDevices)                           \
	F(vkGetDeviceProcAddr)                                  \
	F(vkGetPhysicalDeviceFeatures2)                         \
	F(vkGetPhysicalDeviceMemoryProperties2)                 \
	F(vkGetPhysicalDeviceProperties2)                       \
	F(vkGetPhysicalDeviceQueueFamilyProperties2)            \
	F(vkGetPhysicalDeviceSurfaceCapabilitiesKHR)            \
	F(vkGetPhysicalDeviceSurfaceFormatsKHR)                 \
	F(vkSetDebugUtilsObjectNameEXT)

struct Fns {
#define F(Fn) PFN_##Fn Fn;
	DEVICE_FUNCS(F)

	INSTANCE_FUNCS(F)
#undef F

	PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;
};

struct DeviceProps {
	VkPhysicalDeviceProperties2 _1_0;
};

typedef struct Swapchain Swapchain;
struct Swapchain {
	VkFormat format;
	VkColorSpaceKHR colorSpace;
	uint32_t width, height;
	VkSwapchainKHR vk;
	uint32_t imageCount;
	Image **images; // [imageCount]
	VkSemaphore *presentSems; // [imageCount]
	// TODO: support for acquiring immediately after present? We may have
	// resize happen in the meantime, so swapchain needs to keep a few extra
	// bits around.
};

typedef struct PipelineShaderStageCg2CreateInfo PipelineShaderStageCg2CreateInfo;
struct PipelineShaderStageCg2CreateInfo {
	VkStructureType                     sType;
	const void*                         pNext;
	const Cg2cInputVk*                  inputs;
	size_t                              ninput;
	const Cg2cOutputVk*                 outputs;
	size_t                              noutput;
	const void*                         extra;
	const char* const*                  files;
	size_t                              nfile;
};

struct Image {
	VkImage vk;
	VkImageLayout layout; // TODO: make this per-subresource
};

// TODO: can also rename to something else, to make it clear that's a command
// processing + host lock.
typedef struct CPLock CPLock;
struct CPLock {
	// everything here is private, do not touch
	CPLock *noCopy;
	int token;
};

// If image is NULL, this is a global barrier. Otherwise, it's also an
// image barrier.
//
// TODO: rename to Access or AccessInfo
typedef struct Node Node;
struct Node {
	int                     token; // zero token is reserved; TODO: replace by CPLock*
	VkPipelineStageFlags2   stageMask;
	VkAccessFlags2          accessMask;
	bool                    discard;
	VkImageLayout           layout;
	Image*                  image;
	VkImageSubresourceRange subresourceRange;

	const char *label;
};

// TODO: rename to PassInfo?
struct Pass {
	VkSemaphore waitSem;

	Node *nodes;
	size_t nodeCount;

	VkCommandBuffer cb;

	const char *label;
};

typedef struct Zombie Zombie;
struct Zombie {
	void (*f)(void*);
	void *data;
};

// TODO: should we make Submission private?
struct Submission {
	// Zombie *zombies;
	// size_t zombieCount;

	// The following fields are private

	VkSemaphore waitOnSema;

	VkCommandPool cmdPool;
	VkCommandBuffer cbs[100];
	size_t nextcb;

	Pass *passes;
	size_t passCount;
};

// TODO: we might not need instance to be visible outside of init.c
extern VkInstance instance;

extern bool haveDebugUtils;
extern bool haveSwapchainColorspace;

extern DeviceProps deviceProps;

extern VkPhysicalDevice physicalDevice;

extern VkSurfaceFormatKHR swapchainFormat;

extern const void *deviceFeatures;

extern VkDevice device;

extern uint32_t queueFamily;
extern VkQueue queue;

extern VkDescriptorSetLayout bindlessLayout;
extern VkDescriptorSet bindless;

extern Fns fns;

PFN_vkGetInstanceProcAddr openVulkan(void);

void init(void);
void initbindless(void);

enum {
	DEVICE_MALLOC_SYSMEM = 1,
	DEVICE_MALLOC_VRAM,
};

void* deviceMalloc(size_t size, uint32_t flags);
void  deviceFree(void *p);

void createImage(const VkImageCreateInfo *createInfo, Image **im);
void destroyImage(Image *im);

void        createImageView(const VkImageViewCreateInfo *createInfo, ImageView **view);
void        destroyImageView(ImageView *view);
VkImageView getImageViewVk(ImageView *view); // or getVkImageView?
// TODO: maybe we could have a shareable (between host and device) ImageView and
// somehow avoid getImageViewHandle thing. That would be fun
uint32_t getImageViewSamplingHandle(ImageView *view, VkImageLayout layout);
uint32_t getImageViewHandle(ImageView *view, VkImageLayout layout);

// same thing as with view handles, could we make this stuff shareable?
void createSampler(const VkSamplerCreateInfo *createInfo, Sampler **sampler);
uint32_t getSamplerHandle(Sampler*);

VkCommandBuffer getcb(Submission *ba); // TODO: rename to something along the lines of "get free command buffer"?
void            execPass(Submission *ba, Pass *pass);
void            submit(Submission *ba);

void* deviceMallocAsync(Submission *s, size_t size, uint32_t flags);
void  deviceFreeAsync(Submission *s, void *p);

// Compatibility

typedef struct BufferAndOffset BufferAndOffset;
struct BufferAndOffset {
	VkDeviceMemory memory;
	VkBuffer       buffer;
	VkDeviceSize   offset;
};

void*           device2hostptr(VkDeviceAddress ptr);
BufferAndOffset deviceptrinfo(VkDeviceAddress ptr); // TODO: add a variant of this call which allows pointers one past the end of the array

typedef struct PerStageCg2cInputs PerStageCg2cInputs;
struct PerStageCg2cInputs {
	VkShaderStageFlags stage;
	size_t inputCount;
	Cg2cInputVk *inputs;
};

typedef struct PipelineLayoutCg2CreateInfo2 PipelineLayoutCg2CreateInfo2;
struct PipelineLayoutCg2CreateInfo2 {
	size_t stageCount;
	PerStageCg2cInputs *stages;
};

// TODO: custom pipeline layout object for easier push constants and binding?

void createPipelineLayout2(const PipelineLayoutCg2CreateInfo2 *createInfo, VkPipelineLayout *out);

void createGraphicsPipeline(const VkGraphicsPipelineCreateInfo *createInfo, VkPipeline *out);
void createComputePipeline(const VkComputePipelineCreateInfo *createInfo, VkPipeline *out);

void nameObject(uint64_t handle, VkObjectType type, const char *format, ...);

void fatalvk(const char*, VkResult);

/*
enum {
	MaxMipLevels = 31,
};
*/

typedef struct TextureHeader TextureHeader;
struct TextureHeader {
	VkImageType imageType;
	VkFormat format;
	VkExtent3D extent;
	uint32_t levelCount; // mipLevels
	uint32_t arrayLayers;
	struct {
		uint64_t off;
		uint64_t len;
	} levels[31];
};

// Image creation + upload is up to the caller
int readKTX2(FILE *f, TextureHeader *out);

struct UserTexture {
	Image **image;
	ImageView **view;
};

struct UserTextures {
	// should contain a list of uploads and a list of images to transition
};

// TODO: get rid of this struct? parameters describing dst and src are nice to
// keep distinct though.
typedef struct DoUpload DoUpload;
struct DoUpload {
	Image *dst;
	const void *src;

	uint32_t                    bufferRowLength;
	uint32_t                    bufferImageHeight;
	VkImageSubresourceLayers    imageSubresource;
	VkOffset3D                  imageOffset;
	VkExtent3D                  imageExtent;
};

void doImageUpload(Submission *ba, const DoUpload *info);

typedef struct DoFFT DoFFT;
struct DoFFT {
	// TODO: data format

	const void *src;
	void *dst;
	int lgN; // TODO: should be just size
};

void doFFT(Submission *ba, const DoFFT *info);
