#include "all.h"

typedef struct InlinerState InlinerState;
struct InlinerState {
	Map   *renamed;
	Func  *inlinee;
	Value *retc;
};

static Value*
inliner(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc_)
{
	InlinerState *etc = etc_;

	switch (op) {
	case OpPhi: {
		assert(0); // do not inline with phis
		break;
	}

	case OpFunc: {
		Func *f = aux;
		Func *g = *(Func* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, etc->renamed, &f);
		if (g != NULL)
			return cg2c_buildFuncValue(b, g);
		break;
	}

	case OpRet: {
		Func *f = cg2c_valueAuxFunc(*(Value**)cg2c_slindex(sizeof(Value*), args, 0));
		if (f == etc->inlinee)
			return etc->retc;
		break;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

static Func*
cg2c_newBlock(Type *type, GC *gc)
{
	assert(cg2c_typeKind(type) == KindFunc);

	Func *f = cg2c_malloc(sizeof *f, gc);
	f->type = type;

	return f;
}

// This inlining interface is kinda bad, but we'll see what we can improve later
//
// Improvement #1: we don't really need arg here, the caller can just call the
// returned block.
Func*
cg2c_inline(Builder b, Func *f, Value *retc)
{
	assert(cg2c_funcResult(f->type) == cg2c_funcParam(retc->type));

	Scope scope = cg2c_computeScope(f, b.ctxt->gc);

	Map *renamed = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope.funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope.funcs, i);
		Type *t = g->type;
		if (i == 0)
			t = cg2c_funct(b.ctxt, cg2c_funcParam(g->type), cg2c_types[KindNoret]);
		Func *h = cg2c_newBlock(t, b.ctxt->gc);

		*(Func**)cg2c_mapassign(cg2c_ptr_ptr_map, renamed, &g, b.ctxt->gc) = h;
	}

	Func *entry = *(Func* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, renamed, &f);

	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);
	for (size_t i = 0; i < cg2c_sllen(scope.funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope.funcs, i);
		Func *h = *(Func* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, renamed, &g);

		cg2c_funcTerminate(h, cg2c_rewriteFunc2(b, inliner, &(InlinerState) {
			.renamed = renamed,
			.inlinee = entry,
			.retc = retc,
		}, cg2c_funcTerminator(g), re), b.ctxt->gc);
	}

	return entry;
}
