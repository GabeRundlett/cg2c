#!/usr/bin/env python3

import sys
import typing

# TODO: handle commutative ops

class Pattern(typing.NamedTuple):
    var: str = ""
    op: str = ""
    args: list = []
    ddd: bool = False

class RewriteRule(typing.NamedTuple):
    pattern: Pattern = None
    guard: str = ""
    replace: str = ""

with open(sys.argv[1]) as f:
    exec(compile(f.read(), sys.argv[1], "exec"), globals())

def fprint(f, *args):
    f.write(" ".join(args))
    f.write("\n")

with open(sys.argv[2], "w") as out:
    fprint(out, "// This file is generated")

    fprint(out, prologue)

    for i, rule in enumerate(rules):
        fprint(out, )
        fprint(out, "static Value*")
        fprint(out, "rule" + str(i) + "(Builder b, Value *_v)")
        fprint(out, "{")

        def f(pat: Pattern, v: str):
            if pat.var != "":
                fprint(out, "Value *" + pat.var + " = " + v + ";")
            for i in reversed(range(len(pat.args))):
                fprint(out, "Value *" + v + "_" + str(i) + " = cg2c_valueArg(" + v + ", " + str(i) + ");")
            for i, a in enumerate(pat.args):
                f(a, v + "_" + str(i))

        f(rule.pattern, "_v")

        if rule.guard != "":
            fprint(out, "if (!(" + rule.guard + "))")
            fprint(out, "return _v;")

        fprint(out, rule.replace)

        fprint(out, "}")

    fprint(out)
    fprint(out, "static RewriteRule rules[] = {")
    for i, rule in enumerate(rules):
        fprint(out, "{")
        fprint(out, ".pattern = ")
        def f(pat: Pattern):
            if pat.op == "":
                fprint(out, "NULL,")
                return
            fprint(out, "&(Pattern) {")
            fprint(out, ".op = Op" + pat.op + ",")
            fprint(out, ".args = (Pattern*[]) {")
            for a in pat.args:
                f(a)
            fprint(out, "},")
            fprint(out, ".narg = " + str(len(pat.args)) + ",")
            if pat.ddd:
                fprint(out, ".ddd = true,")
            fprint(out, "},")
        f(rule.pattern)
        fprint(out, ".replace = rule" + str(i) + ",")
        fprint(out, "},")
    fprint(out, "};")

    fprint(out)
    fprint(out, "Slice " + label + " = cg2c_mkslicenocopy_macro(sizeof rules[0], rules, nelem(rules));")
