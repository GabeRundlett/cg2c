import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'ir_view.dart';
import 'op_info.dart';
import 'ops.dart';

class IRView extends StatelessWidget {
  const IRView({super.key, required this.values, required this.blocks});

  static const indent = '    ';

  final List<Value> values;
  final List<Block> blocks;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;

    var commentStyle = TextStyle(color: colorScheme.onSurface.withOpacity(0.5));

    var settings = Config.of(context);

    var valueHues = ValueHues.of(context);

    var nextvid = 0;
    var valueNames = <String, String>{};

    // TODO: when flutter's rich text gains tooltip support, hovering over a
    // value definition either in lhs or rhs should give same tooltip we'd show
    // in graph view

    var lines = <InlineSpan>[];
    for (var block in blocks) {
      lines.add(const TextSpan(text: 'F0 = v0\n'));

      Pos? curPos;
      for (var v in block.values) {
        if (({Op.Func, Op.Const}).contains(v.op)) continue;

        var pos = !settings.diColumns ? v.pos.stripColumn() : v.pos;
        if (curPos == null || curPos.filename != pos.filename) {
          var comment =
              pos.filename != '' ? '"${pos.filename}"' : 'no source location';
          lines.add(TextSpan(text: '$indent; $comment\n', style: commentStyle));
        }
        if (curPos != pos && pos.filename != '') {
          var comment =
              !settings.diColumns ? '${pos.line}' : '${pos.line}:${pos.col}';
          lines
              .add(TextSpan(text: '$indent; :$comment\n', style: commentStyle));
        }
        curPos = pos;

        var vColor = valueHues.foregroundColor(v.id, colorScheme.onSurface);

        var args = <InlineSpan>[];
        for (var a in v.args) {
          args.add(const TextSpan(text: ' '));

          switch (a.op) {
            case Op.Const:
              args.add(TextSpan(
                  text: formatBits(a.auxInt!, 32, opHints[v.op] ?? Hint.int)));
              break;

            case Op.Func:
              // TODO: provide functions on taps
              args.add(const TextSpan(text: 'F0'));
              break;

            default:
              args.add(TextSpan(
                text: valueNames.putIfAbsent(a.id, () => 'v${++nextvid}'),
                style: TextStyle(
                    color:
                        valueHues.foregroundColor(a.id, colorScheme.onSurface)),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    // TODO: scroll to definition
                  }
                  ..onSecondaryTap = () {
                    // TODO: open context menu that lists uses + more info
                  },
              ));
              break;
          }
        }

        // Name this instruction after we've named the missing arguments
        var name = valueNames.putIfAbsent(v.id, () => 'v${++nextvid}');

        lines.addAll([
          const TextSpan(text: indent),
          TextSpan(
            text: name,
            style: TextStyle(color: vColor),
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                context.dispatchNotification(ValueDefClickNotification(v));
              }
              ..onSecondaryTap = () {
                // TODO: open context menu that lists uses + possibly more info
              },
          ),
          if (!opsWithObviousTypes.contains(v.op)) TextSpan(text: ' ${v.type}'),
          TextSpan(text: ' = ${v.op.name}'),
          ...args,
          const TextSpan(text: '\n'),
        ]);
      }

      lines.add(const TextSpan(text: '\n'));
    }

    return SelectableText.rich(
      TextSpan(
        style: TextStyle(fontFamily: settings.monospaceFontFamily),
        children: lines,
      ),
      showCursor: true,
      selectionHeightStyle: BoxHeightStyle.includeLineSpacingMiddle,
      selectionWidthStyle: BoxWidthStyle.max,
    );
  }
}
