#include "all.h"

// Note: our inlining is beyond broken. Notably, inlineCallc does weird stuff
// that it shouldn't be doing, and cg2c_inline sucks somewhat as well.

static bool
cg2c_typeIsMem(Type *t)
{
	if (cg2c_typeKind(t) == KindMem)
		return true;

	if (cg2c_typeKind(t) == KindTuple) {
		// TODO: add a field to cache the result?

		for (size_t i = 0; i < cg2c_tupleNElem(t); i++) {
			if (cg2c_typeIsMem(cg2c_tupleElem(t, i)))
				return true;
		}
	}

	return false;
}

static Func*
scopeAddFunc(Scope *scope, Type *type, GC *gc)
{
	Func *f = cg2c_malloc(sizeof *f, gc);
	f->type = type;
	scope->funcs = cg2c_slappend(sizeof(Func*), scope->funcs, &f, gc);
	return f;
}

static bool
inlineCallc(Builder b, Func *f, Value *v, Scope *scope, Map *re)
{
	bool changed = false;

	switch (v->op) {
	case OpCallc2: {
		Func *target = cg2c_valueArgFunc(v, 0); // just ignore other stuff
		Value *arg = cg2c_valueArg(v, 1);

		Func *landing = scopeAddFunc(scope, cg2c_funct(b.ctxt, cg2c_funcResult(target->type), cg2c_types[KindNoret]), b.ctxt->gc);
		Func *inlinedTarget = cg2c_inline(b, target, cg2c_buildFuncValue(b, landing));

		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &v, b.ctxt->gc) = cg2c_buildParam(b, landing);

		cg2c_funcTerminate(landing, cg2c_rewrite(b, cg2c_funcTerminator(f), re), b.ctxt->gc);

		cg2c_funcTerminate(f, cg2c_buildCall(b, cg2c_buildFuncValue(b, inlinedTarget), arg), b.ctxt->gc);

		changed = true;

		break;
	}

	default:
		break;
	}

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);

		if (cg2c_typeIsMem(a->type))
			changed = inlineCallc(b, f, a, scope, re) || changed;
	}

	return changed;
}

// Once again, we need to insert control flow, which is a massive pain in the
// ass. Is there something we could do to improve this?
//
// And of course it's broken
bool
cg2c_inlineCallc(Builder b, Scope *scope)
{
	// TODO: utility for creating control flow in the middle of blocks

	bool changed = false;

	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		changed = inlineCallc(b, f, cg2c_funcTerminator(f), scope, re) || changed;
	}

	if (changed) {
		changed = cg2c_scopeRewrite(b, scope, re) || changed;
	}

	return changed;
}
