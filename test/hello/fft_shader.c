#include "fft.h"

/*
static float complex
powfW_N(float N, float k)
{
	double twoPi = 2.0 * M_PI;
	return cexpf(-I * (float)twoPi * k / N);
}
*/

uint32_t
bitrev32(uint32_t x)
{
	x = (x << 16) | (x >> 16);
	x = ((x << 8) & 0xff00ff00) | ((x >> 8) & 0x00ff00ff);
	x = ((x << 4) & 0xf0f0f0f0) | ((x >> 4) & 0x0f0f0f0f);
	x = ((x << 2) & 0xcccccccc) | ((x >> 2) & 0x33333333);
	x = ((x << 1) & 0xaaaaaaaa) | ((x >> 1) & 0x55555555);
	return x;
}

#ifdef __cg2c__

float complex
cexpf_i(float x)
{
	struct { float real, imag; } ω;
	ω.real = __cosf(x);
	ω.imag = __sinf(x);
	return *(float complex*)&ω;
}

#endif

/*
void
butterflies(
	void *io,
	float complex (*load)(void*, uint32_t),
	void (*store)(void*, uint32_t, float complex),
	int lgN,
	int s,
	float complex (*cexpf)(float complex),
	uint32_t i)
*/
void
butterflies(FFTParams *arg)
{
	uint32_t n = (uint32_t)1 << arg->lgN;
	uint32_t m = (uint32_t)1 << arg->s;

	// if (arg->i >= n/2)
	//	return;

	uint32_t k = 2 * (arg->i & ~(m/2 - 1));
	uint32_t j = arg->i & (m/2 - 1);

	float x = -2 * ((float)31459 / 10000) * (float)j / (float)m;
	float complex ω;
#ifndef __cg2c__
	ω = cexpf(-I * x);
#else
	ω = cexpf_i(x);
#endif

	uint32_t kj = k + j;
	if ((arg->flags & FFTBitReverseIndex) != 0)
		kj = bitrev32(kj) >> (32 - arg->lgN);

	uint32_t kjm2 = k + j + m/2;
	if ((arg->flags & FFTBitReverseIndex) != 0)
		kjm2 = bitrev32(kjm2) >> (32 - arg->lgN);

	// float complex u = arg->x[k+j];
	// float complex t = ω * arg->x[k+j+m/2];

	float complex u = arg->x[kj];
	float complex t = ω * arg->x[kjm2];

	arg->X[k+j] = u + t;
	arg->X[k+j+m/2] = u - t;
}

void
scanAvg(struct {
	float *in;
	size_t inStride;
	float *out;
	size_t outStride;
	size_t n;
	size_t i;
} *arg)
{
	float x = arg->in[arg->i * arg->inStride];
	float y = arg->in[(arg->i + 1) * arg->inStride];
	arg->out[arg->i * arg->outStride] = (x + y) / 2;
}
