#include "all.h"

static void*
memdup(void *p, size_t size)
{
	void *q = calloc(1, size);
	if (q != NULL && size > 0)
		memmove(q, p, size);
	return q;
}

struct Cg2cBlob {
	void   *data;
	size_t len;
};

Cg2cBlob*
cg2c_export(void *data, size_t len, const Cg2cAllocFuncs *_unused)
{
	assert(_unused == NULL);

	Cg2cBlob *blob = calloc(1, sizeof *blob);
	if (blob == NULL)
		goto fail;
	if ((blob->data = memdup(data, len)) == NULL)
		goto fail_memdup_data;
	blob->len = len;
	return blob;

fail_memdup_data:
	free(blob);
fail:
	return NULL;
}

void
cg2cFreeBlob(Cg2cBlob *blob, const Cg2cAllocFuncs *_unused)
{
	assert(_unused == NULL);

	free(blob->data);
	free(blob);
}

const void* cg2cBlobData(const Cg2cBlob *blob) { return blob->data; }

size_t cg2cBlobLen(const Cg2cBlob *blob) { return blob->len; }
