#include "all.h"

#include "map_type.h"

// ceil(x / y)
#define ceilDiv(x, y) (((x) + (y) - 1) / (y))

enum {
	sizeBits =
		~(size_t)0 == 0xffffffffffffffff ? 64 :
		~(size_t)0 == 0xffffffff ? 32 :
		0,
};

struct Map {
	size_t hash0;
	size_t load;
	int    b;
	size_t *occupied;
	char   *keys;
	char   *elems;

	const MapType *_t; // for diagnostics
};

static size_t maplook(const MapType*, const Map*, const void*, bool*);
static void   mapgrow(const MapType*, Map*, GC*);

static size_t
mapcap(const Map *h)
{
	if (h == NULL || h->occupied == NULL)
		return 0;
	return (size_t)1 << h->b;
}

static size_t
maplen(const Map *h)
{
	size_t n = 0;
	for (size_t i = 0; i < mapcap(h); i++) {
		if ((h->occupied[i/sizeBits] & ((size_t)1 << (i % sizeBits))) == 0)
			continue;
		n++;
	}
	return n;
}

Map*
cg2c_mkmap(const MapType *t, GC *gc)
{
	Map *h = cg2c_malloc(sizeof *h, gc);
	h->_t = t;
	return h;
}

const void*
cg2c_mapaccess1(const MapType *t, const Map *h, const void *key)
{
	bool pres;
	return cg2c_mapaccess2(t, h, key, &pres);
}

const void*
cg2c_mapaccess2(const MapType *t, const Map *h, const void *key, bool *pres)
{
	if (h != NULL && h->_t != t)
		assert(!"map type mismatch");

	size_t i = maplook(t, h, key, pres);
	if (!*pres)
		return t->zero;
	return h->elems + i*t->elemsize;
}

bool
cg2c_mapcontains(const MapType *t, const Map *h, const void *key)
{
	bool pres;
	cg2c_mapaccess2(t, h, key, &pres);
	return pres;
}

void*
cg2c_mapassign(const MapType *t, Map *h, const void *key, GC *gc)
{
	if (h == NULL)
		assert(!"assignment to NULL map");
	if (h->_t != t)
		assert(!"map type mismatch");

	if (h->load >= mapcap(h)/2)
		mapgrow(t, h, gc);
	assert(h->load < mapcap(h)/2);

	bool pres;
	size_t i = maplook(t, h, key, &pres);
	if (!pres) {
		h->load++;
		h->occupied[i/sizeBits] |= (size_t)1 << (i % sizeBits);
		memmove(h->keys+i*t->keysize, key, t->keysize);
	}
	void *e = h->elems + i*t->elemsize;
	memset(e, 0, t->elemsize);
	return e;
}

size_t
maplook(const MapType *t, const Map *h, const void *key, bool *pres)
{
	*pres = false;

	if (mapcap(h) == 0)
		return 0;

	size_t m = mapcap(h) - 1;
	size_t i_0 = t->hasher(key, h->hash0) & m;
	size_t i = i_0;
	do {
		if ((h->occupied[i/sizeBits] & ((size_t)1 << (i % sizeBits))) == 0)
			return i;
		if (!t->equal(h->keys+i*t->keysize, key))
			continue;
		break;
	} while ((i = (i + 1) & m) != (i_0 & m));

	*pres = true;
	return i;
}

void
mapgrow(const MapType *t, Map *h, GC *gc)
{
	size_t elemCnt = maplen(h);

	// Choose b such that the load factor after grow will be less than a
	// half. Anything less than 3 will grow on each assign
	int b = 3;
	while (elemCnt >= ((size_t)1<<b)/2)
		b++;

	size_t n = (size_t)1 << b;
	Map *tmp = &(Map) {
		//.hash0    = 42,
		.b        = b,
		.occupied = cg2c_malloc(ceilDiv(n, sizeBits)*sizeof tmp->occupied[0], gc),
		.keys     = cg2c_malloc(n*t->keysize, gc),
		.elems    = cg2c_malloc(n*t->elemsize, gc),
		._t       = h->_t,
	};
	for (size_t i = 0; i < mapcap(h); i++) {
		if ((h->occupied[i/sizeBits] & ((size_t)1 << (i % sizeBits))) == 0)
			continue;
		void *key = h->keys + i*t->keysize;
		bool pres;
		size_t j = maplook(t, tmp, key, &pres);
		tmp->load++;
		tmp->occupied[j/sizeBits] |= (size_t)1 << (j % sizeBits);
		memmove(tmp->keys+j*t->keysize, key, t->keysize);
		memmove(tmp->elems+j*t->elemsize, h->elems+i*t->elemsize, t->elemsize);
	}
	*h = *tmp;
}
