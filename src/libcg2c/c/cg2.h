typedef enum COp      COp;
typedef struct CExpr  CExpr;
typedef struct CField CField;
typedef struct CStmt  CStmt;
typedef struct CSym   CSym;
typedef struct CType  CType;
typedef struct Node   Node;

// TODO: try out Plan 9 CC style parser where there's just Node that does everything

enum {
	TXXX,

	// Boolean, integer and floating point kinds are arranged in order of
	// increasing precedence when performing the "usual arithmetic
	// conversions".

	TBOOL,

	// TODO: maybe replace TINT* and TUINT* types with one, and add a type
	// parameter for bit width. This would let work nicer

	TINT8,
	TUINT8,
	TINT16,
	TUINT16,
	TINT32,
	TUINT32,
	TINT64,
	TUINT64,

	TFLOAT16,
	TFLOAT32,
	TFLOAT64,

	TCOMPLEX,

	TVEC2,
	TVEC3,
	TVEC4,
	// TODO: maybe TMAT, but Clang's matrix_type is stinky

	TPTR,   // elem*
	TFUNC,  // elem(fields...) if !ddd, elem(fields..., ...) otherwise
	TARRAY, // elem[len]

	TSTRUCT, // struct tag { fields... }
	TUNION,  // union tag { fields... }

	TVOID, // void

	NTYPE,
};

enum {
	GXXX      = 0,
	GCONST    = 1 << 0,
	GVOLATILE = 1 << 1,
	GRESTRICT = 1 << 2,
	GATOMIC   = 1 << 4,
};

struct CField {
	String  name;
	CType   *type;
	int64_t off;
	// TODO: we might want to add an align field too

	Pos pos;
};

struct CType {
	int     kind;
	int     garb;
	int64_t size, align;
	CType   *elem;
	Slice   fields; // of CField
	bool    ddd;

	Pos pos;
};

enum COp {
	OXXX,

	OBAD,

	ONAME, // TODO: rename

	OCONST, // OLITERAL

	OAS,         // x = y
	OASOP,       // x asOp= y
	OPOSTINCDEC, // x++ or x--
	OPREINCDEC,  // ++x or --x

	OCALL,

	OCONV, // (type)x

	ODOT,   // x.field
	OARROW, // x->field

	ODOTSHUF,   // v.xyzw
	OARROWSHUF, // v->xyzw

	OADD, // x + y
	OSUB, // x - y
	OMUL, // x * y
	ODIV, // x / y
	OMOD, // x % y

	OAND, // x & y
	OOR,  // x | y
	OXOR, // x ^ y

	OLSH, // x << y
	ORSH, // x >> y

	OEQ,  // x == y
	ONE,  // x != y
	OLT,  // x < y
	OLE,  // x <= y
	OGT,  // x > y
	OGE,  // x >= y

	OANDAND, // x && y
	OOROR,   // x || y

	ONOT,  // !x
	OCOM,  // ~x
	OPLUS, // +x
	ONEG,  // -x

	OCOND, // cond ? x : y

	OINDEX, // x[y]
	ODEREF, // *x
	OADDR,  // &x

	OCOMMA, // x, y

	OPAREN, // (x)

	ODCLFUNC,
	ODCLLIST, // rename to ODCLVARS
	ODCL, // get rid of this node, ODCLLIST is enough

	ONOP,
	OBLOCK,
	OIF,
	OSWITCH,
	OFOR,
	OBREAK,
	OCONTINUE,
	OGOTO,
	ORETURN,

	OEND,
};

struct Node {
	int   op;
	CType *type;
	int   asOp;
	int shuf[4];
	size_t field;
	int64_t asd;
	CSym  *sym;
	void  *aux;
	Node *x, *y;
	Slice args;  // of Node*
	Slice stmts; // of Node*

	Pos  pos;
	bool implicit;
};

/*
struct CNode {
	COp   op;
	CType *type;
	int8_t shuffle[4];
	int64_t auxInt;
	CNode *x, *y;
	CNode *cond;
	CNode *init;
	CNode *cond;
	CNode *step;
	CNode *then; // also body
	CNode *else;
	Slice stmts; // OBLOCK
	Slice args; // function call

	Pos pos;
};
*/

enum {
	CXXX,
	CTYPEDEF,
	CCONST,
	CGLOBL,
	CAUTO,
	CPARAM,
	CEXTERN,
};

// TODO: rename to something better like Name?
struct CSym {
	String  name;
	CType   *type;
	int     class;
	Slice   args; // of CSym*
	Node    *def; // this should only be used for certain storage classes
	int64_t defInt;
};

enum {
	LXxx,

	LEOF,

	LName,   // name
	LNumber, // numeric constant
	LChar_,  // character constant
	LString, // string literal

	LOp,       // op
	LStar,     // *
	LAssign,   // =
	LAssignOp, // op=
	LOpOp,     // opop

	LLParen,    // (
	LLBrack,    // [
	LLBrace,    // {
	LRParen,    // )
	LRBrack,    // ]
	LRBrace,    // }
	LComma,     // ,
	LSemi,      // ;
	LArrow,     // ->
	LDot,       // .
	LDotDotDot, // ...
	LColon,     // :
	LQuestion,  // ?

	// By convention, extension keywords that appear only in storage class
	// specifiers (__shared and __thread) do not have an alias with two
	// trailing underscores.

	LAlignas,      // _Alignas
	LAlignof,      // _Alignof, __alignof, __alignof__
	LAtomic,       // _Atomic
	LAuto,         // auto
	LBool,         // _Bool
	LBreak,        // break
	LCase,         // case
	LChar,         // char
	LComplex,      // _Complex, __complex, __complex__
	LConst,        // const
	LContinue,     // continue
	LDefault,      // default
	LDo,           // do
	LDouble,       // double
	LElse,         // else
	LEnum,         // enum
	LExtern,       // extern
	LFloat,        // float
	LFloat16,      // _Float16
	LFor,          // for
	LGeneric,      // _Generic
	LGoto,         // goto
	LIf,           // if
	LInline,       // inline, __inline, __inline__
	LInt,          // int
	LLong,         // long
	LRestrict,     // restrict, __restrict, __restrict__
	LReturn,       // return
	LShared,       // __shared
	LShort,        // short
	LSigned,       // signed
	LSizeof,       // sizeof
	LStatic,       // static
	LStaticAssert, // _Static_assert
	LStruct,       // struct
	LSwitch,       // switch
	LThread,       // _Thread_local, __thread
	LTypedef,      // typedef
	LUnion,        // union
	LUnsigned,     // unsigned
	LVoid,         // void
	LVolatile,     // volatile, __volatile, __volatile__
	LWhile,        // while

	NToken,
};

// TODO: use Operator prefix
enum {
	KXxx,

	KNot, // !
	KCom, // ~

	KOrOr, // ||

	KAndAnd, // &&

	KOr, // |

	KXor, // ^

	KAnd, // &

	KEq, // ==
	KNe, // !=

	KLt, // <
	KLe, // <=
	KGt, // >
	KGe, // >=

	KShl, // <<
	KShr, // >>

	KAdd, // +
	KSub, // -

	KMul, // *
	KDiv, // /
	KRem, // %

	NOperator,
};

typedef struct Kwtab Kwtab;
struct Kwtab {
	char *name;
	int  token;
};

extern Kwtab  cg2c_kwtab[];
extern size_t cg2c_nkwtab;

typedef struct CScope CScope;
struct CScope {
	Map *names; // String → CSym*
	Map *tags;  // String → CType*

	// diagnostics

	// Slice usecheck; // of CSym*
};

// rename to Parser
typedef struct Noder Noder;
struct Noder {
	Compile *ctxt;

	PosBase *posBase;

	CType *types[NTYPE];
	// int typeIntptr;

	Slice scopes; // of CScope

	Slice syms; // of CSym*

	bool syntaxErrored;

	// Scanner

	Map *keywords; // String → int

	String  in;
	size_t  off, roff;
	int32_t line, col;
	Rune    ch;

	// Token after cg2c_scan

	Pos    pos;
	int    tok;
	String lit; // for LName, LNumber, LChar_ and LString
	int    op;  // for LOp, LAssignOp and LOpOp
};

void cg2c_scan(Noder*);

// TODO: these should be its own thing (CPP)

// TODO: rename to getch and peekch respectively? or getrune and peekrune

void cg2c_getc(Noder*);
Rune cg2c_peekc(Noder*);

Sym*    cg2c_gen(Compile *ctxt, CSym *s);
int64_t cg2c_ceval(Compile *ctxt, Node *n);
