#include "hello.h"

enum {
	coherent = true,
};

enum {
	BUFFER_USAGE_ALL =
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT |
		VK_BUFFER_USAGE_TRANSFER_DST_BIT |
		//VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT |
		//VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT |
		VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT |
		VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
		VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
		VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT |
		VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
		//VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT |
		//VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT |
		//VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT |
		//VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
		//VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
		//VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR,
};

typedef struct Range Range;
struct Range {
	VkBuffer        vkBuffer;
	VkDeviceMemory  memory;
	VkDeviceSize    size;
	VkDeviceAddress devicePtr;
	void            *hostPtr;
};

static Range *linear[1000];
static size_t rangeseq;

static int32_t findMemoryTypeIndex(uint32_t memoryTypeBits);

static void _Noreturn // TODO: use [[noreturn]]
fatalDevicePtr(const char *f, VkDeviceAddress ptr)
{
	fprintf(stderr, "%s: 0x%llx was previously freed or not allocated\n", f, (unsigned long long)ptr);
	abort();
}

static VkDeviceAddress devicemalloc(VkDeviceSize size);
static void            devicefree(VkDeviceAddress ptr);

void*
deviceMalloc(size_t size, uint32_t flags)
{
	assert(flags == DEVICE_MALLOC_SYSMEM);
	return (void*)devicemalloc((VkDeviceSize)size);
}

void
deviceFree(void *p)
{
	devicefree((VkDeviceAddress)p);
}

VkDeviceAddress
devicemalloc(VkDeviceSize size)
{
	VkResult r;

	VkBuffer buffer;
	if ((r = fns.vkCreateBuffer(device, &(VkBufferCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = size,
		.usage = BUFFER_USAGE_ALL,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	}, NULL, &buffer)) != VK_SUCCESS)
		fatalvk("vkCreateBuffer: ", r);

	VkMemoryRequirements2 reqs = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2,
	};
	fns.vkGetBufferMemoryRequirements2(device, &(VkBufferMemoryRequirementsInfo2) {
		.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2,
		.buffer = buffer,
	}, &reqs);

	// TODO: check return value of findMemoryTypeIndex

	VkDeviceMemory memory;
	if ((r = fns.vkAllocateMemory(device, &(VkMemoryAllocateInfo) {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = &(VkMemoryAllocateFlagsInfo) {
			.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
			.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT,
		},
		.allocationSize = reqs.memoryRequirements.size,
		.memoryTypeIndex = (uint32_t)findMemoryTypeIndex(reqs.memoryRequirements.memoryTypeBits),
	}, NULL, &memory)) != VK_SUCCESS)
		fatalvk("vkAllocateMemory: ", r);

	if ((r = fns.vkBindBufferMemory2(device, 1, &(VkBindBufferMemoryInfo) {
		.sType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO,
		.buffer = buffer,
		.memory = memory,
		.memoryOffset = 0,
	})) != VK_SUCCESS)
		fatalvk("vkBindBufferMemory2: ", r);

	VkDeviceAddress devicePtr = fns.vkGetBufferDeviceAddressKHR(device, &(VkBufferDeviceAddressInfo) {
		.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
		.buffer = buffer,
	});

	// Cg2 uses bits 62:63 for tag. TODO: we might want to do some more
	// elaborate sanity checks.
	assert(devicePtr+reqs.memoryRequirements.size < (VkDeviceAddress)1<<62);

	void *hostPtr;
	if ((r = fns.vkMapMemory(device, memory, 0, VK_WHOLE_SIZE, 0 /* flags */, &hostPtr)) != VK_SUCCESS)
		fatalvk("vkMapMemory: ", r);

	Range *ra = emalloc(sizeof *ra);
	ra->vkBuffer = buffer;
	ra->memory = memory;
	ra->size = size;
	ra->devicePtr = devicePtr;
	ra->hostPtr = hostPtr;

	assert(rangeseq < nelem(linear));
	linear[rangeseq++] = ra;

	fprintf(stderr, "devicemalloc: allocated %lld bytes at 0x%llx 0x%llx\n", (long long)size, (unsigned long long)devicePtr, (unsigned long long)hostPtr);

	return devicePtr;
}

void
devicefree(VkDeviceAddress ptr)
{
	fprintf(stderr, "devicefree: 0x%llx\n", (unsigned long long)ptr);
}

void*
device2hostptr(VkDeviceAddress ptr)
{
	for (size_t i = 0; i < rangeseq; i++) {
		Range *ra = linear[i];
		if (ra->devicePtr <= ptr && ptr < ra->devicePtr+ra->size)
			return (char*)ra->hostPtr + (ptr - ra->devicePtr);
	}

	fatalDevicePtr("device2hostptr", ptr);
}

BufferAndOffset
deviceptrinfo(VkDeviceAddress ptr)
{
	for (size_t i = 0; i < rangeseq; i++) {
		Range *ra = linear[i];
		if (ra->devicePtr <= ptr && ptr < ra->devicePtr+ra->size) {
			return (BufferAndOffset) {
				.memory = ra->memory,
				.buffer = ra->vkBuffer,
				.offset = 0,
			};
		}
	}

	fatalDevicePtr("deviceptrinfo", ptr);
}

void
createImage(const VkImageCreateInfo *createInfo, Image **out)
{
	VkResult r;

	Image *image = emalloc(sizeof *image);

	if ((r = fns.vkCreateImage(device, createInfo, NULL, &image->vk)) != VK_SUCCESS)
		fatalvk("vkCreateImage: ", r);

	image->layout = createInfo->initialLayout;

	VkMemoryRequirements2 reqs = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2,
	};
	fns.vkGetImageMemoryRequirements2(device, &(VkImageMemoryRequirementsInfo2) {
		.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2,
		.image = image->vk,
	}, &reqs);

	// TODO: alignment requirement might be stricter than what allocator of
	// linear buffers is working with
	VkDeviceAddress ptr = devicemalloc(reqs.memoryRequirements.size);
	BufferAndOffset ptrInfo = deviceptrinfo(ptr);

	if ((r = fns.vkBindImageMemory2(device, 1, &(VkBindImageMemoryInfo) {
		.sType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO,
		.image = image->vk,
		.memory = ptrInfo.memory,
		.memoryOffset = ptrInfo.offset,
	})) != VK_SUCCESS)
		fatalvk("vkBindImageMemory2: ", r);

	*out = image;
}

int32_t
findMemoryTypeIndex(uint32_t memoryTypeBits)
{
	VkPhysicalDeviceMemoryProperties2 memProps = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2,
	};
	fns.vkGetPhysicalDeviceMemoryProperties2(physicalDevice, &memProps);

	uint32_t try[] = {
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
	};

	for (uint32_t j = 0; j < nelem(try); j++) {
		VkMemoryPropertyFlags wantProps = try[j];
		if (coherent && (wantProps & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0)
			wantProps |= VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		for (uint32_t i = 0; i < memProps.memoryProperties.memoryTypeCount; i++) {
			if ((memoryTypeBits & ((uint32_t)1 << i)) != 0 &&
				(memProps.memoryProperties.memoryTypes[i].propertyFlags & wantProps) == wantProps)
				return (int32_t)i;
		}
	}

	assert(0);
	return -1;
}
