#ifndef CG2C_H
#define CG2C_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

typedef struct Cg2cAllocFuncs Cg2cAllocFuncs;
typedef struct Cg2cBlob       Cg2cBlob;

struct Cg2cAllocFuncs {
	void* (*callocFunc)(size_t, size_t, void*);
	void  (*freeFunc)(void*, void*);
	void* userData;
};

const void* cg2cBlobData(const Cg2cBlob*);
size_t      cg2cBlobLen(const Cg2cBlob*);
void        cg2cFreeBlob(Cg2cBlob*, const Cg2cAllocFuncs*);

#ifdef __cplusplus
}
#endif

#endif
