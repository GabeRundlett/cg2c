#include "all.h"

/*
typedef struct Thunk Thunk;
struct Thunk {
	Value  *v;  // value
	size_t arg; // next arg we will check
};

static Slice
appendThunkIfNotVisited(Slice s, Value *v, Map *visited, GC *gc)
{
	if (!cg2c_mapcontains(cg2c_ptr_set, visited, &v))
		s = cg2c_slappend(sizeof(Thunk), s, &(Thunk) {v, 0}, gc);
	return s;
}

// TODO: not sure if we should make visited a map (compatibility with replace)
// or a set (less memory)
void
cg2c_foreach(Compile *ctxt, Map *visited, Value *v, void (*f)(Compile*, Value*, void*), void *etc)
{
	Slice stack = appendThunkIfNotVisited(NULL_SLICE, v, visited, ctxt->gc);

	while (cg2c_sllen(stack) > 0) {
		Thunk *thunk = &(*(Thunk*)cg2c_slindex(sizeof(Thunk), stack, cg2c_sllen(stack)-1));

		if (thunk->arg < cg2c_valueNArg(thunk->v)) {
			stack = appendThunkIfNotVisited(stack, cg2c_valueArg(thunk->v, thunk->arg), visited, ctxt->gc);
			thunk->arg++;
		} else {
			stack = cg2c_slice(sizeof(Thunk), stack, 0, cg2c_sllen(stack)-1);
			f(ctxt, thunk->v, etc);
			cg2c_mapassign(cg2c_ptr_set, visited, &thunk->v, ctxt->gc);
		}
	}
}
*/

void
cg2c_foreach(Compile *ctxt, void (*f)(Compile*, Value*, void*), void *etc, Value *v, Map *visited)
{
	GC *gc = ctxt->gc;

	if (cg2c_mapcontains(cg2c_ptr_set, visited, &v))
		return;

	cg2c_mapassign(cg2c_ptr_set, visited, &v, gc);

	for (size_t i = 0; i < cg2c_valueNArg(v); i++)
		cg2c_foreach(ctxt, f, etc, cg2c_valueArg(v, i), visited);

	f(ctxt, v, etc);
}
