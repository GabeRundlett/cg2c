import 'package:flutter/painting.dart';

extension ColorToHexString on Color {
  String toHexString() {
    var argb = value;
    assert(0 <= argb && argb <= 0xffffffff);
    var rgba = ((argb & 0x00ffffff) << 8) | (argb >> 24);
    return '#${rgba.toRadixString(16).padLeft(8, '0')}';
  }
}
