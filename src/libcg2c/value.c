#include "all.h"

Func*
cg2c_valueAuxFunc(Value *v)
{
	assert(v->op == OpFunc);
	return v->aux;
}

int64_t
cg2c_valueAuxInt(Value *v)
{
	assert(v->op == OpConst);
	assert(0);
	return 0;
}

uint64_t
cg2c_valueAuxUint(Value *v)
{
	assert(v->op == OpConst);
	return *(uint64_t*)v->aux;
}

size_t cg2c_valueNArg(Value *v) { return cg2c_sllen(v->args); }
Value* cg2c_valueArg(Value *v, size_t i) { return *(Value**)cg2c_slindex(sizeof(Value*), v->args, i); }

// Like cg2c_valueArg, but asserts that the resulting value is an OpFunc, and
// returns the func that describes.
Func* cg2c_valueArgFunc(Value *v, size_t i) { return cg2c_valueAuxFunc(cg2c_valueArg(v, i)); }
