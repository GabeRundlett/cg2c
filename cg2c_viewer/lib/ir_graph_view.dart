import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:xml/xml.dart';

import 'color_to_hex_string.dart';
import 'dot.dart';
import 'graphviz.dart';
import 'ir_view.dart';
import 'op_info.dart';
import 'ops.dart';

class IRView2 extends StatelessWidget {
  // TODO: we'd perform clusterization in the constructor
  const IRView2({super.key, required this.values, required this.blocks});

  static const String bullet = '•';

  final List<Value> values;
  final List<Block> blocks;

  @override
  Widget build(BuildContext context) {
    // TODO: make a dedicated theme object for our use so that we can break
    // dependency on material Theme
    var colorScheme = Theme.of(context).colorScheme;

    var settings = Config.of(context);

    var valueHues = ValueHues.of(context);

    var onSurfaceHex = colorScheme.onSurface.toHexString();

    Attrs defaultDotStyle = {
      'color': onSurfaceHex,
      'fontcolor': onSurfaceHex,
      'fontname': settings.monospaceFontFamily,
    };

    var dot = <Stmt>[
      DefaultAttr(wat: Wat.graph, attrs: {
        ...defaultDotStyle,
        'bgcolor': 'transparent',
        'newrank': 'true',
      }),
      DefaultAttr(wat: Wat.node, attrs: {
        ...defaultDotStyle,
        'shape': 'rectangle',
        'width': '0',
        'height': '0',
        'margin': '0.04,0.02',
      }),
      DefaultAttr(wat: Wat.edge, attrs: {
        ...defaultDotStyle,
      }),
    ];

    for (var v in values) {
      if (v.op == Op.Const) continue;

      var vColor = valueHues.foregroundColor(v.id, colorScheme.onSurface);

      var label = <XmlElement>[];

      label.add(XmlElement(XmlName('td'), [], [XmlText(v.op.name)]));

      for (var i = 0; i < v.args.length; i++) {
        var a = v.args[i];

        XmlNode xmlNode;
        switch (a.op) {
          case Op.Const:
            xmlNode =
                XmlText(formatBits(a.auxInt!, 32, opHints[v.op] ?? Hint.int));

            break;

          default:
            var aColor = valueHues.foregroundColor(a.id, colorScheme.onSurface);

            dot.add(Edge(
              src: EdgeEndpoint(id: a.id),
              dst: EdgeEndpoint(id: v.id, port: 'a$i'),
              attrs: {
                if (aColor != null) 'color': aColor.toHexString(),
              },
            ));

            if (aColor != null) {
              xmlNode = XmlElement(
                  XmlName('font'),
                  [XmlAttribute(XmlName('color'), aColor.toHexString())],
                  [XmlText(bullet)]);
            } else {
              xmlNode = XmlText(bullet);
            }

            break;
        }

        label.add(XmlElement(
            XmlName('td'), [XmlAttribute(XmlName('port'), 'a$i')], [xmlNode]));
      }

      dot.add(Node(
        id: v.id,
        label: HtmlLikeLabel(XmlElement(XmlName('table'), [
          XmlAttribute(XmlName('border'), '0'),
          XmlAttribute(XmlName('cellborder'), '0'),
          XmlAttribute(XmlName('cellspacing'), '0'),
        ], [
          XmlElement(XmlName('tr'), [], label),
        ])),
        attrs: {
          if (vColor != null) 'color': vColor.toHexString(),
        },
      ));
    }

    // TODO: nicer error and placeholders

    return FutureBuilder(
      future: graphviz(dot: dotEncode(dot)),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          var error = snapshot.error!;
          var comment = error is ProcessException
              ? 'Ensure Graphviz is installed and available in the \$PATH'
              : 'There might be an issue with your Graphviz installation or a'
                  ' bug in Cg2c Dump Viewer';
          return Text(
            '$error\n'
            '\n'
            '$comment',
          );
        }

        if (snapshot.hasData) {
          var pageStorage = PageStorage.of(context);
          var transformationController = TransformationController(
              pageStorage.readState(context) as Matrix4?);
          return InteractiveViewer(
            maxScale: double.infinity,
            minScale: 1.0,
            transformationController: transformationController,
            onInteractionEnd: (details) {
              pageStorage.writeState(context, transformationController.value);
            },
            child: SvgPicture.string(
              snapshot.data!,
              placeholderBuilder: (context) => const Text('???'),
            ),
          );
        }

        return const Text('Laying out...');
      },
    );
  }
}
