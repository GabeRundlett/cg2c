#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <vulkan/vulkan_core.h>

#include <cg2c/cg2c.h>
#include <cg2c/cg2c_vulkan.h>

#define nelem(x) (sizeof(x)/sizeof((x)[0]))

char *entry = "";
char *outfile = "a.spv";
char **infiles;

typedef struct Flag Flag;
struct Flag {
	char *name;
	char *usage;
	char **p;
} flags[] = {
	{"-e", "entry point", &entry},
	{"-o", "place the output into `file`", &outfile},
};

static void* erealloc(void *p, size_t size);

static char*
ereadfile(const char *name)
{
	FILE *f;

	if ((f = fopen(name, "rb")) == NULL) // we deal with the CR LF brain damage by ourselves
		perror("fopen");

	char *p = NULL;
	size_t n = 0;
	size_t m = 0;
	while (!feof(f) && !ferror(f)) {
		if (n == m) {
			if (m > 100000000)
				abort();
			m = 2*m + 1;
			p = erealloc(p, m*sizeof p[0]);
		}
		n += fread(p+n, 1, m-n, f);
	}
	if (ferror(f))
		abort();

	fclose(f); // don't care about fclose failure, we're reading

	p[n] = 0;
	return p;
}

void*
erealloc(void *p, size_t size)
{
	void *q = realloc(p, size);
	if (q == NULL && size == 0)
		q = malloc(1);
	if (q == NULL) {
		fprintf(stderr, "erealloc: out of memory\n");
		abort();
	}
	return q;
}

static const char*
readFile(const char *filename, void *ud)
{
	return ereadfile(filename);
}

void
usage(void)
{
	for (Flag *f = flags; f != NULL && f < flags+nelem(flags); f++) {
		fprintf(stderr, "  %-24s %s\n", f->name, f->usage);
	}
}

static void
diag(const char *msg, int severity, void *ud)
{
	fprintf(stderr, "%s\n", msg);
	fflush(stderr);
}

int
main(int argc, char **argv)
{
	while (argv++, argc-- > 1) {
		char *a = *argv;

		if (a[0] != '-' || strcmp(a, "-") == 0)
			break; // or continue
		if (strcmp(a, "--") == 0) {
			argv++, argc--; // consume "--"
			break;
		}

		Flag *f = flags;
		while (f != NULL && f < flags+nelem(flags) && strcmp(a, f->name) != 0)
			f++;
		if (f == flags+nelem(flags)) {
			if (strcmp(a, "--help") == 0 || strcmp(a, "-h") == 0) {
				usage();
				exit(0);
			}
			fprintf(stderr, "flag provided but not defined: %s\n", a);
			exit(2);
		}

		if (argv++, argc-- <= 1) {
			fprintf(stderr, "flag needs an argument: %s\n", a);
			exit(2);
		}
		*f->p = *argv;
	}

	const char *const *files = (const char* const*)argv;

	size_t nfile = 0;
	while (files[nfile] != NULL)
		nfile++;

	Cg2cInputVk inputs[] = {};

	Cg2cBlob *a;
	if (cg2cCompileVk(&(Cg2cCompileInfoVk) {
		.func = &(Cg2cFunctionVk) {
			.stage  = VK_SHADER_STAGE_COMPUTE_BIT,
			.name   = entry,
			.inputs = inputs,
			.ninput = nelem(inputs),
			.extra = &(Cg2cComputeVk) {
				.workgroupSize = {1, 1, 1},
			},
		},
		.files    = files,
		.nfile    = nfile,
		.readFile = readFile,
		.diag     = diag,
	}, NULL, &a) < 0) {
		// Cg2c has already printed diagnostics, so we don't have to do
		// anything extra.
		exit(2);
	}

	FILE *out = fopen(outfile, "wb");
	fwrite(cg2cBlobData(a), 1, cg2cBlobLen(a), out);
	fclose(out);

	cg2cFreeBlob(a, NULL);

	return 0;
}
