import 'package:xml/xml.dart';

// TODO: use StringBuffer

abstract class Stmt {}

typedef Attrs = Map<String, String>;

abstract class Label {}

class PlainLabel implements Label {
  const PlainLabel(this.label);

  final String label;

  @override
  String toString() {
    return '"$label"';
  }
}

class HtmlLikeLabel implements Label {
  const HtmlLikeLabel(this.label);

  final XmlElement label;

  @override
  String toString() {
    return '<$label>';
  }
}

class Node implements Stmt {
  const Node({required this.id, this.label, this.attrs = const {}});

  final String id;
  final Label? label;
  final Attrs attrs;

  @override
  String toString() {
    var s = '"$id" [';
    if (attrs.isNotEmpty) {
      s += _attrsToString(attrs);
    }
    if (label != null) {
      assert(!attrs.containsKey("label"));
      if (attrs.isNotEmpty) s += ' ';
      s += 'label=$label';
    }
    return '$s]';
  }
}

enum CompassPoint { n, ne, e, se, s, sw, w, nw }

class EdgeEndpoint {
  EdgeEndpoint({required this.id, this.port, this.compassPoint});

  final String id;
  final String? port;
  final CompassPoint? compassPoint;

  @override
  String toString() {
    var s = '"$id"';
    if (port != null) s += ':"$port"';
    if (compassPoint != null) s += ':${compassPoint!.name}';
    return s;
  }
}

class Edge implements Stmt {
  Edge({required this.src, required this.dst, this.attrs = const {}});

  final EdgeEndpoint src, dst;
  final Attrs attrs;

  @override
  String toString() {
    var s = '$src -> $dst';
    if (attrs.isNotEmpty) {
      s += '[${_attrsToString(attrs)}]';
    }
    return s;
  }
}

enum Wat { graph, node, edge }

class DefaultAttr implements Stmt {
  DefaultAttr({this.wat = Wat.graph, this.attrs = const {}});

  final Wat wat; // TODO: rename
  final Attrs attrs;

  @override
  String toString() => '${wat.name} [${_attrsToString(attrs)}]';
}

String dotEncode(List<Stmt> stmts) {
  var b = StringBuffer();

  b.write('digraph {\n');

  for (var stmt in stmts) {
    if (stmt is Node) {
      b.writeAll([stmt, '\n']);
    } else if (stmt is Edge) {
      b.writeAll([stmt, '\n']);
    } else if (stmt is DefaultAttr) {
      b.writeAll([stmt, '\n']);
    } else {
      assert(false);
    }
  }

  b.write('}\n');

  return b.toString();
}

String _attrsToString(Attrs attrs) {
  return attrs.entries
      .map((attr) => _attrToString(attr.key, attr.value))
      .join(' ');
}

String _attrToString(String key, String value) => '"$key"="$value"';
