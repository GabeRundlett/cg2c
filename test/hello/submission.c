#include "hello.h"

VkCommandBuffer
getcb(Submission *ba)
{
	VkResult r;

	if (ba->cmdPool == VK_NULL_HANDLE) {
		if ((r = fns.vkCreateCommandPool(device, &(VkCommandPoolCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
			.queueFamilyIndex = queueFamily,
		}, NULL, &ba->cmdPool)) != VK_SUCCESS)
			fatalvk("vkCreateCommandPool: ", r);
	}

	if (ba->cbs[ba->nextcb] == VK_NULL_HANDLE) {
		if ((r = fns.vkAllocateCommandBuffers(device, &(VkCommandBufferAllocateInfo) {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = ba->cmdPool,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			.commandBufferCount = 1,
		}, &ba->cbs[ba->nextcb])) != VK_SUCCESS)
			fatalvk("vkAllocateCommandBuffers: ", r);
	}

	VkCommandBuffer cb = ba->cbs[ba->nextcb++];

	if ((r = fns.vkBeginCommandBuffer(cb, &(VkCommandBufferBeginInfo) {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
	})) != VK_SUCCESS)
		fatalvk("vkBeginCommandBuffer: ", r);

	return cb;
}

void
execPass(Submission *ba, Pass *pass)
{
	Node *nodes = emalloc(pass->nodeCount * sizeof nodes[0]);

	for (size_t i = 0; i < pass->nodeCount; i++) {
		Node n = pass->nodes[i];

		assert(n.token > 0);

		if (n.image == VK_NULL_HANDLE) {
			assert(n.subresourceRange.aspectMask == 0);
			assert(n.subresourceRange.baseMipLevel == 0);
			assert(n.subresourceRange.levelCount == 0);
			assert(n.subresourceRange.baseArrayLayer == 0);
			assert(n.subresourceRange.layerCount == 0);
			assert(n.layout == 0);
			assert(n.discard == false);
		}

		nodes[i] = n;
	}

	if (pass->waitSem != VK_NULL_HANDLE) {
		assert(ba->waitOnSema == VK_NULL_HANDLE); // we don't support waiting on multiple semaphores just yet... also maybe submission should be split here?
		ba->waitOnSema = pass->waitSem;
	}

	if (ba->passes == NULL)
		ba->passes = emalloc(128 * sizeof ba->passes[0]);
	ba->passes[ba->passCount++] = (Pass) {
		.cb        = pass->cb,
		.nodes     = nodes,
		.nodeCount = pass->nodeCount,
	};
}

void
submit(Submission *ba)
{
	VkResult r;

	VkCommandBufferSubmitInfo cbs[128] = {};
	size_t ncb = 0;

	// TODO: don't duplicate stuff!
	{
		VkCommandBuffer cb = getcb(ba);

		Pass *nextPass = &ba->passes[0];

		VkImageMemoryBarrier2 imbs[10] = {};
		size_t nimb = 0;
		for (size_t i = 0; nextPass != NULL && i < nextPass->nodeCount; i++) {
			Node *n = &nextPass->nodes[i];
			if (n->image != VK_NULL_HANDLE) {
				assert(nimb < nelem(imbs));
				imbs[nimb++] = (VkImageMemoryBarrier2) {
					.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
					.srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
					.srcAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT | VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
					.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
					.oldLayout = !n->discard ? n->image->layout : VK_IMAGE_LAYOUT_UNDEFINED,
					.newLayout = n->layout,
					.image = n->image->vk,
					.subresourceRange = n->subresourceRange,
				};

				n->image->layout = n->layout;
			}
		}

		fns.vkCmdPipelineBarrier2KHR(cb, &(VkDependencyInfo) {
			.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
			.memoryBarrierCount = 1,
			.pMemoryBarriers = &(VkMemoryBarrier2) {
				.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2,
				.srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
				.srcAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
				.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT | VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
				.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
			},
			.imageMemoryBarrierCount = (uint32_t)nimb,
			.pImageMemoryBarriers = imbs,
		});

		if ((r = fns.vkEndCommandBuffer(cb)) != VK_SUCCESS)
			fatalvk("vkEndCommandBuffer: ", r);

		cbs[ncb++] = (VkCommandBufferSubmitInfo) {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO,
			.commandBuffer = cb,
			.deviceMask = 0b1,
		};
	}

	for (Pass *pass = ba->passes; pass != NULL && pass < ba->passes+ba->passCount; pass++) {
		VkCommandBuffer cb = pass->cb;

		Pass *nextPass = NULL;
		if ((pass-ba->passes)+1 < ba->passCount)
			nextPass = pass+1;

		VkImageMemoryBarrier2 imbs[10] = {};
		size_t nimb = 0;
		for (size_t i = 0; nextPass != NULL && i < nextPass->nodeCount; i++) {
			Node *n = &nextPass->nodes[i];
			if (n->image != VK_NULL_HANDLE) {
				assert(nimb < nelem(imbs));
				imbs[nimb++] = (VkImageMemoryBarrier2) {
					.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
					.srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
					.srcAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
					.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT | VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
					.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
					.oldLayout = !n->discard ? n->image->layout : VK_IMAGE_LAYOUT_UNDEFINED,
					.newLayout = n->layout,
					.image = n->image->vk,
					.subresourceRange = n->subresourceRange,
				};

				n->image->layout = n->layout;
			}
		}

		fns.vkCmdPipelineBarrier2KHR(cb, &(VkDependencyInfo) {
			.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
			.memoryBarrierCount = 1,
			.pMemoryBarriers = &(VkMemoryBarrier2) {
				.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2,
				.srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
				.srcAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
				.dstStageMask = VK_PIPELINE_STAGE_2_HOST_BIT | VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
				.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
			},
			.imageMemoryBarrierCount = (uint32_t)nimb,
			.pImageMemoryBarriers = imbs,
		});

		/*
		// BUG: don't skip naming the first pass
		if (pass != ba->passes && pass->label != NULL)
			fns.vkCmdEndDebugUtilsLabelEXT(cb);

		if ((pass-ba->passes)+1 < ba->passCount && (pass+1)->label != NULL) {
			fns.vkCmdBeginDebugUtilsLabelEXT(cb, &(VkDebugUtilsLabelEXT) {
				.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT,
				.pLabelName = (pass+1)->label,
			});
		}
		*/

		if ((r = fns.vkEndCommandBuffer(cb)) != VK_SUCCESS)
			fatalvk("vkEndCommandBuffer: ", r);

		cbs[ncb++] = (VkCommandBufferSubmitInfo) {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO,
			.commandBuffer = cb,
			.deviceMask = 0b1,
		};
	}

	if ((r = fns.vkQueueSubmit2KHR(queue, 1, &(VkSubmitInfo2) {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2,
		.waitSemaphoreInfoCount = ba->waitOnSema != VK_NULL_HANDLE ? 1 : 0,
		.pWaitSemaphoreInfos = (VkSemaphoreSubmitInfo[]) {
			{
				.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO,
				.semaphore = ba->waitOnSema,
				.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
			},
		},
		.commandBufferInfoCount = (uint32_t)ncb,
		.pCommandBufferInfos = cbs,
	}, VK_NULL_HANDLE /* fence */)) != VK_SUCCESS)
		fatalvk("vkQueueSubmit2: ", r);

	if ((r = fns.vkDeviceWaitIdle(device)) != VK_SUCCESS)
		fatalvk("vkDeviceWaitIdle: ", r);

	for (size_t i = 0; i < ba->passCount; i++) {
		Pass *pass = &ba->passes[i];
		free(pass->nodes);
		pass->nodes = NULL;
	}

	// TODO:

	ba->waitOnSema = VK_NULL_HANDLE;

	if (ba->cmdPool != VK_NULL_HANDLE)
		fns.vkResetCommandPool(device, ba->cmdPool, 0);
	ba->nextcb = 0;
	ba->passCount = 0;
}

static VkAccessFlags2 ACCESS_ALL_READ =
	VK_ACCESS_2_INDIRECT_COMMAND_READ_BIT |
	VK_ACCESS_2_INDEX_READ_BIT |
	VK_ACCESS_2_VERTEX_ATTRIBUTE_READ_BIT |
	VK_ACCESS_2_UNIFORM_READ_BIT |
	VK_ACCESS_2_INPUT_ATTACHMENT_READ_BIT |
	VK_ACCESS_2_SHADER_READ_BIT |
	VK_ACCESS_2_COLOR_ATTACHMENT_READ_BIT |
	VK_ACCESS_2_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
	VK_ACCESS_2_TRANSFER_READ_BIT |
	VK_ACCESS_2_HOST_READ_BIT |
	VK_ACCESS_2_MEMORY_READ_BIT |
	VK_ACCESS_2_SHADER_SAMPLED_READ_BIT |
	VK_ACCESS_2_SHADER_STORAGE_READ_BIT |
	VK_ACCESS_2_COMMAND_PREPROCESS_READ_BIT_NV |
	VK_ACCESS_2_FRAGMENT_SHADING_RATE_ATTACHMENT_READ_BIT_KHR |
	VK_ACCESS_2_ACCELERATION_STRUCTURE_READ_BIT_KHR;

static bool
writesToMemory(Node *n)
{
	return (n->accessMask & ~ACCESS_ALL_READ) != 0;
}
