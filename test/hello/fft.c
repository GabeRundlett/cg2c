#include "hello.h"

#include "fft.h"

static const char *files[] = {"fft_shader.c"}; // TODO: rename to shaderFiles

static uint32_t roundUp32(uint32_t n, uint32_t a) { return (n + a - 1) & ~(a - 1); }

static VkPipelineLayout xfliesLayout;
static VkPipeline xflies;

void
initfft(void)
{
	VkResult r;

	{
		Cg2cInputVk inputs[] = {
			{.type = CG2C_IN_PUSH_CONSTANT, .dstOffset = 0, .size = offsetof(FFTParams, i), .srcOffset = 0},
			{.type = CG2C_IN_GLOBAL_INVOCATION_ID_X, .dstOffset = offsetof(FFTParams, i), .size = sizeof((FFTParams) {}.i)},
		};

		createPipelineLayout2(&(PipelineLayoutCg2CreateInfo2) {
			.stageCount = 1,
			.stages = (PerStageCg2cInputs[]) {
				{
					.stage = VK_SHADER_STAGE_COMPUTE_BIT,
					.inputCount = nelem(inputs),
					.inputs = inputs,
				},
			},
		}, &xfliesLayout);

		createComputePipeline(&(VkComputePipelineCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
			.stage = {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = &(PipelineShaderStageCg2CreateInfo) {
					.sType  = 0,
					.inputs = inputs,
					.ninput = nelem(inputs),
					.extra = &(Cg2cComputeVk) {
						.workgroupSize = {256, 1, 1},
					},
					.files = files,
					.nfile = nelem(files),
				},
				.stage = VK_SHADER_STAGE_COMPUTE_BIT,
				.pName = "butterflies",
			},
			.layout = xfliesLayout,
		}, &xflies);
	}
}

void
doFFT(Submission *ba, const DoFFT *info)
{
	int N = 1 << info->lgN;

	assert(N % 256 == 0);

	for (int s = 1; s <= info->lgN; s++) {
		Node nodes[] = {
			{
				.token = 42,
				.stageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT,
				.accessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
			},
		};

		VkCommandBuffer cb = getcb(ba);

		fns.vkCmdBindDescriptorSets(cb,
			VK_PIPELINE_BIND_POINT_COMPUTE,
			xfliesLayout,
			0,
			1, (VkDescriptorSet[]) {
				bindless,
			},
			0, NULL);

		fns.vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_COMPUTE, xflies);

		FFTParams params = {
			.x     = s == 1 ? info->src : info->dst,
			.X     = info->dst,
			.lgN   = info->lgN,
			.s     = s,
			.flags = s == 1 ? FFTBitReverseIndex : 0, // TODO: specialize things with special flags
		};
		fns.vkCmdPushConstants(cb, xfliesLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, offsetof(FFTParams, i), &params);
		fns.vkCmdDispatch(cb, roundUp32(N/2, 256) / 256, 1, 1);

		execPass(ba, &(Pass) {
			.cb = cb,
			.nodes = nodes,
			.nodeCount = nelem(nodes),
			.label = "fft",
		});
	}
}
