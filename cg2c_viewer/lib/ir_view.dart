import 'dart:typed_data';

import 'package:flutter/widgets.dart';

import 'ops.dart';
import 'op_info.dart';

// Keep in sync with ../src/libcg2c/all.h:Kind
enum Kind {
  bool,
  int8,
  int16,
  int32,
  int64,
  ptr,
  func,
  mem,
  rmem,
  noret,
  tuple,
}

class Type {
  final Kind kind;
  final List<Type>? elems;

  const Type({required this.kind, this.elems});

  @override
  String toString() {
    switch (kind) {
      case Kind.func:
        var param = elems![0];
        var result = elems![1];
        return 'func $param $result';

      case Kind.tuple:
        return '(${elems!.map((e) => '$e').join(', ')})';

      default:
        return kind.name;
    }
  }
}

class Pos {
  final String filename;
  final int line;
  final int col;

  const Pos({this.filename = '', this.line = 0, this.col = 0});

  Pos stripColumn() {
    return Pos(filename: filename, line: line);
  }

  @override
  bool operator ==(Object other) {
    return other is Pos &&
        filename == other.filename &&
        line == other.line &&
        col == other.col;
  }

  @override
  int get hashCode => Object.hash(filename, line, col);
}

class Value {
  final String id;
  final Op op;
  final Type type;
  final BigInt? auxInt;
  final List<Value> args;
  final Pos pos;
  final String? trace;

  const Value({
    required this.id,
    required this.op,
    required this.type,
    required this.auxInt,
    required this.args,
    required this.pos,
    required this.trace,
  });
}

class Block {
  final String id;
  final List<Value> values;

  const Block({
    required this.id,
    required this.values,
  });
}

class ValueDefClickNotification extends Notification {
  final Value value;

  const ValueDefClickNotification(this.value);
}

class ValueUseClickNotification extends Notification {
  final Value value;

  const ValueUseClickNotification(this.value);
}

class ValueHues extends InheritedWidget {
  const ValueHues({
    super.key,
    required this.hues,
    required this.ctr,
    required super.child,
  });

  final Map<String, int> hues;
  final int ctr;

  static ValueHues of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<ValueHues>()!;

  Color? color(String id) {
    var hue = hues[id];
    if (hue == null) return null;
    // The multiplicative factor should be coprime 360
    return HSLColor.fromAHSL(1, (((hue % 360) * 71) % 360).toDouble(), 1, 0.5)
        .toColor();
  }

  Color? foregroundColor(String id, Color reference) {
    // TODO: get theme object here instead of using reference

    var color = this.color(id);
    if (color == null) return null;

    return HSVColor.lerp(
      HSVColor.fromColor(reference),
      HSVColor.fromColor(color),
      0.67,
    )!
        .toColor();
  }

  @override
  bool updateShouldNotify(ValueHues oldWidget) => ctr != oldWidget.ctr;
}

// TODO: implement diSource

// TODO: use InheritedModel to make these updates more granular

class Config extends InheritedWidget {
  // TODO: wire all of these config options up

  final String monospaceFontFamily = 'monospace';
  final bool verboseNonConsts = false;
  final bool verboseConsts = false;
  final bool alwaysShowTypes = false;
  final bool hexFPLiterals = false;
  final bool diSource; // rename prefices to debugInfo
  final bool diColumns;

  const Config({
    super.key,
    required this.diSource,
    required this.diColumns,
    required super.child,
  });

  static Config of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<Config>()!;

  @override
  bool updateShouldNotify(Config oldWidget) =>
      diSource != oldWidget.diSource || diColumns != oldWidget.diColumns;
}

String formatBits(BigInt x, int width, Hint how) {
  assert(x.sign >= 0);

  switch (how) {
    case Hint.int:
      return x.toRadixString(10);

    case Hint.hexInt:
      return x < BigInt.from(10)
          ? x.toRadixString(10)
          : '0x${x.toRadixString(16)}';

    case Hint.fp:
      // TODO: improve printing of floats
      return '${_floatFromBits(x, 32)}f';
  }
}

double _floatFromBits(BigInt x, int width) {
  assert(width == 32);
  assert(x.sign >= 0 && x.bitLength <= width);

  // TODO: expand 16-bit floats to 32-bit

  var widthBytes = width ~/ 8;

  var b = Uint8List(widthBytes);
  for (int i = 0; i < widthBytes; i++, x >>= 8) {
    b[i] = x.toUnsigned(8).toInt();
  }

  var view = ByteData.sublistView(b);

  switch (width) {
    case 32:
      return view.getFloat32(0, Endian.little);

    case 64:
      return view.getFloat64(0, Endian.little);

    default:
      throw UnimplementedError('???');
  }
}
