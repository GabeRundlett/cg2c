# Common rules that can't be gone wrong with.
#
# Rules that make assumptions about cost of instructions should not be added
# here. For example, an optimization that turns integer division by a constant
# into a right shift and a bunch of other ops does not belong to common rules.

prologue="""
#include "all.h"

static uint64_t intMask[KindLast] = {
    [KindBool]  = 0x1,
    [KindInt8]  = 0xff,
    [KindInt16] = 0xffff,
    [KindInt32] = 0xffffffff,
    [KindInt64] = 0xffffffffffffffff,
};

static uint64_t signMask[KindLast] = {
    [KindInt8]  = 0x80,
    [KindInt16] = 0x8000,
    [KindInt32] = 0x80000000,
    [KindInt64] = 0x8000000000000000,
};

// TODO: rename to make it clear it's about Const?
static bool cg2c_valueAuxIntZero(Value *v) { return cg2c_valueAuxUint(v) == 0; }

static int64_t
cg2c_valueAuxInt64(Value *v)
{
    assert(v->op == OpConst);
    uint64_t x = cg2c_valueAuxUint(v);
    uint64_t y = ~intMask[cg2c_typeKind(v->type)];
    if ((x & (y >> 1)) != 0)
        x |= y;
    return (int64_t)x;
}

static size_t
cg2c_valueAuxSize(Value *v)
{
    assert(v->op == OpExtract);
    return (size_t)*(uint64_t*)v->aux;
}

static int64_t
cg2c_valueAuxAlign(Value *v)
{
    assert(v->op == OpLoad || v->op == OpStore);
    return (int64_t)*(uint64_t*)v->aux;
}

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
    Value *args[] = {a0};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue4A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1, Value *a2, Value *a3)
{
    Value *args[] = {a0, a1, a2, a3};
    return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newConst(Builder b, Type *type, uint64_t aux)
{
    return cg2c_buildConst(b, type, aux&intMask[cg2c_typeKind(type)]);
}

static int64_t
gcd(int64_t x, int64_t y)
{
    if (y == 0)
        return x;
    if (x == y)
        return x;
    if (x > y)
        return gcd(x-y, y);
    return gcd(x, y-x);
}

static Value*
buildBoolToInt(Builder b, Type *type, Value *x)
{
    assert(x->type == cg2c_types[KindBool]);
    Value *zero = cg2c_buildConst(b, type, 0);
    Value *one = cg2c_buildConst(b, type, 1);
    return newValue3(b, OpIf, type, x, one, zero);
}

static uint32_t
floatbits(float x)
{
    uint32_t y;
    memmove(&y, &x, sizeof y);
    return y;
}

static float
floatfrombits(uint32_t x)
{
    float y;
    memmove(&y, &x, sizeof y);
    return y;
}

static Value*
buildArith2(Builder b, Op op, Value *x, Value *y)
{
    assert(x->type == y->type);
    return newValue2(b, op, x->type, x, y);
}

static size_t
cg2c_phiNArg(Value *v)
{
    assert(v->op == OpPhi);
    assert((cg2c_valueNArg(v) - 1) % 2 == 0);
    return (cg2c_valueNArg(v) - 1) / 2;
}

static Incoming
cg2c_phiArg(Value *v, size_t i)
{
    assert(v->op == OpPhi);
    assert((cg2c_valueNArg(v) - 1) % 2 == 0);

    return (Incoming) {
        .v = cg2c_valueArg(v, 1 + 2 * i + 0),
        .f = cg2c_valueArgFunc(v, 1 + 2 * i + 1),
    };
}
"""

label = "cg2c_commonRules"

rules = [
    # Constant folding

    RewriteRule(
        pattern=Pattern(var="v", op="Add", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)+cg2c_valueAuxUint(y));",
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Sub", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)-cg2c_valueAuxUint(y));",
    ),

    RewriteRule(
        pattern=Pattern(var="v", op="Mul", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(x)*cg2c_valueAuxUint(y));",
    ),

    # TODO: for fp rules, we might not want to fold expressions that produce
    # NaN, as that will destroy extra NaN bits, which could e.g. embed pc in
    # theory
    RewriteRule(
        pattern=Pattern(var="v", op="FDiv", args=[Pattern(var="x", op="Const"), Pattern(var="y", op="Const")]),
        replace="""
            switch (cg2c_typeKind(v->type)) {
            case KindInt32: {
                float x_ = floatfrombits((uint32_t)cg2c_valueAuxUint(x));
                float y_ = floatfrombits((uint32_t)cg2c_valueAuxUint(y));
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits(x_ / y_));
            }

            default:
                assert(0); // not implemented
            }
        """,
    ),

    #

    RewriteRule(
        pattern=Pattern(var="v", op="Add", args=[Pattern(var="x"), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return x;",
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Sub", args=[Pattern(var="x"), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return x;",
    ),

    RewriteRule(
        pattern=Pattern(var="v", op="Mul", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="MulHi", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="UMulHi", args=[Pattern(), Pattern(var="y", op="Const")]),
        guard="cg2c_valueAuxIntZero(y)",
        replace="return cg2c_buildConst(b, v->type, 0);",
    ),

    # TODO: maybe somehow make these rules more general

    # (c ? k1 : k0) != k0 => c, where k0, k1 are constants and k0 != k1
    RewriteRule(
        pattern=Pattern(op="Neq", args=[
            Pattern(op="If", args=[Pattern(var="c"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(var="k0_", op="Const"),
        ]),
        guard="k0 != k1 && k0 == k0_",
        replace="return c;",
    ),

    # TODO: make these into something more general? to support for example fp
    # conditionals: (c0 ? 1.0 : 0.0) & (c1 ? 1.0 : 0.0)

    RewriteRule(
        pattern=Pattern(var="v", op="And", args=[
            Pattern(op="If", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="If", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, buildArith2(b, OpAnd, c0, c1));
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Or", args=[
            Pattern(op="If", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="If", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, buildArith2(b, OpOr, c0, c1));
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Xor", args=[
            Pattern(op="If", args=[Pattern(var="c0"), Pattern(var="k1", op="Const"), Pattern(var="k0", op="Const")]),
            Pattern(op="If", args=[Pattern(var="c1"), Pattern(var="k1_", op="Const"), Pattern(var="k0_", op="Const")]),
        ]),
        guard="""cg2c_valueAuxUint(k1) == 1 && cg2c_valueAuxIntZero(k0) &&
            cg2c_valueAuxUint(k1_) == 1 && cg2c_valueAuxIntZero(k0_)""",
        replace="""
            return buildBoolToInt(b, v->type, buildArith2(b, OpNeq, c0, c1));
        """,
    ),

    # Fold selections by a constant.

    RewriteRule(
        pattern=Pattern(op="If", args=[Pattern(var="c", op="Const"), Pattern(var="x"), Pattern(var="y")]),
        replace="return !cg2c_valueAuxIntZero(c) ? x : y;",
    ),

    RewriteRule(
        pattern=Pattern(op="Neg", args=[Pattern(var="c", op="Const")]),
        # Note: signed negation suffers from overflow
        replace="return newConst(b, c->type, -cg2c_valueAuxInt64(c));"
    ),

    # TODO: add a rule to split tuple phis?

    # TODO: rules like these need more elaborate guard support
    RewriteRule(
        pattern=Pattern(var="v", op="Phi", ddd=True),
        replace="""
            // Tuple phis should've been split
            assert(cg2c_typeKind(v->type) != KindTuple);

            // Don't optimize phis that select mem values.
            if (v->type == cg2c_types[KindMem])
                return v;

            Value *a0_v = cg2c_phiArg(v, 0).v;
            for (size_t i = 1; i < cg2c_phiNArg(v); i++) {
                if (a0_v != cg2c_phiArg(v, i).v)
                    return v;
            }

            return a0_v;
        """,
    ),

    # Fold conversions

    RewriteRule(
        pattern=Pattern(var="v", op="SignExt", args=[Pattern(var="k", op="Const")]),
        replace="return newConst(b, v->type, (uint64_t)cg2c_valueAuxInt64(k));",
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="ZeroExt", args=[Pattern(var="k", op="Const")]),
        replace="return newConst(b, v->type, cg2c_valueAuxUint(k));",
    ),

    RewriteRule(
        pattern=Pattern(var="w", op="IToF", args=[Pattern(var="v", op="Const")]),
        replace="""
            double y;
            switch (cg2c_typeKind(v->type)) {
            case KindInt32: {
                y = (double)(int32_t)cg2c_valueAuxUint(v);
                break;
            }

            default:
                assert(0); // not implemented
            }

            switch (cg2c_typeKind(w->type)) {
            case KindInt32: {
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits((float)y));
            }

            default:
                assert(0); // not implemented
            }
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="w", op="UToF", args=[Pattern(var="v", op="Const")]),
        replace="""
            double y;
            switch (cg2c_typeKind(v->type)) {
            case KindInt32: {
                y = (double)(uint32_t)cg2c_valueAuxUint(v);
                break;
            }

            default:
                assert(0); // not implemented
            }

            switch (cg2c_typeKind(w->type)) {
            case KindInt32: {
                return cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)floatbits((float)y));
            }

            default:
                assert(0); // not implemented
            }
        """,
    ),

    # Get rid of useless conversions.
    #
    # These rules are required to produce valid SPIR-V.
    #
    # BUG: these are deprecated, we'll disallow useless conversions.

    RewriteRule(pattern=Pattern(var="v", op="SignExt", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),
    RewriteRule(pattern=Pattern(var="v", op="ZeroExt", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),
    RewriteRule(pattern=Pattern(var="v", op="FToF", args=[Pattern(var="x")]), guard="v->type == x->type", replace="return x;"),

    RewriteRule(pattern=Pattern(op="PtrToInt", args=[Pattern(op="IntToPtr", args=[Pattern(var="x")])]), replace="return x;"),
    RewriteRule(pattern=Pattern(op="IntToPtr", args=[Pattern(op="PtrToInt", args=[Pattern(var="x")])]), replace="return x;"),

    # TODO: this again would benefit from better guard support
    RewriteRule(
        pattern=Pattern(var="v", op="MakeTuple", args=[Pattern(op="Extract", args=[Pattern(var="u")])], ddd=True),
        replace="""
            if (v->type != u->type)
                return v;
            for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
                Value *x = cg2c_valueArg(v, i);
                if (x->op != OpExtract || cg2c_valueAuxSize(x) != i || cg2c_valueArg(x, 0) != u)
                    return v;
            }
            return u;
        """,
    ),

    # Forward Extracts.
    RewriteRule(
        pattern=Pattern(var="x", op="Extract", args=[Pattern(var="constructor", op="MakeTuple", ddd=True)]),
        replace="return cg2c_valueArg(constructor, cg2c_valueAuxSize(x));",
    ),

    RewriteRule(
        pattern=Pattern(op="AddPtr", args=[Pattern(var="base"), Pattern(var="off", op="Const")]),
        guard="cg2c_valueAuxIntZero(off)",
        replace="return base;",
    ),

    # Collapse composite AddPtrs and let other transformations deal with offset
    # arithmetic.
    #
    # Note: we could mark the Add no-signed-overflow. c/gen.c could also mark
    # Mul by sizeof as no-signed-overflow. Not clear whether it's worth doing.
    RewriteRule(
        pattern=Pattern(op="AddPtr", args=[Pattern(op="AddPtr", args=[Pattern(var="base"), Pattern(var="off0")]), Pattern(var="off1")]),
        replace="return cg2c_buildAddPtr(b, base, buildArith2(b, OpAdd, off0, off1));",
    ),

    # Split loads and stores of tuples.

    RewriteRule(
        pattern=Pattern(var="load", op="Load", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="cg2c_typeKind(load->type) == KindTuple",
        replace="""
            int64_t align = cg2c_valueAuxAlign(load);

            // TODO: just reserve the entire slice at once
            Slice result = NULL_SLICE;
            int64_t off = 0;
            for (size_t i = 0; i < cg2c_tupleNElem(load->type); i++) {
                Type *et = cg2c_tupleElem(load->type, i);

                Value *q = cg2c_buildAddPtrConst(b, p, off);
                Value *e = cg2c_buildLoad(b, et, gcd(align, off), mem, q);

                result = cg2c_slappend(sizeof(Value*), result, &e, b.ctxt->gc);
                off += cg2c_typeSize(b.ctxt, et);
            }

            // Extract forwarding will take care from here on.
            return cg2c_buildMakeTuple(b, result);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="store", op="Store", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        guard="cg2c_typeKind(v->type) == KindTuple",
        replace="""
            int64_t align = cg2c_valueAuxAlign(store);

            int64_t off = 0;
            for (size_t i = 0; i < cg2c_tupleNElem(v->type); i++) {
                Type *et = cg2c_tupleElem(v->type, i);

                Value *q = cg2c_buildAddPtrConst(b, p, off);
                Value *e = cg2c_buildExtract(b, i, v);
                mem = cg2c_buildStore(b, gcd(align, off), mem, q, e);

                off += cg2c_typeSize(b.ctxt, et);
            }

            return mem;
        """,
    ),

    # BUG: bunch of hack-rules until we properly support function calls and
    # inlining
    RewriteRule(pattern=Pattern(var="v", op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="x"),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__sqrtf\")) == 0", replace="""
        Value *y = newValue1(b, OpSqrt, cg2c_types[KindInt32], x);
        return cg2c_buildMakeTuple2(b, mem, y);
    """),
    RewriteRule(pattern=Pattern(var="v", op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="x"),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__sinf\")) == 0", replace="""
        Value *y = newValue1(b, OpSin, cg2c_types[KindInt32], x);
        return cg2c_buildMakeTuple2(b, mem, y);
    """),
    RewriteRule(pattern=Pattern(var="v", op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="x"),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__cosf\")) == 0", replace="""
        Value *y = newValue1(b, OpCos, cg2c_types[KindInt32], x);
        return cg2c_buildMakeTuple2(b, mem, y);
    """),
    RewriteRule(pattern=Pattern(var="v", op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="x"),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__log2f\")) == 0", replace="""
        Value *y = newValue1(b, OpLog2, cg2c_types[KindInt32], x);
        return cg2c_buildMakeTuple2(b, mem, y);
    """),
    RewriteRule(
        pattern=Pattern(var="v", op="Callc2", args=[
            Pattern(var="s", op="Sym"),
            Pattern(op="MakeTuple", args=[
                Pattern(var="mem"),
                Pattern(var="x"),
                Pattern(var="y"),
            ]),
        ]),
        guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__powrf\")) == 0",
        replace="""
            Value *z = newValue2(b, OpPowr, cg2c_types[KindInt32], x, y);
            return cg2c_buildMakeTuple2(b, mem, z);
        """,
    ),
    RewriteRule(pattern=Pattern(op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="ptr"),
            Pattern(var="val"),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__atomicStore32\")) == 0", replace="""
        mem = newValue3(b, OpAtomicStore, cg2c_types[KindMem], mem, ptr, val);
        Value *u = cg2c_buildMakeTuple(b, NULL_SLICE);
        return cg2c_buildMakeTuple2(b, mem, u);
    """),
    RewriteRule(pattern=Pattern(op="Callc2", args=[
        Pattern(var="s", op="Sym"),
        Pattern(op="MakeTuple", args=[
            Pattern(var="mem"),
            Pattern(var="im"),
            Pattern(var="x"),
            Pattern(var="y"),
            Pattern(op="MakeTuple", args=[
                Pattern(var="r"),
                Pattern(var="g"),
                Pattern(var="b_"),
                Pattern(var="a"),
            ]),
        ]),
    ]), guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__image2DStore\")) == 0", replace="""
        AuxImage *aux = cg2c_malloc(sizeof *aux, b.ctxt->gc);
        aux->comp = 1;
        aux->dim = Dim2D;
        aux->array = false;
        aux->ms = false;
        Value *coord = cg2c_buildMakeTuple2(b, x, y);
        Value *data = cg2c_buildMakeTuple4(b, r, g, b_, a);
        mem = newValue4A(b, OpImageStore, cg2c_types[KindMem], aux, mem, im, coord, data);
        Value *u = cg2c_buildMakeTuple(b, NULL_SLICE);
        return cg2c_buildMakeTuple2(b, mem, u);
    """),
    RewriteRule(
        pattern=Pattern(var="v", op="Callc2", args=[
            Pattern(var="s", op="Sym"),
            Pattern(op="MakeTuple", args=[
                Pattern(var="mem"),
                Pattern(var="im"),
                Pattern(var="sampler"),
                Pattern(var="x"),
                Pattern(var="y"),
            ]),
        ]),
        guard="cg2c_strcmp(*(String*)s->aux, cg2c_mkstringnocopy(\"__tex2D\")) == 0",
        replace="""
            Value *coord = cg2c_buildMakeTuple2(b, x, y);
            Value *result = newValue4A(b, OpTex, cg2c_tupleElem(v->type, 1), NULL, mem, im, sampler, coord);
            return cg2c_buildMakeTuple2(b, mem, result);
        """,
    ),

    RewriteRule(
        pattern=Pattern(op="CallStructuredIf", args=[
            Pattern(var="cond", op="Const"),
            Pattern(var="then"),
            Pattern(var="else_"),
            Pattern(var="merge"),
            Pattern(var="arg"),
        ]),
        replace="""
            Value *target = !cg2c_valueAuxIntZero(cond) ? then : else_;
            // Generic addressing lowering erroneously assigns non-noret type to
            // OpCallStructuredIf, get these asserts back on once we fix that
            //
            // assert(then->type == cg2c_types[KindNoret]);
            // assert(else_->type == cg2c_types[KindNoret]);
            return cg2c_buildCall(b, target, arg);
        """,
    ),
    RewriteRule(
        pattern=Pattern(op="CallStructuredIf", args=[
            Pattern(var="cond"),
            Pattern(var="then"),
            Pattern(var="else_"),
            Pattern(var="merge"),
            Pattern(var="arg"),
        ]),
        guard="then == else_",
        replace="""
            assert(then->type == cg2c_types[KindNoret]);
            assert(else_->type == cg2c_types[KindNoret]);
            return cg2c_buildCall(b, then, arg);
        """,
    ),

    # TODO: worth it?
    #
    # It might also be worth exploring doing on the fly SSA construction in
    # frontend.
    #
    # RewriteRule(
    #     pattern=Pattern(var="load", op="Load", args=[Pattern(var="mem"), Pattern(var="p")]),
    #     replace="""
    #     """,
    # ),
]
