#include "hello.h"

static const char magic[] = "\xab\x4b\x54\x58\x20\x32\x30\xbb\x0d\x0a\x1a\x0a";

enum {
	MaxLevelCount = 31,
};

typedef struct Section32 Section32;
struct Section32 {
	uint32_t off, len;
};

typedef struct Section64 Section64;
struct Section64 {
	uint64_t off, len;
};

typedef struct Level Level;
struct Level {
	uint64_t off, len, uncompressedLen;
};

typedef struct KTX2Header KTX2Header;
struct KTX2Header {
	char magic[12];
	VkFormat vkFormat;
	uint32_t typeSize;
	uint32_t width;
	uint32_t height;
	uint32_t depth;
	uint32_t layerCount; // at most MaxLevelCount
	uint32_t faceCount;
	uint32_t levelCount;
	uint32_t supercompressionScheme;
	Section32 dfd;
	Section32 kvd;
	Section64 sgd;
};

_Static_assert(sizeof(KTX2Header) == 80);

int
readKTX2(FILE *f, TextureHeader *out)
{
	// assert(ftell(f) == 0);

	KTX2Header h = {};
	Level levels[MaxLevelCount] = {};

	if (fread(&h, sizeof h, 1, f) != 1)
		return -1;

	_Static_assert(sizeof h.magic <= nelem(magic));
	if (memcmp(h.magic, magic, sizeof h.magic) != 0)
		return -1;

	// TODO: emit an error message or warning or something
	if (h.levelCount == 0)
		h.levelCount = 1;

	if (h.levelCount > MaxLevelCount)
		return -1;

	if (fread(&levels, h.levelCount * sizeof levels[0], 1, f) != 1)
		return -1;

	out->imageType = VK_IMAGE_TYPE_2D;
	out->format = h.vkFormat;
	out->extent = (VkExtent3D) {
		h.width,
		h.height > 0 ? h.height : 1,
		h.depth > 0 ? h.height : 1,
	};
	out->levelCount = h.levelCount;
	for (uint32_t i = 0; i < h.levelCount; i++) {
		out->levels[i].off = levels[i].off;
		out->levels[i].len = levels[i].len;
	}

	return 0;
}
