#!/usr/bin/env python3

import sys
import subprocess

preprocessed = subprocess.run("cpp --traditional-cpp -nostdinc -C -D__cg2c__ -I../cg2include".split(' ') + [sys.argv[1]],
    capture_output=True)

with open(sys.argv[2], "w") as f:
    for b in preprocessed.stdout:
        f.write("0x{:02x}, ".format(b))
    f.write("\n")
