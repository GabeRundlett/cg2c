prologue = """
#include "all.h"

static uint64_t signMask[KindLast] = {
    [KindInt8]  = 0x80,
    [KindInt16] = 0x8000,
    [KindInt32] = 0x80000000,
    [KindInt64] = 0x8000000000000000,
};

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
    Value *args[] = {a0};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newBoolToInt(Builder b, Type *type, Value *x)
{
    Value *zero = cg2c_buildConst(b, type, 0);
    Value *one = cg2c_buildConst(b, type, 1);
    return newValue3(b, OpIf, type, x, one, zero);
}

static int64_t
cg2c_valueAuxAlign(Value *v)
{
    assert(v->op == OpLoad || v->op == OpStore);
    return (int64_t)*(uint64_t*)v->aux;
}

static int64_t
gcd(int64_t x, int64_t y)
{
    if (y == 0)
        return x;
    if (x == y)
        return x;
    if (x > y)
        return gcd(x-y, y);
    return gcd(x, y-x);
}
"""

label = "cg2c_int64ToInt32Rules"

rules = [
    RewriteRule(
        pattern=Pattern(var="v", op="Add", args=[
            Pattern(op="MakeInt64", args=[Pattern(var="xLo"), Pattern(var="xHi")]),
            Pattern(op="MakeInt64", args=[Pattern(var="yLo"), Pattern(var="yHi")]),
        ]),
        replace="""
            Value *lo = cg2c_buildArith2(b, OpAdd, xLo, yLo);
            Value *carry = cg2c_buildArith2(b, OpAddc, xLo, yLo);
            Value *hi = cg2c_buildArith2(b, OpAdd, cg2c_buildArith2(b, OpAdd, xHi, yHi), carry);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="Sub", args=[
            Pattern(op="MakeInt64", args=[Pattern(var="xLo"), Pattern(var="xHi")]),
            Pattern(op="MakeInt64", args=[Pattern(var="yLo"), Pattern(var="yHi")]),
        ]),
        replace="""
            Value *lo = cg2c_buildArith2(b, OpSub, xLo, yLo);
            Value *borrow = cg2c_buildArith2(b, OpSubb, xLo, yLo);
            Value *hi = cg2c_buildArith2(b, OpSub, cg2c_buildArith2(b, OpSub, xHi, yHi), borrow);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="v", op="Mul", args=[
            Pattern(op="MakeInt64", args=[Pattern(var="xLo"), Pattern(var="xHi")]),
            Pattern(op="MakeInt64", args=[Pattern(var="yLo"), Pattern(var="yHi")]),
        ]),
        replace="""
            Value *lo = cg2c_buildArith2(b, OpMul, xLo, yLo);
            Value *loHi = cg2c_buildArith2(b, OpMul, xLo, yHi);
            Value *hiLo = cg2c_buildArith2(b, OpMul, xHi, yLo);
            Value *hiHi = cg2c_buildArith2(b, OpUMulHi, xHi, yHi);
            Value *hi = cg2c_buildArith2(b, OpAdd, cg2c_buildArith2(b, OpAdd, loHi, hiLo), hiHi);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="v", op="SignExt", args=[Pattern(var="x")]),
        guard="v->type == cg2c_types[KindInt64]",
        replace="""
            Value *lo = x;
            if (lo->type != cg2c_types[KindInt32])
                lo = newValue1(b, OpSignExt, cg2c_types[KindInt32], lo);
            Value *hi = cg2c_buildShiftConst(b, OpRsh, lo, 31);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="v", op="ZeroExt", args=[Pattern(var="x")]),
        guard="v->type == cg2c_types[KindInt64]",
        replace="""
            Value *lo = x;
            if (lo->type != cg2c_types[KindInt32])
                lo = newValue1(b, OpZeroExt, cg2c_types[KindInt32], lo);
            Value *hi = cg2c_buildConst(b, cg2c_types[KindInt32], 0);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),

    RewriteRule(
        pattern=Pattern(var="v", op="Const"),
        guard="v->type == cg2c_types[KindInt64]",
        replace="""
            uint64_t val = cg2c_valueAuxUint(v);

            Value *lo = cg2c_buildConst(b, cg2c_types[KindInt32], val & 0xffffffff);
            Value *hi = cg2c_buildConst(b, cg2c_types[KindInt32], val >> 32);

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),

    # RewriteRule(
    #     pattern=Pattern(op="AddPtr", args=[Pattern(var="base"), Pattern(op="MakeInt64", args=[Pattern(var="off"), Pattern()])]),
    #     replace="""
    #         // BUG: currently the offset is unconditionally shrunk to 32-bit.
    #         // This isn't correct, but should work for our current programs
    #         return newValue2(b, OpAddPtr, cg2c_types[KindPtr], base, off);
    #     """,
    # ),

    RewriteRule(
        pattern=Pattern(var="load", op="Load", args=[Pattern(var="mem"), Pattern(var="p")]),
        guard="load->type == cg2c_types[KindInt64]",
        replace="""
            int64_t align = cg2c_valueAuxAlign(load);

            assert(!b.ctxt->bigEndian);

            Value *lo = cg2c_buildLoad(b, cg2c_types[KindInt32], align, mem, p);
            Value *hi = cg2c_buildLoad(b, cg2c_types[KindInt32], gcd(align, 4), mem, cg2c_buildAddPtrConst(b, p, 4));

            return newValue2(b, OpMakeInt64, cg2c_types[KindInt64], lo, hi);
        """,
    ),
    RewriteRule(
        pattern=Pattern(var="store", op="Store", args=[
            Pattern(var="mem"),
            Pattern(var="p"),
            Pattern(op="MakeInt64", args=[Pattern(var="lo"), Pattern(var="hi")]),
        ]),
        replace="""
            int64_t align = cg2c_valueAuxAlign(store);

            assert(!b.ctxt->bigEndian);

            mem = cg2c_buildStore(b, align, mem, p, lo);
            mem = cg2c_buildStore(b, gcd(align, 4), mem, cg2c_buildAddPtrConst(b, p, 4), hi);

            return mem;
        """,
    ),
]