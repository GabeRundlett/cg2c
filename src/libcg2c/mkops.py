import json
import sys
import typing

import mako.template

# TODO: rulegen needs access to ops too so it might make sense to reintroduce
# ops.py that would be reading from ops.json

class Op(typing.NamedTuple):
    name: str
    aux_type: str = ""
    commutative: bool = False # commutative in the first two arguments
    description: str = ""

def opFromDict(x):
    return Op(
        name = x["op"],
        aux_type = x.get("aux_type", ""),
        commutative = x.get("commutative", False),
        description = x.get("description", ""),
    )

ops = list(map(opFromDict, filter(lambda x: "op" in x, json.load(open(sys.argv[1], "rb")))))

ops_h = mako.template.Template(r"""// generated, do not edit, etc

enum Op {
	OpXxx,
% for op in ops:

    % if op.description != "":
        % for l in op.description.replace("\n", "\n\n").split("\n"):
	// ${l}
        % endfor
    % endif
	Op${op.name},
% endfor

	OpLast,
};
""")

with open(sys.argv[2], "w") as f:
    f.write(ops_h.render(ops=ops))

op_info_c = mako.template.Template(r"""// generated

#include "all.h"

OpInfo cg2c_opInfo[OpLast] = {
% for op in ops:
	[Op${op.name}] = {
		.name = "${op.name}",
        % if op.aux_type in ["int", "uint32_t", "int64_t", "uint64_t", "size_t"]:
		.smallaux = true,
        % endif
        % if op.commutative:
		.commutative = true,
        % endif
	},
% endfor
};
""")

with open(sys.argv[3], "w") as f:
    f.write(op_info_c.render(ops=ops))
