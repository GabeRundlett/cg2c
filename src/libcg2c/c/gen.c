#include "all.h"
#include "cg2.h"

// TODO: think of how to reconcile gen with typechecker

// TODO: provide (Pos) {} as position information for internal constants

static bool
needsMem(Node *n)
{
	return true;
}

typedef struct State State;
struct State {
	Compile *ctxt;

	Func  *cc;   // continuation that is being constructed
	Map   *vars; // CSym* → Value*
	Value *mem;  // current mem definition
	Value *ret;  // return continuation

	Type *cmplx[KindLast];
	Type *vec[KindLast][5];
	Type *bb;     // (mem) noret; TODO: rename into something else

	Rewriter *rewriter;
	CSE      *cse;
};

static Builder
builder(State *s, Pos pos)
{
	return (Builder) {.ctxt = s->ctxt, .rewriter = s->rewriter, .cse = s->cse, .pos = pos};
}

static Value*
newValue0A(State *s, Op op, Type *type, void *aux, Pos pos)
{
	return cg2c_buildValue(builder(s, pos), op, type, aux, NULL_SLICE);
}

static Value*
newValue1(State *s, Op op, Type *type, Value *x, Pos pos)
{
	Value *args[] = {x};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(State *s, Op op, Type *type, Value *x, Value *y, Pos pos)
{
	Value *args[] = {x, y};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(State *s, Op op, Type *type, Value *x, Value *y, Value *z, Pos pos)
{
	Value *args[] = {x, y, z};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue5(State *s, Op op, Type *type, Value *a0, Value *a1, Value *a2, Value *a3, Value *a4, Pos pos)
{
	Value *args[] = {a0, a1, a2, a3, a4};
	return cg2c_buildValue(builder(s, pos), op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static void
endBlock(State *s, Value *t)
{
	cg2c_funcTerminate(s->cc, t, s->ctxt->gc);
}

// TODO: maybe Func should just have Pos?
static void
beginBlock(State *s, Func *f, Pos pos)
{
	s->cc = f;
	s->mem = cg2c_buildParam(builder(s, pos), f);
}

// TODO: better name for this and the following

static int ckind2kind[NTYPE] = {
	[TBOOL]    = KindBool,
	[TINT8]    = KindInt8,
	[TUINT8]   = KindInt8,
	[TINT16]   = KindInt16,
	[TUINT16]  = KindInt16,
	[TINT32]   = KindInt32,
	[TUINT32]  = KindInt32,
	[TINT64]   = KindInt64,
	[TUINT64]  = KindInt64,
	[TFLOAT16] = KindInt16,
	[TFLOAT32] = KindInt32,
	[TFLOAT64] = KindInt64,
	[TPTR]     = KindPtr,
};

static Type*
ctype2type(State *s, CType *type)
{
	switch (type->kind) {
	case TBOOL:
	case TINT8: case TUINT8:
	case TINT16: case TUINT16:
	case TINT32: case TUINT32:
	case TINT64: case TUINT64:
	case TFLOAT16:
	case TFLOAT32:
	case TFLOAT64:
		return cg2c_types[ckind2kind[type->kind]];

	case TCOMPLEX:
		return s->cmplx[ckind2kind[type->elem->kind]];

	case TVEC4:
		return s->vec[ckind2kind[type->elem->kind]][4];

	case TPTR:
		// TODO: handle pointers to functions in same way we handle
		// TFUNC case
		return cg2c_types[ckind2kind[type->kind]];

	case TFUNC: {
		// TODO: clean this mess up

		Slice param = NULL_SLICE;
		param = cg2c_slappend(sizeof(Type*), param, &cg2c_types[KindMem], s->ctxt->gc);
		for (size_t i = 0; i < cg2c_sllen(type->fields); i++) {
			CField f = *(CField*)cg2c_slindex(sizeof(CField), type->fields, i);
			Type *t = ctype2type(s, f.type);
			param = cg2c_slappend(sizeof(Type*), param, &t, s->ctxt->gc);
		}
		Type *par = cg2c_tuple(s->ctxt, param);

		Slice result = NULL_SLICE;
		result = cg2c_slappend(sizeof(Type*), result, &cg2c_types[KindMem], s->ctxt->gc);
		Type *tmp = ctype2type(s, type->elem);
		result = cg2c_slappend(sizeof(Type*), result, &tmp, s->ctxt->gc);
		Type *res = cg2c_tuple(s->ctxt, result);

		return cg2c_funct(s->ctxt, par, res);
	}

	case TSTRUCT:
	case TUNION: {
		Slice meh = NULL_SLICE;
		// TODO: use min(type->align, 8) or something, or always use the maximum possible element
		assert(type->align >= 4);
		for (int64_t i = 0; i < type->size; i += 4)
			meh = cg2c_slappend(sizeof(Type*), meh, &cg2c_types[KindInt32], s->ctxt->gc);
		return cg2c_tuple(s->ctxt, meh);
	}

	case TVOID:
		return cg2c_tuple(s->ctxt, NULL_SLICE); // TODO: do this only once
	}

	assert(!"shouldn't get here");
	return NULL;
}

static Value* genLoad(State *s, Type *t, Value *p, int64_t align, Pos pos);
static void   genStore(State *s, Value *p, Value *v, int64_t align, Pos pos);

static int64_t
sizeof_(CType *t)
{
	assert(t->align > 0);
	return t->size;
}

static int64_t
alignof_(CType *t)
{
	assert(t->align > 0);
	return t->align;
}

static Value*
genVar(State *s, int64_t size, int64_t align, Pos pos)
{
	return cg2c_buildVar(builder(s, pos), size, align);
}

static Value*
newRet(State *s, Func *f, Pos pos)
{
	Type *t = cg2c_funct(s->ctxt, cg2c_funcResult(f->type), cg2c_types[KindNoret]);
	Value *fv = cg2c_buildFuncValue(builder(s, pos), f);
	return newValue1(s, OpRet, t, fv, pos);
}

Value*
genLoad(State *s, Type *pointee, Value *p, int64_t align, Pos pos)
{
	return cg2c_buildLoad(builder(s, pos), pointee, align, s->mem, p);
}

void
genStore(State *s, Value *p, Value *v, int64_t align, Pos pos)
{
	s->mem = cg2c_buildStore(builder(s, pos), align, s->mem, p, v);
}

static Node* getarg(Node *n, size_t i) { return *(Node**)cg2c_slindex(sizeof(Node*), n->args, i); }
static Node* getarg0(Node *n) { return getarg(n, 0); }
static Node* getarg1(Node *n) { return getarg(n, 1); }

// TODO: get rid of this
static void
vars(State *b, Node *n)
{
	switch (n->op) {
	case ODCLFUNC:
	case ODCLLIST: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = *(Node**)cg2c_slindex(sizeof(Node*), n->stmts, i);
			vars(b, s);
		}
		break;
	}

	case ODCL: {
		Value *p = genVar(b, sizeof_(n->sym->type), alignof_(n->sym->type), n->pos);
		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, b->vars, &(void*) {n->sym}, b->ctxt->gc) = p;
		break;
	}

	case OBLOCK: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++) {
			Node *s = *(Node**)cg2c_slindex(sizeof(Node*), n->stmts, i);
			vars(b, s);
		}
		break;
	}

	case OIF: {
		vars(b, getarg1(n));
		if (getarg(n, 2) != NULL)
			vars(b, getarg(n, 2));
		break;
	}
	}
}

static bool
integer(CType *t)
{
	switch (t->kind) {
	case TINT8: case TUINT8:
	case TINT16: case TUINT16:
	case TINT32: case TUINT32:
	case TINT64: case TUINT64:
		return true;

	default:
		return false;
	}
}

static bool
unsigned_(CType *t)
{
	switch (t->kind) {
	case TUINT8: case TUINT16: case TUINT32: case TUINT64:
		return true;

	default:
		return false;
	}
}

static bool
floating(CType *t)
{
	switch (t->kind) {
	case TFLOAT16: case TFLOAT32: case TFLOAT64:
		return true;

	default:
		return false;
	}
}

static Value* genExpr(State *s, Node *n);

static Value*
index(State *s, Node *x, Node *y, Pos pos)
{
	Type *intptrt = cg2c_intptrt(s->ctxt);

	Builder b = builder(s, pos);

	Value *base = genExpr(s, x);
	Value *index = genExpr(s, y);
	// TODO: symmetry
	assert(x->type->kind == TPTR);
	Value *index2 = newValue1(s, OpSignExt, intptrt, index, pos);
	Value *size = cg2c_buildConst(b, intptrt, (uint64_t)sizeof_(x->type->elem));
	Value *off = cg2c_buildArith2(b, OpMul, index2, size);
	return cg2c_buildAddPtr(b, base, off);
}

static Value*
genAddr(State *s, Node *n)
{
	switch (n->op) {
	case ONAME: {
		Value *v = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, s->vars, &n->sym);
		assert(v);
		return v;
	}

	case OINDEX:
		return index(s, getarg0(n), getarg1(n), n->pos);

	case ODOT: {
		CType *stype = n->x->type;

		// TODO: handle vectors

		assert(stype->kind == TSTRUCT || stype->kind == TUNION);
		CField f = *(CField*)cg2c_slindex(sizeof(CField), stype->fields, n->field);

		// TODO: handle bit-fields

		Value *base = genAddr(s, n->x);
		return cg2c_buildAddPtrConst(builder(s, n->pos), base, f.off);
	}

	case OARROW: {
		CType *stype = n->x->type->elem;

		// TODO: handle vectors

		assert(stype->kind == TSTRUCT || stype->kind == TUNION);
		CField f = *(CField*)cg2c_slindex(sizeof(CField), stype->fields, n->field);

		// TODO: handle bit-fields

		Value *base = genExpr(s, n->x);
		return cg2c_buildAddPtrConst(builder(s, n->pos), base, f.off);
	}

	case ODEREF:
		return genExpr(s, n->x);
	}

	assert(0);
	return NULL;
}

static Value*
boolToInt(State *s, Value *v, Pos pos)
{
	Value *zero = cg2c_buildConst(builder(s, pos), cg2c_types[KindInt32], 0);
	Value *one = cg2c_buildConst(builder(s, pos), cg2c_types[KindInt32], 1);
	return newValue3(s, OpIf, cg2c_types[KindInt32], v, one, zero, pos);
}

static Value*
anyToBool(State *s, Value *x, Pos pos)
{
	Value *zero = cg2c_buildConst(builder(s, pos), x->type, 0);
	return newValue2(s, OpNeq, cg2c_types[KindBool], x, zero, pos);
}

static Value*
genExpr(State *s, Node *n)
{
	Builder b = builder(s, n->pos);

	switch (n->op) {
	// TODO: should be same as OAS
	case ODCL: {
		Value *loc = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, s->vars, &n->sym);
		assert(loc);
		Value *v = genExpr(s, getarg0(n));
		genStore(s, loc, v, alignof_(getarg0(n)->type), n->pos);
		return v;
	}

	case ONAME: {
		if (n->sym->class == CGLOBL || n->sym->class == CEXTERN) {
			// TODO: clone n->sym->name so that we don't keep the
			// whole tree live
			return newValue0A(s, OpSym, ctype2type(s, n->sym->type), &n->sym->name, n->pos);
		}
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->sym->type), loc, alignof_(n->sym->type), n->pos);
	}

	case OAS: {
		if (n->x->op == ODOTSHUF) {
			// TODO: load the entire vector, insert element and
			// store back, rather than just storing at a particular
			// location. Let the compiler optimize redundant stores
			// out, if necessary
			Value *base = genAddr(s, n->x->x);
			Value *p = cg2c_buildAddPtrConst(builder(s, n->pos), base, (int64_t)n->x->shuf[0] * 4);
			Value *v = genExpr(s, n->y);
			genStore(s, p, v, alignof_(n->y->type), n->pos);
			return v;
		}
		Value *p = genAddr(s, n->x);
		Value *v = genExpr(s, n->y);
		genStore(s, p, v, alignof_(n->y->type), n->pos);
		return v;
	}

	case OCALL: {
		assert(getarg0(n)->type->kind == TFUNC);

		Value *x = genExpr(s, getarg0(n));

		Slice args = NULL_SLICE;
		args = cg2c_slappend(sizeof(Value*), args, &s->mem, s->ctxt->gc);
		for (size_t i = 1; i < cg2c_sllen(n->args); i++) {
			Value *a = genExpr(s, *(Node**)cg2c_slindex(sizeof(Node*), n->args, i));
			args = cg2c_slappend(sizeof(Value*), args, &a, s->ctxt->gc);
		}
		Value *arg = cg2c_buildMakeTuple(builder(s, n->pos), args);

		Value *call = newValue2(s, OpCallc2, cg2c_funcResult(x->type), x, arg, n->pos);

		s->mem = cg2c_buildExtract(builder(s, n->pos), 0, call);

		return cg2c_buildExtract(builder(s, n->pos), 1, call);
	}

	case OLT: {
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpLt;
			if (unsigned_(n->x->type))
				op = OpLtU;
		} else if (floating(n->x->type))
			op = OpLtF;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		Value *cmp = newValue2(s, op, cg2c_types[KindBool], x, y, n->pos);
		return boolToInt(s, cmp, n->pos);
	}

	case OLE: {
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpLte;
			if (unsigned_(n->x->type))
				op = OpLteU;
		} else if (floating(n->x->type))
			op = OpLteF;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		Value *cmp = newValue2(s, op, cg2c_types[KindBool], x, y, n->pos);
		return boolToInt(s, cmp, n->pos);
	}

	case OGE: {
		// This is very compelling to replace integer and float types
		// with TINT, TUINT, TFLOAT and have them be parametrizable by
		// bitwidth.
		Op op = OpXxx;
		if (integer(n->x->type)) {
			op = OpLte;
			if (unsigned_(n->x->type))
				op = OpLteU;
		} else if (floating(n->x->type))
			op = OpLteF;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		Value *cmp = newValue2(s, op, cg2c_types[KindBool], y, x, n->pos);
		return boolToInt(s, cmp, n->pos);
	}

	case OEQ: {
		Op op = OpXxx;
		if (integer(n->x->type))
			op = OpEq;
		else if (floating(n->x->type))
			op = OpEqF;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		Value *cmp = newValue2(s, op, cg2c_types[KindBool], y, x, n->pos);
		return boolToInt(s, cmp, n->pos);
	}

	case ONE: {
		Op op = OpXxx;
		if (integer(n->x->type))
			op = OpNeq;
		else if (floating(n->x->type))
			op = OpNeqF;
		assert(op != OpXxx);
		Value *x = genExpr(s, n->x);
		Value *y = genExpr(s, n->y);
		Value *cmp = newValue2(s, op, cg2c_types[KindBool], y, x, n->pos);
		return boolToInt(s, cmp, n->pos);
	}

	case OCONV: {
		Value *x = genExpr(s, getarg0(n));
		Type *yT = ctype2type(s, n->type);

		if (integer(getarg0(n)->type) && integer(n->type)) {
			if (!unsigned_(getarg0(n)->type))
				return newValue1(s, OpSignExt, yT, x, n->pos);
			else
				return newValue1(s, OpZeroExt, yT, x, n->pos);
		}

		if (integer(getarg0(n)->type) && floating(n->type)) {
			if (!unsigned_(getarg0(n)->type))
				return newValue1(s, OpIToF, yT, x, n->pos);
			else
				return newValue1(s, OpUToF, yT, x, n->pos);
		}

		if (integer(getarg0(n)->type) && n->type->kind == TPTR)
			return newValue1(s, OpIntToPtr, yT, x, n->pos);

		if (floating(getarg0(n)->type) && integer(n->type)) {
			if (!unsigned_(n->type))
				return newValue1(s, OpFToI, yT, x, n->pos);
			else
				return newValue1(s, OpFToU, yT, x, n->pos);
		}

		if (floating(getarg0(n)->type) && floating(n->type))
			return newValue1(s, OpFToF, yT, x, n->pos);

		if (n->type->kind == TCOMPLEX) {
			assert(n->type->elem->kind == TFLOAT32);
			assert(n->type->elem->kind == getarg0(n)->type->kind);
			Value *imag = cg2c_buildConst(b, ctype2type(s, n->type->elem), 0);
			return cg2c_buildMakeTuple2(b, x, imag);
		}

		if (n->type->kind != TCOMPLEX && getarg0(n)->type->kind == TCOMPLEX) {
			assert(n->type->kind == getarg0(n)->type->elem->kind);
			return cg2c_buildExtract(b, 0, x);
		}

		if (n->type->kind == TPTR && getarg0(n)->type->kind == TPTR)
			return x;

		assert(0);
		return NULL;
	}

	case ODOT: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type) /* TODO: get alignment from field */, n->pos);
	}

	case OARROW: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type) /* TODO: get alignment from field */, n->pos);
	}

	case OADD: {
		if (n->type->kind == TPTR)
			return index(s, getarg0(n), getarg1(n), n->pos);

		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (n->type->kind == TCOMPLEX) {
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, 0, x);
			Value *x_im = cg2c_buildExtract(b, 1, x);
			Value *y_re = cg2c_buildExtract(b, 0, y);
			Value *y_im = cg2c_buildExtract(b, 1, y);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_re, y_re);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_im, y_im);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpAdd : OpFAdd, x, y);
	}

	case OSUB: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		// TODO: handle pointer - integer and pointer - pointer
		if (n->type->kind == TCOMPLEX) {
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, 0, x);
			Value *x_im = cg2c_buildExtract(b, 1, x);
			Value *y_re = cg2c_buildExtract(b, 0, y);
			Value *y_im = cg2c_buildExtract(b, 1, y);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_re, y_re);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_im, y_im);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpSub : OpFSub, x, y);
	}

	case OMUL: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (n->type->kind == TCOMPLEX) {
			// BUG: ±inf and nan are not handled correctly
			CType *elemt = n->type->elem;
			Value *x_re = cg2c_buildExtract(b, 0, x);
			Value *x_im = cg2c_buildExtract(b, 1, x);
			Value *y_re = cg2c_buildExtract(b, 0, y);
			Value *y_im = cg2c_buildExtract(b, 1, y);
			// complex(x_re, x_im) * complex(y_re, y_im)
			// = (x_re + x_im*i) * (y_re + y_im*i)
			// = x_re*(y_re + y_im*i) + x_im*i*(y_re + y_im*i)
			// = x_re*y_re + x_re*y_im*i + x_im*i*y_re + x_im*i*y_im*i
			// = x_re*y_re + x_re*y_im*i + x_im*i*y_re - x_im*y_im
			// = complex(x_re*y_re - x_im*y_im, x_re*y_im + x_im*i*y_re)
			Value *x_re_y_re = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_re, y_re);
			Value *x_im_y_im = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_im, y_im);
			Value *x_re_y_im = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_re, y_im);
			Value *x_im_y_re = cg2c_buildArith2(b, !floating(elemt) ? OpMul : OpFMul, x_im, y_re);
			Value *re = cg2c_buildArith2(b, !floating(elemt) ? OpSub : OpFSub, x_re_y_re, x_im_y_im);
			Value *im = cg2c_buildArith2(b, !floating(elemt) ? OpAdd : OpFAdd, x_re_y_im, x_im_y_re);
			return cg2c_buildMakeTuple2(b, re, im);
		}
		return cg2c_buildArith2(b, !floating(n->type) ? OpMul : OpFMul, x, y);
	}

	case ODIV: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (integer(n->type)) {
			// An anchor so that the scheduler doesn't schedule too
			// early
			// Value *anchor = cg2c_buildParam(b, s->cc);
			return newValue2(s, !unsigned_(n->type) ? OpDiv : OpUDiv, ctype2type(s, n->type), x, y, /* anchor, */ n->pos);
		}
		if (floating(n->type)) {
			return cg2c_buildArith2(b, OpFDiv, x, y);
		}
		assert(0);
		break;
	}

	case OMOD: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		if (integer(n->type)) {
			// An anchor so that the scheduler doesn't schedule too
			// early
			// Value *anchor = cg2c_buildParam(b, s->cc);
			return newValue2(s, !unsigned_(n->type) ? OpRem : OpURem, ctype2type(s, n->type), x, y, /* anchor, */ n->pos);
		}
		assert(0);
		break;
	}

	case OOR: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildArith2(b, OpOr, x, y);
	}

	case OAND: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildArith2(b, OpAnd, x, y);
	}

	case OLSH: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		return cg2c_buildShift(b, OpLsh, x, y);
	}

	case ORSH: {
		Value *x = genExpr(s, getarg0(n));
		Value *y = genExpr(s, getarg1(n));
		assert(integer(n->type) && unsigned_(n->type));
		return cg2c_buildShift(b, OpURsh, x, y);
	}

	case ONEG: {
		Value *x = genExpr(s, getarg0(n));
		return newValue1(s, !floating(n->type) ? OpNeg : OpFNeg, ctype2type(s, n->type), x, n->pos);
	}

	case OCOM: {
		Value *x = genExpr(s, getarg0(n));
		return newValue1(s, OpNot, ctype2type(s, n->type), x, n->pos);
	}

	case OADDR:
		return genAddr(s, getarg0(n));

	case OINDEX: {
		// TODO: handle vectors

		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type), n->pos);
	}

	case ODEREF: {
		Value *loc = genAddr(s, n);
		return genLoad(s, ctype2type(s, n->type), loc, alignof_(n->type), n->pos);
	}

	case ODOTSHUF: {
		// TODO: load the entire vector and extract
		Value *base = genAddr(s, n->x);
		Value *p = cg2c_buildAddPtrConst(b, base, (int64_t)n->shuf[0] * 4);
		return genLoad(s, ctype2type(s, n->type), p, alignof_(n->type), n->pos);
	}

	case OCONST:
		return cg2c_buildConst(b, ctype2type(s, n->type), n->asd);

	default:
		fprintf(stderr, "%d\n", n->op);
		assert(0);
		return NULL;
	}
}

static void
genStmt(State *b, Node *n)
{
	switch (n->op) {
	case ODCLFUNC:
	case ODCLLIST:
	case OBLOCK: {
		for (size_t i = 0; i < cg2c_sllen(n->stmts); i++)
			genStmt(b, *(Node**)cg2c_slindex(sizeof(Node*), n->stmts, i));
		break;
	}

	case ODCL:
		if (cg2c_sllen(n->args) == 0)
			break;
	case OAS:
	case OCONV:
	case OCALL:
	case OADD:
	case OMUL: {
		genExpr(b, n);
		break;
	}

	case ONOP:
		break;

	case OIF: {
		Type *typeNoret = cg2c_types[KindNoret];

		Func *bbThen = cg2c_malloc(sizeof *bbThen, b->ctxt->gc);
		bbThen->type = b->bb;

		Value *then = cg2c_buildFuncValue(builder(b, n->pos), bbThen);

		Func *bbElse = cg2c_malloc(sizeof *bbElse, b->ctxt->gc);
		bbElse->type = b->bb;

		Value *else_ = cg2c_buildFuncValue(builder(b, n->pos), bbElse);

		Func *bbMerge = cg2c_malloc(sizeof *bbMerge, b->ctxt->gc);
		bbMerge->type = b->bb;

		Value *merge = cg2c_buildFuncValue(builder(b, n->pos), bbMerge);

		Value *cond = genExpr(b, getarg0(n));
		Value *cmp = anyToBool(b, cond, n->pos);
		endBlock(b, newValue5(b, OpCallStructuredIf, typeNoret, cmp, then, else_, merge, b->mem, n->pos));

		beginBlock(b, bbThen, n->pos);
		genStmt(b, getarg1(n));
		endBlock(b, newValue2(b, OpCall, typeNoret, merge, b->mem, n->pos));

		beginBlock(b, bbElse, n->pos);
		if (getarg(n, 2) != NULL)
			genStmt(b, getarg(n, 2));
		endBlock(b, newValue2(b, OpCall, typeNoret, merge, b->mem, n->pos));

		beginBlock(b, bbMerge, n->pos);

		break;
	}

	case ORETURN: {
		Value *result;
		if (getarg0(n) != NULL) {
			Value *x = genExpr(b, getarg0(n));
			result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
		} else {
			Value *x = cg2c_buildMakeTuple(builder(b, n->pos), NULL_SLICE);
			result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
		}

		// Terminate current continuation
		endBlock(b, newValue2(b, OpCall, cg2c_funcResult(b->ret->type), b->ret, result, n->pos));

		// Continuation for trailing code. This will be DCE'd
		Func *bb = cg2c_malloc(sizeof *bb, b->ctxt->gc);
		bb->type = b->bb;
		beginBlock(b, bb, n->pos);

		break;
	}

	case OCONST:
		break;

	default:
		assert(0);
		break;
	}
}

Sym*
cg2c_gen(Compile *ctxt, CSym *cs)
{
	Node *n = cs->def;

	assert(n->op == ODCLFUNC);

	Slice rules = cg2c_commonRules;

	State *b = &(State) {.ctxt = ctxt};

	b->rewriter = cg2c_newRewriter(rules, ctxt->gc);
	b->cse = cg2c_newCSE(ctxt->gc);

	b->cmplx[KindInt8] = cg2c_tuple2(b->ctxt, cg2c_types[KindInt8], cg2c_types[KindInt8]);
	b->cmplx[KindInt16] = cg2c_tuple2(b->ctxt, cg2c_types[KindInt16], cg2c_types[KindInt16]);
	b->cmplx[KindInt32] = cg2c_tuple2(b->ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32]);
	b->cmplx[KindInt64] = cg2c_tuple2(b->ctxt, cg2c_types[KindInt64], cg2c_types[KindInt64]);

	b->vec[KindInt32][2] = cg2c_tuple2(b->ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32]);

	b->vec[KindInt32][3] = cg2c_tuple3(b->ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32]);

	b->vec[KindInt32][4] = cg2c_tuple4(b->ctxt, cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32], cg2c_types[KindInt32]);

	Type *fun = ctype2type(b, cs->type);

	b->bb = cg2c_funct(b->ctxt, cg2c_types[KindMem], cg2c_types[KindNoret]);

	Func *f = cg2c_malloc(sizeof *f, ctxt->gc);
	f->type = fun;

	b->ret = newRet(b, f, n->pos);

	b->vars = cg2c_mkmap(cg2c_ptr_ptr_map, ctxt->gc);

	beginBlock(b, f, n->pos);
	// HACK: maybe remove it by inserting an empty basic block with a Call?
	b->mem = cg2c_buildExtract(builder(b, n->pos), 0, b->mem);

	Value *arg_f = cg2c_buildParam(builder(b, n->pos), f);

	for (size_t i = 0; i < cg2c_sllen(cs->args); i++) {
		CSym *a = *(CSym**)cg2c_slindex(sizeof(CSym*), cs->args, i);

		Value *p = genVar(b, sizeof_(a->type), alignof_(a->type), (Pos) {});
		*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, b->vars, &a, b->ctxt->gc) = p;

		Value *x = cg2c_buildExtract(builder(b, (Pos) {}), i+1, arg_f);
		genStore(b, p, x, alignof_(a->type), (Pos) {});
	}
	vars(b, n);
	genStmt(b, n);

	// Append return if necessary
	//
	// BUG: return the correct value
	{
		Value *x = cg2c_buildMakeTuple(builder(b, n->pos), NULL_SLICE);
		Value *result = cg2c_buildMakeTuple2(builder(b, n->pos), b->mem, x);
		Value *callRet = newValue2(b, OpCall, cg2c_funcResult(b->ret->type), b->ret, result, n->pos);
		endBlock(b, callRet);
	}

	Sym *s = cg2c_malloc(sizeof *s, ctxt->gc);
	s->name = cs->name;
	s->type = f->type;
	s->func = f;
	s->cse = b->cse;
	return s;
}

int64_t
cg2c_ceval(Compile *ctxt, Node *n)
{
	Slice rules = cg2c_commonRules;

	// We shouldn't be generating any instructions that need vars, mem or
	// the current continuation, so don't initialize those.
	State *b = &(State) {
		.ctxt = ctxt,

		.rewriter = cg2c_newRewriter(rules, ctxt->gc),
		.cse = cg2c_newCSE(ctxt->gc),
	};

	Value *v = genExpr(b, n);
	return cg2c_valueAuxUint(v);
}
