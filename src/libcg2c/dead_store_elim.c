#include "all.h"

/*

For each block (continuation) we accumulate stores into vars.

Then, on load, we mark all stores storing into some var as used, in this and all
predecessor blocks (continuations).

Stores into weird locations and escaped vars are always "used".

*/

typedef struct DeadStoreElim DeadStoreElim;
struct DeadStoreElim {
	Map *preds;
	Map *vars;   // set of vars that escape or are loaded from
	Map *vars2;  // Value *var -> (Func* -> Slice)
	Map *stores; // set of stores that are "used"
};

// TODO: this could be cached in a funny way for better performance
static Func*
currentFunc(Value *v)
{
	for (;;) {
		switch (v->op) {
		case OpParam:
			return cg2c_valueArgFunc(v, 0);

		case OpMakeTuple: {
			// won't work for nested tuples but oh well
			for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
				Value *a = cg2c_valueArg(v, i);
				if (a->type == cg2c_types[KindMem]) {
					v = a;
					goto good;
				}
			}
			assert(0);
		good:
			break;
		}

		case OpExtract:
			v = cg2c_valueArg(v, 0);
			break;

		case OpStore:
			v = cg2c_valueArg(v, 0);
			break;

		case OpImageStore:
			v = cg2c_valueArg(v, 0);
			break;

		case OpStoreOutput:
			v = cg2c_valueArg(v, 0);
			break;

		case OpCallc2:
			v = cg2c_valueArg(v, 1);
			break;

		default:
			fprintf(stderr, "%s\n", cg2c_opInfo[v->op].name);
			assert(0); // don't know how to deal with this op
		}
	}
}

static void
useStores(DeadStoreElim *dse, Func *f, Value *var, GC *gc)
{
	Map *perFuncStores = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, dse->vars2, &var);
	Slice stores = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, perFuncStores, &f);

	for (size_t i = 0; i < cg2c_sllen(stores); i++) {
		Value *store = *(Value**)cg2c_slindex(sizeof(Value*), stores, i);
		cg2c_mapassign(cg2c_ptr_set, dse->stores, &store, gc);
	}
	if (cg2c_sllen(stores) > 0) {
		stores = cg2c_slice(sizeof(Value*), stores, 0, 0);
		*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, perFuncStores, &f, gc) = stores;
	}

	// "Use" stores in the predecessors as well

	Slice incs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, dse->preds, &f);
	for (size_t i = 0; i < cg2c_sllen(incs); i++) {
		useStores(dse, (*(Incoming*)cg2c_slindex(sizeof(Incoming), incs, i)).f, var, gc);
	}
}

static void
findDeadStores(Compile *ctxt, Value *v, void *etc_)
{
	GC *gc = ctxt->gc;
	DeadStoreElim *dse = etc_;

	switch (v->op) {
	case OpAddPtr:
		break;

	case OpLoad: {
		Value *load = v;

		Value *mem = cg2c_valueArg(load, 0);
		Value *p = cg2c_valueArg(load, 1);
		Value *pBase = cg2c_ptrchase(p);

		if (pBase->op == OpVar) {
			useStores(dse, currentFunc(mem), pBase, gc);
		}

		break;
	}

	case OpStore: {
		Value *store = v;

		Value *mem = cg2c_valueArg(store, 0);
		Value *p = cg2c_valueArg(store, 1);
		Value *pBase = cg2c_ptrchase(p); // could this be smarter, e.g. look into predecessors too?
		Value *v = cg2c_valueArg(store, 2);

		if (pBase->op == OpVar) {
			// Add this store to a list of stores for this var in function f

			Func *f = currentFunc(mem);

			Map *perFuncStores = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, dse->vars2, &pBase);
			if (perFuncStores == NULL) {
				perFuncStores = cg2c_mkmap(cg2c_ptr_slice_map, gc);
				*(Map**)cg2c_mapassign(cg2c_ptr_ptr_map, dse->vars2, &pBase, gc) = perFuncStores;
			}

			Slice stores = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, perFuncStores, &f);
			stores = cg2c_slappend(sizeof(Value*), stores, &store, gc);
			*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, perFuncStores, &f, gc) = stores;
		} else {
			// Store into an unclear location, escapes
			cg2c_mapassign(cg2c_ptr_set, dse->stores, &store, gc);
		}

		// Var being stored somewhere, escapes
		if (cg2c_typeKind(v->type) == KindPtr) {
			Value *vBase = cg2c_ptrchase(v);
			if (vBase->op == OpVar) {
				cg2c_mapassign(cg2c_ptr_set, dse->vars, &vBase, gc);
			}
		}

		break;
	}

	default:
		// Var being passed somewhere, escapes
		for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
			Value *a = cg2c_valueArg(v, i);
			if (cg2c_typeKind(a->type) == KindPtr) {
				Value *aBase = cg2c_ptrchase(a);
				if (aBase->op == OpVar) {
					cg2c_mapassign(cg2c_ptr_set, dse->vars, &aBase, gc);
				}
			}
		}
		break;
	}
}

static Value*
elimDeadStores(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc_)
{
	DeadStoreElim *dse = etc_;

	switch (op) {
	case OpStore: {
		Value *mem = *(Value**)cg2c_slindex(sizeof(Value*), args, 0);
		Value *p = *(Value**)cg2c_slindex(sizeof(Value*), args, 1);
		Value *pBase = cg2c_ptrchase(p);

		if (pBase->op == OpVar) {
			if (!cg2c_mapcontains(cg2c_ptr_set, dse->vars, &pBase) &&
				!cg2c_mapcontains(cg2c_ptr_set, dse->stores, &u)) {
				return mem;
			}
		}

		break;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

bool
cg2c_elimDeadStores(Builder b, Scope *scope)
{
	DeadStoreElim *dse = &(DeadStoreElim) {
		.preds = cg2c_predecessors(scope, b.ctxt->gc),
		.vars = cg2c_mkmap(cg2c_ptr_set, b.ctxt->gc),
		.vars2 = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc),
		.stores = cg2c_mkmap(cg2c_ptr_set, b.ctxt->gc),
	};

	cg2c_scopeForeach(b.ctxt, findDeadStores, dse, scope);

	return cg2c_scopeRewriteFunc2(b, elimDeadStores, dse, scope, NULL);
}
