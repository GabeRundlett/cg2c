#include "all.h"

// TODO: rewrite these functions to maintain our own stack

Value*
cg2c_rewrite(Builder b, Value *v, Map *re)
{
	GC *gc = b.ctxt->gc;

	Value *u = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &v);
	if (u != NULL)
		return u;

	Slice args = v->args;
	bool copied = false;
	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);
		Value *aRewritten = cg2c_rewrite(b, a, re);
		if (a != aRewritten) {
			if (!copied)
				args = cg2c_slclone(sizeof(Value*), v->args, gc);
			*(Value**)cg2c_slindex(sizeof(Value*), args, i) = aRewritten;
			copied = true;
		}
	}

	u = cg2c_buildValue(cg2c_builderWithPos(b, v->pos, v), v->op, v->type, v->aux, args);

	*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &v, gc) = u;
	return u;
}

Value*
cg2c_rewriteFunc2(Builder b, Value *(*f)(Builder, Value*, Op, Type*, void*, Slice, void *etc), void *etc, Value *v, Map *re)
{
	GC *gc = b.ctxt->gc;

	Value *u = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &v);
	if (u != NULL)
		return u;

	Slice args = v->args;
	bool copied = false;
	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);
		Value *aRewritten = cg2c_rewriteFunc2(b, f, etc, a, re);
		if (a != aRewritten) {
			if (!copied)
				args = cg2c_slclone(sizeof(Value*), v->args, gc);
			*(Value**)cg2c_slindex(sizeof(Value*), args, i) = aRewritten;
			copied = true;
		}
	}

	u = f(cg2c_builderWithPos(b, v->pos, v), v, v->op, v->type, v->aux, args, etc);

	*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &v, gc) = u;
	return u;
}
