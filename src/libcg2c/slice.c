#include "all.h"

static Slice growslice(size_t, Slice, size_t, GC *gc);

Slice
cg2c_mkslice(size_t elemsize, const void *elems, size_t len, GC *gc)
{
	Slice s = {
		.data      = cg2c_memdup(elems, len*elemsize, gc),
		.len       = len,
		.cap       = len,
		._elemsize = elemsize,
	};
	return s;
}

Slice
cg2c_mkslicenocopy(size_t elemsize, void *elems, size_t n)
{
	return (Slice) {elems, n, n, elemsize};
}

Slice
cg2c_slappend(size_t elemsize, Slice s, const void *e, GC *gc)
{
	size_t i = cg2c_sllen(s);
	size_t n = i + 1;
	if (cg2c_slcap(s) < n)
		s = growslice(elemsize, s, n, gc);
	s = cg2c_slice(elemsize, s, 0, n);
	memmove(cg2c_slindex(elemsize, s, i), e, elemsize);
	return s;
}

Slice
cg2c_slcat(size_t elemsize, Slice s1, Slice s2, GC *gc)
{
	if (s1.cap > 0 && s1._elemsize != elemsize || s2.cap > 0 && s2._elemsize != elemsize)
		assert(!"slice type mismatch");

	if (cg2c_sllen(s2) == 0)
		return s1;
	size_t i = cg2c_sllen(s1);
	size_t n = cg2c_sllen(s1) + cg2c_sllen(s2); // TODO: overflow check?
	if (cg2c_slcap(s1) < n)
		s1 = growslice(elemsize, s1, n, gc);
	s1 = cg2c_slice(elemsize, s1, 0, n);
	memmove(cg2c_slindex(elemsize, s1, i), cg2c_slindex(elemsize, s2, 0), cg2c_sllen(s2)*elemsize);
	return s1;
}

Slice cg2c_slclone(size_t elemsize, Slice s, GC *gc) { return cg2c_slcat(elemsize, NULL_SLICE, s, gc); }

Slice
growslice(size_t elemsize, Slice old, size_t cap, GC *gc)
{
	// TODO: add overflow checks

	size_t newcap = 1;
	while (0 < newcap && newcap < cap)
		newcap *= 2;

	void *p = cg2c_malloc(newcap*elemsize, gc);
	if (old.len > 0)
		memmove(p, old.data, old.len*elemsize);

	return (Slice) {
		.data      = p,
		.len       = old.len,
		.cap       = newcap,
		._elemsize = elemsize,
	};
}
