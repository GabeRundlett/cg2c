// TODO: autogenerate this from ops.json
enum Op {
  Add,

  Addc,

  Sub,

  Subb,

  Mul,

  MulHi,

  // UMulHi returns the high bits of the product of its arguments
  UMulHi,

  Div,

  UDiv,

  Rem,

  URem,

  FAdd,

  FSub,

  FMul,

  FMA,

  FDiv,

  Sqrt,

  And,

  Or,

  // Note: Xor is not defined for boolean operands. Neq should be used instead.
  Xor,

  Lsh,

  Rsh,

  URsh,

  Eq,

  EqF,

  Neq,

  NeqF,

  Lt,

  LtU,

  LtF,

  Lte,

  LteU,

  LteF,

  If,

  FMin,

  FMax,

  FAbs,

  Neg,

  FNeg,

  Not,

  Floor,

  Ceil,

  RoundEven,

  Sin,

  Cos,

  // Found in Turing+ Nvidia hardware
  //
  // https://gitlab.freedesktop.org/mesa/mesa/-/blob/e68adf19bc3b55a76a12a58a5b32d2a341fd78dd/src/intel/compiler/brw_eu_defines.h#L1604
  Tanh,

  Exp2,

  Log2,

  // Found in Intel hardware
  //
  // https://gitlab.freedesktop.org/mesa/mesa/-/blob/e68adf19bc3b55a76a12a58a5b32d2a341fd78dd/src/intel/compiler/brw_eu_defines.h#L1604
  //
  // Results might differ from exp2(log2(a) * b). NIR rewrite rules apply to only if exp2 is not marked exact: https://gitlab.freedesktop.org/mesa/mesa/-/blob/e68adf19bc3b55a76a12a58a5b32d2a341fd78dd/src/compiler/nir/nir_opt_algebraic.py#L1276
  Powr,

  Param,

  // Phi is like Param, but specifies its predecessors, and there can be many Phis in one Func.
  Phi,

  // Note: by convention, Const definitions should not carry source location info. Users of cg2c_buildValue should pass zero Pos when creating a Const definition.
  //
  // TODO: store constants sign-extended?
  Const,

  Func,

  Sym,

  Trunc,

  SignExt,

  ZeroExt,

  IToF,

  UToF,

  FToI,

  FToU,

  FToF,

  RoundF16,

  RoundF32,

  PtrToInt,

  IntToPtr,

  MakeTuple,

  Extract,

  SubgroupShuffle,

  SubgroupShuffleXor,

  SubgroupReduce,

  SubgroupInclusiveScan,

  SubgroupExclusiveScan,

  // AddPtr offsets a pointer by a signed offset
  AddPtr,

  // Var returns a pointer to some location, uniquely corresponding to an instance of this instruction within a dynamic instance of the function this instruction belongs in called by Callc.
  Var,

  // Load loads a value from an aux-aligned a₁
  Load,

  // Store stores a value to an aux-aligned a₁
  Store,

  AtomicLoad,

  AtomicStore,

  AtomicExchange,

  AtomicCompareAndSwap,

  AtomicRMW,

  ImageLoad,

  ImageStore,

  // Implicit LOD sample.
  //
  // Projector sampling intentionally left out.
  Tex,

  If2,

  Loop,

  Call,

  CallStructuredIf,

  Callc2,

  // Ret produces a return continuation for a function. It can not actually be called.
  //
  // The argument must a Func op.
  Ret,

  // MakeInt64 helps lowering int64 arithmetic. It should not appear in the final program.
  MakeInt64,

  GetIndirectConstant,

  GetPushConstant,

  // LoadSysval returns a system value specified in the aux field
  LoadSysval,

  // Like LoadSysval, but doesn't take mem
  GetSysval,

  GetInput,

  StoreOutput,

  // It's up to backends to decide which address space a pointer defined by certain operations (such as Var) points to, and optimize appropriately.
  PtrIsGlobal,

  PtrIsShared,

  PtrIsScratch,

  LoadGlobal,

  LoadShared,

  LoadScratch,

  StoreGlobal,

  StoreShared,

  StoreScratch,
}
