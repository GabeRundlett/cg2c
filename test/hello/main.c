#define _POSIX_C_SOURCE 199309L

#include "hello.h"

#include <assert.h>
#include <complex.h>
#include <errno.h>
#include <math.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <vulkan/vulkan_core.h>

#include <SDL.h>
#include <SDL_vulkan.h>

#include "fft.h"
#include "llama.h"

#ifdef __ANDROID_API__
#include <android/log.h>
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

static int64_t
timespec_diff(struct timespec a, struct timespec b)
{
	return (int64_t)(a.tv_sec - b.tv_sec) * 1000000000 + (int64_t)a.tv_nsec - (int64_t)b.tv_nsec;
}

static uint32_t
reversebits(uint32_t x)
{
	x = ((x >> 16) & 0x0000ffff) | ((x & 0x0000ffff) << 16);
	x = ((x >> 8) & 0x00ff00ff) | ((x & 0x00ff00ff) << 8);
	x = ((x >> 4) & 0x0f0f0f0f) | ((x & 0x0f0f0f0f) << 4);
	x = ((x >> 2) & 0x33333333) | ((x & 0x33333333) << 2);
	x = ((x >> 1) & 0x55555555) | ((x & 0x55555555) << 1);
	return x;
}

static float complex
powfW_N(float N, float k)
{
	double twoPi = 2.0 * M_PI;
	return cexpf(-I * (float)twoPi * k / N);
}

static void
fft(const float complex *x, float complex *X, size_t n, size_t s)
{
	assert(n > 0 && (n&(n-1)) == 0);

	if (n == 1) {
		X[0] = x[0];
		return;
	}

	fft(x, X, n/2, 2*s);
	fft(x+s, X+n/2, n/2, 2*s);

	for (size_t k = 0; k < n/2; k++) {
		float complex X_k = X[k];
		float complex z = powfW_N((float)n, (float)k) * X[k+n/2];
		X[k] = X_k + z;
		X[k+n/2] = X_k - z;
	}
}

static uint32_t roundUp32(uint32_t n, uint32_t a) { return (n + a - 1) & ~(a - 1); }

void initfft(void);
void initllama(void);

static Submission *ba = &(Submission) {};

enum {
	lgN = 11,
	N   = 1 << lgN,
};

typedef struct Sample Sample;
struct Sample {
	int16_t left; // TODO: should be char[2]
	int16_t right;
};

typedef struct Sink Sink;
struct Sink {
	uint8_t *audioData;
	uint32_t audioLen;
	size_t playbackCursor;

	_Atomic size_t fftCursor;
};

static void
audiocb(void *sink_, uint8_t *buf, int len)
{
	Sink *sink = sink_;

	size_t nsample = (size_t)len / 4;

	// TODO: fill-in silence instead
	assert(sink->playbackCursor+nsample < sink->audioLen/4);

	size_t n = nsample;

	memmove(buf, sink->audioData+sink->playbackCursor*4, n*4);
	sink->playbackCursor += nsample;

	atomic_store(&sink->fftCursor, sink->playbackCursor);
}

static int64_t
nanotime(void)
{
	struct timespec t = {};
	clock_gettime(CLOCK_MONOTONIC, &t);
	return (int64_t)t.tv_sec*1000000000 + (int64_t)t.tv_nsec;
}

static bool
surfaceFormatEqual(VkSurfaceFormatKHR a, VkSurfaceFormatKHR b)
{
	return a.format == b.format && a.colorSpace == b.colorSpace;
}

extern VkRenderPass dunno;

extern VkPipelineLayout llamaLayout;
extern VkPipeline llama;

extern VkPipelineLayout trianglePipelineLayout;
extern VkPipeline trianglePipeline;

Swapchain swapchain2 = {};

ImageView *swapchainImageViews[10] = {};

static void resize(Swapchain *swapchain, VkSurfaceKHR surface, uint32_t width, uint32_t height);

static VkSurfaceFormatKHR pickSurfaceFormat(VkSurfaceKHR surface);

static uint32_t
maxu32(uint32_t a, uint32_t b)
{
	if (a > b)
		return a;
	return b;
}

static void
cmdBindTheMassiveDescriptorSet(VkCommandBuffer cb, VkPipelineBindPoint bindPoint, VkPipelineLayout pipelineLayout)
{
	VkDescriptorSet sets[] = {bindless};

	fns.vkCmdBindDescriptorSets(cb, bindPoint, pipelineLayout, 0, nelem(sets), sets, 0, NULL);
}

static void
cmdSetZeroedViewportAndScissor(VkCommandBuffer cb, uint32_t width, uint32_t height)
{
	fns.vkCmdSetViewport(cb, 0, 1, &(VkViewport) {
		.x = 0.0f,
		.y = 0.0f,
		.width = (float)width,
		.height = (float)height,
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	});

	fns.vkCmdSetScissor(cb, 0, 1, &(VkRect2D) {
		.offset = {0, 0},
		.extent = {width, height},
	});
}

int
SDL_main(int argc, char **argv)
{
	SDL_InitSubSystem(SDL_INIT_AUDIO | SDL_INIT_VIDEO);

	VkResult r;

	init();
	initbindless();

	uint32_t width = N / 2; // symmetry
	uint32_t height = 800;

	// Window/surface has to be created prior to any shaders to figure out
	// the formats
	SDL_Window *window = SDL_CreateWindow("Hello",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
		SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	VkSurfaceKHR surface;
	if (!SDL_Vulkan_CreateSurface(window, instance, &surface))
		abort();

	// SDL2 doesn't send us a resize message when the window is just
	// created, so do the first resize by ourselves
	resize(&swapchain2, surface, width, height);

	initfft();
	initllama();

	// We need one for each swapchain image with multiple frames in flight
	VkSemaphore swapchainImageSem;
	if ((r = fns.vkCreateSemaphore(device, &(VkSemaphoreCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	}, NULL, &swapchainImageSem)) != VK_SUCCESS)
		fatalvk("vkCreateSemaphore: ", r);

	float complex *x = deviceMalloc(N * sizeof x[0], DEVICE_MALLOC_SYSMEM);
	float complex *x_hostptr = device2hostptr((VkDeviceAddress)x);

	float complex *X = deviceMalloc(N * sizeof X[0], DEVICE_MALLOC_SYSMEM);
	float complex *X_hostptr = device2hostptr((VkDeviceAddress)X);

	float *snow = deviceMalloc(width * sizeof snow[0], DEVICE_MALLOC_SYSMEM);

	typedef struct {
		float x0;
		float y0;
		float x1;
		float y1;
	} Rect;

	Rect *rects = deviceMalloc(100 * sizeof rects[0], DEVICE_MALLOC_SYSMEM);
	Rect *rects_hostptr = device2hostptr((VkDeviceAddress)rects);

	Sampler *sampler;
	createSampler(&(VkSamplerCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
		.magFilter = VK_FILTER_LINEAR,
		.minFilter = VK_FILTER_LINEAR,
		.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
		.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
		.mipLodBias = 0.0f, // this is what we'd use if we used some TAAU thing
		.anisotropyEnable = VK_TRUE,
		.maxAnisotropy = 4.0f,
		.minLod = 0.0f,
		.maxLod = VK_LOD_CLAMP_NONE,
	}, &sampler);

	Image *frog;

	{
		FILE *f = fopen("test/hello/frog.ktx2", "rb");
		if (f == NULL)
			assert(0);

		TextureHeader h = {};
		if (readKTX2(f, &h) < 0)
			assert(0);

		// I think we will need to transition *all* subresources of this
		// image before we use it in sampling, even if lod clamp is
		// used.
		//
		// It's not really clear when to do this, when we could do
		// uploads on any random queue we want. I guess we'll need to
		// verify the above statement as it doesn't sound super
		// plausible.
		createImage(&(VkImageCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
			// TODO: mark it mutable to all formats?
			.imageType = h.imageType,
			.format = h.format,
			.extent = h.extent,
			.mipLevels = h.levelCount,
			.arrayLayers = 1,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			// TODO: also share it to all queues if applicable
		}, &frog);

		for (uint32_t i = 0; i < h.levelCount; i++) {
			void *staging = deviceMalloc(h.levels[i].len, DEVICE_MALLOC_SYSMEM);
			void *staging_hostptr = device2hostptr((VkDeviceAddress)staging);

			fseek(f, h.levels[i].off, SEEK_SET);
			fread(staging_hostptr, h.levels[i].len, 1, f);

			// TODO: stage instead of uploading immediately

			doImageUpload(ba, &(DoUpload) {
				.dst = frog,
				.src = staging,
				.imageSubresource = {
					.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
					.mipLevel = i,
					.baseArrayLayer = 0,
					.layerCount = 1,
				},
				.imageOffset = {0, 0, 0},
				.imageExtent = {
					maxu32(h.extent.width >> i, 1),
					maxu32(h.extent.height >> i, 1),
					maxu32(h.extent.depth >> i, 1),
				},
			});
		}

		fclose(f);
	}

	ImageView *frogView;
	createImageView(&(VkImageViewCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.image = frog->vk,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = VK_FORMAT_R8G8B8A8_SRGB,
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
			.baseMipLevel = 0,
			.levelCount = VK_REMAINING_MIP_LEVELS,
			.baseArrayLayer = 0,
			.layerCount = VK_REMAINING_ARRAY_LAYERS,
		},
	}, &frogView);

	Sink *sink = emalloc(sizeof *sink);

	{
		SDL_AudioSpec spec = {};
		if (argc < 2) {
			fprintf(stderr, "usage: hello file.wav\n");
			return 1;
		}
		if (SDL_LoadWAV(argv[1], &spec, &sink->audioData, &sink->audioLen) == NULL) {
			fprintf(stderr, "error: could not load wav: %s\n", argv[1]);
			return 1;
		}

		assert(spec.freq == 48000);
		assert(spec.channels == 2);
		assert(spec.format == AUDIO_S16LSB);
	}

	SDL_AudioSpec audioDeviceSpec;
	SDL_AudioDeviceID audioDevice = SDL_OpenAudioDevice(NULL, 0, &(SDL_AudioSpec) {
		.freq     = 48000,
		.format   = AUDIO_S16LSB,
		.channels = 2,
		.callback = audiocb,
		.userdata = sink,
	}, &audioDeviceSpec, 0);
	SDL_PauseAudioDevice(audioDevice, 0);

	int64_t last = nanotime();

	double t = 0.0;

	for (;;) {
		bool quit = false;
		for (SDL_Event e; SDL_PollEvent(&e);) {
			switch (e.type) {
			case SDL_QUIT: {
				quit = true; // eww goto emulation
				break;
			}

			case SDL_WINDOWEVENT: {
				if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED && e.window.data1 > 0 && e.window.data2 > 0)
					resize(&swapchain2, surface, width = e.window.data1, height = e.window.data2);
				break;
			}

			default:
				break;
			}
		}

		if (quit)
			break;

		int64_t frametimeNs = nanotime() - last;
		last = nanotime();

		size_t off = atomic_fetch_add(&sink->fftCursor, 48000 * frametimeNs / 1000000000);

		Sample *samples = (void*)sink->audioData;
		size_t nsample = sink->audioLen / 4;

		for (size_t i = 0; i < N; i++) {
			int16_t s = 0;
			if (off+2*i < nsample)
				s = samples[off+2*i].left;
			x_hostptr[i] = (float)s / 32767.0f;
		}

		BufferAndOffset X_info = deviceptrinfo((VkDeviceAddress)X);
		// 256 (which coincides to be the max nonCoherentAtomSize) is
		// enough, that's the minimum alignment our allocator uses
		assert((N * sizeof(float complex)) % 256 == 0);
		if ((r = fns.vkFlushMappedMemoryRanges(device, 1, &(VkMappedMemoryRange) {
			.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
			.memory = X_info.memory,
			.offset = X_info.offset,
			.size = N * sizeof(float complex),
		})) != VK_SUCCESS)
			fatalvk("vkFlushMappedMemoryRanges: ", r);

		uint32_t swapchainImageIndex;
		if ((r = fns.vkAcquireNextImageKHR(device, swapchain2.vk, UINT64_MAX, swapchainImageSem, VK_NULL_HANDLE, &swapchainImageIndex)) < 0)
			fatalvk("vkAcquireNextImage2KHR: ", r);

		if (false) {
			fft(x_hostptr, X_hostptr, 1<<lgN, 1);
		} else {
			doFFT(ba, &(DoFFT) {
				.src = x,
				.dst = X,
				.lgN = lgN,
			});
		}

		VkFramebuffer fb0;
		if ((r = fns.vkCreateFramebuffer(device, &(VkFramebufferCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.pNext = &(VkFramebufferAttachmentsCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO,
				.attachmentImageInfoCount = 1,
				.pAttachmentImageInfos = (VkFramebufferAttachmentImageInfo[]) {
					{
						.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO,
						.flags = VK_IMAGE_CREATE_EXTENDED_USAGE_BIT | VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT,
						.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
						.width = width,
						.height = height,
						.layerCount = 1,
						.viewFormatCount = 2,
						.pViewFormats = (VkFormat[]) {
							swapchain2.format,
							VK_FORMAT_B8G8R8A8_SRGB, // TODO: use a function
						},
					},
				},
			},
			.flags = VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT,
			.renderPass = dunno,
			.attachmentCount = 1,
			.width = width,
			.height = height,
			.layers = 1,
		}, NULL, &fb0)) != VK_SUCCESS)
			fatalvk("vkCreateFramebuffer: ", r);

		ImageView *swapchainAttachmentView;
		createImageView(&(VkImageViewCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.pNext = &(VkImageViewUsageCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO,
				.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			},
			.image = swapchain2.images[swapchainImageIndex]->vk,
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = VK_FORMAT_B8G8R8A8_SRGB,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		}, &swapchainAttachmentView);

		{
			// TODO: tokens should be specified separately and as
			// part of Submission, this should enable better
			// composability

			Node nodes[] = {
				{
					.token = 69,
					.stageMask = VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT, // VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT?
					.accessMask = VK_ACCESS_2_MEMORY_READ_BIT | VK_ACCESS_2_MEMORY_WRITE_BIT,
					.image = swapchain2.images[swapchainImageIndex],
					.discard = true,
					.layout = VK_IMAGE_LAYOUT_GENERAL,
					.subresourceRange = {
						.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
						.baseMipLevel = 0,
						.levelCount = VK_REMAINING_MIP_LEVELS,
						.baseArrayLayer = 0,
						.layerCount = VK_REMAINING_ARRAY_LAYERS,
					},
					.label = "write to image",
				},
			};

			VkCommandBuffer cb = getcb(ba);
			cmdBindTheMassiveDescriptorSet(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, trianglePipelineLayout);

			fns.vkCmdBeginRenderPass2KHR(cb, &(VkRenderPassBeginInfo) {
				.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				.pNext = &(VkRenderPassAttachmentBeginInfo) {
					.sType = VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO,
					.attachmentCount = 1,
					.pAttachments = (VkImageView[]) {
						getImageViewVk(swapchainAttachmentView),
					},
				},
				.renderPass = dunno,
				.framebuffer = fb0,
				.renderArea = {{0, 0}, {width, height}},
				.clearValueCount = 1,
				.pClearValues = (VkClearValue[]) {
					{.color.float32 = {0.0f, 0.0f, 0.0f, 1.0f}},
				},
			}, &(VkSubpassBeginInfo) {.sType = VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO});

			float aspectRatio = (float)width / (float)height;

			// We need tables of each dot's phase and then smoothly switch between these tables

			int n = 7;
			for (int i = 0; i < n; i++) {
				double t2 = 2.0 * M_PI * ((double)i / (double)n + t / 10.0 * ((double)i / (double)n + 1.0));

				float x = (float)(sin(t2) / 2.0);
				float y = (float)(cos(t2) / 2.0);

				Rect rect = {
					.x0 = x - 0.1f,
					.y0 = (y - 0.1f) * aspectRatio,
					.x1 = x + 0.1f,
					.y1 = (y + 0.1f) * aspectRatio,
				};

				rects_hostptr[i] = rect;
			}

			t += (double)frametimeNs / 1e9;
			{
				double w = 2.0*M_PI*t/5.0;

				t += (sin(w)*sin(w)) * 0.01;
			}

			struct {
				float aspectRatio;
				Rect *rects;
				uint32_t imageHandle;
				uint32_t samplerHandle;
			} params = {
				.aspectRatio = aspectRatio,
				.rects = (Rect*)rects,
				.imageHandle = getImageViewSamplingHandle(frogView, VK_IMAGE_LAYOUT_GENERAL),
				.samplerHandle = getSamplerHandle(sampler),
			};

			fns.vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, trianglePipeline);
			cmdSetZeroedViewportAndScissor(cb, width, height);

			fns.vkCmdPushConstants(cb, trianglePipelineLayout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof params, &params);
			fns.vkCmdDraw(cb, n * 6, 1, 0, 0);

			fns.vkCmdEndRenderPass2KHR(cb, &(VkSubpassEndInfo) {.sType = VK_STRUCTURE_TYPE_SUBPASS_END_INFO});

			execPass(ba, &(Pass) {
				.waitSem = swapchainImageSem,
				.nodes = nodes,
				.nodeCount = nelem(nodes),
				.cb = cb,
				.label = "llama2",
			});
		}

		{
			Node nodes[] = {
				{
					.token = 42,
					.stageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT,
					.accessMask = VK_ACCESS_2_MEMORY_READ_BIT,
					.label = "read A",
				},
				{
					.token = 69,
					.stageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT,
					.accessMask = VK_ACCESS_2_MEMORY_WRITE_BIT,
					.image = swapchain2.images[swapchainImageIndex],
					.layout = VK_IMAGE_LAYOUT_GENERAL,
					.subresourceRange = {
						.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
						.baseMipLevel = 0,
						.levelCount = VK_REMAINING_MIP_LEVELS,
						.baseArrayLayer = 0,
						.layerCount = VK_REMAINING_ARRAY_LAYERS,
					},
					.label = "write to image",
				},
			};

			VkCommandBuffer cb = getcb(ba);
			cmdBindTheMassiveDescriptorSet(cb, VK_PIPELINE_BIND_POINT_COMPUTE, llamaLayout);

			fns.vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_COMPUTE, llama);

			LlamaParams params = {
				.A = (float complex*)X,
				.snow = (float*)snow,
				.snowGravity = frametimeNs / 1000000000.0,
				.out = getImageViewHandle(swapchainImageViews[swapchainImageIndex], VK_IMAGE_LAYOUT_GENERAL),
				.outHeight = height,
			};
			fns.vkCmdPushConstants(cb, llamaLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 28 /* sizeof params */, &params);
			fns.vkCmdDispatch(cb, roundUp32(N / 2, 8) / 8, roundUp32(height, 8) / 8, 1);

			execPass(ba, &(Pass) {
				.nodes = nodes,
				.nodeCount = nelem(nodes),
				.cb = cb,
				.label = "llama",
			});
		}

		{
			Node nodes[] = {
				{
					.token = 69,
					.stageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
					.accessMask = 0,
					.image = swapchain2.images[swapchainImageIndex],
					.layout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
					.subresourceRange = {
						.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
						.baseMipLevel = 0,
						.levelCount = VK_REMAINING_MIP_LEVELS,
						.baseArrayLayer = 0,
						.layerCount = VK_REMAINING_ARRAY_LAYERS,
					},
					.label = "transition to present src",
				},
			};

			// TODO: allow passes without command buffers?
			VkCommandBuffer cb = getcb(ba);

			execPass(ba, &(Pass) {
				.cb = cb,
				.nodes = nodes,
				.nodeCount = nelem(nodes),
				.label = "transition to present src",
			});
		}

		submit(ba);

		r = fns.vkQueuePresentKHR(queue, &(VkPresentInfoKHR) {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.swapchainCount = 1,
			.pSwapchains = &swapchain2.vk,
			.pImageIndices = &swapchainImageIndex,
		});
		if (r != VK_SUCCESS && r != VK_SUBOPTIMAL_KHR && r != VK_ERROR_OUT_OF_DATE_KHR)
			fatalvk("vkQueuePresentKHR: ", r);

		destroyImageView(swapchainAttachmentView);

		fns.vkDestroyFramebuffer(device, fb0, NULL);
	}

	deviceFree(X);

	return 0;
}

// this should take swapchain object by reference, and also have bool for hdr
// and possibly no. of images to request
void
resize(Swapchain *swapchain, VkSurfaceKHR surface, uint32_t width, uint32_t height)
{
	VkResult r;

	swapchain->format = VK_FORMAT_B8G8R8A8_UNORM;
	swapchain->colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

	if (false) {
		if ((r = fns.vkDeviceWaitIdle(device)) != VK_SUCCESS)
			fatalvk("vkDeviceWaitIdle: ", r);

		fns.vkDestroySwapchainKHR(device, swapchain->vk, NULL);
		swapchain->vk = VK_NULL_HANDLE;
	}

	VkSurfaceCapabilitiesKHR caps;
	if ((r = fns.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &caps)) != VK_SUCCESS)
		fatalvk("vkGetPhysicalDeviceSurfaceCapabilitiesKHR: ", r);

	enum {
		wantImageCount = 2,
	};

	uint32_t minImageCount = wantImageCount;
	if (minImageCount < caps.minImageCount)
		minImageCount = caps.minImageCount;
	if (caps.maxImageCount > 0 && minImageCount > caps.maxImageCount)
		minImageCount = caps.maxImageCount;

	/*
	 * Try VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR, but should it not be
	 * supported, pick any other compositing mode.
	 *
	 * Vulkan spec 1.2.165, section 33.5.1. Surface Capabilities:
	 *
	 * "supportedCompositeAlpha ... at least one bit will be set. Opaque
	 * composition can be achieved in any alpha compositing mode by either
	 * using an image format that has no alpha component, or by ensuring
	 * that all pixels in the presentable images have an alpha value of
	 * 1.0."
	 */
	VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	while (!(caps.supportedCompositeAlpha & compositeAlpha))
		compositeAlpha <<= 1;

	if ((r = fns.vkCreateSwapchainKHR(device, &(VkSwapchainCreateInfoKHR) {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.pNext = &(VkImageFormatListCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO,
			.viewFormatCount = 2,
			.pViewFormats = (VkFormat[]) {
				swapchain->format,
				VK_FORMAT_B8G8R8A8_SRGB, // TODO: use a function
			},
		},
		.flags = VK_SWAPCHAIN_CREATE_MUTABLE_FORMAT_BIT_KHR,
		.surface = surface,
		.minImageCount = minImageCount,
		.imageFormat = swapchain->format,
		.imageColorSpace = swapchain->colorSpace,
		.imageExtent = {width, height},
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
		.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
		.compositeAlpha = compositeAlpha,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR,
		.clipped = VK_TRUE,
		.oldSwapchain = swapchain->vk,
	}, NULL, &swapchain->vk)) != VK_SUCCESS)
		fatalvk("vkCreateSwapchainKHR: ", r);

	// TODO: don't do this
	VkImage swapchainImages[10];
	swapchain->imageCount = nelem(swapchainImages);
	if ((r = fns.vkGetSwapchainImagesKHR(device, swapchain->vk, &swapchain->imageCount, swapchainImages)) != VK_SUCCESS)
		fatalvk("vkGetSwapchainImagesKHR: ", r);

	swapchain->images = emalloc(swapchain->imageCount * sizeof swapchain->images[0]);
	for (uint32_t i = 0; i < swapchain->imageCount; i++) {
		swapchain->images[i] = emalloc(sizeof(Image*));
		swapchain->images[i]->vk = swapchainImages[i];
		swapchain->images[i]->layout = VK_IMAGE_LAYOUT_UNDEFINED;
	}

	for (uint32_t i = 0; i < swapchain->imageCount; i++) {
		// TODO: it's fine to create view every time we record commands

		createImageView(&(VkImageViewCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.pNext = &(VkImageViewUsageCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO,
				.usage = VK_IMAGE_USAGE_STORAGE_BIT,
			},
			.image = swapchain->images[i]->vk,
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = VK_FORMAT_B8G8R8A8_UNORM,
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		}, &swapchainImageViews[i]);
	}
}

/*
VkSurfaceFormatKHR
pickSurfaceFormat(VkSurfaceKHR surface) // bool hdr
{
	VkSurfaceFormatKHR wantFormats[] = {
		{VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR},
		{VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR}, // I hate BGRA I hate BGRA I hate BGRA
	};

	for (size_t j = 0; j < nelem(wantFormats); j++) {
		for (uint32_t i = 0; i < surfaceFormatCount; i++) {
			if (surfaceFormatEqual(surfaceFormats[i], wantFormats[j]))
				return surfaceFormats[i];
		}
	}

	return (VkSurfaceFormatKHR) {VK_FORMAT_UNDEFINED, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
}
*/
