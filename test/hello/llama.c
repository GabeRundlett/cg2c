#include "hello.h"

#include "llama.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

VkRenderPass dunno;

VkPipelineLayout llamaLayout;
VkPipeline llama;

VkPipelineLayout trianglePipelineLayout;
VkPipeline trianglePipeline;

void
initllama(void)
{
	VkResult r;

	if ((r = fns.vkCreateRenderPass2KHR(device, &(VkRenderPassCreateInfo2) {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2,
		.attachmentCount = 1,
		.pAttachments = (VkAttachmentDescription2[]) {
			{
				.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2,
				.format = VK_FORMAT_B8G8R8A8_SRGB, // swapchainFormat.format,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
				.initialLayout = VK_IMAGE_LAYOUT_GENERAL,
				.finalLayout = VK_IMAGE_LAYOUT_GENERAL,
			},
		},
		.subpassCount = 1,
		.pSubpasses = (VkSubpassDescription2[]) {
			{
				.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2,
				.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
				.colorAttachmentCount = 1,
				.pColorAttachments = (VkAttachmentReference2[]) {
					{
						.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2,
						.attachment = 0,
						.layout = VK_IMAGE_LAYOUT_GENERAL,
					},
				},
			},
		},
	}, NULL, &dunno)) != VK_SUCCESS)
		fatalvk("vkCreateRenderPass2: ", r);

	{
		const char *files[] = {"llama_shader.c"};

		Cg2cInputVk llamaMappings[] = {
			{.type = CG2C_IN_PUSH_CONSTANT, .dstOffset = 0, .size = 28, .srcOffset = 0},
			{.type = CG2C_IN_GLOBAL_INVOCATION_ID_XY, .dstOffset = offsetof(LlamaParams, x), .size = 8},
		};

		createPipelineLayout2(&(PipelineLayoutCg2CreateInfo2) {
			.stageCount = 1,
			.stages = (PerStageCg2cInputs[]) {
				{
					.stage = VK_SHADER_STAGE_COMPUTE_BIT,
					.inputCount = nelem(llamaMappings),
					.inputs = llamaMappings,
				},
			},
		}, &llamaLayout);

		createComputePipeline(&(VkComputePipelineCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
			.stage = {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = &(PipelineShaderStageCg2CreateInfo) {
					.sType  = 0,
					.inputs = llamaMappings,
					.ninput = nelem(llamaMappings),
					.extra = &(Cg2cComputeVk) {
						.workgroupSize = {8, 8, 1},
					},
					.files = files,
					.nfile = nelem(files),
				},
				.stage = VK_SHADER_STAGE_COMPUTE_BIT,
				.pName = "llama",
			},
			.layout = llamaLayout,
		}, &llama);
	}

	{
		const char *files[] = {"llama2_shader.c"};

		Cg2cInputVk llama2VertInputs[] = {
			{.type = CG2C_IN_PUSH_CONSTANT, .dstOffset = 0, .size = 16, .srcOffset = 0},
			{.type = CG2C_IN_VERTEX_ID, .dstOffset = 16, .size = 4},
		};

		Cg2cOutputVk llama2VertOutputs[] = {
			{.type = CG2C_OUT_POSITION, .offset = 0, .size = 16},
			{.type = CG2C_OUT_ATTRIBUTE, .offset = 16, .size = 16, .location = 0},
		};

		Cg2cInputVk llama2FragInputs[] = {
			{.type = CG2C_IN_PUSH_CONSTANT, .dstOffset = 0, .size = 8, .srcOffset = 16},
			{.type = CG2C_IN_FS_INPUT, .dstOffset = 16, .size = 16, .location = 0},
		};

		Cg2cOutputVk llama2FragOutputs[] = {
			{.type = CG2C_OUT_COLOR, .offset = 0, .size = 16, .location = 0},
		};

		createPipelineLayout2(&(PipelineLayoutCg2CreateInfo2) {
			.stageCount = 2,
			.stages = (PerStageCg2cInputs[]) {
				{
					.stage = VK_SHADER_STAGE_VERTEX_BIT,
					.inputCount = nelem(llama2VertInputs),
					.inputs = llama2VertInputs,
				},
				{
					.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
					.inputCount = nelem(llama2FragInputs),
					.inputs = llama2FragInputs,
				},
			},
		}, &trianglePipelineLayout);

		VkPipelineShaderStageCreateInfo stages[] = {
			{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = &(PipelineShaderStageCg2CreateInfo) {
					.sType   = 0,
					.inputs  = llama2VertInputs,
					.ninput  = nelem(llama2VertInputs),
					.outputs = llama2VertOutputs,
					.noutput = nelem(llama2VertOutputs),
					.files   = files,
					.nfile   = nelem(files),
				},
				.stage = VK_SHADER_STAGE_VERTEX_BIT,
				.pName = "llama2Vert",
			},
			{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.pNext = &(PipelineShaderStageCg2CreateInfo) {
					.sType   = 0,
					.inputs  = llama2FragInputs,
					.ninput  = nelem(llama2FragInputs),
					.outputs = llama2FragOutputs,
					.noutput = nelem(llama2FragOutputs),
					.files   = files,
					.nfile   = nelem(files),
				},
				.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pName = "llama2Frag",
			},
		};

		// TODO: read about blend equations and fix this
		VkPipelineColorBlendAttachmentState colorBlendAttachmentStates[] = {
			{
				.blendEnable = VK_TRUE,
				// Samples with non-1 alpha should blend
				// together, other stuff shouldn't.
				.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
				.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
				.colorBlendOp = VK_BLEND_OP_ADD,
				// TODO: which alpha factors should we pick?
				.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
				.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
				.alphaBlendOp = VK_BLEND_OP_ADD,
				.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
			},
		};

		VkDynamicState dynamicStates[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_SCISSOR,
		};

		createGraphicsPipeline(&(VkGraphicsPipelineCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
			.stageCount = nelem(stages),
			.pStages = stages,
			.pVertexInputState = &(VkPipelineVertexInputStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			},
			.pInputAssemblyState = &(VkPipelineInputAssemblyStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
				.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			},
			.pViewportState = &(VkPipelineViewportStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
				.viewportCount = 1,
				.scissorCount = 1,
			},
			.pRasterizationState = &(VkPipelineRasterizationStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
				.polygonMode = VK_POLYGON_MODE_FILL,
				.cullMode = VK_CULL_MODE_BACK_BIT,
				.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
				.lineWidth = 1.0f,
			},
			.pMultisampleState = &(VkPipelineMultisampleStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
				.rasterizationSamples = 1,
			},
			.pColorBlendState = &(VkPipelineColorBlendStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
				.attachmentCount = nelem(colorBlendAttachmentStates),
				.pAttachments = colorBlendAttachmentStates,
			},
			.pDynamicState = &(VkPipelineDynamicStateCreateInfo) {
				.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
				.dynamicStateCount = nelem(dynamicStates),
				.pDynamicStates = dynamicStates,
			},
			.layout = trianglePipelineLayout,
			.renderPass = dunno,
			.subpass = 0,
		}, &trianglePipeline);
	}
}
