#include "all.h"

#include <spirv/unified1/spirv.h>
#include <spirv/unified1/GLSL.std.450.h>

#include <vulkan/vulkan_core.h>

#include <cg2c/cg2c_vulkan.h>

// TODO: move the implementation of compile into this file
int compile(const Cg2cCompileInfoVk *info, const Cg2cAllocFuncs *_unused, Cg2cBlob **aOut, GC *gc);

typedef struct A A;
struct A {
	const Cg2cCompileInfoVk *info;
	const Cg2cAllocFuncs *allocFuncs;
	Cg2cBlob **aOut;
	int result;
};

static void
gcshim(void *_a, GC *gc)
{
	A *a = _a;
	a->result = compile(a->info, a->allocFuncs, a->aOut, gc);
}

int
cg2cCompileVk(const Cg2cCompileInfoVk *info, const Cg2cAllocFuncs *allocFuncs, Cg2cBlob **aOut)
{
	A a = {
		.info = info,
		.allocFuncs = allocFuncs,
		.aOut = aOut,
	};

	int r;
	if ((r = cg2c_gccall(gcshim, &a, allocFuncs)) < 0)
		return r;

	return a.result;
}
