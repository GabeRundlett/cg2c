#include "all.h"

typedef struct ComputeScopeState ComputeScopeState;
struct ComputeScopeState {
	Slice funcs;

	Map *visited;

	GC *gc;
};

static void
computeScopeImpl(ComputeScopeState *state, Value *v)
{
	if (cg2c_mapcontains(cg2c_ptr_set, state->visited, &v))
		return;

	cg2c_mapassign(cg2c_ptr_set, state->visited, &v, state->gc);

	if (v->op != OpFunc) {
		for (size_t i = 0; i < cg2c_valueNArg(v); i++)
			computeScopeImpl(state, cg2c_valueArg(v, i));
	} else {
		if (cg2c_mapcontains(cg2c_ptr_set, state->visited, &v->aux))
			return;

		state->funcs = cg2c_slappend(sizeof(Func*), state->funcs, &v->aux, state->gc);

		cg2c_mapassign(cg2c_ptr_set, state->visited, &v->aux, state->gc);
	}
}

// TODO: should be plural, i.e. computeScopes
//
// For now just find all reachable funcs
Scope
cg2c_computeScope(Func *f, GC *gc)
{
	ComputeScopeState state = {
		.visited = cg2c_mkmap(cg2c_ptr_set, gc),

		.gc = gc,
	};

	state.funcs = cg2c_slappend(sizeof(Func*), state.funcs, &f, gc);

	cg2c_mapassign(cg2c_ptr_set, state.visited, &f, gc);

	for (size_t i = 0; i < cg2c_sllen(state.funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), state.funcs, i);

		computeScopeImpl(&state, cg2c_funcTerminator(g));
	}

	return (Scope) {.funcs = state.funcs};
}

void
cg2c_scopeForeach(Compile *ctxt, void (*f)(Compile*, Value*, void*), void *etc, Scope *scope)
{
	Map *visited = cg2c_mkmap(cg2c_ptr_set, ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		cg2c_foreach(ctxt, f, etc, cg2c_funcTerminator(g), visited);
	}
}

bool
cg2c_scopeRewrite(Builder b, Scope *scope, Map *re)
{
	bool changed = false;

	if (re == NULL)
		re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Value *t = cg2c_funcTerminator(g);
		Value *t2 = cg2c_rewrite(b, t, re);

		if (t != t2) {
			cg2c_funcTerminate(g, t2, b.ctxt->gc);

			changed = true;
		}
	}

	return changed;
}

bool
cg2c_scopeRewriteFunc2(Builder b, Value *(*f)(Builder, Value*, Op, Type*, void*, Slice, void*), void *etc, Scope *scope, Map *re)
{
	bool changed = false;

	if (re == NULL)
		re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Value *t = cg2c_funcTerminator(g);
		Value *t2 = cg2c_rewriteFunc2(b, f, etc, t, re);

		if (t != t2) {
			cg2c_funcTerminate(g, t2, b.ctxt->gc);

			changed = true;
		}
	}

	return changed;
}
