#include "all.h"

// Rewrite some Loads into uses of previously stored values.
//
// Based on The Simple and Efficient Construction of Static Single Assignment
// Form by M. Braun, S. Buchwald, S. Hack, R. Leißa, C. Mallon, and A. Zwinkau
// https://pp.info.uni-karlsruhe.de/uploads/publikationen/braun13cc.pdf
//
// Note that this algorithm may scale too badly for certain programs, see
// https://github.com/golang/go/issues/16407 for more info.

// TODO: seems like dead store elimination could be merged into vars to SSA

// TODO: take interference into account

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
	Value *args[] = {a0};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
	Value *args[] = {a0, a1};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

typedef struct LoadsToSSA LoadsToSSA;
struct LoadsToSSA {
	Map *preds;
	// TODO: rename to something else
	Map *vars; // Value* -> (Map* of Func* -> Value*)
};

static Value* loadsToSSA(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc);

bool
cg2c_loadsToSSA(Builder b, Scope *scope)
{
	LoadsToSSA *ssa = &(LoadsToSSA) {
		.preds = cg2c_predecessors(scope, b.ctxt->gc),
		.vars = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc),
	};

	bool progress = cg2c_scopeRewriteFunc2(b, loadsToSSA, ssa, scope, NULL);

	// Rewrite phis into params
	/* if (progress) {
		progress = cg2c_phisToParams(b, scope) || progress;
	} */

	return progress;
}

static Value* findMem(Value *v);

// TODO: this could be cached in a funny way for better performance
static Func*
currentFunc(Value *v)
{
	for (;;) {
		switch (v->op) {
		case OpParam:
			return cg2c_valueArgFunc(v, 0);

		case OpMakeTuple: {
			// won't work for nested tuples but oh well
			for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
				Value *a = cg2c_valueArg(v, i);
				if (a->type == cg2c_types[KindMem]) {
					v = a;
					goto good;
				}
			}
			assert(0);
		good:
			break;
		}

		case OpExtract:
			v = cg2c_valueArg(v, 0);
			break;

		case OpStore:
			v = cg2c_valueArg(v, 0);
			break;

		case OpImageStore:
			v = cg2c_valueArg(v, 0);
			break;

		case OpStoreOutput:
			v = cg2c_valueArg(v, 0);
			break;

		case OpCallc2:
			v = cg2c_valueArg(v, 1);
			break;

		default:
			fprintf(stderr, "%s\n", cg2c_opInfo[v->op].name);
			assert(0); // don't know how to deal with this op
		}
	}
}

static Value*
findMem(Value *v)
{
	if (v->type == cg2c_types[KindMem])
		return v;

	switch (v->op) {
	case OpMakeTuple:
		for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
			Value *mem = findMem(cg2c_valueArg(v, i));
			if (mem != NULL)
				return mem;
		}
		break;

	case OpCall:
		return findMem(cg2c_valueArg(v, 1));

	case OpCallStructuredIf:
		return findMem(cg2c_valueArg(v, 4));

	default:
		break;
	}

	return NULL;
}

static void
store(Compile *ctxt, Func *f, Value *p, Value *v, LoadsToSSA *ssa)
{
	GC *gc = ctxt->gc;

	Map *defs = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, ssa->vars, &f);
	if (defs == NULL) {
		defs = cg2c_mkmap(cg2c_ptr_ptr_map, gc);
		*(Map**)cg2c_mapassign(cg2c_ptr_ptr_map, ssa->vars, &f, gc) = defs;
	}

	*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, defs, &p, gc) = v;
}

// TODO: warnings should be gated behind a ctxt->flag
static Value*
load(Builder b, Func *f, Type *type, int64_t align, Value *mem, Value *p, LoadsToSSA *ssa)
{
	GC *gc = b.ctxt->gc;

	Map *defs = *(Map* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, ssa->vars, &f);

	Value *v = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, defs, &p);
	if (v != NULL) {
		if (v->type == type) {
			return v;
		} else {
			// hacky hack
			//
			// TODO: make this more general
			assert(type == cg2c_types[KindPtr]);
			Value *w = load(b, f, cg2c_types[KindInt32], 4, mem, cg2c_buildAddPtrConst(b, p, 4), ssa);
			if (w != NULL) {
				Value *v64 = newValue1(b, OpZeroExt, cg2c_types[KindInt64], v);
				Value *w64 = newValue1(b, OpZeroExt, cg2c_types[KindInt64], w);
				Value *w64lsh32 = cg2c_buildShiftConst(b, OpLsh, w64, 32);
				return cg2c_buildIntToPtr(b, newValue2(b, OpOr, cg2c_types[KindInt64], v64, w64lsh32));
			}
			goto rip;
		}
	} else {
		Slice incs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, ssa->preds, &f);
		if (cg2c_sllen(incs) == 0)
			goto rip;
		Value *fv = cg2c_buildFuncValue(b, f);
		Slice phi = cg2c_slappend(sizeof(Value*), NULL_SLICE, &fv, gc);
		for (size_t i = 0; i < cg2c_sllen(incs); i++) {
			Func *g = (*(Incoming*)cg2c_slindex(sizeof(Incoming), incs, i)).f;
			Value *gv = cg2c_buildFuncValue(b, g);
			Value *u = load(b, g, type, align, mem, p, ssa);
			// TODO: could we do anything better in this case?
			if (u == NULL)
				goto rip;
			phi = cg2c_slappend(sizeof(Value*), phi, &u, gc);
			phi = cg2c_slappend(sizeof(Value*), phi, &gv, gc);
		}
		return cg2c_buildValue(b, OpPhi, type, NULL, phi);
	}

rip:
	return NULL;
}

static Value*
loadsToSSA(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	GC *gc = b.ctxt->gc;

	LoadsToSSA *ssa = etc;

	switch (op) {
	case OpStore: {
		Value *mem = *(Value**)cg2c_slindex(sizeof(Value*), args, 0);
		Value *p = *(Value**)cg2c_slindex(sizeof(Value*), args, 1);
		Value *w = *(Value**)cg2c_slindex(sizeof(Value*), args, 2);

		store(b.ctxt, currentFunc(mem), p, w, ssa);

		break;
	}

	case OpLoad: {
		int64_t align = *(int64_t*)aux;

		Value *mem = *(Value**)cg2c_slindex(sizeof(Value*), args, 0);
		Value *p = *(Value**)cg2c_slindex(sizeof(Value*), args, 1);

		Value *v = load(b, currentFunc(mem), type, align, mem, p, ssa);
		if (v != NULL) {
			// cg2c_warnfAt(b.ctxt, b.pos, "replaced a load with the value previously stored at shader.c:0:0");
			return v;
		}

		// cg2c_warnfAt(b.ctxt, b.pos, "couldn't replace a load");

		break;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}
