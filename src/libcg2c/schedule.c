#include "all.h"

typedef struct Etc Etc;
struct Etc {
	Slice defs;

	// Map a value to values that depend on it. Useful for scheduling loads
	// before any other uses of mem.
	Map *dependents; // Value* → Slice of Value*
};

// TODO: scheduler will never be able to schedule phi-using programs with loops
// in them. Make sure we remvoe that stuff first.

static void
walker(Compile *ctxt, Value *v, void *_etc)
{
	Etc *etc = _etc;

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *v_i = cg2c_valueArg(v, i);

		Slice d = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, etc->dependents, &v_i);
		d = cg2c_slappend(sizeof(Value*), d, &v, ctxt->gc);
		*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, etc->dependents, &v_i, ctxt->gc) = d;
	}

	etc->defs = cg2c_slappend(sizeof(Value*), etc->defs, &v, ctxt->gc);
}

// TODO: set this in opInfo, though I think a better idea is to introduce a type
// along the lines of "MemRef"
static bool
borrowsMem(Value *v)
{
	switch (v->op) {
	case OpLoad:
	case OpTex:
	case OpImageLoad:
	case OpLoadSysval:
	case OpLoadGlobal:
	case OpLoadShared:
	case OpLoadScratch:
		return true;

	default:
		return false;
	}
}

static bool
isTerminator(Value *v)
{
	// TODO: change to return cg2c_typeKind(v->type) == KindNoret;
	return v->op == OpCall || v->op == OpCallStructuredIf;
}

static bool
allBorrowingUsesScheduled(Value *v, Map *dependents, Map *scheduled)
{
	Slice d = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, dependents, &v);

	for (size_t i = 0; i < cg2c_sllen(d); i++) {
		Value *u = *(Value**)cg2c_slindex(sizeof(Value*), d, i);

		if (borrowsMem(u) && !cg2c_mapcontains(cg2c_ptr_set, scheduled, &u))
			return false;
	}

	return true;
}

void
cg2c_schedule(Compile *ctxt, Scope *scope)
{
	// Schedule instructions as soon as all of their inputs are available
	// and other constraints are met

	Map *visited = cg2c_mkmap(cg2c_ptr_set, ctxt->gc);

	Etc *etc = &(Etc) {
		.dependents = cg2c_mkmap(cg2c_ptr_slice_map, ctxt->gc),
	};

	cg2c_scopeForeach(ctxt, walker, etc, scope);

	Map *scheduled = cg2c_mkmap(cg2c_ptr_set, ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Slice values2 = NULL_SLICE;

		// First, schedule Args and Funcs. These must be scheduled before any other
		// instructions
		for (size_t j = 0; j < cg2c_sllen(etc->defs); j++) {
			Value *v = *(Value**)cg2c_slindex(sizeof(Value*), etc->defs, j);

			if ((v->op == OpParam || v->op == OpPhi) && cg2c_valueArgFunc(v, 0) == g || v->op == OpFunc && cg2c_valueAuxFunc(v) == g) {
				if (v->op == OpFunc && cg2c_mapcontains(cg2c_ptr_set, scheduled, &v))
					continue;

				assert(!cg2c_mapcontains(cg2c_ptr_set, scheduled, &v));

				values2 = cg2c_slappend(sizeof(Value*), values2, &v, ctxt->gc);
				cg2c_mapassign(cg2c_ptr_set, scheduled, &v, ctxt->gc);
			}
		}

		bool progress;
		do {
			progress = false;

			for (size_t j = 0; j < cg2c_sllen(etc->defs); j++) {
				Value *v = *(Value**)cg2c_slindex(sizeof(Value*), etc->defs, j);

				// Don't schedule args here
				if (v->op == OpParam || v->op == OpPhi)
					continue;

				if (isTerminator(v))
					continue;

				if (cg2c_mapcontains(cg2c_ptr_set, scheduled, &v))
					continue;

				// Note: this should be ignored for Phis if we
				// ever to intend to schedule them
				for (size_t k = 0; k < cg2c_valueNArg(v); k++) {
					Value *v_k = cg2c_valueArg(v, k);
					if (!cg2c_mapcontains(cg2c_ptr_set, scheduled, &v_k))
						goto bad; // not all args are scheduled yet
				}

				if (!borrowsMem(v)) {
					for (size_t k = 0; k < cg2c_valueNArg(v); k++) {
						Value *v_k = cg2c_valueArg(v, k);
						if (cg2c_typeKind(v_k->type) == KindMem && !allBorrowingUsesScheduled(v_k, etc->dependents, scheduled))
							goto bad; // not all loads that use this mem are scheduled
					}
				}

				values2 = cg2c_slappend(sizeof(Value*), values2, &v, ctxt->gc);
				cg2c_mapassign(cg2c_ptr_set, scheduled, &v, ctxt->gc);

				progress = true;

			bad:;
			}
		} while (progress);

		{
			Value *v = cg2c_funcTerminator(g);

			assert(!cg2c_mapcontains(cg2c_ptr_set, scheduled, &v));

			for (size_t k = 0; k < cg2c_valueNArg(v); k++) {
				Value *v_k = cg2c_valueArg(v, k);
				assert(cg2c_mapcontains(cg2c_ptr_set, scheduled, &v_k));
			}

			values2 = cg2c_slappend(sizeof(Value*), values2, &v, ctxt->gc);
			cg2c_mapassign(cg2c_ptr_set, scheduled, &v, ctxt->gc);
		}

		g->values = values2;
	}
}
