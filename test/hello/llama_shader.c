#include "llama.h"

void
llama(LlamaParams *arg)
{
	int32_t x = arg->x;
	float y = arg->y + (float)1 / 2;
	float yRef = arg->A[x];
	yRef = yRef * (2*(yRef >= 0) - 1);

	int draw = y < yRef;
	int clipped = y >= (int)(arg->outHeight * ((float)9 / 10));

	float drawSnowflake = y - (int)arg->snow[x];
	drawSnowflake = 0 <= drawSnowflake & drawSnowflake < 1;

	__float4 color;
	color.x = clipped + drawSnowflake;
	color.y = (1-clipped) + drawSnowflake;
	color.z = drawSnowflake;
	color.w = 1;
	if (draw | drawSnowflake != (float)0)
		__image2DStore(arg->out, x, arg->outHeight-1-y, color);

	float snowflake = arg->snow[x];
	if (0 < snowflake)
		snowflake = snowflake - arg->snowGravity;
	if (snowflake < yRef)
		snowflake = yRef;
	arg->snow[x] = snowflake;
}
