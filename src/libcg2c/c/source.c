#include "all.h"
#include "cg2.h"

// TODO: introduce functions to begin and end segment for backslash-at-eol support

static Pos pos(Noder *p) { return (Pos) {NULL, p->line, p->col}; }

void
cg2c_getc(Noder *p)
{
	p->off = p->roff;
	if (p->ch == '\n') {
		p->line++;
		p->col = 1;
	} else {
		p->col++;
	}

	if (p->off < cg2c_strlen(p->in)) {
		Rune r = (Rune)(unsigned char)cg2c_strdata(p->in)[p->off]; // fast path for common runes
		size_t w = 1;
		if (r >= Runeself) {
			w = (size_t)cg2c_chartorune(&r, cg2c_strdata(p->in)+p->off, cg2c_strlen(p->in)-p->off);
			if (r == Runeerror && w == 1)
				cg2c_errorfAt(p->ctxt, pos(p), "invalid UTF-8 encoding");
			else if (r == 0xFEFF)
				cg2c_errorfAt(p->ctxt, pos(p), "invalid BOM");
		} else if (r == 0) {
			cg2c_errorfAt(p->ctxt, pos(p), "invalid NUL character");
		}
		p->roff += w;
		p->ch = r;
	} else {
		p->ch = -1;
	}
}

Rune
cg2c_peekc(Noder *p)
{
	// cg2c_peekc is called rarely so we don't do anything special.
	Rune r;
	cg2c_chartorune(&r, cg2c_strdata(p->in)+p->roff, cg2c_strlen(p->in)-p->roff);
	return r;
}
