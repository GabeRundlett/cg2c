#include "hello.h"

void
nameObject(uint64_t handle, VkObjectType type, const char *format, ...)
{
	VkResult r;

	// TODO: actually format

	if ((r = fns.vkSetDebugUtilsObjectNameEXT(device, &(VkDebugUtilsObjectNameInfoEXT) {
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT,
		.objectType = type,
		.objectHandle = handle,
		.pObjectName = format,
	})) != VK_SUCCESS)
		fatalvk("vkSetDebugUtilsObjectNameEXT: ", r);
}

void
fatalvk(const char *s, VkResult resno)
{
	fprintf(stderr, "%svulkan error #%d\n", s, (int)resno);
	_Exit(1); // see fatalf
}
