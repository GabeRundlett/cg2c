import 'dart:async';
import 'dart:convert';
import 'dart:io';

class GraphvizException implements Exception {
  final String message;

  GraphvizException(this.message);

  @override
  String toString() => 'GraphvizException: $message';
}

Future<String> graphviz({
  required String dot, // TODO: streaming dot?
}) async {
  var process = await Process.start('dot', ['-T', 'svg']);

  var dataFuture = process.stdout.transform(utf8.decoder).join();
  var errorFuture = process.stderr.transform(utf8.decoder).join();

  process.stdin.write(dot);
  await process.stdin.flush();
  await process.stdin.close();

  var data = await dataFuture;
  var error = await errorFuture;

  var exitCode = await process.exitCode;
  if (exitCode != 0) throw GraphvizException(error);

  if (error != "") print(error);

  return data;
}
