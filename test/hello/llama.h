#include <complex.h>
#include <math.h>
#include <stdint.h>

// TODO: could we somehow get rid of this ifdef so that the header is included
// on host too?

#ifdef __cg2c__
#include <cg2_image_load_store.h>
#endif

typedef struct LlamaParams {
	float complex *A;
	float *snow;
	float snowGravity;
	uint32_t out;
	int32_t outHeight;
	uint32_t x;
	uint32_t y;
} LlamaParams;
