#ifndef __CG2_IMAGE_SAMPLING_H__
#define __CG2_IMAGE_SAMPLING_H__

__float4 __tex2D(unsigned int image, unsigned int sampler, float x, float y);

#endif
