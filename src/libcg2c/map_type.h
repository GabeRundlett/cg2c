struct MapType {
	const void *zero; // returned by mapaccess for non-existing keys
	bool       (*equal)(const void*, const void*);
	size_t     (*hasher)(const void*, size_t);
	size_t     keysize;
	size_t     elemsize;
};
