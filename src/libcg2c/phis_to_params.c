#include "all.h"

// TODO: this all looks pretty ugly, could we perhaps do function copies instead
// of patching?

static size_t
cg2c_phiNArg(Value *v)
{
	assert(v->op == OpPhi);
	assert((cg2c_valueNArg(v) - 1) % 2 == 0);
	return (cg2c_valueNArg(v) - 1) / 2;
}

static Incoming
cg2c_phiArg(Value *v, size_t i)
{
	assert(v->op == OpPhi);
	assert((cg2c_valueNArg(v) - 1) % 2 == 0);

	return (Incoming) {
		.v = cg2c_valueArg(v, 1 + 2 * i + 0),
		.f = cg2c_valueArgFunc(v, 1 + 2 * i + 1),
	};
}

typedef struct PhisToParams PhisToParams;
struct PhisToParams {
	Map *params;
	Map *args;
};

static void
phis(Compile *ctxt, Value *v, void *etc)
{
	PhisToParams *wat = etc;
	GC *gc = ctxt->gc;

	switch (v->op) {
	case OpPhi: {
		// I guess we could fill out args and params here, so that we
		// don't need to do bunch of weird work in the replacement loop
		// further

		Value *phi = v;

		Func *bb = cg2c_valueArgFunc(phi, 0);

		Slice params = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, wat->params, &bb);
		params = cg2c_slappend(sizeof(Value*), params, &phi, ctxt->gc);
		*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, wat->params, &bb, gc) = params;

		for (size_t i = 0; i < cg2c_phiNArg(phi); i++) {
			Incoming a = cg2c_phiArg(phi, i);
			assert(cg2c_funcTerminator(a.f)->op == OpCall); // no critical edges allowed

			Slice args = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, wat->args, &a.f);
			args = cg2c_slappend(sizeof(Value*), args, &a.v, ctxt->gc);
			*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, wat->args, &a.f, gc) = args;

			assert(cg2c_sllen(params) == cg2c_sllen(args)); // indices must match
		}

		break;
	}

	default:
		break;
	}
}

// TODO: split critical edges

// TODO: fix things in a less hacky way
static Value*
rewritestuff(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc)
{
	Map *re = etc;

	Value *replacement = *(Value* const*)cg2c_mapaccess1(cg2c_ptr_ptr_map, re, &u);
	if (replacement != NULL)
		return replacement;

	return cg2c_buildValue(b, op, type, aux, args);
}

bool
cg2c_phisToParams(Builder b, Scope *scope)
{
	GC *gc = b.ctxt->gc;

	PhisToParams *wat = &(PhisToParams) {
		.args = cg2c_mkmap(cg2c_ptr_slice_map, gc),
		.params = cg2c_mkmap(cg2c_ptr_slice_map, gc),
	};

	cg2c_scopeForeach(b.ctxt, phis, wat, scope);

	// TODO: better just clone the functions so that we don't run into any
	// weirdness, like in inline

	// Adjust block types and params

	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *bb = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Slice params = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, wat->params, &bb);

		if (cg2c_sllen(params) > 0) {
			// TODO: verify that all callers must be single-exit, or
			// maybe somehow arrange this pass to also split
			// critical edges?

			Value *func = cg2c_buildFuncValue(b, bb);
			Value *param = cg2c_buildParam(b, bb);

			Type *type = cg2c_funcParam(bb->type);

			Slice elemts = {};
			if (cg2c_typeKind(type) != KindTuple) {
				elemts = cg2c_slappend(sizeof(Type*), elemts, &type, gc);
			} else {
				/*
				for (size_t j = 0; j < cg2c_tupleNElem(type); j++) {
					Type *et = cg2c_tupleElem(type, j);
					elemts = cg2c_slappend(sizeof(Type*), elemts, &et, gc);
				}
				*/
				assert(0);
			}

			for (size_t j = 0; j < cg2c_sllen(params); j++) {
				Value *phi = *(Value**)cg2c_slindex(sizeof(Value*), params, j);
				elemts = cg2c_slappend(sizeof(Type*), elemts, &phi->type, gc);
			}

			bb->type = cg2c_funct(b.ctxt, cg2c_tuple(b.ctxt, elemts), cg2c_funcResult(bb->type));

			Value *newFunc = cg2c_buildFuncValue(b, bb);
			Value *newParam = cg2c_buildParam(b, bb);

			// Replace the func because it's used in e.g. control
			// flow
			*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &func, gc) = newFunc;

			// Slice elems = {};
			if (cg2c_typeKind(type) != KindTuple) {
				*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &param, gc) = cg2c_buildExtract(b, 0, newParam);
			} else {
				/*
					for (size_t j = 0; j < cg2c_tupleNElem(type); j++) {
						Type *et = cg2c_tupleElem(type, j);
						elemts = cg2c_slappend(sizeof(Type*), elemts, &et, gc);
					}
				*/
				assert(0);
			}

			assert(cg2c_typeKind(type) != KindTuple);
			size_t k = 1;
			for (size_t j = 0; j < cg2c_sllen(params); j++, k++) {
				Value *phi = *(Value**)cg2c_slindex(sizeof(Value*), params, j);
				Value *phiReplacement = cg2c_buildExtract(b, k, newParam);
				assert(phi->type == phiReplacement->type);
				*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &phi, gc) = phiReplacement;
			}
		}
	}

	// Adjust calls

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *bb = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Slice args = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, wat->args, &bb);

		Value *t = cg2c_funcTerminator(bb);

		if (cg2c_sllen(args) > 0) {
			Value *arg = NULL;
			switch (t->op) {
			case OpCall: {
				arg = cg2c_valueArg(t, 1);

				break;
			}

			default:
				assert(0); // TODO: handle more
			}

			Slice elems = {};
			if (cg2c_typeKind(arg->type) != KindTuple) {
				elems = cg2c_slappend(sizeof(Value*), elems, &arg, gc);
			} else {
				assert(0);
			}

			for (size_t j = 0; j < cg2c_sllen(args); j++) {
				Value *a = *(Value**)cg2c_slindex(sizeof(Value*), args, j);
				elems = cg2c_slappend(sizeof(Value*), elems, &a, gc);
			}

			Value *newArg = cg2c_buildMakeTuple(b, elems);

			Value *t2 = NULL;
			switch (t->op) {
			case OpCall: {
				t2 = cg2c_buildCall(b, cg2c_valueArg(t, 0), newArg);

				break;
			}

			default:
				assert(0); // TODO: handle more stuff
			}

			cg2c_funcTerminate(bb, t2, gc);
		}
	}

	return cg2c_scopeRewriteFunc2(b, rewritestuff, re, scope, NULL);
}
