#include "all.h"

const char *cg2c_kindName[KindLast] = {
	[KindBool] = "bool",
	[KindInt8] = "int8",
	[KindInt16] = "int16",
	[KindInt32] = "int32",
	[KindInt64] = "int64",
	[KindPtr] = "ptr",
	[KindFunc] = "func",
	[KindMem] = "mem",
	[KindRMem] = "rmem",
	[KindNoret] = "noret",
	[KindTuple] = "tuple",
};

struct Type {
	Kind  kind;
	Slice elems; // of Type*
};

Type* cg2c_types[KindLast] = {
	[KindBool]  = &(Type) {.kind = KindBool},
	[KindInt8]  = &(Type) {.kind = KindInt8},
	[KindInt16] = &(Type) {.kind = KindInt16},
	[KindInt32] = &(Type) {.kind = KindInt32},
	[KindInt64] = &(Type) {.kind = KindInt64},
	[KindPtr]   = &(Type) {.kind = KindPtr},
	[KindMem]   = &(Type) {.kind = KindMem},
	[KindNoret] = &(Type) {.kind = KindNoret},
};

Type*
cg2c_funct(Compile *ctxt, Type *param, Type *result)
{
	// TODO: change to use ptr[2] hasher

	Slice elems = cg2c_mkslicenocopy(sizeof(Type*), (Type*[]) {param, result}, 2);

	Type *t = *(Type* const*)cg2c_mapaccess1(cg2c_ptrSlice_ptr_map, ctxt->funcs, &elems);
	if (t == NULL) {
		t = cg2c_malloc(sizeof *t, ctxt->gc);
		t->kind = KindFunc;
		t->elems = cg2c_slclone(sizeof(Type*), elems, ctxt->gc);

		*(Type**)cg2c_mapassign(cg2c_ptrSlice_ptr_map, ctxt->funcs, &t->elems, ctxt->gc) = t;
	}
	return t;
}

Type*
cg2c_tuple(Compile *ctxt, Slice elems)
{
	Type *t = *(Type* const*)cg2c_mapaccess1(cg2c_ptrSlice_ptr_map, ctxt->tuples, &elems);
	if (t == NULL) {
		t = cg2c_malloc(sizeof *t, ctxt->gc);
		t->kind = KindTuple;
		// Clone elems in case it's stack-allocated.
		t->elems = cg2c_slclone(sizeof(Type*), elems, ctxt->gc);

		// Use cloned elems as key, as elems might be stack-allocated.
		*(Type**)cg2c_mapassign(cg2c_ptrSlice_ptr_map, ctxt->tuples, &t->elems, ctxt->gc) = t;
	}
	return t;
}

Kind cg2c_typeKind(Type *t) { return t->kind; }

Type*
cg2c_tuple2(Compile *ctxt, Type *e0, Type *e1)
{
	Type *elems[] = {e0, e1};
	return cg2c_tuple(ctxt, cg2c_mkslicenocopy(sizeof elems[0], elems, nelem(elems)));
}

Type*
cg2c_tuple3(Compile *ctxt, Type *e0, Type *e1, Type *e2)
{
	Type *elems[] = {e0, e1, e2};
	return cg2c_tuple(ctxt, cg2c_mkslicenocopy(sizeof elems[0], elems, nelem(elems)));
}

Type*
cg2c_tuple4(Compile *ctxt, Type *e0, Type *e1, Type *e2, Type *e3)
{
	Type *elems[] = {e0, e1, e2, e3};
	return cg2c_tuple(ctxt, cg2c_mkslicenocopy(sizeof elems[0], elems, nelem(elems)));
}

Type*
cg2c_funcParam(Type *t)
{
	assert(cg2c_typeKind(t) == KindFunc);
	return *(Type**)cg2c_slindex(sizeof(Type*), t->elems, 0);
}

Type*
cg2c_funcResult(Type *t)
{
	assert(cg2c_typeKind(t) == KindFunc);
	return *(Type**)cg2c_slindex(sizeof(Type*), t->elems, 1);
}

size_t
cg2c_tupleNElem(Type *t)
{
	assert(cg2c_typeKind(t) == KindTuple);
	return cg2c_sllen(t->elems);
}

Type*
cg2c_tupleElem(Type *t, size_t i)
{
	assert(cg2c_typeKind(t) == KindTuple);
	return *(Type**)cg2c_slindex(sizeof(Type*), t->elems, i);
}

static int64_t typeSizes[KindLast] = {
	[KindInt8]  = 1,
	[KindInt16] = 2,
	[KindInt32] = 4,
	[KindInt64] = 8,
};

int64_t
cg2c_typeSize(Compile *ctxt, Type *t)
{
	switch (cg2c_typeKind(t)) {
	case KindInt8: case KindInt16: case KindInt32: case KindInt64:
		return typeSizes[cg2c_typeKind(t)];

	case KindPtr:
		return ctxt->ptrSize;

	case KindFunc:
		return ctxt->funcSize;

	case KindTuple: {
		int64_t size = 0;
		for (size_t i = 0; i < cg2c_tupleNElem(t); i++)
			size += cg2c_typeSize(ctxt, cg2c_tupleElem(t, i));
		return size;
	}

	default:
		assert(0); // unreachable
	}
}
