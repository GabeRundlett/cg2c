// Derived from Go unicode/utf8.
// https://github.com/golang/go/blob/go1.18/src/unicode/utf8/utf8.go
//
// Copyright (c) 2009 The Go Authors. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "all.h"

enum {
	maskx = 0b00111111,
	mask2 = 0b00011111,
	mask3 = 0b00001111,
	mask4 = 0b00000111,

	// The default lowest and highest continuation byte.
	locb = 0b10000000,
	hicb = 0b10111111,

	// These names of these constants are chosen to give nice alignment in the
	// table below. The first nibble is an index into acceptRanges or F for
	// special one-byte cases. The second nibble is the Rune length or the
	// Status for the special one-byte case.
	xx = 0xF1, // invalid: size 1
	as = 0xF0, // ASCII: size 1
	s1 = 0x02, // accept 0, size 2
	s2 = 0x13, // accept 1, size 3
	s3 = 0x03, // accept 0, size 3
	s4 = 0x23, // accept 2, size 3
	s5 = 0x34, // accept 3, size 4
	s6 = 0x04, // accept 0, size 4
	s7 = 0x44, // accept 4, size 4
};

// first is information about the first byte in a UTF-8 sequence.
static uint8_t first[256] = {
	//   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x00-0x0F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x10-0x1F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x20-0x2F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x30-0x3F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x40-0x4F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x50-0x5F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x60-0x6F
	as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, as, // 0x70-0x7F
	//   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
	xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, // 0x80-0x8F
	xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, // 0x90-0x9F
	xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, // 0xA0-0xAF
	xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, // 0xB0-0xBF
	xx, xx, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, // 0xC0-0xCF
	s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, s1, // 0xD0-0xDF
	s2, s3, s3, s3, s3, s3, s3, s3, s3, s3, s3, s3, s3, s4, s3, s3, // 0xE0-0xEF
	s5, s6, s6, s6, s7, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, // 0xF0-0xFF
};

// AcceptRange gives the range of valid values for the second byte in a UTF-8
// sequence.
typedef struct AcceptRange AcceptRange;
struct AcceptRange {
	uint8_t lo; // lowest value for second byte.
	uint8_t hi; // highest value for second byte.
};

// acceptRanges has size 16 to avoid bounds checks in the code that uses it.
static AcceptRange acceptRanges[16] = {
	[0] = {locb, hicb},
	[1] = {0xA0, hicb},
	[2] = {locb, 0x9F},
	[3] = {0x90, hicb},
	[4] = {locb, 0x8F},
};

int
cg2c_chartorune(Rune *r, const char *p, size_t n)
{
	if (n < 1) {
		*r = Runeerror;
		return 0;
	}
	uint8_t p0 = (uint8_t)p[0];
	uint8_t x = first[p0];
	if (x >= as) {
		if (x != xx) {
			*r = p0;
			return 1;
		} else {
			*r = Runeerror;
			return 1;
		}
	}
	size_t sz = (size_t)(x & 7);
	if (n < sz)
		goto bad;
	AcceptRange accept = acceptRanges[x>>4];
	uint8_t b1 = (uint8_t)p[1];
	if (b1 < accept.lo || accept.hi < b1)
		goto bad;
	if (sz == 2) {
		*r = (Rune)(p0&mask2)<<6 | (Rune)(b1&maskx);
		return 2;
	}
	uint8_t b2 = (uint8_t)p[2];
	if (b2 < locb || hicb < b2)
		goto bad;
	if (sz == 3) {
		*r = (Rune)(p0&mask3)<<12 | (Rune)(b1&maskx)<<6 | (Rune)(b2&maskx);
		return 3;
	}
	uint8_t b3 = (uint8_t)p[3];
	if (b3 < locb || hicb < b3)
		goto bad;
	if (sz == 4) {
		*r = (Rune)(p0&mask4)<<18 | (Rune)(b1&maskx)<<12 | (Rune)(b2&maskx)<<6 | (Rune)(b3&maskx);
		return 4;
	}
	assert(!"unreachable");
bad:
	*r = Runeerror;
	return 1;
}
