#include "all.h"

// TODO: tolerate invalid Pos

void
cg2c_warnfAt(Compile *ctxt, Pos pos, const char *format, ...)
{
	PosBase h = {};
	if (pos.base == NULL)
		pos.base = &h;

	char b[1024] = {};
	va_list arg;
	va_start(arg, format);
	vsnprintf(b, (sizeof b)-1, format, arg);
	va_end(arg);
	cg2c_warnf(ctxt, "%.*s:%d:%d: %s", cg2c_strlen2(pos.base->filename), cg2c_strdata(pos.base->filename), pos.line, pos.col, b);
}

void
cg2c_warnf(Compile *ctxt, const char *format, ...)
{
	char b[1024] = {};
	va_list arg;
	va_start(arg, format);
	vsnprintf(b, (sizeof b)-1, format, arg);
	va_end(arg);
	ctxt->diag(b, 1, ctxt->udata);
}

void
cg2c_errorfAt(Compile *ctxt, Pos pos, const char *format, ...)
{
	PosBase h = {};
	if (pos.base == NULL)
		pos.base = &h;

	char b[1024] = {};
	va_list arg;
	va_start(arg, format);
	vsnprintf(b, (sizeof b)-1, format, arg);
	va_end(arg);
	cg2c_errorf(ctxt, "%.*s:%d:%d: %s", cg2c_strlen2(pos.base->filename), cg2c_strdata(pos.base->filename), pos.line, pos.col, b);
}

void
cg2c_errorf(Compile *ctxt, const char *format, ...)
{
	va_list arg;
	va_start(arg, format);
	cg2c_verrorf(ctxt, format, arg);
	va_end(arg);
}

void
cg2c_verrorf(Compile *ctxt, const char *format, va_list arg)
{
	char b[1024] = {};
	vsnprintf(b, (sizeof b)-1, format, arg);
	ctxt->diag(b, 0, ctxt->udata);
	ctxt->errored = true;
}
