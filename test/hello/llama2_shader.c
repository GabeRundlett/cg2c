#include <cg2_image_sampling.h>
#include <math.h>
#include <stdint.h>

typedef struct {
	float x0;
	float y0;
	float x1;
	float y1;
} Rect;

/*
 * 0--2 5
 * | / /|
 * |/ / |
 * 1 3--4
 */

typedef struct {
	__float4 gl_Position;
	__float4 uv;
} VSOutput;

VSOutput
llama2Vert(struct {
	float aspectRatio; // width / height
	Rect *rects;
	unsigned int gl_VertexID;
} *arg)
{
	unsigned int id = arg->gl_VertexID;
	unsigned int id6 = id % 6;
	Rect rect = arg->rects[id / 6]; // 6 verts make up one rect

	VSOutput vsOut;

	vsOut.gl_Position.x = rect.x0;
	if (id6 == 2 | id6 == 4 | id6 == 5)
		vsOut.gl_Position.x = rect.x1;
	vsOut.gl_Position.y = rect.y0;
	if (id6 == 1 | id6 == 3 | id6 == 4)
		vsOut.gl_Position.y = rect.y1;
	vsOut.gl_Position.z = 0;
	vsOut.gl_Position.w = 1;

	vsOut.uv.x = id6 == 2 | id6 == 4 | id6 == 5;
	vsOut.uv.y = id6 == 1 | id6 == 3 | id6 == 4;
	vsOut.uv.z = 0;
	vsOut.uv.w = 0;

	return vsOut;
}

__float4
llama2Frag(struct {
	uint32_t imageHandle;
	uint32_t samplerHandle;
	__float4 uv;
} *arg)
{
	return __tex2D(arg->imageHandle, arg->samplerHandle, arg->uv.x, arg->uv.y);
}
