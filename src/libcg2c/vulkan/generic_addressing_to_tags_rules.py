prologue="""
#include "all.h"

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
	Value *args[] = {a0};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
    Value *args[] = {a0, a1};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *a0, Value *a1, Value *a2)
{
    Value *args[] = {a0, a1, a2};
    return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
buildCmp(Builder b, Op op, Value *x, Value *y)
{
    // TODO: assert for "builder class"
    assert(x->type == y->type);
    return newValue2(b, op, cg2c_types[KindBool], x, y);
}
"""

label = "cg2c_genericAddressingToTagsVkRules"

# Some of these rules might be made common

# TODO: remove PtrIsGlobal etc rules in favor of a pass that can follow args?

rules = [
    RewriteRule(
        pattern=Pattern(op="AddPtr", args=[Pattern(var="p"), Pattern(var="off")]),
        replace="return cg2c_buildIntToPtr(b, cg2c_buildArith2(b, OpAdd, cg2c_buildPtrToInt(b, p), off));",
    ),

    RewriteRule(
        pattern=Pattern(op="PtrIsGlobal", args=[Pattern(var="p")]),
        replace="""
            Type *intptrt = cg2c_intptrt(b.ctxt);

            // TODO: check the tag (high two bits) instead of this

            Value *nullIntptr = cg2c_buildConst(b, intptrt, 0);
            Value *pIntptr = cg2c_buildPtrToInt(b, p);
            return buildCmp(b, OpLte, nullIntptr, pIntptr);
        """,
    ),

    # Split scratch stores to individual bytes (int32s for now)
    RewriteRule(
        pattern=Pattern(op="StoreScratch", args=[Pattern(var="mem"), Pattern(var="p"), Pattern(var="v")]),
        guard="v->type == cg2c_types[KindInt64] || v->type == cg2c_types[KindPtr]",
        replace="""
            // TODO: split this off into a separate rule?
            if (v->type == cg2c_types[KindPtr])
                v = cg2c_buildPtrToInt(b, v);

            Value *lo = newValue1(b, OpSignExt, cg2c_types[KindInt32], v);
            Value *hi = newValue1(b, OpSignExt, cg2c_types[KindInt32], cg2c_buildShiftConst(b, OpRsh, v, 32));

            // TODO: we should pass alignment information along
            mem = newValue3(b, OpStoreScratch, cg2c_types[KindMem], mem, p, lo);
            mem = newValue3(b, OpStoreScratch, cg2c_types[KindMem], mem, cg2c_buildAddPtrConst(b, p, 4), hi);

            return mem;
        """,
    ),
]
