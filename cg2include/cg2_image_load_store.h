#ifndef __CG2_IMAGE_LOAD_STORE_H__
#define __CG2_IMAGE_LOAD_STORE_H__

void __image2DStore(uint32_t image, int32_t x, int32_t y, __float4 color);
uint32_t __uimage2DAtomicMax(uint32_t image, int32_t x, int32_t y, uint32_t val);

#endif
