#include "hello.h"

// Including windows.h slows compilation down too much

#define WINAPI __stdcall

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

typedef char CHAR;
typedef short SHORT;
typedef int INT;
typedef long LONG;
typedef wchar_t WCHAR;

typedef void *PVOID;
typedef void *LPVOID;
typedef const void *LPCVOID;

typedef CHAR *LPSTR;
typedef const CHAR *LPCSTR;
typedef WCHAR *LPWSTR;
typedef const WCHAR *LPCWSTR;

#if defined(_WIN64)
typedef __int64 INT_PTR;
#else
typedef int INT_PTR;
#endif

#if defined(_WIN64)
typedef INT_PTR (WINAPI *FARPROC)(void);
#else
typedef int (WINAPI *FARPROC)(void);
#endif

typedef LPVOID HANDLE;
typedef HANDLE HINSTANCE;
typedef HINSTANCE HMODULE;

DWORD WINAPI GetLastError(void);

HMODULE WINAPI LoadLibraryW(LPCWSTR lpLibFileName);
FARPROC WINAPI GetProcAddress(HMODULE hModule, LPCSTR lProcName);

PFN_vkGetInstanceProcAddr
openVulkan(void)
{
	HMODULE libvulkan = LoadLibraryA("vulkan-1.dll");
	if (libvulkan == NULL)
		fatalf("LoadLibrary vulkan-1.dll: winapi error #%d", (int)GetLastError());
	return (PFN_vkGetInstanceProcAddr)GetProcAddress(libvulkan, "vkGetInstanceProcAddr");
}
