typedef enum Cg2cInputTypeVk     Cg2cInputTypeVk;
typedef enum Cg2cOutputTypeVk    Cg2cOutputTypeVk;
typedef struct Cg2cBindlessVk    Cg2cBindlessVk;
typedef struct Cg2cCompileInfoVk Cg2cCompileInfoVk;
typedef struct Cg2cFunctionVk    Cg2cFunctionVk;
typedef struct Cg2cInputVk       Cg2cInputVk;
typedef struct Cg2cOutputVk      Cg2cOutputVk;

enum Cg2cInputTypeVk {
	// User data

	CG2C_IN_CONSTANT = 1,
	CG2C_IN_INDIRECT_CONSTANT, // uniform block
	CG2C_IN_PUSH_CONSTANT,

	// VS-specific system values

	CG2C_IN_VERTEX_ID,
	CG2C_IN_INSTANCE_ID,

	CG2C_IN_DRAW_ID, // requires shaderDrawParameters

	// FS-specific inputs

	CG2C_IN_FS_INPUT, // TODO: don't distinguish between different stage I/O?

	// CG2C_IN_SAMPLE_MASK,
	CG2C_IN_FRAG_COORD, // XY{,Z{,W}} variants?

	// CS-specific inputs

	CG2C_IN_GLOBAL_INVOCATION_ID_X,
	CG2C_IN_GLOBAL_INVOCATION_ID_XY,
	CG2C_IN_GLOBAL_INVOCATION_ID,
};

struct Cg2cInputVk {
	Cg2cInputTypeVk type;
	uint32_t    dstOffset; // TODO: rename to offset
	uint32_t    size;
	// TODO: place these in their own structs
	const void* data;         // CG2C_IN_SPECIALIZATION
	uint32_t    set, binding; // CG2C_IN_INDIRECT_CONSTANT; TODO: maybe give these a vk prefix?
	uint32_t    srcOffset;    // CG2C_IN_PUSH_CONSTANT
	uint32_t    location;     // CG2C_IN_FS_INPUT
	// TODO: interpolation flags, at least just flat/non-flat
};

enum Cg2cOutputTypeVk {
	// VS outputs

	CG2C_OUT_POSITION = 1,
	CG2C_OUT_ATTRIBUTE,

	// FS outputs

	CG2C_OUT_COLOR,
	CG2C_OUT_DEPTH,
	CG2C_OUT_STENCIL,
	CG2C_OUT_DISCARD, // size can be 1 (C _Bool) or 4 (legacy GLSL/HLSL bool)
};

struct Cg2cOutputVk {
	Cg2cOutputTypeVk type;
	uint32_t offset;
	uint32_t size;
	uint32_t location; // CG2C_OUT_ATTRIBUTE and CG2C_OUT_COLOR
	// TODO: interpolation flags for CG2C_OUT_ATTRIBUTE, phones with graphics
	// pipeline library will need this
};

typedef struct Cg2cFragmentVk Cg2cFragmentVk;
struct Cg2cFragmentVk {
	bool earlyFragmentTests;
};

typedef struct Cg2cComputeVk Cg2cComputeVk;
struct Cg2cComputeVk {
	uint32_t workgroupSize[3];
};

struct Cg2cFunctionVk {
	const Cg2cInputVk*    inputs;
	size_t                ninput;
	const Cg2cOutputVk*   outputs;
	size_t                noutput;
	VkShaderStageFlagBits stage;
	const char*           name;
	const void*           extra;
};

struct Cg2cBindlessVk {
	uint32_t set;
	struct {
		uint32_t samplers;
		uint32_t sampledImages;
		uint32_t storageImages;
	} bindings;
};

struct Cg2cCompileInfoVk {
	// TODO: possibly also pass device properties, as e.g. subgroup ops
	// aren't always usable from all stages
	const void *features;

	uint32_t spirvVersion;

	const Cg2cFunctionVk* func; // TODO: flatten

	const Cg2cBindlessVk* bindlessImages;

	const char* const* files;
	size_t             nfile;

	const char* (*readFile)(const char *filename, void *userData);

	void (*diag)(const char *message, int verbosity, void *userData);

	void* userData;
};

int cg2cCompileVk(const Cg2cCompileInfoVk *compileInfo, const Cg2cAllocFuncs *allocFuncs, Cg2cBlob **aOut);
