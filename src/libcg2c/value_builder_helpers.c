#include "all.h"

Value*
cg2c_buildArith2(Builder b, Op op, Value *x, Value *y)
{
	// TODO: assert op
	assert(x->type == y->type); // remove this once we get typecheking in buildValue
	Type *type = x->type;
	Value *args[] = {x, y};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildShift(Builder b, Op op, Value *x, Value *y)
{
	// TODO: replace this assert with assert for "builder class" from opInfo
	assert(op == OpLsh || op == OpRsh || op == OpURsh);
	Type *type = x->type;
	Value *args[] = {x, y};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

// or buildShiftByConst
Value*
cg2c_buildShiftConst(Builder b, Op op, Value *x, int k)
{
	Value *y = cg2c_buildConst(b, cg2c_types[KindInt32], (uint64_t)k);
	return cg2c_buildShift(b, op, x, y);
}

Value*
cg2c_buildParam(Builder b, Func *f)
{
	Value *args[] = {cg2c_buildFuncValue(b, f)};
	return cg2c_buildValue(b, OpParam, cg2c_funcParam(f->type), NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildConst(Builder b, Type *type, uint64_t val)
{
	return cg2c_buildValue(cg2c_builderWithPos(b, (Pos) {}, NULL), OpConst, type, &val, NULL_SLICE);
}

Value*
cg2c_buildFuncValue(Builder b, Func *f)
{
	return cg2c_buildValue(b, OpFunc, f->type, f, NULL_SLICE);
}

Value*
cg2c_buildPtrToInt(Builder b, Value *v)
{
	assert(v->type == cg2c_types[KindPtr]);
	Value *args[] = {v};
	return cg2c_buildValue(b, OpPtrToInt, cg2c_intptrt(b.ctxt), NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildIntToPtr(Builder b, Value *v)
{
	assert(v->type == cg2c_intptrt(b.ctxt));
	Value *args[] = {v};
	return cg2c_buildValue(b, OpIntToPtr, cg2c_types[KindPtr], NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildMakeTuple(Builder b, Slice elems)
{
	Slice elemts = NULL_SLICE;
	elemts.data = (Type*[5]) {};
	elemts.cap = 5;
	elemts._elemsize = sizeof(Type*);
	for (size_t i = 0; i < cg2c_sllen(elems); i++) {
		Value *e = *(Value**)cg2c_slindex(sizeof(Value*), elems, i);
		elemts = cg2c_slappend(sizeof(Type*), elemts, &e->type, b.ctxt->gc);
	}
	Type *type = cg2c_tuple(b.ctxt, elemts);
	return cg2c_buildValue(b, OpMakeTuple, type, NULL, elems);
}

Value*
cg2c_buildMakeTuple2(Builder b, Value *e0, Value *e1)
{
	Value *args[] = {e0, e1};
	return cg2c_buildMakeTuple(b, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildMakeTuple4(Builder b, Value *e0, Value *e1, Value *e2, Value *e3)
{
	Value *args[] = {e0, e1, e2, e3};
	return cg2c_buildMakeTuple(b, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildExtract(Builder b, size_t i, Value *tup)
{
	Type *type = cg2c_tupleElem(tup->type, i);
	uint64_t aux = (uint64_t)i;
	Value *args[] = {tup};
	return cg2c_buildValue(cg2c_builderWithPos(b, tup->pos, b.trace), OpExtract, type, &aux, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildAddPtr(Builder b, Value *base, Value *off)
{
	Value *args[] = {base, off};
	return cg2c_buildValue(b, OpAddPtr, base->type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildAddPtrConst(Builder b, Value *base, int64_t off)
{
	return cg2c_buildAddPtr(b, base, cg2c_buildConst(b, cg2c_intptrt(b.ctxt), (uint64_t)off));
}

Value*
cg2c_buildVar(Builder b, int64_t size, int64_t align)
{
	Value *sizec = cg2c_buildConst(b, cg2c_intptrt(b.ctxt), (uint64_t)size);
	Value *alignc = cg2c_buildConst(b, cg2c_intptrt(b.ctxt), (uint64_t)align);
	Value *args[] = {sizec, alignc};
	int64_t auxi = ++b.ctxt->varId;
	return cg2c_buildValue(b, OpVar, cg2c_types[KindPtr], &auxi, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildLoad(Builder b, Type *type, int64_t align, Value *mem, Value *ptr)
{
	Value *args[] = {mem, ptr};
	return cg2c_buildValue(b, OpLoad, type, &align, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildStore(Builder b, int64_t align, Value *mem, Value *ptr, Value *val)
{
	Value *args[] = {mem, ptr, val};
	return cg2c_buildValue(b, OpStore, cg2c_types[KindMem], &align, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

Value*
cg2c_buildCall(Builder b, Value *target, Value *arg)
{
	Value *args[] = {target, arg};
	return cg2c_buildValue(b, OpCall, cg2c_types[KindNoret], NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}
