Cg2c compiles a dialect of C with a number of restrictions and extensions,
targeting shader execution environments found in common graphical APIs, such as
Vulkan.

# Building and using

To build the compiler, run
```
meson build -Db_lto=true && meson compile -C build
```

## Vulkan

At least Vulkan 1.1 with VK_KHR_spirv_1_4, shaderInt16,
storageBuffer16BitAccess, storageBuffer8BitAccess, shaderInt8,
bufferDeviceAddress and vulkanMemoryModel enabled is required. Features
shaderInt64 and shaderFloat16 are optional and might result in better
performance if enabled.

<!-- vertexPipelineStoresAndAtomics -->

### Graphics stages

### Image operations

<!-- TODO: mention headers that provide this functionality -->

For image sampling and load/store operations, non-uniform descriptor indexing
for respective descriptor types, as well as runtimeDescriptorArray, are required
to be enabled.

Additionally, for image load/store operations,
shaderStorageImageReadWithoutFormat and shaderStorageImageWriteWithoutFormat, or
VK_KHR_format_feature_flags2 (promoted to Vulkan 1.3 Core), are required to be
enabled.

**Note:** while most drivers that meet the base requirements also support
shaderStorageImageReadWithoutFormat, this is not the case for Intel. See column
TR in the format info table found at
https://gitlab.freedesktop.org/mesa/mesa/-/blob/main/src/intel/isl/isl_format.c#L103
for formats that can be used with image load/store operations in Cg2, on various
generations of Intel hardware. Note that hardware prior to Skylake (GEN9) does
not meet the minimum requirements for image operations in Cg2.

# Differences from the standard

`intmax_t`, `uintmax_t` and `long double` types, variable length arrays, as well
as some obsolescent features, such as trigraphs and function definitions with
identifier lists, were intentionally left out.

Half-precision floating point arithmetic and the `_Float16` type are supported.

Complex arithmetic as in C99 Annex G is supported and an extension for [complex
literals][gcc-complex] is provided. Note that other GCC extensions for complex
numbers, such as Gaussian integers and use of operator `~` for conjugation, were
intentionally left out.

Vector types similar to [Clang's extended vectors][clang-extended-vectors] are
provided.

<!-- TODO: atomics -->

[gcc-complex]: https://gcc.gnu.org/onlinedocs/gcc/Complex.html

[clang-extended-vectors]: https://clang.llvm.org/docs/LanguageExtensions.html#vectors-and-extended-vectors
