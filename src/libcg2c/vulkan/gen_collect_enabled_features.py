#!/usr/bin/env python3

import sys
import xml.etree.ElementTree as et

import mako.template

interesting_features = {
    "vertexPipelineStoresAndAtomics",
    "shaderInt64",
    "shaderFloat16",
}

skipped_feature_structs = {
    "VkPhysicalDevicePortabilitySubsetFeaturesKHR",
}

def struct_type_enum(_type):
    return _type.find(".member/name[.='sType']/..").attrib["values"]

def struct_extends(_type):
    return _type.attrib.get("structextends", "").split(",")

def contains_interesting_features(_type):
    for member in _type.iterfind("member"):
        if member.find("name").text in interesting_features:
            return True
    return False

def handle_feature_struct(s_type, type_name, _type, member_prefix=""):
    print("case {}: {{".format(s_type))
    if contains_interesting_features(_type):
        print("const {} *features = (const {} *)pNext;".format(type_name, type_name))
        for member in _type.iterfind("member"):
            member_name = member.find("name").text
            if member_name in interesting_features:
                print("if (features->{}{})".format(member_prefix, member_name))
                print("out->{} = true;".format(member_name))
    print("break;")
    print("}")

# TODO: minimize the number of basic blocks by using empty fallthrough cases to
# decrease compile time

def main():
    vk_xml = et.parse(sys.argv[1])

    registry = vk_xml.getroot()

    print(r"""#include "all.h"

#include <spirv/unified1/spirv.h>
#include <spirv/unified1/GLSL.std.450.h>

#include <vulkan/vulkan_core.h>

#include <cg2c/cg2c_vulkan.h>

#include "vulkan/compile.h"

void
cg2c_collectEnabledFeaturesVk(Compile *ctxt, const void *features_, DeviceFeatures *out)
{
	for (const VkBaseInStructure *pNext = features_; pNext != NULL; pNext = pNext->pNext) {
		switch (pNext->sType) {""")

    if True:
        _type = registry.find(".types/type[@category='struct'][@name='VkPhysicalDeviceFeatures']")
        handle_feature_struct(
            "VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2",
            "VkPhysicalDeviceFeatures2",
            _type,
            member_prefix="features.")

    for _type in registry.iterfind(".types/type[@category='struct']"):
        if _type.attrib["name"] in skipped_feature_structs:
            continue
        if "VkPhysicalDeviceFeatures2" in struct_extends(_type):
            handle_feature_struct(
                struct_type_enum(_type),
                _type.attrib["name"],
                _type)

    print(r"""
		default: {
			cg2c_warnf(ctxt, "compileInfo->features chain contains a structure with an unknown type %d\n", (int)pNext->sType);
			break;
		}
		}
	}
}""")

if __name__ == "__main__":
    main()

test_template = mako.template.Template(r"""
""")
