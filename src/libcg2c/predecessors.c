#include "all.h"

static void
addIncoming(Map *preds, Func *bb, Func *pred, Value *arg, GC *gc)
{
	Slice incs = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, preds, &bb);
	incs = cg2c_slappend(sizeof(Incoming), incs, &(Incoming) {.f = pred, .v = arg}, gc);
	*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, preds, &bb, gc) = incs;
}

Map*
cg2c_predecessors(Scope *scope, GC *gc)
{
	Map *preds = cg2c_mkmap(cg2c_ptr_slice_map, gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Value *t = cg2c_funcTerminator(f);
		switch (t->op) {
		case OpCall: {
			Value *target = cg2c_valueArg(t, 0);
			Value *arg = cg2c_valueArg(t, 1);

			if (target->op == OpFunc)
				addIncoming(preds, cg2c_valueAuxFunc(target), f, arg, gc);

			break;
		}

		case OpCallStructuredIf: {
			Func *succs[] = {
				cg2c_valueArgFunc(t, 1),
				cg2c_valueArgFunc(t, 2),
			};
			Value *arg = cg2c_valueArg(t, 4);

			for (size_t i = 0; i < nelem(succs); i++)
				addIncoming(preds, succs[i], f, arg, gc);

			break;
		}

		default:
			assert(0); // unknown terminator
		}
	}

	return preds;
}
