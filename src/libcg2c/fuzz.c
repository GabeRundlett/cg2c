#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include "all.h"
#include "cg2.h"

#include "gc.h"

static void f(const char *msg, int severity, void *ud) {}

int
LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size)
{
	GC *gc = calloc(1, sizeof *gc);
	if (setjmp(gc->oom)) {
		fprintf(stderr, "out of memory\n");
		goto out;
	}

	Compile *compile = &(Compile) {
		.unsignedChar = CHAR_MIN == 0,
		.longSize     = sizeof(long),
		.ptrSize      = 8,

		.gc = gc,

		.diagFunc = f,
	};

	cg2c_parse(compile, (const char*)Data, Size);

out:
	free_all(gc);
	free(gc);
	return !compile->errored ? 1 : 0;
}
