#include "all.h"

static void
ana(Compile *ctxt, Value *v, void *etc_)
{
	Map *perFunc = etc_;

	switch (v->op) {
	case OpParam:
		return;

	case OpPhi:
		assert(0);
		return;

	default:
		break;
	}

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);

		if (a->op == OpFunc) {
			Func *f = cg2c_valueAuxFunc(a);

			int32_t nentry = *(const int32_t*)cg2c_mapaccess1(cg2c_ptr_32_map, perFunc, &f);

			// BUG: this is wrong, what if it's a Func value being
			// passed as argument to another func?
			if (v->op == OpCall) {
				if (nentry < 2)
					nentry++;
			} else {
				if (nentry < 2)
					nentry = 2;
			}

			*(int32_t*)cg2c_mapassign(cg2c_ptr_32_map, perFunc, &f, ctxt->gc) = nentry;
		}
	}
}

typedef struct RewriteArg RewriteArg;
struct RewriteArg {
	Func *func;
	Value *v;
};

static Value*
rewriteArg(Builder b, Value *u, Op op, Type *type, void *aux, Slice args, void *etc_)
{
	RewriteArg *arg = etc_;

	switch (op) {
	case OpParam: {
		Func *f = cg2c_valueAuxFunc(*(Value**)cg2c_slindex(sizeof(Value*), args, 0));
		if (f == arg->func)
			return arg->v;
		break;
	}

	default:
		break;
	}

	return cg2c_buildValue(b, op, type, aux, args);
}

// TODO: this isn't really about no. of entries. Structured if children are also
// single-entry, but we wouldn't do anything about those.
static bool
singleEntry(Map *perFunc, Func *f)
{
	return *(const int32_t*)cg2c_mapaccess1(cg2c_ptr_32_map, perFunc, &f) == 1;
}

// TODO: rename to something nicer
bool
cg2c_uselessCalls(Builder b, Scope *scope)
{
	bool changed = false;

	Map *perFunc = cg2c_mkmap(cg2c_ptr_32_map, b.ctxt->gc);

	cg2c_scopeForeach(b.ctxt, ana, perFunc, scope);

	// Remove about-to-be-merged funcs from the scope.

	size_t j = 0;
	for (size_t i = 0; i < cg2c_sllen(scope->funcs);) {
		Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		if (j < i)
			*(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, j) = f;
		if (!singleEntry(perFunc, f))
			j++;
		i++;
	}
	scope->funcs = cg2c_slice(sizeof(Func*), scope->funcs, 0, j);

	// Merge single-entry funcs into their single-exit callers.

	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		Value *t = cg2c_funcTerminator(f);

		for (;;) {
			if (t->op != OpCall)
				break;

			Value *target = cg2c_valueArg(t, 0);

			if (target->op != OpFunc)
				break;

			Func *g = cg2c_valueAuxFunc(target);

			if (!singleEntry(perFunc, g))
				break;

			Value *t2 = cg2c_rewriteFunc2(b, rewriteArg, &(RewriteArg) {
				.func = g,
				.v = cg2c_valueArg(t, 1),
			}, cg2c_funcTerminator(g), re);

			assert(t != t2); // impossible

			cg2c_funcTerminate(f, t2, b.ctxt->gc);

			t = t2;

			changed = true;
		}
	}

	if (changed) {
		// TODO: this might not be necessary, it seems we could do some
		// more rewriting in the preceeding loop
		cg2c_scopeRewrite(b, scope, re);
	}

	// Validation
	/*
		Scope scope2 = cg2c_computeScope(*(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, 0), b.ctxt->gc);

		assert(cg2c_sllen(scope->funcs) == cg2c_sllen(scope2.funcs));

		for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
			Func *f = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);
			Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope2.funcs, i);

			assert(f == g);
		}
	*/

	return changed;
}