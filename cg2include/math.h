#ifndef __MATH_H__
#define __MATH_H__

float sqrtf(float x);

float sinf(float x);

float cosf(float x);

float __sqrtf(float x);

float __fminf(float x, float y);

float __fmaxf(float x, float y);

float __powrf(float x, float y);

float __sinf(float x);

float __cosf(float x);

float __tanhf(float x);

float __exp2f(float x);

float __log2f(float x);

#endif
