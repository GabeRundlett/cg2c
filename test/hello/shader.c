#include "hello.h"

enum {
	// SPIR-V version we want Cg2c to use. Newer versions may introduce more
	// reasonable out of box behavior.
	spirvVersion = 0x10400,
};

// TODO: come up with sType values for our stuff

// TODO: we really should just gather everything into a tar
static struct {
	const char* name;
	const char *data;
} archive[] = {
	{
		.name = "fft_shader.c",
		.data = (unsigned char[]) {
			#include "fft_shader.c.inc"
			0
		},
	},
	{
		.name = "llama_shader.c",
		.data = (unsigned char[]) {
			#include "llama_shader.c.inc"
			0
		},
	},
	{
		.name = "llama2_shader.c",
		.data = (unsigned char[]) {
			#include "llama2_shader.c.inc"
			0
		},
	},
};

static const char*
readshader(const char *filename, void *ud)
{
	for (size_t i = 0; i < nelem(archive); i++) {
		if (strcmp(archive[i].name, filename) == 0)
			return (const char*)archive[i].data;
	}
	assert(0);
	return NULL;
}

static void
diag_cg2c(const char *msg, int verbosity, void *ud)
{
	fprintf(stderr, "%s\n", msg);
	fflush(stderr);
}

void
createPipelineLayout2(const PipelineLayoutCg2CreateInfo2 *createInfo, VkPipelineLayout *out)
{
	VkResult r;

	// RADV seems to make use of push constants with holes, but that
	// involves a bit loop. Let's just be nice and have all push constants
	// tightly and nicely packed so that driver's vkCmdPushConstants remains
	// fast.

	VkShaderStageFlags stageMask = 0;
	uint32_t size = 0;
	for (size_t i = 0; i < createInfo->stageCount; i++) {
		PerStageCg2cInputs *stage = &createInfo->stages[i];

		for (size_t j = 0; j < stage->inputCount; j++) {
			Cg2cInputVk *input = &stage->inputs[j];

			if (input->type == CG2C_IN_PUSH_CONSTANT) {
				stageMask |= stage->stage;

				uint32_t begin = input->srcOffset;
				uint32_t end = begin + input->size;

				if (size < end)
					size = end;
			}
		}
	}

	VkPushConstantRange ranges[] = {
		{
			.stageFlags = stageMask,
			.offset = 0,
			.size = size,
		}
	};

	VkDescriptorSetLayout setLayouts[] = {bindlessLayout};

	if ((r = fns.vkCreatePipelineLayout(device, &(VkPipelineLayoutCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pushConstantRangeCount = nelem(ranges),
		.pPushConstantRanges = ranges,
		.setLayoutCount = nelem(setLayouts),
		.pSetLayouts = setLayouts,
	}, NULL, out)) != VK_SUCCESS)
		fatalvk("vkCreatePipelineLayout: ", r);
}

// TODO: change it to accept PipelineShaderStageCg2CreateInfo
static void
createShaderModule(const Cg2cCompileInfoVk *compileInfo, VkShaderModule *module)
{
	VkResult r;

	Cg2cBlob *aSpv;
	if (cg2cCompileVk(compileInfo, NULL, &aSpv) < 0)
		abort(); // recover

	if ((r = fns.vkCreateShaderModule(device, &(VkShaderModuleCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = cg2cBlobLen(aSpv),
		.pCode = cg2cBlobData(aSpv),
	}, NULL, module)) != VK_SUCCESS)
		fatalvk("vkCreateShaderModule: ", r);

	cg2cFreeBlob(aSpv, NULL);
}

void
createGraphicsPipeline(const VkGraphicsPipelineCreateInfo *createInfo, VkPipeline *out)
{
	VkResult r;

	VkPipelineShaderStageCreateInfo *stages2 = emalloc(createInfo->stageCount * sizeof stages2[0]);

	for (uint32_t i = 0; i < createInfo->stageCount; i++) {
		VkPipelineShaderStageCreateInfo stage = createInfo->pStages[i];

		const PipelineShaderStageCg2CreateInfo *cg2Stage = stage.pNext;

		stage.pNext = cg2Stage->pNext;

		createShaderModule(&(Cg2cCompileInfoVk) {
			.features = deviceFeatures,
			.spirvVersion = spirvVersion,
			.func = &(Cg2cFunctionVk) {
				.inputs  = cg2Stage->inputs,
				.ninput  = cg2Stage->ninput,
				.outputs = cg2Stage->outputs,
				.noutput = cg2Stage->noutput,
				.stage   = stage.stage,
				.name    = stage.pName,
				.extra   = cg2Stage->extra,
			},
			.bindlessImages = &(Cg2cBindlessVk) {
				.set = 0,
				.bindings = {
					.samplers = 0,
					.sampledImages = 1,
					.storageImages = 2,
				},
			},
			.files    = cg2Stage->files,
			.nfile    = cg2Stage->nfile,
			.readFile = readshader,
			.diag     = diag_cg2c,
		}, &stage.module);

		stages2[i] = stage;
	}

	VkGraphicsPipelineCreateInfo createInfo2 = *createInfo;
	createInfo2.pStages = stages2;

	if ((r = fns.vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &createInfo2, NULL, out)) != VK_SUCCESS)
		fatalvk("vkCreateGraphicsPipelines: ", r);

	for (uint32_t i = 0; i < createInfo->stageCount; i++)
		fns.vkDestroyShaderModule(device, stages2[i].module, NULL);

	free(stages2);
}

void
createComputePipeline(const VkComputePipelineCreateInfo *createInfo, VkPipeline *out)
{
	VkResult r;

	VkPipelineShaderStageCreateInfo stage = createInfo->stage;

	const PipelineShaderStageCg2CreateInfo *cg2Stage = stage.pNext;

	VkShaderModule module;
	createShaderModule(&(Cg2cCompileInfoVk) {
		.features = deviceFeatures,
		.spirvVersion = spirvVersion,
		.func = &(Cg2cFunctionVk) {
			.inputs  = cg2Stage->inputs,
			.ninput  = cg2Stage->ninput,
			.outputs = cg2Stage->outputs,
			.noutput = cg2Stage->noutput,
			.stage   = stage.stage,
			.name    = stage.pName,
			.extra   = cg2Stage->extra,
		},
		.bindlessImages = &(Cg2cBindlessVk) {
			.set = 0,
			.bindings = {
				.samplers = 0,
				.sampledImages = 1,
				.storageImages = 2,
			},
		},
		.files = cg2Stage->files,
		.nfile = cg2Stage->nfile,
		.readFile = readshader,
		.diag = diag_cg2c,
	}, &module);

	VkComputePipelineCreateInfo createInfo2 = *createInfo;
	createInfo2.stage.pNext = cg2Stage->pNext;
	createInfo2.stage.module = module;

	if ((r = fns.vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &createInfo2, NULL, out)) != VK_SUCCESS)
		fatalvk("vkCreateComputePipelines: ", r);

	fns.vkDestroyShaderModule(device, module, NULL);
}
