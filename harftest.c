#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ft.h>

#include <freetype/freetype.h>
#include <freetype/ftglyph.h>

typedef int int2 __attribute__((ext_vector_type(2)));
typedef int int3 __attribute__((ext_vector_type(3)));
typedef int int4 __attribute__((ext_vector_type(4)));

typedef float float2 __attribute__((ext_vector_type(2)));
typedef float float3 __attribute__((ext_vector_type(3)));
typedef float float4 __attribute__((ext_vector_type(4)));

enum {
	WIDTH = 1024,
	HEIGHT = 200,
};

/* origin is the upper left corner */
unsigned char image[HEIGHT][WIDTH];

/* Replace this function with something useful. */

void
draw_bitmap(FT_Bitmap* bitmap, FT_Int x, FT_Int y)
{
	FT_Int  i, j, p, q;
	FT_Int  x_max = x + bitmap->width;
	FT_Int  y_max = y + bitmap->rows;

	/* for simplicity, we assume that `bitmap->pixel_mode' */
	/* is `FT_PIXEL_MODE_GRAY' (i.e., not a bitmap font)   */

	for (i = x, p = 0; i < x_max; i++, p++) {
		for (j = y, q = 0; j < y_max; j++, q++) {
			if (i < 0 || j < 0 || i >= WIDTH || j >= HEIGHT)
				continue;

			image[j][i] |= bitmap->buffer[q * bitmap->width + p];
		}
	}
}

void
show_image(void)
{
	FILE *out = fopen("out.ppm", "wb");

	fprintf(out, "P6 %d %d 255\n", WIDTH, HEIGHT);

	for (uint32_t y = 0; y < HEIGHT; y++) {
		for (uint32_t x = 0; x < WIDTH; x++) {
			uint8_t ch = image[y][x];

			fprintf(out, "%c%c%c", ch, ch, ch);
		}
	}

	fclose(out);
}

typedef struct Curve Curve;
struct Curve {
	uint32_t _0;
	uint32_t _1;
};

typedef struct Tile Tile;
struct Tile {
	float2 *points;
	Curve *curves;
	uint32_t curveCount;
	bool inside;
};

typedef struct Glyph Glyph;
struct Glyph {
	// To support efficient aniso sampling, we will want a hierarchy of
	// tiles, like mip chain in a bitmap image? Alternatively, we can just
	// not care about aniso and let TAA take care of aliasing
	Tile *tiles; // [width * height]
	// TODO: we also need tile offset as most glyphs don't have their
	// bounding boxes contain 0, 0... or maybe don't bother
	float tileWidth; // should these be uint32_t too?
	float tileHeight;
	uint32_t widthInTiles;
	uint32_t heightInTiles;
};

typedef struct VectorTexture VectorTexture;
struct VectorTexture {
	Glyph **glyphs; // not sure if this extra indirection is ok or not for GPU
	float2 *glyphPos;
	uint32_t glyphCount;
};

typedef union Matrix2x2 {
	struct {
		float _00, _01;
		float _10, _11;
	};
	float m[2][2];
} Matrix2x2;

static float2
normalize2f(float2 a)
{
	float norm = sqrtf(a.x*a.x + a.y*a.y);
	if (norm == 0.0f)
		return 0.0f;
	return a / norm;
}

static float2
mat2x2mul2f(Matrix2x2 A, float2 v)
{
	return (float2) {A._00*v.x + A._01*v.y, A._10*v.x + A._11*v.y};
}

typedef struct ScanconvParams ScanconvParams;
struct ScanconvParams {
	VectorTexture *vg;

	uint32_t x;
	uint32_t y;
};

static int
all2(int2 v)
{
	return v.x && v.y;
}

static int
or2(int2 v)
{
	return v.x || v.y;
}

// find zeros of cx + d
static int
zeros(float a, float b, float c, float d, float *t)
{
	assert(a == 0 && b == 0);

	t[0] = 0.0f;
	t[1] = 0.0f;
	t[2] = 0.0f;

	if (c == 0)
		return 0;

	t[0] = -d / c;
	return 1;
}

static float
eval(float a, float b, float c, float d, float t)
{
	return c*t + d;
}

static float
evaldt(float a, float b, float c, float d, float t)
{
	return c;
}

static float dot2f(float2 v, float2 w) { return v.x*w.x + v.y*w.y; }

static float
distance2f_squared(float2 v, float2 w)
{
	float2 d = w - v;
	return dot2f(d, d);
}

static float distance2f(float2 v, float2 w) { return sqrtf(distance2f_squared(v, w)); }

static float
fclampf(float x, float minVal, float maxVal)
{
	return fminf(fmaxf(x, minVal), maxVal);
}

static float
closestp(float2 v, float2 w, float2 p)
{
	float l2 = distance2f_squared(v, w);
	if (l2 == 0.0)
		return distance2f(p, v);
	float t = fclampf(dot2f(p - v, w - v) / l2, 0.0f, 1.0f);
	float2 projection = v + t * (w - v);
	return distance2f(p, projection);
}

static float
dist(float2 a, float2 b, float2 c, float2 d)
{
	assert(all2(a == 0 && b == 0));

	return closestp(d, d+c, 0.0f);
}

// https://raphlinus.github.io/graphics/curves/2019/12/23/flatten-quadbez.html

// https://pomax.github.io/bezierinfo

float
pathSample(VectorTexture *path, bool disableAA, float2 P, float2 dPdx, float2 dPdy)
{
	float distance = INFINITY;

	bool inside = false;
	float closest = INFINITY;

	for (uint32_t i = 0; i < path->glyphCount; i++) {
		Glyph *glyph = path->glyphs[i];

		float2 uv = P - path->glyphPos[i];

		if (or2(uv < (float2)0.0f) || or2(uv > (float2) {glyph->tileWidth, glyph->tileHeight}))
			continue;

		int32_t tileX = uv.x / glyph->tileWidth;
		int32_t tileY = uv.x / glyph->tileHeight;

		Tile *tile = &glyph->tiles[0];

		float2 tileLocation = {glyph->tileWidth / 2.0f, glyph->tileHeight / 2.0f};

		for (uint32_t k = 0; k < tile->curveCount; k++) {
			float2 *points = tile->points;
			Curve curve = tile->curves[k];

			// Help compiler
			if (!disableAA) {
				float2 p0 = points[curve._0] - uv;
				float2 p1 = points[curve._1] - uv;

				distance = fminf(distance, dist(0.0f, 0.0f, p1-p0, p0));
			}

			{
				float2 direction = normalize2f(tileLocation - uv);

				Matrix2x2 R = {
					direction.x, direction.y,
					-direction.y, direction.x,
				};

				float2 p0 = mat2x2mul2f(R, points[curve._0] - uv);
				float2 p1 = mat2x2mul2f(R, points[curve._1] - uv);

				float t[3] = {};
				int n = zeros(0.0f, 0.0f, p1.y-p0.y, p0.y, t);

				for (int j = 0; j < n; j++) {
					if (t[j] < 0.0f || 1.0f <= t[j])
						continue;

					float x = eval(0.0f, 0.0f, p1.x-p0.x, p0.x, t[j]);
					float dydt = evaldt(0.0f, 0.0f, p1.y-p0.y, p0.y, t[j]);

					if (0.0f <= x && x < closest) {
						inside = dydt < 0.0f;

						closest = x;
					}
				}
			}
		}
	}

	if (disableAA)
		distance = 0.5f;

	float signedDistance = inside ? distance : -distance;

	return 0.5f + signedDistance;
}

void
scanconv(ScanconvParams *arg)
{
	float r = pathSample(arg->vg, false, (float2) {arg->x + 0.5f, arg->y + 0.5f - 50.0f}, 0.0f, 0.0f);

	if (true)
		image[HEIGHT-1-arg->y][arg->x] = fclampf(r, 0.0, 1.0f) * 0xff;
	else
		image[HEIGHT-1-arg->y][arg->x] = fabsf(fclampf(r, 0.0, 1.0f) - image[HEIGHT-1-arg->y][arg->x] / 255.0f) * 0xff;
}

void
scanconvTest(ScanconvParams *arg, uint32_t width, uint32_t height)
{
	for (uint32_t y = 0; y < height; y++) {
		for (uint32_t x = 0; x < width; x++) {
			ScanconvParams arg2 = *arg;
			arg2.x = x;
			arg2.y = y;
			scanconv(&arg2);
		}
	}
}

// clang -O3 -lm -lfreetype -lharfbuzz -I/usr/include/freetype2 harftest.c && ./a.out fonts/DSEG14-Classic/DSEG14Classic-Italic.ttf 'hello' && xdg-open out.ppm
int
main(int argc, char** argv)
{
	FT_Library    library;
	FT_Face       face;

	FT_GlyphSlot  slot;
	FT_Vector     pen;                    /* untransformed origin  */
	FT_Error      fterr;

	char*         filename;
	char*         text;

	int           target_height;
	int           num_chars;

	if (argc != 3) {
		fprintf(stderr, "usage: %s font sample-text\n", argv[0]);
		exit(1);
	}

	filename      = argv[1];                           /* first argument     */
	text          = argv[2];                           /* second argument    */
	num_chars     = strlen(text);
	target_height = HEIGHT;

	fterr = FT_Init_FreeType(&library);              /* initialize library */

	fterr = FT_New_Face(library, filename, 0, &face); /* create face object */

	fterr = FT_Set_Char_Size(face, 20 * 64 /* char width */, 0 /* char height */, 72 /* horiz. resolution */, 0 /* vert. resolution */);

	Glyph *myGlyphs = malloc((size_t)face->num_glyphs * sizeof myGlyphs[0]);

	for (FT_Long i = 0; i < face->num_glyphs; i++) {
		fterr = FT_Load_Glyph(face, i, FT_LOAD_DEFAULT);
		if (fterr != 0) {
			fprintf(stderr, "warning: could not load glyph %lld\n", (long long)i);
			continue;
		}

		FT_Glyph glyph;
		FT_Get_Glyph(face->glyph, &glyph);

		if (glyph->format != FT_GLYPH_FORMAT_OUTLINE) {
			fprintf(stderr, "warning: glyph %lld does not have outline\n", (long long)i);
			continue;
		}

		FT_OutlineGlyph outlineGlyph = (FT_OutlineGlyph)glyph;

		FT_Outline *outline = &outlineGlyph->outline;

		float2 *points = malloc(10000 * sizeof points[0]);
		Curve *curves = malloc(10000 * sizeof curves[0]);

		float2 *pp = points;

		uint32_t curveCount = 0;

		float2 min = (float2)INFINITY;
		float2 max = (float2)-INFINITY;

		for (short k = 0; k < outline->n_contours; k++) {
			short first = 0;
			if (k > 0)
				first = outline->contours[k-1] + 1;

			short last = outline->contours[k] + 1;

			for (short j = first; j < last; j++) {
				FT_Vector p = outline->points[j];
				uint8_t tag = outline->tags[j];

				*pp++ = (float2) {p.x / 64.0f, p.y / 64.0f};

				switch (FT_CURVE_TAG(tag)) {
				case FT_CURVE_TAG_ON:
				case FT_CURVE_TAG_CONIC:
				case FT_CURVE_TAG_CUBIC: {
					short nextp = j + 1;
					if (nextp >= last)
						nextp = first;
					curves[curveCount++] = (Curve) {j, nextp};
					break;
				}

				default:
					assert(!"unreachable");
				}

				min.x = fminf(min.x, p.x / 64.0f);
				min.y = fminf(min.y, p.y / 64.0f);
				max.x = fmaxf(max.x, p.x / 64.0f);
				max.y = fmaxf(max.y, p.y / 64.0f);
			}
		}

		// just do single tile for now

		Tile *tiles = malloc(1 * sizeof tiles[0]);
		tiles[0].points = points;
		tiles[0].curves = curves;
		tiles[0].curveCount = curveCount;
		tiles[0].inside = false; // TODO

		myGlyphs[i].tiles = tiles;
		myGlyphs[i].tileWidth = max.x;
		myGlyphs[i].tileHeight = max.y;
		myGlyphs[i].widthInTiles = 1;
		myGlyphs[i].heightInTiles = 1;

		printf("glyph %d has %d curves mins=[%g; %g] maxs=[%g; %g]\n", (int)i, tiles[0].curveCount, min.x, min.y, max.x, max.y);
	}

	hb_buffer_t *buf;
	buf = hb_buffer_create();
	hb_buffer_add_utf8(buf, text, -1, 0, -1);

	hb_buffer_set_direction(buf, HB_DIRECTION_LTR);
	hb_buffer_set_script(buf, HB_SCRIPT_LATIN);
	hb_buffer_set_language(buf, hb_language_from_string("en", -1));

	hb_blob_t *blob = hb_blob_create_from_file(filename); /* or hb_blob_create_from_file_or_fail() */
	hb_face_t *hbface = hb_face_create(blob, 0);
	// hb_font_t *hbfont = hb_font_create(hbface);
	// hb_font_set_ptem(hbfont, 50.0f);
	hb_font_t *hbfont = hb_ft_font_create_referenced(face);
	hb_ft_font_set_funcs(hbfont);

	hb_shape(hbfont, buf, NULL, 0);

	unsigned int glyph_count;
	hb_glyph_info_t *glyph_info    = hb_buffer_get_glyph_infos(buf, &glyph_count);
	hb_glyph_position_t *glyph_pos = hb_buffer_get_glyph_positions(buf, &glyph_count);

	slot = face->glyph;

	/* the pen position in 26.6 cartesian space coordinates; */
	/* start at (300,200) relative to the upper left corner  */
	pen.x = 0;
	pen.y = 0;

	VectorTexture vg = {};
	vg.glyphs = malloc(glyph_count * sizeof vg.glyphs[0]);
	vg.glyphPos = malloc(glyph_count * sizeof vg.glyphPos[0]);
	vg.glyphCount = glyph_count;

	for (unsigned int i = 0; i < glyph_count; i++) {
		hb_position_t x_offset  = glyph_pos[i].x_offset;
		hb_position_t y_offset  = glyph_pos[i].y_offset;
		hb_position_t x_advance = glyph_pos[i].x_advance;
		hb_position_t y_advance = glyph_pos[i].y_advance;

		vg.glyphs[i] = &myGlyphs[glyph_info[i].codepoint];
		vg.glyphPos[i] = (float2) {pen.x / 64.0f, pen.y / 64.0f};

		printf("%d\n", (int)glyph_info[i].codepoint);

		fterr = FT_Load_Glyph(face, glyph_info[i].codepoint, FT_LOAD_DEFAULT);
		if (fterr)
			continue;

		FT_Glyph glyph;
		FT_Get_Glyph(face->glyph, &glyph);
		assert(glyph->format == FT_GLYPH_FORMAT_OUTLINE);
		//do_something_about_glyph((FT_OutlineGlyph)glyph);

		if (false) {
			fterr = FT_Load_Glyph(face, glyph_info[i].codepoint, FT_LOAD_RENDER);
			if (fterr)
				abort();

			draw_bitmap(&slot->bitmap, slot->bitmap_left, target_height - slot->bitmap_top);
		}

		/* increment pen position */
		pen.x += x_advance;
		pen.y += y_advance;
	}

	scanconvTest(&(ScanconvParams) {
		.vg = &vg,
	}, WIDTH, HEIGHT);

	show_image();

	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return 0;
}

/* EOF */
