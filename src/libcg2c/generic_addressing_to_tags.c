#include "all.h"

static bool
cg2c_typeIsMem(Type *t)
{
	if (cg2c_typeKind(t) == KindMem)
		return true;

	if (cg2c_typeKind(t) == KindTuple) {
		// TODO: add a field to cache the result?

		for (size_t i = 0; i < cg2c_tupleNElem(t); i++) {
			if (cg2c_typeIsMem(cg2c_tupleElem(t, i)))
				return true;
		}
	}

	return false;
}

static Value*
newValue1(Builder b, Op op, Type *type, Value *a0)
{
	Value *args[] = {a0};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2(Builder b, Op op, Type *type, Value *a0, Value *a1)
{
	Value *args[] = {a0, a1};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue2A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1)
{
	Value *args[] = {a0, a1};
	return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3(Builder b, Op op, Type *type, Value *x, Value *y, Value *z)
{
	Value *args[] = {x, y, z};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue3A(Builder b, Op op, Type *type, void *aux, Value *a0, Value *a1, Value *a2)
{
	Value *args[] = {a0, a1, a2};
	return cg2c_buildValue(b, op, type, aux, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Value*
newValue5(Builder b, Op op, Type *type, Value *a0, Value *a1, Value *a2, Value *a3, Value *a4)
{
	Value *args[] = {a0, a1, a2, a3, a4};
	return cg2c_buildValue(b, op, type, NULL, cg2c_mkslicenocopy(sizeof args[0], args, nelem(args)));
}

static Func*
scopeNewFunc(Scope *scope, Type *type, GC *gc)
{
	Func *f = cg2c_malloc(sizeof *f, gc);
	f->type = type;
	scope->funcs = cg2c_slappend(sizeof(Func*), scope->funcs, &f, gc);
	return f;
}

// TODO: maybe this should take Value*s instead of Func*?
static Value*
buildIf(Builder b, Value *cond, Func *then, Func *else_, Func *merge, Value *arg)
{
	assert(then->type == else_->type);
	Value *vthen = cg2c_buildFuncValue(b, then);
	Value *velse = cg2c_buildFuncValue(b, else_);
	Value *vmerge = cg2c_buildFuncValue(b, merge);
	return newValue5(b, OpCallStructuredIf, cg2c_types[KindNoret], cond, vthen, velse, vmerge, arg);
}

static void
collectLoadStores(Compile *ctxt, Value *v, void *etc)
{
	Map *loads = etc;

	switch (v->op) {
	case OpLoad: {
		Value *mem = cg2c_valueArg(v, 0);

		Slice loadss = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, loads, &mem);
		loadss = cg2c_slappend(sizeof(Value*), loadss, &v, ctxt->gc);
		*(Slice*)cg2c_mapassign(cg2c_ptr_slice_map, loads, &mem, ctxt->gc) = loadss;

		break;
	}

	default:
		break;
	}
}

static bool
genericAddressingToTags(Builder b, Value *v, Func *curF, Map *re, Map *visited, Map *loads, Scope *scope)
{
	bool changed = false;

	// TODO: why is visited tracking necessary?
	if (cg2c_mapcontains(cg2c_ptr_set, visited, &v))
		return false;
	cg2c_mapassign(cg2c_ptr_set, visited, &v, b.ctxt->gc);

	b = cg2c_builderWithPos(b, v->pos, v);

	if (cg2c_typeKind(v->type) == KindMem) {
		Slice loadss = *(const Slice*)cg2c_mapaccess1(cg2c_ptr_slice_map, loads, &v);

		Value *w = v;
		switch (v->op) {
		case OpStore: {
			Value *mem = cg2c_valueArg(v, 0);
			Value *p = cg2c_valueArg(v, 1);
			Value *pBase = cg2c_ptrchase(p);

			Value *isGlobal = newValue1(b, OpPtrIsGlobal, cg2c_types[KindBool], pBase);

			Type *bb = cg2c_funct(b.ctxt, cg2c_types[KindMem], cg2c_types[KindNoret]);

			Func *bbGlobal = scopeNewFunc(scope, bb, b.ctxt->gc);
			Func *bbScratch = scopeNewFunc(scope, bb, b.ctxt->gc);
			Func *bbMerge = scopeNewFunc(scope, bb, b.ctxt->gc);

			Value *doMerge = cg2c_buildFuncValue(b, bbMerge);

			{
				Value *mem = cg2c_buildParam(b, bbGlobal);
				mem = newValue3A(b, OpStoreGlobal, cg2c_types[KindMem], v->aux, mem, p, cg2c_valueArg(v, 2));
				cg2c_funcTerminate(bbGlobal, cg2c_buildCall(b, doMerge, mem), b.ctxt->gc);
			}

			{
				Value *mem = cg2c_buildParam(b, bbScratch);
				mem = newValue3A(b, OpStoreScratch, cg2c_types[KindMem], v->aux, mem, p, cg2c_valueArg(v, 2));
				cg2c_funcTerminate(bbScratch, cg2c_buildCall(b, doMerge, mem), b.ctxt->gc);
			}

			{
				w = cg2c_buildParam(b, bbMerge);

				// Stash the old terminator away for now, we'll rewrite
				// it once we're done inserting control flow
				Value *t = cg2c_funcTerminator(curF);
				cg2c_funcTerminate(bbMerge, t, b.ctxt->gc);
			}

			cg2c_funcTerminate(curF, buildIf(b, isGlobal, bbGlobal, bbScratch, bbMerge, mem), b.ctxt->gc);

			break;
		}

		default:
			break;
		}

		/*
		for (size_t i = 0; i < cg2c_sllen(loadss); i++) {
			Value *load = *(Value**)cg2c_slindex(sizeof(Value*), loadss, i);

			Value *mem = w;
			Value *p = cg2c_valueArg(load, 1);
			Value *pBase = cg2c_ptrchase(p);

			Value *isGlobal = newValue1(b, OpPtrIsGlobal, cg2c_types[KindBool], pBase);

			Type *bb = cg2c_funct(b.ctxt, cg2c_types[KindMem], cg2c_types[KindNoret]);
			Type *bb2 = cg2c_funct(b.ctxt, cg2c_tuple2(b.ctxt, cg2c_types[KindMem], load->type), cg2c_types[KindNoret]);

			Func *bbGlobal = scopeNewFunc(scope, bb, b.ctxt->gc);
			Func *bbScratch = scopeNewFunc(scope, bb, b.ctxt->gc);
			Func *bbMerge = scopeNewFunc(scope, bb2, b.ctxt->gc);

			Value *doMerge = cg2c_buildFuncValue(b, bbMerge);

			{
				Value *mem = cg2c_buildParam(b, bbGlobal);
				Value *res = newValue2A(b, OpLoadGlobal, cg2c_types[KindMem], v->aux, mem, p);
				cg2c_funcTerminate(bbGlobal, cg2c_buildCall(b, doMerge, cg2c_buildMakeTuple2(b, mem, res)), b.ctxt->gc);
			}

			{
				Value *mem = cg2c_buildParam(b, bbScratch);
				Value *res = newValue2A(b, OpLoadScratch, cg2c_types[KindMem], v->aux, mem, p);
				cg2c_funcTerminate(bbScratch, cg2c_buildCall(b, doMerge, cg2c_buildMakeTuple2(b, mem, res)), b.ctxt->gc);
			}

			{
				Value *arg = cg2c_buildParam(b, bbMerge);
				w = cg2c_buildExtract(b, 0, arg);
				Value *val = cg2c_buildExtract(b, 1, arg);

				assert(load->type == val->type);
				*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &load, b.ctxt->gc) = val;

				// Stash the old terminator away for now, we'll rewrite
				// it once we're done inserting control flow
				Value *t = cg2c_funcTerminator(curF);
				cg2c_funcTerminate(bbMerge, t, b.ctxt->gc);
			}

			cg2c_funcTerminate(curF, buildIf(b, isGlobal, bbGlobal, bbScratch, bbMerge, mem), b.ctxt->gc);
		}
		*/

		if (v != w) {
			assert(v->type == w->type);
			*(Value**)cg2c_mapassign(cg2c_ptr_ptr_map, re, &v, b.ctxt->gc) = w;

			changed = true;
		}
	}

	for (size_t i = 0; i < cg2c_valueNArg(v); i++) {
		Value *a = cg2c_valueArg(v, i);

		if (cg2c_typeIsMem(a->type))
			changed = genericAddressingToTags(b, a, curF, re, visited, loads, scope) || changed;
	}

	return changed;
}

bool
cg2c_genericAddressingToTags(Builder b, Scope *scope)
{
	bool changed = false;

	/*
	Map *loads = cg2c_mkmap(cg2c_ptr_slice_map, b.ctxt->gc);

	cg2c_scopeForeach(b.ctxt, collectLoadStores, loads, scope);
	*/

	Map *loads = NULL;

	Map *visited = cg2c_mkmap(cg2c_ptr_set, b.ctxt->gc);

	Map *re = cg2c_mkmap(cg2c_ptr_ptr_map, b.ctxt->gc);

	for (size_t i = 0; i < cg2c_sllen(scope->funcs); i++) {
		Func *g = *(Func**)cg2c_slindex(sizeof(Func*), scope->funcs, i);

		changed = genericAddressingToTags(b, cg2c_funcTerminator(g), g, re, visited, loads, scope) || changed;
	}

	changed = cg2c_scopeRewrite(b, scope, re) || changed;

	return changed;
}
