#include "all.h"

String
cg2c_mkstringnocopy(const char *s)
{
	if (s == NULL)
		return (String) {};
	return (String) {s, strlen(s)};
}

int
cg2c_strcmp(String a, String b)
{
	int r = 0;

	size_t n = a.len < b.len ? a.len : b.len;
	// memcmp requires valid pointers even with zero n
	if (n > 0)
		r = memcmp(a.data, b.data, n);
	if (r == 0) {
		if (a.len < b.len)
			r = -1;
		else if (a.len > b.len)
			r = 1;
	}
	return r;
}
