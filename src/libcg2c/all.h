#include <assert.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cg2c/cg2c.h>

#ifndef __GNUC__
#define __attribute__(x)
#endif

#define nelem(x) (sizeof(x)/sizeof((x)[0]))

typedef int32_t Rune;

enum {
	Runeerror = 0xFFFD, // error rune or "Unicode replacement character"
	Runeself  = 0x80,   // runes represented in a single byte
	UTFmax    = 4,      // maximum length of any UTF sequence
};

typedef struct GC GC;

typedef struct String String;
struct String {
	const char *data;
	size_t     len;
};

// cg2c_mkstringnocopy constructs a String from a C string s.
String cg2c_mkstringnocopy(const char *s);

// See strcmp(3).
int cg2c_strcmp(String s, String t);

// cg2c_strdata returns a pointer either to underlying contents of s or some
// other valid pointer, suitable for use with standard C library routines such
// as memcmp and printf.
static const char* cg2c_strdata(String s) { return s.len > 0 ? s.data : ""; }
// To get int length for use with C library routines such as printf, use
// cg2c_strlen2.
static size_t cg2c_strlen(String s) { return s.len; }

// rename to something like strilen?
static int
cg2c_strlen2(String s)
{
	assert(s.len <= INT_MAX);
	return (int)s.len;
}

static String
cg2c_strslice(String s, size_t i, size_t j)
{
	if (i > j || j > s.len)
		assert(!"slice bounds out of range");
	return (String) {i > 0 ? s.data+i : s.data, j - i};
}

typedef struct Slice Slice;
struct Slice {
	void   *data;
	size_t len;
	size_t cap;

	size_t _elemsize; // for diagnostics
};

#define NULL_SLICE (Slice) {}

Slice cg2c_mkslice(size_t elemsize, const void *elems, size_t n, GC *gc);
Slice cg2c_mkslicenocopy(size_t elemsize, void *elems, size_t n);

#define cg2c_mkslicenocopy_macro(elemsize, elems, n) ((Slice) {elems, n, n, elemsize})

static size_t cg2c_sllen(Slice s) { return s.len; }
static size_t cg2c_slcap(Slice s) { return s.cap; }

static void*
cg2c_slindex(size_t elemsize, Slice s, size_t i)
{
	if (s.cap > 0 && s._elemsize != elemsize)
		assert(!"slice type mismatch");
	if (i >= s.len)
		assert(!"slice index out of range");

	return (char*)s.data + i*elemsize;
}

static Slice
cg2c_slice(size_t elemsize, Slice s, size_t i, size_t j)
{
	if (s.cap > 0 && s._elemsize != elemsize)
		assert(!"slice type mismatch");
	if (i > j || j > s.cap)
		assert(!"slice bounds out of range");

	return (Slice) {i > 0 ? (char*)s.data+i*elemsize : s.data, j - i, s.cap - i, elemsize};
}

Slice cg2c_slappend(size_t elemsize, Slice s, const void *e, GC *gc);
Slice cg2c_slcat(size_t elemsize, Slice s1, Slice s2, GC *gc);
Slice cg2c_slclone(size_t elemsize, Slice s, GC *gc);

typedef struct Map     Map;
typedef struct MapType MapType;

Map*        cg2c_mkmap(const MapType *t, GC *gc);
const void* cg2c_mapaccess1(const MapType *t, const Map *h, const void *key);
const void* cg2c_mapaccess2(const MapType *t, const Map *h, const void *key, bool *pres);
bool        cg2c_mapcontains(const MapType *t, const Map *h, const void *key);
void*       cg2c_mapassign(const MapType *t, Map *h, const void *key, GC *gc);
void        cg2c_mapdelete(const MapType *t, Map *h, const void *key, GC *gc);

// Map types

extern const MapType* cg2c_32_32_map;
extern const MapType* cg2c_64_32_map;
extern const MapType* cg2c_str_32_map;
extern const MapType* cg2c_str_ptr_map;
extern const MapType* cg2c_ptr_32_map;
extern const MapType* cg2c_ptr_ptr_map;
extern const MapType* cg2c_ptr_slice_map;
extern const MapType* cg2c_ptr_set;
extern const MapType* cg2c_ptrSlice_ptr_map; // careful: slices used as keys must not be modified

size_t cg2c_memhash(const void *p, size_t n, size_t seed);

// cg2c_runetochar writes the UTF sequence of rune r to s.
int cg2c_runetochar(char *s, Rune r);
// cg2c_chartorune decodes an at most n-byte long UTF sequence starting at s,
// writes the corresponding rune to r and returns the length of the sequence.
int cg2c_chartorune(Rune *r, const char *s, size_t n);

void* cg2c_malloc(size_t size, GC *gc);
void* cg2c_memdup(const void *p, size_t size, GC *gc);

int cg2c_gccall(void (*f)(void*, GC*), void *aux, const Cg2cAllocFuncs *_unused);

typedef enum Kind           Kind;
typedef enum Op             Op;
typedef struct Builder      Builder;
typedef struct CSE          CSE;
typedef struct Compile      Compile;
typedef struct Func         Func;
typedef struct OpInfo       OpInfo;
typedef struct Pattern      Pattern;
typedef struct Pos          Pos;
typedef struct PosBase      PosBase;
typedef struct RewriteRule  RewriteRule;
typedef struct Rewriter     Rewriter;
typedef struct Scope        Scope;
typedef struct Sym          Sym;
typedef struct Type         Type;
typedef struct Value        Value;

struct PosBase {
	String filename;
};

struct Pos {
	PosBase *base;
	uint32_t line, col;
};

bool cg2c_posEqual(Pos a, Pos b);

// TODO: move language-specific options into separate struct

/*
struct CConfig {
	bool unsignedChar;

	// longSize must be either 4 or 8.
	int64_t longSize;
};
*/

struct Compile {
	bool unsignedChar;

	// longSize must be either 4 or 8.
	int64_t longSize;

	// ptrSize must be 8.
	int64_t ptrSize;

	int64_t funcSize;

	// bigEndian must be false.
	bool bigEndian;

	// Interned types. TODO: these should be weak maps once we get real GC
	// and be renamed to something clearer

	Map *funcs;  // Type*[2] → Type*
	Map *tuples; // Slice of Type* → Type*

	// TODO: rename to uidCtr?
	uint64_t varId;

	bool errored;

	void *dumper;

	void (*diag)(const char*, int, void*);
	void *udata;

	GC *gc;
};

void cg2c_warnf(Compile*, const char*, ...) __attribute__((format(printf, 2, 3)));
void cg2c_warnfAt(Compile*, Pos, const char*, ...) __attribute__((format(printf, 3, 4)));
void cg2c_errorfAt(Compile*, Pos, const char*, ...) __attribute__((format(printf, 3, 4)));
void cg2c_errorf(Compile*, const char*, ...) __attribute__((format(printf, 2, 3)));
void cg2c_verrorf(Compile*, const char*, va_list);
// TODO: cg2c_fatalf(Compile*, const char*, ...);

enum Kind {
	KindXxx,
	KindBool,
	KindInt8,
	KindInt16,
	KindInt32,
	KindInt64,
	KindPtr,
	KindFunc,
	KindMem,
	KindRMem,
	KindNoret,
	KindTuple,
	KindLast,
};

extern const char* cg2c_kindName[KindLast];

// Basic types
extern Type* cg2c_types[KindLast];

// TODO: rename to just cg2c_intptr? or cg2c_intptrt
static Type*
cg2c_intptrt(Compile *ctxt)
{
	switch (ctxt->ptrSize) {
	case 4:
		assert(0); // untested, don't use
		return cg2c_types[KindInt32];

	case 8:
		return cg2c_types[KindInt64];

	default:
		assert(0);
		return NULL;
	}
}

// result is deprecated and should always be noret
Type* cg2c_funct(Compile *ctxt, Type *param, Type *result);

Type* cg2c_tuple(Compile *ctxt, Slice elems);

Type* cg2c_tuple2(Compile *ctxt, Type *e0, Type *e1);

Type* cg2c_tuple3(Compile *ctxt, Type *e0, Type *e1, Type *e2);

Type* cg2c_tuple4(Compile *ctxt, Type *e0, Type *e1, Type *e2, Type *e3);

Kind cg2c_typeKind(Type *t);

Type* cg2c_funcParam(Type *t);

Type* cg2c_funcResult(Type *t);

size_t cg2c_tupleNElem(Type *t);

Type* cg2c_tupleElem(Type *t, size_t i);

int64_t cg2c_typeSize(Compile *ctxt, Type *t);

#include "ops.h"

struct OpInfo {
	const char *name;
	bool smallaux; // TODO: replace with a more descriptive auxType
	bool commutative;
};

extern OpInfo cg2c_opInfo[OpLast];

/*

// Subgroup reduction and scans need to be parametrized by a reduction op.
//
// Some subgroup voting needs to be parametrized by a comparison op.
//
// Most atomic accesses only need scope and order. In addition to these, compare
// exchange needs fail order and RMW needs op.
//
// Image ops need to specify image dimensionality, component type (float, int,
// uint, int64, uint64) and whether it's multisampled or not.
//
// Image atomics are same as plain atomics but also have all the image bits (see
// ImageTexelPointer in SPIR-V, which is used to implement image atomics.)
//
// Texture ops are hard, TODO

struct AuxType {
	hasher and equal
};

enum {
	auxTypeXxx,
	auxTypePtr,
	auxTypeInt,
	auxTypeVar,
	auxTypeSubgroup,
	auxTypeAtomic,
	auxTypeImage,
	auxTypeImageAtomic,
	auxTypeTex,
	auxTypeLast,
};
*/

enum {
	SVXxx,

	SVVertexID,

	SVFragCoord, // (int32, int32, int32, int32)

	SVGlobalInvocationID,   // (int32, int32, int32)
	SVLocalInvocationID,    // (int32, int32, int32)
	SVSubgroupInvocationID, // int32

	SVGlobalInvocationIDY, // temp hack sv, get rid of this!

	SVLast,
};

typedef struct AuxSubgroup AuxSubgroup;
struct AuxSubgroup {
	Op op; // T × T → T op for reductions and scans
};

typedef struct AuxAtomic AuxAtomic;
struct AuxAtomic {
	int64_t align;
	// int scope;
	// int order;
	// int failOrder;
	Op rmwOp; // T × T → T op for AtomicRMW, must be zero otherwise
};

typedef enum Dim Dim;
enum Dim {
	DimXxx,
	Dim1D,
	Dim2D,
	Dim3D,
	DimCube,
	DimLast,
};

typedef struct AuxImage AuxImage;
struct AuxImage {
	int  comp; // 1=float, 2=int32, 3=uint32, 4=int64, 5=int64
	Dim  dim;
	bool array;
	bool ms;
};

/*
typedef struct AuxImageAtomic AuxImageAtomic;
struct AuxImageAtomic {
	AuxImage image;
	// + atomic bits
};

typedef struct AuxTex AuxTex;
struct AuxTex {

};
*/

// Value definition
struct Value {
	Op    op;
	Type  *type;
	void  *aux;
	Slice args; // of Value*

	Pos pos;

	// TRACING ONLY: Value* or some other object this Value* is related to.
	// When tracing is enabled, all objects should be kept alive throughout
	// the entire compilation process to avoid
	const void *trace;

	uint64_t auxstorage; // storage for small aux; do not access directly
};

struct Builder {
	Compile  *ctxt;
	Rewriter *rewriter;
	CSE      *cse;
	Pos      pos; // source location that the built instructions will be assigned
	const void *trace; // see Value.trace
};

Builder cg2c_builderWithPos(Builder b, Pos pos, const void *trace);

// Replacing an op in-place (with a hypothetical cg2c_replaceValue function)
// could be done by removing an entry from CSE and re-adding the replaced Value,
// but this is likely to be a horrible footgun. Notably if replaced Value
// already existed in CSE context and was used by something, CSE would cease to
// work properly until all Values have been rewritten. But it could still be
// worth it for some passes, that currently run rewrite on the entire scope
// after they're done. We should verify whether this whole thing actually brings
// any benefits once we have bigger programs to compile and rewriter is actually
// fast.

Value* cg2c_buildValue(Builder b, Op op, Type *type, void *aux, Slice args);

Value* cg2c_buildArith2(Builder b, Op op, Value *x, Value *y);

Value* cg2c_buildShift(Builder b, Op op, Value *x, Value *y);

Value* cg2c_buildShiftConst(Builder b, Op op, Value *x, int k);

Value* cg2c_buildParam(Builder b, Func *f);

Value* cg2c_buildConst(Builder b, Type *type, uint64_t val);

Value* cg2c_buildFuncValue(Builder b, Func *f);

Value* cg2c_buildPtrToInt(Builder b, Value *v);

Value* cg2c_buildIntToPtr(Builder b, Value *v);

Value* cg2c_buildMakeTuple(Builder b, Slice elems);

Value* cg2c_buildMakeTuple2(Builder b, Value *e0, Value *e1);

Value* cg2c_buildMakeTuple4(Builder b, Value *e0, Value *e1, Value *e2, Value *e3);

// TODO: make index come after tup?
Value* cg2c_buildExtract(Builder b, size_t i, Value *tup);

Value* cg2c_buildAddPtr(Builder b, Value *base, Value *off);

Value* cg2c_buildAddPtrConst(Builder b, Value *base, int64_t off);

Value* cg2c_buildVar(Builder b, int64_t size, int64_t align);

Value* cg2c_buildLoad(Builder b, Type *type, int64_t align, Value *mem, Value *ptr);

Value* cg2c_buildStore(Builder b, int64_t align, Value *mem, Value *ptr, Value *val);

Value* cg2c_buildCall(Builder b, Value *target, Value *arg);

// TODO: move implementations out of the header

Func* cg2c_valueAuxFunc(Value *v);

// or Const and ConstU

int64_t cg2c_valueAuxInt(Value *v);

uint64_t cg2c_valueAuxUint(Value *v);

size_t cg2c_valueNArg(Value *v);
Value* cg2c_valueArg(Value *v, size_t i);

// Like cg2c_valueArg, but asserts that the resulting value is an OpFunc, and
// returns the func that describes.
Func* cg2c_valueArgFunc(Value *v, size_t i);

// deprecated without replacement
static Value*
cg2c_ptrchase(Value *v)
{
	for (;;) {
		assert(cg2c_typeKind(v->type) == KindPtr);

		if (v->op == OpAddPtr)
			v = cg2c_valueArg(v, 0);
		else
			break;
	}

	return v;
}

void cg2c_foreach(Compile *ctxt, void (*f)(Compile*, Value*, void*), void *etc, Value *v, Map *visited);

Value* cg2c_rewrite(Builder b, Value *v, Map *re);
Value* cg2c_rewriteFunc2(Builder b, Value *(*f)(Builder, Value*, Op, Type*, void*, Slice, void*), void *etc, Value *v, Map *re);

// TODO: rename this into Block
struct Func {
	Type  *type;
	Slice values;
};

// TODO: move implementations out of the header

static Value*
cg2c_funcTerminator(Func *f)
{
	Value *t = *(Value**)cg2c_slindex(sizeof(Value*), f->values, cg2c_sllen(f->values)-1);
	assert(t->op == OpCall || t->op == OpCallStructuredIf);
	return t;
}

static void
cg2c_funcTerminate(Func *f, Value *t, GC *gc)
{
	assert(t->op == OpCall || t->op == OpCallStructuredIf);
	assert(cg2c_sllen(f->values) == 0 || cg2c_sllen(f->values) == 1);
	// BUG: c/gen.c is being bad
	//assert(cg2c_funcResult(f->type) == t->type);

	// TODO: optimization: we can just overwrite the last item instead of
	// allocating new slice

	f->values = cg2c_mkslice(sizeof(Value*), (Value*[]) {t}, 1, gc);
}

// A set of interdependent funcs. First func is the entry func.
struct Scope {
	Slice funcs; // of Func*
	CSE   *cse;
};

// TODO: rename to just scope? or newScope?
//
// TODO: we will compute scopes only once and maintain them from there on.
Scope cg2c_computeScope(Func *f, GC *gc);

void cg2c_scopeForeach(Compile *ctxt, void (*f)(Compile*, Value*, void*), void *etc, Scope *scope);

bool cg2c_scopeRewrite(Builder b, Scope *scope, Map *re);
bool cg2c_scopeRewriteFunc2(Builder b, Value *(*f)(Builder, Value*, Op, Type*, void*, Slice, void*), void *etc, Scope *scope, Map *re);

// Rewrite rules

// TODO: move these structures (Pattern and RewriteRule) into their own header,
// as only rule files and value.c need these. Actually we can't move
// RewriteRule, but at most Pattern. I guess it doesn't matter much either way.

struct Pattern {
	int     op;
	Pattern **args;
	size_t  narg;
	bool    ddd;
};

struct RewriteRule {
	Pattern *pattern;
	Value   *(*replace)(Builder, Value*);
};

extern Slice cg2c_commonRules;
extern Slice cg2c_int64ToInt32Rules;
// extern Slice cg2c_int128ToInt64Rules;

Rewriter* cg2c_newRewriter(Slice rules, GC *gc);

CSE* cg2c_newCSE(GC *gc);

// Passes
//
// Like cg2c_scopeRewrite and friends, these functions return a boolean
// indicating whether they've made any progress.

bool cg2c_inlineCallc(Builder b, Scope *scope);

// cg2c_loadsToSSA tries to replace loads with uses of previously stored values.
bool cg2c_loadsToSSA(Builder b, Scope *scope);

// Used internally by cg2c_loadsToSSA.
bool cg2c_phisToParams(Builder b, Scope *scope);

bool cg2c_elimDeadStores(Builder b, Scope *scope);

// cg2c_uselessCalls merges single-exit continuations with their single-entry
// successors.
bool cg2c_uselessCalls(Builder b, Scope *scope);

bool cg2c_genericAddressingToTags(Builder b, Scope *scope);

// Demand inlining
Func* cg2c_inline(Builder b, Func *f, Value *retc);

// Scheduling
void cg2c_schedule(Compile *ctxt, Scope *scope);

// TODO: rename this into a PhiArg/Src?
typedef struct Incoming Incoming;
struct Incoming {
	Value *v;
	Func  *f;
};

// cg2c_predecessors computes predecessors of each function.
Map* /* Func* → Slice of Incoming */ cg2c_predecessors(Scope *scope, GC *gc);

struct Sym {
	String name;
	Type   *type;
	Func   *func; // TODO: rename to entry?

	Pos pos; // where symbol was declared

	CSE *cse; // this belongs to Scope instead
};

// TODO: move elsewhere and rename to be more clear that it's parsing C
Slice cg2c_parse(Compile *ctxt, const char *src, size_t len); // of Sym*

Cg2cBlob* cg2c_export(void *data, size_t len, const Cg2cAllocFuncs *_unused);

// Compiler debugging

// TODO: ast dump

void cg2c_dump3(Compile*, const char*, Sym*);
