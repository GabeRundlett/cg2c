#ifndef __COMPLEX_H__
#define __COMPLEX_H__

#define complex _Complex

#define I 1.0if16

float crealf(float complex);

float cimagf(float complex);

float complex cexpf(float complex);

#endif
