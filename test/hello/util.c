#include "hello.h"

void*
emalloc(size_t size)
{
	void *p = calloc(1, size);
	if (p == NULL && size == 0)
		p = malloc(1);
	if (p == NULL) {
		fprintf(stderr, "emalloc: out of memory\n");
		abort();
	}
	return p;
}

void
fatalf(const char *format, ...)
{
	va_list arg;
	va_start(arg, format);
	vfprintf(stderr, format, arg);
	fprintf(stderr, "\n");
	va_end(arg);
	_Exit(1); // do not run anything atexit
}
