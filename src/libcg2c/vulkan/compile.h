typedef struct DeviceFeatures DeviceFeatures;
struct DeviceFeatures {
	bool vertexPipelineStoresAndAtomics;
	bool shaderInt64;
	bool shaderFloat16;
};

void cg2c_collectEnabledFeaturesVk(Compile *ctxt, const void *features, DeviceFeatures *out);

extern Slice cg2c_vulkanRules;
extern Slice cg2c_genericAddressingToTagsVkRules;
