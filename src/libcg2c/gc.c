#include "all.h"

#include <stddef.h>
#include <stdlib.h>
#include <setjmp.h>

struct GC {
	void *garbage[100000];
	size_t slot;

	jmp_buf oom;
};

static void freeall(GC *gc);

int
cg2c_gccall(void (*f)(void*, GC*), void *aux, const Cg2cAllocFuncs *_unused)
{
	int r = 0;

	GC *gc = calloc(1, sizeof *gc);
	if (gc == NULL) {
		r = -1;
		goto out;
	}
	if (setjmp(gc->oom)) {
		r = -1;
		goto cleanup;
	}

	f(aux, gc);

cleanup:
	freeall(gc);
	free(gc);
out:
	return r;
}

void*
cg2c_malloc(size_t size, GC *gc)
{
	void *p = calloc(1, size);
	if (p == NULL && size == 0)
		p = calloc(1, 1);
	if (p == NULL)
		longjmp(gc->oom, 1);
	assert(gc->slot < nelem(gc->garbage));
	gc->garbage[gc->slot++] = p;
	return p;
}

void*
cg2c_memdup(const void *p, size_t size, GC *gc)
{
	void *q = cg2c_malloc(size, gc);
	memmove(q, p, size);
	return q;
}

void
freeall(GC *gc)
{
	for (size_t i = 0; i < nelem(gc->garbage) && gc->garbage[i] != NULL; i++)
		free(gc->garbage[i]);
}
