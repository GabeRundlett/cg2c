#include "all.h"
#include "cg2.h"

Kwtab cg2c_kwtab[] = {
	{"_Alignas", LAlignas},
	{"_Alignof", LAlignof},
	{"_Atomic", LAtomic},
	{"auto", LAuto},
	{"_Bool", LBool},
	{"break", LBreak},
	{"case", LCase},
	{"char", LChar},
	{"_Complex", LComplex},
	{"const", LConst},
	{"continue", LContinue},
	{"default", LDefault},
	{"do", LDo},
	{"double", LDouble},
	{"else", LElse},
	{"enum", LEnum},
	{"extern", LExtern},
	{"float", LFloat},
	{"_Float16", LFloat16},
	{"for", LFor},
	{"_Generic", LGeneric},
	{"goto", LGoto},
	{"if", LIf},
	{"inline", LInline},
	{"int", LInt},
	{"long", LLong},
	{"restrict", LRestrict},
	{"return", LReturn},
	{"short", LShort},
	{"signed", LSigned},
	{"sizeof", LSizeof},
	{"static", LStatic},
	{"_Static_assert", LStaticAssert},
	{"struct", LStruct},
	{"switch", LSwitch},
	{"_Thread_local", LThread},
	{"typedef", LTypedef},
	{"union", LUnion},
	{"unsigned", LUnsigned},
	{"void", LVoid},
	{"volatile", LVolatile},
	{"while", LWhile},

	{"__alignof", LAlignof}, {"__alignof__", LAlignof},
	{"__complex", LComplex}, {"__complex__", LComplex},
	{"__inline", LInline}, {"__inline__", LInline},
	{"__restrict", LRestrict}, {"__restrict__", LRestrict},
	{"__shared", LShared},
	{"__thread", LThread},
	{"__volatile", LVolatile}, {"__volatile__", LVolatile},
};
size_t cg2c_nkwtab = nelem(cg2c_kwtab);

static void scanName(Noder*);
static void scanNumber(Noder*);
static void scanCharSeq(Noder*, const char*);
static void scanLineComment(Noder*);
static void scanComment(Noder*);
static Rune escape(Noder*);

// lower returns lower-case rune iff r is ASCII-letter.
static Rune lower(Rune ch) { return ('a' - 'A') | ch; }

// isLetter returns whether a rune is a letter or a dollar sign (see
// https://gcc.gnu.org/onlinedocs/gcc/Dollar-Signs.html)
static bool isLetter(Rune ch) { return 'a' <= lower(ch) && lower(ch) <= 'z' || ch == '_' || ch == '$' || ch >= Runeself /* && TODO */; }

static bool isDecimal(Rune ch) { return '0' <= ch && ch <= '9'; }

//static bool isHex(Rune ch) { return isDecimal(ch) || 'a' <= lower(ch) && lower(ch) <= 'f'; }

uint64_t cg2c_strtou64(String s, int base, GC *gc);

static void
setLine(Noder *p, String filename, int32_t line)
{
	assert(p->col == 1);

	PosBase *base = cg2c_malloc(sizeof *base, p->ctxt->gc);
	base->filename = filename;

	p->posBase = base;
	p->line = line;
}

void
cg2c_scan(Noder *p)
{
again:
	while (p->ch == '\t' || p->ch == '\n' || p->ch == '\r' || p->ch == ' ')
		cg2c_getc(p);

	assert(p->posBase != NULL);
	p->pos = (Pos) {
		.base = p->posBase,
		.line = p->line,
		.col  = p->col,
	};
	p->lit = (String) {};
	p->op = KXxx;
	p->tok = LXxx;

	if (isLetter(p->ch)) {
		scanName(p);
		return;
	}

	switch (p->ch) {
	case -1:
		p->tok = LEOF;
		break;

	case '#': {
		cg2c_getc(p);

		// BUG: this should be done in a better way

		// TODO: move into its own function

		cg2c_scan(p);
		assert(p->tok == LNumber);

		int32_t line = (int32_t)cg2c_strtou64(p->lit, 10, p->ctxt->gc);

		cg2c_scan(p);
		assert(p->tok == LString);

		String filename = p->lit;

		// TODO: should we stop at '\r'?
		while (p->ch == '\t' || p->ch == '\r' || p->ch == ' ')
			cg2c_getc(p);

		if (p->ch != '\n') {
			cg2c_scan(p);
			assert(p->tok == LNumber);
		}

		// Consume newline
		cg2c_getc(p);

		setLine(p, filename, line);

		goto again;
	}

	case '"':
		scanCharSeq(p, "string");
		p->tok = LString;
		return;

	case '\'':
		scanCharSeq(p, "character constant");
		p->tok = LChar_;
		break;

	case '(':
		cg2c_getc(p);
		p->tok = LLParen;
		break;

	case '[':
		cg2c_getc(p);
		p->tok = LLBrack;
		break;

	case '{':
		cg2c_getc(p);
		p->tok = LLBrace;
		break;

	case ')':
		cg2c_getc(p);
		p->tok = LRParen;
		break;

	case ']':
		cg2c_getc(p);
		p->tok = LRBrack;
		break;

	case '}':
		cg2c_getc(p);
		p->tok = LRBrace;
		break;

	case ';':
		cg2c_getc(p);
		p->tok = LSemi;
		break;

	case ',':
		cg2c_getc(p);
		p->tok = LComma;
		break;

	case '.':
		if (isDecimal(cg2c_peekc(p))) {
			scanNumber(p);
			return;
		}
		cg2c_getc(p);
		if (p->ch == '.' && cg2c_peekc(p) == '.') {
			cg2c_getc(p);
			cg2c_getc(p);
			p->tok = LDotDotDot;
			break;
		}
		p->tok = LDot;
		break;

	case ':':
		cg2c_getc(p);
		p->tok = LColon;
		break;

	case '?':
		cg2c_getc(p);
		p->tok = LQuestion;
		break;

	case '+':
		cg2c_getc(p);
		p->op = KAdd;
		if (p->ch == '+') {
			cg2c_getc(p);
			p->tok = LOpOp;
			break;
		}
		goto assignOp;

	case '-':
		cg2c_getc(p);
		if (p->ch == '>') {
			cg2c_getc(p);
			p->tok = LArrow;
			break;
		}
		p->op = KSub;
		if (p->ch == '-') {
			cg2c_getc(p);
			p->tok = LOpOp;
			break;
		}
		goto assignOp;

	case '*':
		cg2c_getc(p);
		p->op = KMul;
		if (p->ch == '=') {
			cg2c_getc(p);
			p->tok = LAssignOp;
			break;
		}
		p->tok = LStar;
		break;

	case '/':
		cg2c_getc(p);
		if (p->ch == '/') {
			cg2c_getc(p);
			scanLineComment(p);
			goto again;
		}
		if (p->ch == '*') {
			cg2c_getc(p);
			scanComment(p);
			goto again;
		}
		p->op = KDiv;
		goto assignOp;

	case '%':
		cg2c_getc(p);
		p->op = KRem;
		goto assignOp;

	case '&':
		cg2c_getc(p);
		if (p->ch == '&') {
			cg2c_getc(p);
			p->op = KAndAnd;
			p->tok = LOp;
			break;
		}
		p->op = KAnd;
		goto assignOp;

	case '|':
		cg2c_getc(p);
		if (p->ch == '|') {
			cg2c_getc(p);
			p->op = KOrOr;
			p->tok = LOp;
			break;
		}
		p->op = KOr;
		goto assignOp;

	case '^':
		cg2c_getc(p);
		p->op = KXor;
		goto assignOp;

	case '~':
		cg2c_getc(p);
		p->op = KCom;
		p->tok = LOp;
		break;

	case '<':
		cg2c_getc(p);
		if (p->ch == '=') {
			cg2c_getc(p);
			p->op = KLe;
			p->tok = LOp;
			break;
		} else if (p->ch == '<') {
			cg2c_getc(p);
			p->op = KShl;
			goto assignOp;
		}
		p->op = KLt;
		p->tok = LOp;
		break;

	case '>':
		cg2c_getc(p);
		if (p->ch == '=') {
			cg2c_getc(p);
			p->op = KGe;
			p->tok = LOp;
			break;
		} else if (p->ch == '>') {
			cg2c_getc(p);
			p->op = KShr;
			goto assignOp;
		}
		p->op = KGt;
		p->tok = LOp;
		break;

	case '=':
		cg2c_getc(p);
		if (p->ch == '=') {
			cg2c_getc(p);
			p->op = KEq;
			p->tok = LOp;
			break;
		}
		p->tok = LAssign;
		break;

	case '!':
		cg2c_getc(p);
		if (p->ch == '=') {
			cg2c_getc(p);
			p->op = KNe;
			p->tok = LOp;
			break;
		}
		p->op = KNot;
		p->tok = LOp;
		break;

	default:
		if (isDecimal(p->ch)) {
			scanNumber(p);
			return;
		}

		// We might have already complained about a character in
		// cg2c_getc, don't complain twice.
		if (p->ch != 0 && p->ch != 0xFEFF && p->ch != Runeerror)
			cg2c_errorfAt(p->ctxt, p->pos, "invalid character U+%04X", p->ch);

		cg2c_getc(p); // always make progress
		goto again;
	}

	return;

assignOp:
	if (p->ch == '=') {
		cg2c_getc(p);
		p->tok = LAssignOp;
		return;
	}
	p->tok = LOp;
}

void
scanName(Noder *p)
{
	size_t off0 = p->off;
	while (isLetter(p->ch) || isDecimal(p->ch))
		cg2c_getc(p);
	String lit = cg2c_strslice(p->in, off0, p->off);

	p->tok = *(const int*)cg2c_mapaccess1(cg2c_str_32_map, p->keywords, &lit);
	if (p->tok == 0) {
		p->lit = lit;
		p->tok = LName;
	}
}

void
scanNumber(Noder *p)
{
	size_t off0 = p->off;

	for (;;) {
		if (isDecimal(p->ch)) {
			cg2c_getc(p);
		} else if (p->ch == 'e' || p->ch == 'E' || p->ch == 'p' || p->ch == 'P') {
			cg2c_getc(p);
			if (p->ch == '+' || p->ch == '-')
				cg2c_getc(p);
		} else if (isLetter(p->ch) || p->ch == '.') {
			cg2c_getc(p);
		} else {
			break;
		}
	}

	p->lit = cg2c_strslice(p->in, off0, p->off);
	p->tok = LNumber;
}

static Pos pos(Noder *p) { return (Pos) {NULL, p->line, p->col}; }

// TODO: use string builder
static String
cg2c_strcat(String a, String b, GC *gc)
{
	char *data = cg2c_malloc((cg2c_strlen(a) + cg2c_strlen(b)) * sizeof data[0], gc);

	memmove(data+0, cg2c_strdata(a), cg2c_strlen(a));
	memmove(data+cg2c_strlen(a), cg2c_strdata(b), cg2c_strlen(b));

	return (String) {
		.data = data,
		.len  = cg2c_strlen(a) + cg2c_strlen(b),
	};
}

void
scanCharSeq(Noder *p, const char *a)
{
	Rune quote = p->ch; // " or '
	cg2c_getc(p);

	size_t off0 = p->off;

	String lit = {};
	for (;;) {
		if (p->ch == '\\') {
			lit = cg2c_strcat(lit, cg2c_strslice(p->in, off0, p->off), p->ctxt->gc);
			cg2c_getc(p);
			Rune r = escape(p);
			lit = cg2c_strcat(lit, cg2c_mkstringnocopy((char[]) {(char)r, '\0'}), p->ctxt->gc);
			off0 = p->off;
			continue;
		}
		if (p->ch == quote) {
			lit = cg2c_strcat(lit, cg2c_strslice(p->in, off0, p->off), p->ctxt->gc);
			cg2c_getc(p);
			break;
		}
		if (p->ch == '\n' || p->ch < 0) {
			lit = cg2c_strcat(lit, cg2c_strslice(p->in, off0, p->off), p->ctxt->gc);
			cg2c_errorfAt(p->ctxt, pos(p), "%s not terminated", a);
			break;
		}
		cg2c_getc(p);
	}

	p->lit = lit;
}

void
scanLineComment(Noder *p)
{
	while (p->ch != '\n' && p->ch >= 0)
		cg2c_getc(p);
}

void
scanComment(Noder *p)
{
	Pos pos = p->pos;
	while (p->ch >= 0) {
		if (p->ch == '*') {
			cg2c_getc(p);
			if (p->ch == '/') {
				cg2c_getc(p);
				return;
			}
		}
		cg2c_getc(p);
	}
	cg2c_errorfAt(p->ctxt, pos, "comment not terminated");
}

static char simple[256] = {
	['\"'] = '\"',
	['\''] = '\'',
	['?']  = '\?',
	['\\'] = '\\',
	['a']  = '\a',
	['b']  = '\b',
	['f']  = '\f',
	['n']  = '\n',
	['r']  = '\r',
	['t']  = '\t',
	['v']  = '\v',
};

Rune
escape(Noder *p)
{
	Rune r;
	if (0 <= p->ch && p->ch < nelem(simple) && (r = simple[p->ch]) != '\0') {
		cg2c_getc(p);
		return r;
	}

	cg2c_errorfAt(p->ctxt, pos(p), "unknown escape sequence");
	return 0;
}
